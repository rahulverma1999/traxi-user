//
//  APIConstant.swift
//  myDotiNewProject
//
//  Created by Bhanu Dhiman on 21/01/20.
//  Copyright © 2020 Bhanu Dhiman. All rights reserved.
//

import Foundation

struct APIConstants {
    
    static let Login:String  = "login"
    static let SignUp:String = "signup"
    static let PhoneVerification:String = "verify/phone"
    static let logOut :String = "logout"
    static let ResentOtp:String = "resend/otp"
    static let editUser:String = "profile"
    static let getProfile : String = "profile"
    static let uploadImage :String = "upload/aws"
    static let VerificationEmailSendLink :String = "send/verification/email"
    static let getCategories : String = "package/categories"
    static let addAddress : String = "address"
    static let getAddresses : String = "addresses"
    static let postDelivers : String = "delivery"
    static let getDeliveryStatus : String = "delivery/status"
    static let shopCategories : String = "shop/categories"
    static let storeNearYou : String = "stores"
    static let corporateDeliveryContactUs :String = "contact-us"
    static let storeDetails : String = "store/details"
    static let categoriesProducts : String = "store/category/products"
    static let createOrder : String = "order"
    static let activeOrders : String = "active/jobs"
    static let getAllDeliveres : String = "deliveries"
    static let getAllOrders : String = "orders"
    static let getOrderDetail : String = "order"
    static let getDeliveryDetail : String = "delivery"
    static let postComplain : String = "complain"
    static let getCoupn : String = "coupon"
    static let addCard : String = "wallet/attach/card"
    static let getComplains : String = "complains"
    static let getChats : String = "conversation"
    static let getSavedCards : String = "wallet/cards"
    static let deleteCard : String = "wallet/card"
    static let seenMsgs : String = "message/seen"
    static let rating : String = "feedback"
    static let transactionHistory : String = "user/transactions"
    static let notificationUrl : String = "notifications"
    
    func getApiLink(){
        
    }
    
}
