//
//  GetAllDeliversApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman Pvt Ltd on 11/01/21.
//

import Foundation

// MARK: - GetAllDeliveresAPIElement
struct GetAllDeliveresAPIElement: Codable {
    let id: Int?
    let driverID: Int?
    let deliverTypeID: Int?
    let deliveredCharges, taxCharges: Double?
    let timeSensitive: Int?
    let estimatedDeliveryTime, estimatedDeliveryTimeReturn: String?
    let deliveryStatus: Int?
    let pickupDetails, dropoffDetails: [DetailDeliveres]?

    enum CodingKeys: String, CodingKey {
        case id
        case driverID = "driver_id"
        case deliverTypeID = "deliver_type_id"
        case deliveredCharges = "delivered_charges"
        case taxCharges = "tax_charges"
        case timeSensitive = "time_sensitive"
        case estimatedDeliveryTime = "estimated_delivery_time"
        case estimatedDeliveryTimeReturn = "estimated_delivery_time_return"
        case deliveryStatus = "delivery_status"
        case pickupDetails, dropoffDetails
    }
}

// MARK: - Detail
struct DetailDeliveres: Codable {
    let id: Int?
    let address, userName, userPhone, userApartment: String?
    let userLandmark: String?
    let lat, lng: Double?
    let pickupDate, pickupTime: String?

    enum CodingKeys: String, CodingKey {
        case id, address
        case userName = "user_name"
        case userPhone = "user_phone"
        case userApartment = "user_apartment"
        case userLandmark = "user_landmark"
        case lat, lng
        case pickupDate = "pickup_date"
        case pickupTime = "pickup_time"
    }
}

typealias GetAllDeliveresAPI = [GetAllDeliveresAPIElement]
