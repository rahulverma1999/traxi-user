//
//  GetShopCategoriesApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 05/01/21.
//


import Foundation

// MARK: - GetShopCategory
struct GetShopCategory: Codable {
    let id: Int?
    let name: String?
    let image: String?
    let bgColor: String?
    let storeCounts: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, image
        case bgColor = "bg_color"
        case storeCounts = "store_counts"
    }
}

typealias GetShopCategories = [GetShopCategory]
