//
//  ActiveOrderApis.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/01/21.
//

import Foundation

// MARK: - ActiveOrdersAPIElement
struct ActiveOrdersAPIElement: Codable {
    let id: Int?
    let deliveryStatus, orderStatus: Int?
    let jobType: Int?
    let driverID: Int?
    let driverName: String?
    let profileImage: String?
    let lat, lng: Double?
    let driverPhone, storeName, estimatedDeliveryTime, estimatedDeliveryTimeReturn: String?

    enum CodingKeys: String, CodingKey {
        case id
        case deliveryStatus = "delivery_status"
        case estimatedDeliveryTime = "estimated_delivery_time"
        case estimatedDeliveryTimeReturn = "estimated_delivery_time_return"
        case storeName = "store_name"
        case orderStatus = "order_status"
        case jobType = "job_type"
        case driverID = "driver_id"
        case driverName = "driver_name"
        case profileImage = "profile_image"
        case lat, lng
        case driverPhone = "driver_phone"
    }
}

typealias ActiveOrdersAPI = [ActiveOrdersAPIElement]
