//
//  NotificationApi.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 17/02/21.
//

import Foundation

// MARK: - NotificationAPI
struct NotificationAPI: Codable {
    let count: Int?
    var notifications: [NotificationData]?
}

// MARK: - Notification
struct NotificationData: Codable {
    let deliveryID: Int?
    let notificationType: Int?
    let createdAt, title: String?
    let isRead: Int?
    let message, image: String?

    enum CodingKeys: String, CodingKey {
        case deliveryID = "delivery_id"
        case notificationType = "notification_type"
        case createdAt = "created_at"
        case title
        case isRead = "is_read"
        case message, image
    }
}
