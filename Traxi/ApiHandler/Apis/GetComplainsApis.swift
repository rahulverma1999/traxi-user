//
//  GetComplainsApis.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

// MARK: - GetComplainsAPIElement
struct GetComplainsAPIElement: Codable {
    let id, deliveryID, deliverID, userID, admin: Int?
    let message, createdAt: String?
    let storeID, jobType, deliverTypeID, timeSensitive: Int?
    let deliveredCharges: Double?
    let taxCharges, deliveryStatus, orderStatus: Int?
    let storeName, storeAddress, storeImage: String?
    let storeRatings, orderTotal: Double?
    let pickupDate, pickupTime: String?

    enum CodingKeys: String, CodingKey {
        case id
        case deliveryID = "delivery_id"
        case deliverID = "deliver_id"
        case userID = "user_id"
        case admin, message
        case createdAt = "created_at"
        case storeID = "store_id"
        case jobType = "job_type"
        case deliverTypeID = "deliver_type_id"
        case timeSensitive = "time_sensitive"
        case deliveredCharges = "delivered_charges"
        case taxCharges = "tax_charges"
        case deliveryStatus = "delivery_status"
        case orderStatus = "order_status"
        case storeName = "store_name"
        case storeAddress = "store_address"
        case storeImage = "store_image"
        case storeRatings = "store_ratings"
        case pickupDate = "pickup_date"
        case pickupTime = "pickup_time"
        case orderTotal = "order_total"
    }
}

typealias GetComplainsAPI = [GetComplainsAPIElement]
