//
//  GetChatsApi.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

import Foundation

// MARK: - GetChatDataAPIElement
struct GetChatDataAPIElement: Codable {
    var id, deliveryID, userID, admin: Int?
    var message, createdAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case deliveryID = "delivery_id"
        case userID = "user_id"
        case admin, message
        case createdAt = "created_at"
    }
    
    init(msg: String) {
        self.id = nil
        self.message = msg
        self.createdAt = CommonFunctions.getChatDate(Date())
        self.admin = 0
    }
}

typealias GetChatDataAPI = [GetChatDataAPIElement]
