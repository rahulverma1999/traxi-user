//
//  GetCoupanDetailAPi.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 12/01/21.
//

import Foundation

// MARK: - GetCoupnDetailAPI
struct GetCoupnDetailAPI: Codable {
    let id: Int?
    let couponID, coupon: String?
    let couponType, storeID, amountOff: Int?
    let createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case couponID = "coupon_id"
        case coupon
        case couponType = "coupon_type"
        case storeID = "store_id"
        case amountOff = "amount_off"
        case createdAt = "created_at"
    }
}
