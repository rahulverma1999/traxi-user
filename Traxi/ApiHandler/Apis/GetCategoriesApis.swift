//
//  GetCategoriesApis.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 22/12/20.
//

import Foundation

// MARK: - CategoriesAPI
struct CategoriesAPI: Codable {
    let id: Int?
    let name: String?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

typealias CategoriesApis = [CategoriesAPI]
