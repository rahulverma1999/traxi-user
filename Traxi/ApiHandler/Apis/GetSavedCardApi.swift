//
//  GetSavedCardApi.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 14/01/21.
//

import Foundation

// MARK: - GetSavedCardAPI
struct GetSavedCardAPI: Codable {
    let object: String?
    let data: [Datum]?
    let hasMore: Bool?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case object, data
        case hasMore = "has_more"
        case url
    }
}

// MARK: - Datum
struct Datum: Codable {
    let id, object: String?
    let addressCity, addressCountry, addressLine1, addressLine1Check: String?
    let addressLine2, addressState, addressZip, addressZipCheck: String?
    let brand, country, customer, cvcCheck: String?
    let dynamicLast4: Int?
    let expMonth, expYear: Int?
    let fingerprint, funding, last4: String?
    let metadata: Metadata?
    let name, tokenizationMethod: String?

    enum CodingKeys: String, CodingKey {
        case id, object
        case addressCity = "address_city"
        case addressCountry = "address_country"
        case addressLine1 = "address_line1"
        case addressLine1Check = "address_line1_check"
        case addressLine2 = "address_line2"
        case addressState = "address_state"
        case addressZip = "address_zip"
        case addressZipCheck = "address_zip_check"
        case brand, country, customer
        case cvcCheck = "cvc_check"
        case dynamicLast4 = "dynamic_last4"
        case expMonth = "exp_month"
        case expYear = "exp_year"
        case fingerprint, funding, last4, metadata, name
        case tokenizationMethod = "tokenization_method"
    }
}

// MARK: - Metadata
struct Metadata: Codable {
}
