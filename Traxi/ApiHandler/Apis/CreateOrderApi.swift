//
//  CreateOrderApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/01/21.
//

import Foundation

// MARK: - CreateOrderAPI
struct CreateOrderAPI: Codable {
    let message: String?
    let orderID: Int?

    enum CodingKeys: String, CodingKey {
        case message
        case orderID = "orderId"
    }
}

