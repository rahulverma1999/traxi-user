//
//  GetOrderDetail.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/01/21.
//


import Foundation

// MARK: - GetOrderAPI
//struct GetOrderAPI: Codable {
//    let order: Order?
//    let products: [ProductData]?
//}

// MARK: - Order
struct GetOrderAPI: Codable {
    let products: [ProductData]?
    let id, orderID, userRated: Int?
    let storeName, storeAddress, image: String?
    let lat, lng, driverLat, driverLng: Double?
    let storeRatings: Double?
    let reviewsCount, orderStatus: Int?
    let deliveredCharges: Double?
    let taxCharges: Double?
    let orderTotal, discount: Double?
    let createdAt, address, userName, estimatedDeliveryTime: String?
    let userPhone, userApartment, userLandmark, driverName, driverImage, driverPhone: String?
    let driverID: Int?
    let userLat, userLng: Double?

    enum CodingKeys: String, CodingKey {
        case orderID = "order_id"
        case userRated = "user_rated"
        case products
        case id, discount
        case storeName = "store_name"
        case storeAddress = "store_address"
        case estimatedDeliveryTime = "estimated_delivery_time"
        case image, lat, lng
        case storeRatings = "store_ratings"
        case reviewsCount = "reviews_count"
        case orderStatus = "order_status"
        case deliveredCharges = "delivered_charges"
        case taxCharges = "tax_charges"
        case orderTotal = "order_total"
        case createdAt = "created_at"
        case address
        case userName = "user_name"
        case userPhone = "user_phone"
        case userApartment = "user_apartment"
        case userLandmark = "user_landmark"
        case driverName = "driver_name"
        case driverImage = "driver_image"
        case driverPhone = "driver_phone"
        case driverLat = "driver_lat"
        case driverLng = "driver_lng"
        case driverID = "driver_id"
        case userLat = "user_lat"
        case userLng = "user_lng"
    }
}

// MARK: - Product
struct ProductData: Codable {
    let id: Int?
    let name: String?
    let productDescription: String?
    let image: String?
    let quantity: Int?
    let price : Double?

    enum CodingKeys: String, CodingKey {
        case id, name
        case productDescription = "description"
        case image, quantity, price
    }
}
