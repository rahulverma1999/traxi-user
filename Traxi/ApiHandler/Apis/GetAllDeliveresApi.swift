//
//  GetAllDeliveresApi.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 11/01/21.
//

import Foundation

// MARK: - GetAllOrdersAPI
struct GetAllOrdersAPI: Codable {
    let count: Int?
    var orders: [GetAllOrdersAPIElement]?
}



// MARK: - GetAllOrder
struct GetAllOrdersAPIElement: Codable {
    let orderID, orderStatus, storeID: Int?
    let orderTotal: Double?
    let discount: Double?
    let driverID: Int?
    let storeName, storeAddress, storeImage: String?
    let driverLat, driverLng, storeLat, storeLng, storeRatings: Double?
    let name, profileImage, driverPhone, createdAt: String?
    let reviewsCount: Int?
    let products: [ProductOrders]?

    enum CodingKeys: String, CodingKey {
        case orderID = "order_id"
        case orderStatus = "order_status"
        case orderTotal = "order_total"
        case discount
        case storeID = "store_id"
        case storeName = "store_name"
        case createdAt = "created_at"
        case storeAddress = "store_address"
        case storeImage = "store_image"
        case storeLat = "store_lat"
        case storeLng = "store_lng"
        case name
        case profileImage = "profile_image"
        case driverPhone = "driver_phone"
        case driverLat = "driver_lat"
        case driverLng = "driver_lng"
        case storeRatings = "store_ratings"
        case reviewsCount = "reviews_count"
        case products
        case driverID = "driver_id"
    }
}

// MARK: - Product
struct ProductOrders: Codable {
    let id: Int?
    let name, productDescription, image: String?
    let quantity: Int?
    let price: Double?

    enum CodingKeys: String, CodingKey {
        case id, name
        case productDescription = "description"
        case image, quantity, price
    }
}

//typealias GetAllOrdersAPI = [GetAllOrdersAPIElement]
