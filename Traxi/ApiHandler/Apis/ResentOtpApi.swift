//
//  ResentOtpApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 21/12/20.
//

import Foundation

// MARK: - ResentOtpAPI
struct ResentOtpAPI: Codable {
    let message, filename: String?
}
