//
//  GetAddressesVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 23/12/20.
//

import Foundation

// MARK: - GetAddressAPIElement
struct GetAddressAPIElement: Codable {
    let id: Int?
    let tag, address, name, phone: String?
    let apartment, landmark: String?
    let otherAddress: String?
    let lat, lng: Double?

    enum CodingKeys: String, CodingKey {
        case id, tag, address, name, phone, apartment, landmark
        case otherAddress = "other_address"
        case lat, lng
    }
}

typealias GetAddressAPI = [GetAddressAPIElement]
