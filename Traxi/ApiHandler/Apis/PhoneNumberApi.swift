//
//  PhoneNumberApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 21/12/20.
//

import Foundation

// MARK: - PhoneNumberAPI
struct PhoneNumberAPI: Codable {
    let message, token: String?
//    let update: String?
    let user: User?
}

// MARK: - User
struct User: Codable {
    let id: Int?
    let name, profileImage, email, fbID: String?
    let googleID: String?
    let appleID: String?
    let countryCode, address : String?
    let emailVerifyToken: String?
    let phone, phoneVerificationOtp : Int?
    let isEmailVerified, isPhoneVerifed: Int?
    let isDeactivated: Int?
    let userType: Int?
    let createdAt: String?
    let lat, lng : Double?

    enum CodingKeys: String, CodingKey {
        case id, name
        case profileImage = "profile_image"
        case email
        case fbID = "fb_id"
        case googleID = "google_id"
        case appleID = "apple_id"
        case phone
        case countryCode = "country_code"
        case address, lat, lng
        case emailVerifyToken = "email_verify_token"
        case phoneVerificationOtp = "phone_verification_otp"
        case isEmailVerified = "is_email_verified"
        case isPhoneVerifed = "is_phone_verifed"
        case isDeactivated = "is_deactivated"
        case userType = "user_type"
        case createdAt = "created_at"
    }
}

