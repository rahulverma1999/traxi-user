//
//  GetDeliveryStatusApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 31/12/20.
//

import Foundation

// MARK: - GetDeliveryStatus
struct GetDeliveryStatus: Codable {
    let requestAcceptedAt, estimatedDeliveryTime, estimatedDeliveryTimeReturn: String?
    let timeSensitive: Int?
    let estimatedTime, driverAssignedAt, driverName, driverImage: String?
    let driverRatings: Double?
    let driverReviews: Int?
    let driverPhone, vehicleNo, vehicleImage, vehicleName, licenseImage: String?
    let driverLat, driverLng: Double?
    let driverID: Int?
    let pickup: [Pickup]?
    let dropoff: [Dropoff]?

    enum CodingKeys: String, CodingKey {
        case estimatedDeliveryTime = "estimated_delivery_time"
        case estimatedDeliveryTimeReturn = "estimated_delivery_time_return"
        case timeSensitive = "time_sensitive"
        case requestAcceptedAt = "request_accepted_at"
        case estimatedTime = "estimated_time"
        case driverAssignedAt = "driver_assigned_at"
        case driverName = "driver_name"
        case driverImage = "driver_image"
        case driverRatings = "driver_ratings"
        case driverReviews = "driver_reviews"
        case driverPhone = "driver_phone"
        case vehicleNo = "vehicle_no"
        case vehicleImage = "vehicle_image"
        case vehicleName = "vehicle_name"
        case licenseImage = "license_Image"
        case driverLat = "driver_lat"
        case driverLng = "driver_lng"
        case driverID = "driver_id"
        case pickup, dropoff
    }
}

// MARK: - Dropoff
struct Dropoff: Codable {
    let id: Int?
    let deliveredAt, cancelledAt: String?
    let lat, lng: Double?

    enum CodingKeys: String, CodingKey {
        case id, lat, lng
        case deliveredAt = "delivered_at"
        case cancelledAt = "cancelled_at"
    }
}

// MARK: - Pickup
struct Pickup: Codable {
    let id: Int?
    let pickedUpAt, cancelledAt: String?
    let lat, lng: Double?

    enum CodingKeys: String, CodingKey {
        case id, lat, lng
        case pickedUpAt = "picked_up_at"
        case cancelledAt = "cancelled_at"
    }
}
