//
//  StoreNearYouAPi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 05/01/21.
//

import Foundation


// MARK: - StoreNearYouAPI
struct StoreNearYouAPI: Codable {
    let storeCount: Int?
    var stores: [Store]?

    enum CodingKeys: String, CodingKey {
        case storeCount = "store_count"
        case stores
    }
}

// MARK: - StoreNearYouAPIElement
struct Store: Codable {
    let id: Int?
    let distance: Double?
    let categories, name, image, address: String?
    let ratings: Double?
}
