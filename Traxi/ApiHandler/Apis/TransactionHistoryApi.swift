//
//  TransactionHistoryApi.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 16/02/21.
//

import Foundation

// MARK: - TransactionHistoryAPIElement
struct TransactionHistoryAPIElement: Codable {
    let id: Int?
    let amount: String?
    let jobType: Int?
    let deliveryType, status: String?
    let deliveryID: Int?

    enum CodingKeys: String, CodingKey {
        case id, amount
        case jobType = "job_type"
        case deliveryType = "delivery_type"
        case status
        case deliveryID = "delivery_id"
    }
}

typealias TransactionHistoryAPI = [TransactionHistoryAPIElement]
