//
//  GetStoreDetailApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 06/01/21.
//


import Foundation

// MARK: - StoreDetailAPI
struct StoreDetailAPI: Codable {
    let store: StoreDetail?
    let categories: [Category]?
}

// MARK: - Category
struct Category: Codable {
    let id: Int?
    let categoryName, createdAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryName = "category_name"
        case createdAt = "created_at"
    }
}

// MARK: - Store
struct StoreDetail: Codable {
    let id: Int?
    let storeName, storeAddress, image: String?
    let lat, lng, storeRatings: Double?
    let reviewsCount: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case storeName = "store_name"
        case storeAddress = "store_address"
        case image, lat, lng
        case storeRatings = "store_ratings"
        case reviewsCount = "reviews_count"
    }
}
