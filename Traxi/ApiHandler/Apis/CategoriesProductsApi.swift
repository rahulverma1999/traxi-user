//
//  CategoriesProductsApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 06/01/21.
//

import Foundation

// MARK: - CategoriesProductsAPI
struct CategoriesProductsAPI: Codable {
    let category: String?
    let products: [Product]?
}

// MARK: - Product
struct Product: Codable {
    let categoryID, id: Int?
    let name, productDescription, image: String?
    let unitPrice: Double?
    let availability: Int?
    let preprationTime: Int?

    enum CodingKeys: String, CodingKey {
        case categoryID = "category_id"
        case id, name
        case productDescription = "description"
        case image
        case unitPrice = "unit_price"
        case availability
        case preprationTime = "prepration_time"
    }
}
