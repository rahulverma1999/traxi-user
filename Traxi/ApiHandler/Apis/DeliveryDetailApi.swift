//
//  DeliveryDetailApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 29/12/20.
//

import Foundation


// MARK: - DeliveryDetailAPi
struct DeliveryDetailAPi: Codable {
    let id: Int?
    let driverID: Int?
    let deliverTypeID, timeSensitive: Int?
    let deliveryTime: String?
    let deliveryStatus: Int?
    let message: String?
    let name, profileImage: String?
    let userToDriverRatings, driverToUserRatings, deliveredCharges, taxCharges : Double?
    let estimatedDeliveryTime: String?
    let estimatedDeliveryTimeReturn: String?
    let distanceTravelled: String?
    let duration: Int?
    let pickupDetails, dropoffDetails: [Detail]?
    let deliveryPackage: [DeliveryPackage]?

    enum CodingKeys: String, CodingKey {
        case id
        case driverID = "driver_id"
        case estimatedDeliveryTime = "estimated_delivery_time"
        case estimatedDeliveryTimeReturn = "estimated_delivery_time_return"
        case deliverTypeID = "deliver_type_id"
        case deliveredCharges = "delivered_charges"
        case taxCharges = "tax_charges"
        case timeSensitive = "time_sensitive"
        case deliveryTime = "delivery_time"
        case deliveryStatus = "delivery_status"
        case distanceTravelled = "distance_travelled"
        case message
        case userToDriverRatings = "user_to_driver_ratings"
        case driverToUserRatings = "driver_to_user_ratings"
        case name, duration
        case profileImage = "profile_image"
        case pickupDetails, dropoffDetails, deliveryPackage
    }
}

// MARK: - DeliveryPackage
struct DeliveryPackage: Codable {
    let id: Int?
    let name: String?
}

// MARK: - Detail
struct Detail: Codable {
    let id: Int?
    let address, userName, userPhone, userApartment: String?
    let userLandmark: String?
    let lat, lng: Double?
    let pickupTime, pickupDate: String?
    let deliveredAt: String?
    let cancelledAt: String?
    let pickedUpAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, address
        case userName = "user_name"
        case userPhone = "user_phone"
        case userApartment = "user_apartment"
        case userLandmark = "user_landmark"
        case lat, lng
        case pickupTime = "pickup_time"
        case pickupDate = "pickup_date"
        case pickedUpAt = "picked_up_at"
        case deliveredAt = "delivered_at"
        case cancelledAt = "cancelled_at"
    }
}
