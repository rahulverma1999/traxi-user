//
//  GetUserProfileApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 22/12/20.
//

import UIKit
import Foundation

// MARK: - GetProfile
struct GetProfile: Codable {
    let id: Int?
    let name, profileImage, email, fbID: String?
    let googleID: String?
    let appleID: String?
    let phone: Int?
    let countryCode: String?
    let address, emailVerifyToken: String?
    let phoneVerificationOtp, isEmailVerified, isPhoneVerifed: Int?
    let unreadNotifications, unreadComplains: Int?
    let isDeactivated: Int?
    let userType: Int?
    let createdAt: String?
    let lat, lng : Double?
    let walletBalance: Double?
    let apartment, landmark, tagType: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case unreadNotifications = "unread_notifications"
        case unreadComplains = "unread_complains"
        case walletBalance = "wallet_balance"
        case profileImage = "profile_image"
        case email
        case fbID = "fb_id"
        case googleID = "google_id"
        case appleID = "apple_id"
        case phone
        case countryCode = "country_code"
        case address, lat, lng
        case emailVerifyToken = "email_verify_token"
        case phoneVerificationOtp = "phone_verification_otp"
        case isEmailVerified = "is_email_verified"
        case isPhoneVerifed = "is_phone_verifed"
        case isDeactivated = "is_deactivated"
        case userType = "user_type"
        case createdAt = "created_at"
        case apartment, landmark
        case tagType = "tag_type"
    }
}
