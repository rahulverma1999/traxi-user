//
//  AddDeliveriesApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 28/12/20.
//

import Foundation

// MARK: - AddDeliveriesAPI
struct AddDeliveriesAPI: Codable {
    let message: String?
    let deliverID: Int?

    enum CodingKeys: String, CodingKey {
        case message
        case deliverID = "deliver_id"
    }
}

