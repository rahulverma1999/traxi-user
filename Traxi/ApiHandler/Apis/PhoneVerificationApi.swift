//
//  PhoneVerificationApi.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 21/12/20.
//

import Foundation

// MARK: - PhoneVerificationAPI
struct PhoneVerificationAPI: Codable {
    let message, token: String?
    let isEmail: Int?

    enum CodingKeys: String, CodingKey {
        case message, token
        case isEmail = "is_email"
    }
}

