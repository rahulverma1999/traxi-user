import Foundation
import SkyFloatingLabelTextField
import IQKeyboardManagerSwift
import Toaster
import PopupDialog
import SwiftEntryKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG
import Alamofire
import GoogleMaps
import GooglePlaces

class EntryAttributeWrapper {
    var attributes: EKAttributes
    init(with attributes: EKAttributes) {
        self.attributes = attributes
    }
}

class CommonFunctions {
    var attributes = EKAttributes()
    let kView = UIView()
    let lblTitle = UILabel()
    let tf = UITextField()
    let btn = UIButton()

//    static func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
//        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.greatestFiniteMagnitude))
//        label.numberOfLines = 0
//        label.lineBreakMode = NSLineBreakMode.byWordWrapping
//        label.font = font
//        label.text = text
//
//        label.sizeToFit()
//        return label.frame.height
//    }
    
    static func setbuttonBoarder(_ radius : CGFloat, _ btns : [UIButton]){
        for btn in btns{
            btn.layer.cornerRadius = radius
        }
    }

    
    static func loginTextField(_ tfs: [UITextField]){
        for tf in tfs{
            tf.textColor = Colors.btnborderColorbnb.color()
            tf.layer.borderColor = Colors.btnBorderGrayBnb.color().cgColor
            tf.layer.borderWidth = 1.0
            tf.layer.cornerRadius = 2.0
            tf.rightViewMode = UITextField.ViewMode.always
            tf.leftViewMode = UITextField.ViewMode.always
        }
    }
    
    static func sendAttString(_ fonts: [UIFont], colors: [UIColor], texts: [String], align : NSTextAlignment, lineSpacing : CGFloat) -> NSMutableAttributedString{
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = align
        paragraphStyle.lineSpacing = lineSpacing
        let attString : NSMutableAttributedString = NSMutableAttributedString(string: "")
        
        for (num,_) in fonts.enumerated(){
            let attributes = [NSAttributedString.Key.font: fonts[num], NSAttributedString.Key.foregroundColor: colors[num]]
            let myAttrString = NSAttributedString(string: texts[num], attributes: attributes)
            attString.append(myAttrString)
        }
        
        attString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attString.length))
        return attString
    }
    
    static func buttons(_ btns: [UIButton]){
        for btn in btns{
            btn.layer.cornerRadius = 2.0
            btn.layer.borderWidth = 1.0
            btn.layer.borderColor = Colors.btnBorderGrayBnb.color().cgColor
            btn.setTitleColor(.white, for: .normal)
            btn.setTitleColor(Colors.btnborderColorbnb.color(), for: .normal)
            btn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(16.0))
        }
    }
    
    static func buttonHide(_ btns : [UIButton]){
        for btn in btns{
            btn.isHidden = true
            btn.layer.cornerRadius = 2
        }
    }
    
    static func buttonBorder(_ button: [UIButton]){
        for bttn in button{
            bttn.layer.cornerRadius = 4.0
            bttn.layer.borderWidth = 0.6
            bttn.layer.borderColor = Colors.btnborderColorbnb.color().cgColor
//            bttn.backgroundColor = UIColor.white
            bttn.setTitleColor(Colors.btnborderColorbnb.color(), for: .normal)
            bttn.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(16.0))
        }
    }
    
    static func buttonUpcoming(_ btns: [UIButton]){
        for btn in btns{
            btn.layer.cornerRadius = 4.0
//            btn.backgroundColor = Colors.dotiSignUpOrColor.color()
            btn.setTitleColor(.white, for: .normal)
        }
    }
    
    static func buttonOngoing(_ btns: [UIButton]){
        for btn in btns{
            btn.layer.cornerRadius = 4.0
//            btn.backgroundColor = Colors.dotiButtonColor.color()
            btn.setTitleColor(.white, for: .normal)
        }
    }
    
    static func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D, isLoader:Bool, byFoot : Bool = false, completion:@escaping (Bool,String,String,String,Int)->()){
        let url  = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving&key=\(GOOGLEAPIKEY)"
        print(url)
        if isLoader == true{
        }
        Alamofire.request(url, method: .get, encoding:  JSONEncoding.default, headers: nil).responseJSON { (responseObject) in
            switch(responseObject.result) {
            case .success(_):
                if let responseDict = responseObject.value as? [String:Any]{
                    if let routes = responseDict["routes"] as? [Any], let overPolyline = routes.first as? [String:Any],let dictPolyline = overPolyline["overview_polyline"] as? [String:Any],let points = dictPolyline["points"] as? String,let legsArr = overPolyline["legs"] as? [Any],let legfirstDict = legsArr.first as? [String:Any],let duration = legfirstDict["duration"]as?[String:Any],let time = duration["text"]as?String,let timeValue = duration["value"]as?Int,
                        let distanceDict = legfirstDict["distance"]as?[String:Any],let distanceValue = distanceDict["text"]as?String  {
                        let Kms = distanceValue
                        completion(true,points,time,Kms,timeValue)
                    }
                    else{
                        completion(false,"","","",0)
                    }
                }
                break
            case .failure(let error):
                CommonFunctions.hideLoader()
                CommonFunctions.toster(error.localizedDescription)
                break
            }
        }
    }
    
    static func getPolylineRouteTimeInSecs(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D, isLoader:Bool, byFoot : Bool = false, completion:@escaping (Bool,String,Int,String)->()){
        let url  = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving&key=\(GOOGLEAPIKEY)"
        print(url)
        if isLoader == true{
        }
        Alamofire.request(url, method: .get, encoding:  JSONEncoding.default, headers: nil).responseJSON { (responseObject) in
            switch(responseObject.result) {
            case .success(_):
                if let responseDict = responseObject.value as? [String:Any]{
                    if let routes = responseDict["routes"] as? [Any], let overPolyline = routes.first as? [String:Any],let dictPolyline = overPolyline["overview_polyline"] as? [String:Any],let points = dictPolyline["points"] as? String,let legsArr = overPolyline["legs"] as? [Any],let legfirstDict = legsArr.first as? [String:Any],let duration = legfirstDict["duration"]as?[String:Any],let time = duration["value"]as?Int,
                        let distanceDict = legfirstDict["distance"]as?[String:Any],let distanceValue = distanceDict["text"]as?String  {
                        let Kms = distanceValue
                        completion(true,points,time,Kms)
                    }
                    else{
                        completion(false,"",0,"")
                    }
                }
                break
            case .failure(let error):
                CommonFunctions.hideLoader()
                CommonFunctions.toster(error.localizedDescription)
                break
            }
        }
    }
    

    
    
    static func hexStringToUIColor(hex : String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
          cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
          return UIColor.white
        }
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        return UIColor(
          red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
          green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
          blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
          alpha: CGFloat(1.0)
        )
    }
    
    static func secondsToDaysHoursMinutesSecons(seconds : Int) -> (String) {
        if String((seconds % 86400) / 3600) == "0"{
            return "\(String((seconds % 3600) / 60) + " minutes")"
        }else{
            return ("\(String((seconds % 86400) / 3600) + " hours"), \(String((seconds % 3600) / 60) + " minutes")")
        }
      }
    
    
    static func getPriceAcordingToDistance(_ distance : Double) -> Double{
        let numberRange1 = 0.0...10.0
        let numberRange2 = 11.0...15.0
        let numberRange3 = 16.0...20.0
        let numberRange4 = 21.0...25.0
        let numberRange5 = 26.0...30.0
        let numberRange6 = 31.0...35.0
        let numberRange7 = 36.0...40.0
        let numberRange8 = 41.0...45.0
        let numberRange9 = 46.0...50.0
        
        if numberRange1.contains(distance) {
            return 14.95
        }else if numberRange2.contains(distance) {
            return 19.95
        }else if numberRange3.contains(distance) {
            return 24.95
        }else if numberRange4.contains(distance) {
            return 29.95
        }else if numberRange5.contains(distance) {
            return 34.95
        }else if numberRange6.contains(distance) {
            return 39.95
        }else if numberRange7.contains(distance) {
            return 44.95
        }else if numberRange8.contains(distance) {
            return 49.95
        }else{ //if numberRange9.contains(distance) {
            return 54.95
        }
//            else {
//            print("Number is outside the range")
//            return 59.95
//        }
    }
    
    static func toster(_ txt : String){
        let attributesWrapper: EntryAttributeWrapper = {
            var attributes = EKAttributes()
            attributes.positionConstraints = .fullWidth
            attributes.hapticFeedbackType = .success
            attributes.positionConstraints.safeArea = .empty(fillSafeArea: true)
            attributes.entryBackground = .visualEffect(style: .dark)
            return EntryAttributeWrapper(with: attributes)
        }()
            let title = EKProperty.LabelContent(
                text: txt,
                style: EKProperty.LabelStyle(font: UIFont.boldSystemFont(ofSize: 16), color: EKColor.white, alignment: NSTextAlignment.center, displayMode: .light, numberOfLines: 0)
            )
            let description = EKProperty.LabelContent(
                text: "",
                style: EKProperty.LabelStyle(
                    font: UIFont.systemFont(ofSize: 1, weight: .light),
                    color: .black
                )
            )
            let simpleMessage = EKSimpleMessage(
                title: title,
                description: description
            )
            let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
            let contentView = EKNotificationMessageView(with: notificationMessage)
            SwiftEntryKit.display(entry: contentView, using: attributesWrapper.attributes)
    }
    
    static func buttonSocial(_ btnx: [UIButton]){
        for btnn in btnx{
//            setBGColor(btnn)
            btnn.backgroundColor = UIColor.white
            btnn.layer.borderWidth = 1.0
            btnn.layer.borderColor = Colors.btnborderColorbnb.color().cgColor
        }
    }
    
    static func setBorderAndColorView(_ view : [UIView]){
        for vw in view{
            vw.layer.cornerRadius = 6.0
            vw.layer.borderWidth = 1.0
            vw.layer.borderColor = Colors.btnBorderGrayBnb.color().cgColor
        }
    }
    
    static func buttonSetAddress(_ btnns : [UIButton]){
        for b in btnns{
            b.layer.cornerRadius = 14.0
        }
    }
    
    static func btnShadow(_ btns: [UIButton]){
        for btnSH in btns{
            btnSH.clipsToBounds = true
            btnSH.layer.masksToBounds = false
            btnSH.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1.0).cgColor
            btnSH.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            btnSH.layer.shadowOpacity = 0.2
            btnSH.layer.shadowRadius = 5.0
            btnSH.layer.cornerRadius = 4.0
        }
    }
    
    static func shadowView(_ img: [UIView]){
        for imgs in img{
            imgs.clipsToBounds = true
            imgs.layer.masksToBounds = false
            imgs.layer.shadowColor = UIColor.black.cgColor
            imgs.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//            imgs.layer.shadowPath = shadowPath.cgPath
            imgs.layer.shadowOpacity = 0.5
            imgs.layer.shadowRadius = 5.0
            imgs.layer.cornerRadius = 4.0
        }
       }
    
    
    static func sendAttText( _ texts : [String], fonts: [UIFont], color: [UIColor], link: [URL?], alingment : NSTextAlignment) -> NSMutableAttributedString{
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = alingment
        
        let mutString : NSMutableAttributedString = NSMutableAttributedString(string: "")
        
        for (num, text) in texts.enumerated() {
            let att = [NSAttributedString.Key.foregroundColor: color[num], NSAttributedString.Key.font: fonts[num]] as [NSAttributedString.Key : Any]
            let attString = NSAttributedString(string: text, attributes: att)
            
            mutString.append(attString)
            
            if link[num] != nil{
                mutString.addAttributes([.link: link[num]!], range: NSRange(location: mutString.length - text.count, length: text.count))
            }
            
        }
        mutString.addAttributes([NSAttributedString.Key.paragraphStyle:paragraph], range: NSRange(location: 0, length: mutString.string.count))
        return mutString
    }
    
    static func sendAttText( _ texts : [String], fonts: [UIFont], color: [UIColor], alingment : NSTextAlignment) -> NSMutableAttributedString{
        
        let mutString : NSMutableAttributedString = NSMutableAttributedString(string: "")
        
        for (num, text) in texts.enumerated() {
            let att = [NSAttributedString.Key.foregroundColor: color[num], NSAttributedString.Key.font: fonts[num]] as [NSAttributedString.Key : Any]
            let attString = NSAttributedString(string: text, attributes: att)
            
            mutString.append(attString)
            
        }
        return mutString
    }
    
    static func getFont(fontName name : String, maximumFontValue size: CGFloat) -> UIFont{
        let font = UIFont(name: name, size: size)
        let fontMetrics = UIFontMetrics(forTextStyle: .body)
        return fontMetrics.scaledFont(for: font!)
    }
    
    // for getting navigation controller
    static func getNavigationController() -> UINavigationController{
        return UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
    }
    
    // for getting top most view controller
    static func getTopMostViewController() -> UIViewController?{
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
            // topController should now be your topmost view controller
        }
        return nil
    }
    static func alertView(title:String,message:String,button:String, buttonResult: @escaping (String)->(),destructive:Bool,secondButton:String, secondButtonResult: @escaping (String)->())-> UIAlertController{
        
        // call AlertController define its buttons and its functions and show it
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: button, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            buttonResult("")
        }
        if(destructive){
            
            let DestructiveAction = UIAlertAction(title: secondButton, style: UIAlertAction.Style.destructive) { (result : UIAlertAction) -> Void in
                secondButtonResult("")
            }
            alertController.addAction(DestructiveAction)
            
        }
        
        alertController.addAction(okAction)
        //    self.presentViewController(alertController, animated: true, completion: nil)
        return alertController
    }
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func invalidPassTyp(_ checkpass:String) -> Bool{
        let passwordCheck = "(?=.*[A-Z])(?=.*[d$@$!%*?&#.><,)/+*(])(?=.*[0-9])(?=.*[a-z]).{8,}"
        let password = NSPredicate(format:"SELF MATCHES %@", passwordCheck)
        return password.evaluate(with: checkpass)
    }
    
    static func validPhoneNumber(_ checkphone:String) -> Bool{
        let numberCheck = "^((\\+)|(00))[0-9]{6,14}$"
        let number = NSPredicate(format:"SELF MATCHES %@", numberCheck)
        return number.evaluate(with: checkphone)
    }
    
    // for showing error alert message
    static func showErrorAlert(message: String){
        showAlertWithTitle(title: "Error", message: message, firstBtn: "Close", firstButtonResult: { _ in }, firstBtnStyle: UIAlertAction.Style.cancel, secondBtn: nil, secondButtonResult: { _ in }, secondBtnStyle: UIAlertAction.Style.default)
    }
    
    // for showing any alert message
    static func showAlertWithTitle(title : String?,message : String?, firstBtn: String?, firstButtonResult: @escaping (String)->(), firstBtnStyle: UIAlertAction.Style = UIAlertAction.Style.default, secondBtn: String?, secondButtonResult: @escaping (String)->(), secondBtnStyle: UIAlertAction.Style = UIAlertAction.Style.default) {
        let alertVc = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        if firstBtn != nil{
            alertVc.addAction(UIAlertAction.init(title: firstBtn!, style: firstBtnStyle, handler: { _ in
                firstButtonResult("")
            }))
        }
        if secondBtn != nil{
            alertVc.addAction(UIAlertAction.init(title: secondBtn!, style: secondBtnStyle, handler: { _ in
                secondButtonResult("")
            }))
        }
        let currentVc = getTopMostViewController()
        currentVc!.present(alertVc, animated: true, completion: nil)
    }
    
//    //MARK:-  function for get country from lat, long
//    static func getPlace(for location: CLLocation, completion: @escaping (CLPlacemark?) -> Void) {
//        let geocoder = CLGeocoder()
//        geocoder.reverseGeocodeLocation(location) { placemarks, error in
//            guard error == nil else {
//              print("*** Error in \(#function): \(error!.localizedDescription)")
//              completion(nil)
//              return
//            }
//            guard let placemark = placemarks?[0] else {
//              print("*** Error in \(#function): placemark is nil")
//              completion(nil)
//              return
//            }
//            completion(placemark)
//        }
//    }
    
    // for removing any alert message currently above screen
    static func removeAlert(){
        let currentVc = getTopMostViewController()
        if currentVc!.isKind(of: UIAlertController.self){
            currentVc?.dismiss(animated: true, completion: nil)
        }
        for subViews in (currentVc?.view.subviews)!{
            if subViews.isKind(of: UIAlertController.self){
                currentVc?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    static func hideLoader(){
        let topView = getTopMostViewController()?.view
        if topView != nil{
            for (num,subView) in topView!.subviews.enumerated(){
                if subView.tag == 111{
                    topView!.subviews[num].removeFromSuperview()
                }
            }
        }
    }
    
    static func hideLoader(_ onView : UIView){
        let topView = onView
        for (num,subView) in topView.subviews.enumerated(){
            if subView.tag == 111{
                topView.subviews[num].removeFromSuperview()
            }
        }
    }
    
    static func getDate(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "MMMM d, yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getTraxiTime(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//            dateFormatter.timeZone = TimeZone.current//NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "hh:mm a"
//            dateFormatter.timeZone = TimeZone.current//NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateFormatTraxi(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm.ss"
//            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateFormatTraxi2(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm.ss"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd MMM yyyy,hh:mm a"
//            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateFormatTraxi3(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
//            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateFormatTraxi4(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd MMM yyyy"
//            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateFormatTraxi5(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
//            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateFromStringTraxi6(_ date: String) -> Date? {
        if date == ""{
            return nil
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
//            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.date(from: date)
        }
    }
    
    
    static func getOnlyYear(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func dateForStatus(_ date: String) -> String {
       if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "EEE, d MMM"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func dateForMyReviews(_ date: String) -> String {
       if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "EEEE, d MMM."
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1  ?? Date())
        }
    }
    
    static func dateForCollection(_ date: String) -> String {
       if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "h:mm a 'on' d'th' MMM. yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateForSearched(_ date: String) -> String {
       if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd MMM"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func dateForBilling(_ date: String) -> String {
       if date == ""{
            return ""
       }else if date == "Not paid yet"{
            return "Not paid yet"
       }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd MMM. yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func dateForNotofication(_ date: String) -> String {
       if date == "" || date == "Day Month Year" || date == "Not Seen Yet"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "d MMM. 'at' h:mm a"
            dateFormatter.timeZone = NSTimeZone(name: "UTC+5:30") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func onlyMothFroPayment(_ date: String) -> String {
       if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "MMM."
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func dateForInvoice(_ date: String) -> String {
       if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func getDateOfBirth(_ date: String) -> String {
           if date == "N/A"{
              return "N/A"
           }else{
               let dateFormatter = DateFormatter()
                   dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
               dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
               let date1 = dateFormatter.date(from: date)
               dateFormatter.dateFormat = "d MMM yyyy"
               dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
               return dateFormatter.string(from: date1 ?? Date())
           }
       }
    
    static func getTime(_ date: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "h:mm a"
            return dateFormatter.string(from: date1 ?? Date())
    }
    
    static func getTimeIn24(_ date: String) -> String {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
                let date1 = dateFormatter.date(from: date)
                dateFormatter.dateFormat = "HH:mm:ss"
                return dateFormatter.string(from: date1 ?? Date())
        }
    
    static func getTDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        return dateFormatter.string(from: date)
    }
    
    static func getCDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.dateFormat = "d MMM yyyy"
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        return dateFormatter.string(from: date)
    }
    
    static func getChatDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        return dateFormatter.string(from: date) + ".000Z"
    }
    
    static func setDate(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMM yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    static func setDateSubs(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date()) + "T00:00:00.000Z"
        }
    }
    static func setDateSubsAddition(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date()) + "T01:00:00.000Z"
        }
    }
    
    static func setDateForSearch(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date()) + "T01:00:00.000Z"
        }
    }
    
    static func getDateMonth(_ date: String) -> String {
        if date == "" || date == "Day Month Year"{
           return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            let date1 = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "MMM yyyy"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.string(from: date1 ?? Date())
        }
    }
    
    
    static func getOnlyDate(_ date: String) -> Date? {
        if date == ""{
            return nil
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.date(from: date)
        }
    }
    
    static func getDateInDate(_ date: String) -> Date? {
        if date == ""{
            return nil
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
            return dateFormatter.date(from: date)
        }
    }
    
    
    
    static func getImage(_ name : String, quality: Image_Quality) -> String{
        let split_string = name.prefix(4)
        if name == "" || name == "null"{
            return ""
        }
        if name.count < 10{
          print(name)
        }
        if split_string == "http" {
          return name
        }else{
            return "\(base_url_img)file?filename=\(name)\(quality.rawValue)"
        }
    }
    
    static func getCurrentsDatesOnly(_ dates : [String]) -> [String]{
        var cDates : [String] = []
        for date in dates{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM yyyy"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            
            let dt = dateFormatter.date(from: date) ?? Date()
            
            if dt.compare(Date()) == .orderedDescending{
                cDates.append(date)
            }else{
                let dt = dateFormatter.string(from: Date())
                if dt == date{
                    cDates.append(date)
                }
            }
        }
        return cDates
    }
    
    static func getStartDateAndEndDate(_ prevDate : String) -> String{
        let dateFormatter = DateFormatter()
//        let date = Date()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "MMMM-yyyy"
        let date = dateFormatter1.date(from: prevDate) ?? Date()
        
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
        let startOfMonth = Calendar.current.date(from: comp)!
        let startdate = dateFormatter.string(from: startOfMonth)
        print(dateFormatter.string(from: startOfMonth))
        
        var comps2 = DateComponents()
        comps2.month = 1
        comps2.day = -1
        let endOfMonth = Calendar.current.date(byAdding: comps2, to: startOfMonth)
        let endDate = dateFormatter.string(from: endOfMonth!)
        print(dateFormatter.string(from: endOfMonth!))
        return "\(startdate)T00:00:00.000Z" + " \(endDate)T01:00:00.000Z"
    }
    
    static func showLoader(){
        let topView = getTopMostViewController()?.view
        let loadingView = UIView(frame : CGRect(x: 0, y: 0, width: topView?.frame.width ?? 0, height: topView?.frame.height ?? 0))
        loadingView.backgroundColor = UIColor.black
        loadingView.alpha = 0.6
        let laodingFrame = SpinnerView(frame: CGRect(x: (topView?.frame.width ?? 0)/2 - 20, y: (topView?.frame.height ?? 0)/2 - 20, width: 40, height: 40))
        loadingView.addSubview(laodingFrame)
        loadingView.tag = 111
        var present = false
        if topView != nil{
            for (_,subView) in topView!.subviews.enumerated(){
                if subView.tag == 111 || subView.tag == 191 {
                    present = true
                }
            }
        }
        if !present{
            topView?.addSubview(loadingView)
        }
    }
    static func showLoader(_ onView : UIView){
        let topView = onView
        let loadingView = UIView(frame : CGRect(x: 0, y: 0, width: topView.frame.width, height: topView.frame.height))
        loadingView.backgroundColor = UIColor.black
        loadingView.alpha = 0.6
        let laodingFrame = SpinnerView(frame: CGRect(x: topView.frame.width/2 - 20, y: topView.frame.height/2 - 20, width: 40, height: 40))
        loadingView.addSubview(laodingFrame)
        loadingView.tag = 111
        var present = false
        for (_,subView) in topView.subviews.enumerated(){
            if subView.tag == 111 {
                present = true
            }
        }
        if !present{
            topView.addSubview(loadingView)
        }
    }
    
    static func heightForLabel(text:String, width:CGFloat, font : UIFont) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    static func decimalPlaceFromFloat(_ num: Float, decimalPlace: Int) -> String{
        return String(format: "%0.\(decimalPlace)f", num)
    }
    
    static func decimalPlaceFromDouble(_ num: Double, decimalPlace: Int) -> String{
        return String(format: "%0.\(decimalPlace)f", num)
    }
    
    static func decimalPlaceFromString(_ num: String, decimalPlace: Int) -> String{
        return String(format: "%0.\(decimalPlace)f", num)
    }
    
    static func checkingTheValues(_ value: inout String)->String {
      if value.contains(".") {
        let notAcceptedChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        let cs = CharacterSet(charactersIn: notAcceptedChar).inverted
        let filtered: String = (value.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
        print(filtered)
        if filtered == "" {
          value = checkingForZero(value.components(separatedBy: ".").first!) + "." + value.components(separatedBy: ".").last!
        if var lastComponent = value.components(separatedBy: ".").last {
          if lastComponent.count > 10 {
            lastComponent.removeLast(lastComponent.count - 10)
            value = value.components(separatedBy: ".").first! + "." + lastComponent
            return value
          }else if lastComponent.count < 10 {
            for _ in 0..<(10-lastComponent.count) {
              lastComponent.append("0")
            }
            value = value.components(separatedBy: ".").first! + "." + lastComponent
            return value
          }else {
            return value
          }
        }
        }else {
          return value
        }
      }else {
        let notAcceptedChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        let cs = CharacterSet(charactersIn: notAcceptedChar).inverted
        let filtered: String = (value.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
        // print(filtered)
        if filtered == "" {
          value = checkingForZero(value)
         let intValue = (value as NSString).integerValue
        if intValue != nil && intValue != 0 {
          var lastComponent = ""
            for _ in 0..<10 {
              lastComponent.append("0")
            }
            value = value.components(separatedBy: ".").first! + "." + lastComponent
            return value
        }else {
          return value
        }
        }else {
          return value
        }
      }
     return value
    }
    
    static func checkingForZero(_ value: String)->String {
       var finalVal = value
        for myVal in Array(value) {
          if myVal == "0" {
            finalVal.removeFirst()
          }else {
            return finalVal
          }
        }
        return finalVal
    }
    
    static func ActionSheet(editPost: @escaping() -> Void, deletePost: @escaping() -> Void) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        optionMenu.addAction(UIAlertAction(title: "Mail (Default)", style: .default, handler: { _ in
            editPost()
        }))
        optionMenu.addAction(UIAlertAction(title: "Gmail", style: .default, handler: { _ in
            deletePost()
        }))
        optionMenu.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in }))
        optionMenu.view.tintColor = Colors.btnborderColorbnb.color()
        getTopMostViewController()?.present(optionMenu, animated: true, completion: nil)
        
    }
    
    static func MD5(string: String) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)

        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
    
    static func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@!@#$%^"
        let len = UInt32(letters.length)
        var randomString = ""
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
    
    
    static func settingCountDownTimmer(_ timeString: String, complitionHandler: @escaping(_ day : Int?,_ hour: Int?, _ minutes: Int?)->()) {
        // here we set the current date
        let date = NSDate()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date as Date)
        let currentDate = calendar.date(from: components)
        let userCalendar = Calendar.current
        // here we set the due date. When the timer is supposed to finish
        let competitionDate = NSDateComponents()
        competitionDate.year = Int((jsonformatDayndMonthDateConvertor(jsonDate: timeString).year as NSString).intValue)
        competitionDate.month = Int((jsonformatDayndMonthDateConvertor(jsonDate: timeString).monthInt as NSString).intValue)
        competitionDate.day = Int((jsonformatDayndMonthDateConvertor(jsonDate: timeString).day as NSString).intValue)
        competitionDate.hour = Int((jsonformatDayndMonthDateConvertor(jsonDate: timeString).hour as NSString).intValue)
        competitionDate.minute = Int((jsonformatDayndMonthDateConvertor(jsonDate: timeString).minutes as NSString).intValue)
        let competitionDay = userCalendar.date(from: competitionDate as DateComponents)!
        //here we change the seconds to hours,minutes and days
        let CompetitionDayDifference = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate!, to: competitionDay)
        //finally, here we set the variable to our remaining time
        let daysLeft = CompetitionDayDifference.day
        let hoursLeft = CompetitionDayDifference.hour
        let minutesLeft = CompetitionDayDifference.minute
        let secondsLeft = CompetitionDayDifference.second
        print("day:", daysLeft ?? "N/A", "hour:", hoursLeft ?? "N/A", "minute:", minutesLeft ?? "N/A", "Seconds:", secondsLeft ?? "N/A")
        //Set countdown label text
        print("\(daysLeft ?? 0) Days, \(hoursLeft ?? 0) Hours, \(minutesLeft ?? 0) Minutes")
        complitionHandler(daysLeft,hoursLeft,minutesLeft)
    }
     
    static func jsonformatDayndMonthDateConvertor(jsonDate:String)-> (year: String, monthInt: String,day : String, hour:String, minutes:String){
        _ = jsonDate
        let localTime = self.UTCToLocal(date: jsonDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: localTime){
            print(date)
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .weekday], from: date)
            if let finalDate = calendar.date(from:components){
                //MARK:::---This gives month as three letters (Jun, Dec, etc)
                let justMonth = DateFormatter()
                justMonth.dateFormat = "MMM"
                let month = justMonth.string(from: finalDate)
                let justMonthInt = DateFormatter()
                justMonthInt.dateFormat = "MM"
                let montInt = justMonthInt.string(from: finalDate)
                //MARK:::---This gives the day of month, with no preceding 0s (6,14,29)
                let justDay = DateFormatter()
                justDay.dateFormat = "dd"
                let day  = justDay.string(from: finalDate)
                let weekDay = DateFormatter()
                weekDay.dateFormat = "EEE"
                let week  = weekDay.string(from: finalDate)
                print(week)
                // MARK:::--- This Will Give Seperate Year Value
                let justYear = DateFormatter()
                justYear.dateFormat = "yyyy"
                let year = "\(justYear.string(from: finalDate))"
                print(day,month,year)
                let justTime = DateFormatter()
                justTime.dateFormat = "HH:mm"
                _ = justTime.string(from: finalDate)
                let justHour = DateFormatter()
                justHour.dateFormat = "HH"
                let hour = justHour.string(from: finalDate)
                let justMin = DateFormatter()
                justMin.dateFormat = "mm"
                let min = justMin.string(from: finalDate)
                return (year,montInt,day,hour,min)
            }else{
                return ("","","","","")
            }
        }else {
            return ("","","","","")
        }
    }
  
    static func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: dt!)
    }
    
    static func getOnlyTime(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: date)
    }
    
    
    static func myTimeCalculator(_ myTime:String) -> (min:Int,sec:Int) {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let time = myTime
        var stopTime = timeFormatter.date(from: time)
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: stopTime!)
        let minute = calendar.component(.minute, from: stopTime!)
        stopTime = calendar.date(bySetting: .year, value: year, of: stopTime!)
        stopTime = calendar.date(bySetting: .month, value: month, of: stopTime!)
        stopTime = calendar.date(bySetting: .day, value: day, of: stopTime!)
        stopTime = calendar.date(bySetting: .hour, value: hour, of: stopTime!)
        stopTime = calendar.date(bySetting: .minute, value: minute, of: stopTime!)
        let timeDifference = calendar.dateComponents([.hour, .minute, .second], from: stopTime!, to: date)
        print(timeDifference.second!)
        let min = timeDifference.hour! * 60
        let timeDiff = -(min+timeDifference.minute!)
        let secon = -(timeDifference.second!)
        return (timeDiff,secon)
    }
    
    static func gettingDifferenceInDate(_ dateValue: String)->String {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
      dateFormatter.locale = NSLocale.current
      dateFormatter.timeZone = TimeZone(abbreviation: "GMT+05:30");
        let date1 = dateFormatter.date(from: dateValue)
//      if date1 == nil {
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+05:30");
//        date1 = dateFormatter.date(from: dateValue)
//      }
      if "\(Calendar.current.component(.day, from: Date()))" == "\(Calendar.current.component(.day, from: date1!))" {
        let diffComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: Date(), to: date1!)
        let hours = diffComponents.hour
        let minutes = diffComponents.minute
        let seconds = diffComponents.second
      if (hours ?? 0) * -1 > 0 {
        return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "h:mm a"))"//"\((hours ?? 0) * -1) hours ago"
      }
      if (((minutes ?? 0) * -1) ) > 59 {
        return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "h:mm a"))"//  "\(hours ?? 0) hours ago"
        }else if ((minutes ?? 0) * -1) < 1{
            return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "h:mm a"))"// "\((seconds ?? 0) * -1) seconds ago"
        }else {
            return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "h:mm a"))"// "\((minutes ?? 0) * -1) minutes ago"
        }
      }else {
        let difference = Calendar.current.dateComponents([.day], from: date1!, to: Date())
      if (difference.day ?? 0) > 1 {
        if (difference.day ?? 0) > 2 {
          return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "dd MMM yyyy"))"
        }else {
          return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "dd MMM yyyy"))"//"\(difference.day ?? 0) days ago"
        }
//        return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd HH:mm:ss", outPutFormate: "dd MMM yyyy"))"
      }else {
        if (difference.day ?? 0) == 0 {
          let hour = Calendar.current.dateComponents([.hour], from: Date(), to: date1!).hour
            return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "dd MMM yyyy"))"//"\((hour ?? 0) * -1) hours ago"
        }else {
          return "\(dateValue.toTime(inputFormate: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outPutFormate: "dd MMM yyyy"))"//"\(difference.day ?? 0) day ago"
        }
        }
      }
    }
    
    static func getImageWithLbl(imageView: UIImageView){
        let lblNameInitialize = UILabel()
        lblNameInitialize.frame.size = imageView.frame.size
        lblNameInitialize.textColor = UIColor.black
        lblNameInitialize.font = UIFont.NunitoRegular(20)
        lblNameInitialize.text = String((coredata.shared.name.first?.uppercased()) ?? "_")
        lblNameInitialize.textAlignment = NSTextAlignment.center
        if coredata.shared.profileimage == ""{
            if coredata.shared.name != ""{
              if(String((coredata.shared.name.first)!).lowercased() == "a"||String((coredata.shared.name.first)!).lowercased() == "g"||String((coredata.shared.name.first)!).lowercased() == "m"||String((coredata.shared.name.first)!).lowercased() == "s"||String((coredata.shared.name.first)!).lowercased() == "y"){
                lblNameInitialize.backgroundColor = Colors.imagePinkColor.color()
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else if(String((coredata.shared.name.first)!).lowercased() == "b"||String((coredata.shared.name.first)!).lowercased() == "h"||String((coredata.shared.name.first)!).lowercased() == "n"||String((coredata.shared.name.first)!).lowercased() == "t"||String((coredata.shared.name.first)!).lowercased() == "z"){
                lblNameInitialize.backgroundColor = Colors.imageOrangeColor.color()
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else if(String((coredata.shared.name.first)!).lowercased() == "c"||String((coredata.shared.name.first)!).lowercased() == "i"||String((coredata.shared.name.first)!).lowercased() == "o"||String((coredata.shared.name.first)!).lowercased() == "u"){
                lblNameInitialize.backgroundColor = Colors.imageYellowColor.color()
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else if(String((coredata.shared.name.first)!).lowercased() == "d"||String((coredata.shared.name.first)!).lowercased() == "j"||String((coredata.shared.name.first)!).lowercased() == "p"||String((coredata.shared.name.first)!).lowercased() == "v"){
                lblNameInitialize.backgroundColor = Colors.imageGreenColor.color()
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else if(String((coredata.shared.name.first)!).lowercased() == "e"||String((coredata.shared.name.first)!).lowercased() == "k"||String((coredata.shared.name.first)!).lowercased() == "q"||String((coredata.shared.name.first)!).lowercased() == "w"){
                lblNameInitialize.backgroundColor = Colors.imageBlueColor.color()
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else if(String((coredata.shared.name.first)!).lowercased() == "f"||String((coredata.shared.name.first)!).lowercased() == "l"||String((coredata.shared.name.first)!).lowercased() == "r"||String((coredata.shared.name.first)!).lowercased() == "x"){
                lblNameInitialize.backgroundColor = Colors.imagePurpleColor.color()
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else if String((coredata.shared.name.first)!).lowercased() == "y"||String((coredata.shared.name.first)!).lowercased() == "z"{
                lblNameInitialize.backgroundColor = UIColor.gray
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else if(String((coredata.shared.name.first)!).lowercased() == "1"||String((coredata.shared.name.first)!).lowercased() == "2"||String((coredata.shared.name.first)!).lowercased() == "3"||String((coredata.shared.name.first)!).lowercased() == "4"||String((coredata.shared.name.first)!).lowercased() == "5"||String((coredata.shared.name.first)!).lowercased() == "6"||String((coredata.shared.name.first)!).lowercased() == "7"||String((coredata.shared.name.first)!).lowercased() == "8"||String((coredata.shared.name.first)!).lowercased() == "9"||String((coredata.shared.name.first)!).lowercased() == "0"){
                lblNameInitialize.backgroundColor = Colors.imagePurpleColor.color()
                UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
                lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
                imageView.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
              }else{
                imageView.yy_setImage(with: URL(string: CommonFunctions.getImage(coredata.shared.profileimage, quality: .large)), placeholder: UIImage(named: "image_large"))
              }
            }else{
                imageView.yy_setImage(with: URL(string: CommonFunctions.getImage(coredata.shared.profileimage, quality: .large)), placeholder: UIImage(named: "image_large"))
            }
        }else{
            imageView.yy_setImage(with: URL(string: CommonFunctions.getImage(coredata.shared.profileimage, quality: .large)), placeholder: UIImage(named: "image_large"))
        }
    }
}

extension String{
    public func toTime(inputFormate:String,outPutFormate:String)->String{
      var finalstring = ""
      let InputdateForamte = DateFormatter()
      InputdateForamte.dateFormat = inputFormate
      let outputdateForamte = DateFormatter()
      outputdateForamte.dateFormat = outPutFormate
      if let inputdate = InputdateForamte.date(from: self){
        finalstring = outputdateForamte.string(from: inputdate)
      }
      return finalstring
    }
}
