//
//  BasePopUp.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit

class BasePopUp: UIViewController {

    var outerVw : UIView?
    var innerVw : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.0) {
            self.showAnimate()
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dissmissPOP(_:)))
            self.outerVw?.addGestureRecognizer(tap1)
        }

        // Do any additional setup after loading the view.
    }
    

    func showAnimate(){
        self.innerVw?.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        innerVw?.alpha = 0.0
        self.outerVw?.alpha = 0.5
        UIView.animate(withDuration: 0.3, delay: 0.05, options: .curveEaseInOut, animations: {
          self.innerVw?.alpha = 1.0
          self.innerVw?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }) { (value) in
          if value{
            UIView.animate(withDuration: 0.1, animations: {
              self.innerVw?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
          }
        }
//        UIView.animate(withDuration: 0.25, animations: {
//        }) { _ in
//            UIView.animate(withDuration: 0.25, animations: {
//                self.innerVw?.alpha = 1.0
//                self.innerVw?.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//            }) { _ in
//                UIView.animate(withDuration: 0.15, animations: {
//                    self.innerVw?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//                })
//            }
//        }
    }
    
    func stopAnimation(){
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0, options: [], animations: {
            self.innerVw?.transform = CGAffineTransform(scaleX: 2, y: 2)
            self.innerVw?.alpha = 0.0
        }) { (success) in
            
            self.view.removeFromSuperview()
        }
    }

    @objc func dissmissPOP(_ sender: UITapGestureRecognizer) {
        stopAnimation()
    }
}

