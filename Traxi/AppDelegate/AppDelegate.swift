//
//  AppDelegate.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 27/11/20.
//

import UIKit
import CoreData
import Firebase
import UserNotifications
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import GoogleSignIn
import ENSwiftSideMenu
import Stripe

let GOOGLEAPIKEY = "AIzaSyC4_qar6ROgANwCHL2qfOfQFyL-0VuBPuE"

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {


    var window: UIWindow?
    var sideMenuNew : SideMenuVC!

    class var shared: AppDelegate{
        struct singleTon {
            static let instance = AppDelegate()
        }
        return singleTon.instance
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //MARK:- IQ KeyBoard Manager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        //MARK:-Google Map Key
        GMSServices.provideAPIKey(GOOGLEAPIKEY)
        GMSPlacesClient.provideAPIKey(GOOGLEAPIKEY)
        
        GIDSignIn.sharedInstance().clientID = googleClientId
        
        //MARK:- firebase configuration
        FirebaseApp.configure()
        registerRemoteNotifications(application: application)
        Messaging.messaging().isAutoInitEnabled = true
        
        //MARK:- Connect Socket
        SocketIOManager.sharedInstance.establishConnection()
        
        //MARK:- Delay in splash screen
        Thread.sleep(forTimeInterval: 1.5)
        
        //MARK:- Button flags Icons store locally
        Bundle.FlagIcons = Bundle(for: type(of: self))
        coredata.shared.getdata()
        if coredata.shared.accesstoken != ""{
            self.GetProfileApiHit()
        }
        getUser()
        
        //MARK:- Stripe Key
        StripeAPI.defaultPublishableKey = (Stripe_Key)
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        AppDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
    }
    
    //MARK:- Send User to screens
    func getUser(){
        if coredata.shared.accesstoken == ""{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
            }
        }else if coredata.shared.phone_no == "" || coredata.shared.phone_no == "0"{
            let vc = PhoneNumberVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Main)
            vc.isFromSsocialLogin = true
            let navigationController = UINavigationController(rootViewController: vc)
            if UIApplication.shared.windows.count > 0 {
                UIApplication.shared.windows[0].rootViewController = navigationController
                navigationController.setNavigationBarHidden(true, animated: false)
            }
        }else if coredata.shared.email == ""{
            let vc = SignUpVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Main)
            let navigationController = UINavigationController(rootViewController: vc)
            if UIApplication.shared.windows.count > 0 {
                UIApplication.shared.windows[0].rootViewController = navigationController
                navigationController.setNavigationBarHidden(true, animated: false)
            }
        }else{
            sideMenuNew = SideMenuVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
            let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
            
            let navVC : navController = navController(menuViewController: sideMenuNew, contentViewController: vc)
//            let navigationController = UINavigationController(rootViewController: vc)
            if UIApplication.shared.windows.count > 0 {
                UIApplication.shared.windows[0].rootViewController = navVC
                navVC.setNavigationBarHidden(true, animated: false)
            }
        }
    }

    func registerRemoteNotifications(application: UIApplication){
        
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .InstanceIDTokenRefresh,
                                               object: nil)
        
        
        application.registerForRemoteNotifications()
        let application = UIApplication.shared
        application.applicationIconBadgeNumber = 0
    }
    
    
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Traxi")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    class func sharedInstance() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return true //AppDelegate.shared.application( application,
//                                                       open: url,
//                                                       sourceApplication: sourceApplication,
//                                                       annotation: annotation)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("deviceToken : \(deviceToken)")
        if(deviceToken.count > 80){
            return
        }
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
                return
            } else if let result = result {
                device_token = result.token
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        //        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.sandbox)
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print(token)
        
        //        InstanceID.instanceID().setAPNSToken(deviceToken, type: InstanceIDAPNSTokenType.sandbox)
        Messaging.messaging().apnsToken = deviceToken
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
                device_token = token
            } else if let result = result {
                device_token = result.token
                print("Remote instance ID token: \(result.token)")
            }
        }
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                device_token = result.token
                print("Remote instance ID token: \(result.token)")
            }
        }
        //        if let refreshedToken = InstanceID.instanceID().token() {
        //            print("InstanceID token: \(refreshedToken)")
        //        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    
    func connectToFcm() {
        // Won't connect since there is no token
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
                return
            } else if let result = result {
                device_token = result.token
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        if (Messaging.messaging().isDirectChannelEstablished){
            InstanceID.instanceID().instanceID { (result, error) in
                if let error = error {
                    print("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    device_token = result.token
                    print("Remote instance ID token: \(result.token)")
                }
            }

            print("Connected to FCM.")
        }
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("recive here", remoteMessage)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        print("Handle push from background")

        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            
            
            if userInfo["notif_type"] != nil && coredata.shared.accesstoken != ""{
                
                if userInfo["notif_type"] as! String == "1"{//notif_type:- 1 = Delivery, 2 = Order, 3= ratings, 4 = complains, 5 = Admin

                    let vc = DeliveryStatusVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
                    vc.deliveryId = Int(userInfo["delivery_id"] as! String) ?? 0
                    rootViewController.pushViewController(vc, animated: true)
                }else if userInfo["notif_type"] as! String == "2"{
                    let vc = OrderDetailVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.BookingHistory)
                    vc.orderId = Int(userInfo["delivery_id"] as! String) ?? 0
                    rootViewController.pushViewController(vc, animated: true)
                }else if userInfo["notif_type"] as! String == "5"{
                    SelectedIndexValueOfSideMenu = 5
                    let vc = NotificationVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Payment)
                    rootViewController.pushViewController(vc, animated: true)
                }else if userInfo["notif_type"] as! String == "4"{
                    SelectedIndexValueOfSideMenu = 4
                    let vc = ComplaintsVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Chat)
                    rootViewController.pushViewController(vc, animated: true)
                }else{
                    self.sideMenuNew = SideMenuVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
                    let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
                    
                    let navVC : navController = navController(menuViewController: self.sideMenuNew, contentViewController: vc)
                    if UIApplication.shared.windows.count > 0 {
                        UIApplication.shared.windows[0].rootViewController = navVC
                        navVC.setNavigationBarHidden(true, animated: false)
                    }
                }
            }
        }
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Remote instance ID token: \(fcmToken)")
        device_token = fcmToken
        print("device_token",device_token)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        print("Handle push from foreground")
        // custom code to handle push while app is in the foreground
        print("\(notification.request.content.userInfo)")
        let userInfo = notification.request.content.userInfo

        print(userInfo)
        
        if userInfo["notif_type"] != nil{
            if userInfo["notif_type"] as! String == "1"{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PushNotifReloadData"), object: nil)
            }else if userInfo["notif_type"] as! String == "5"{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            }
        }
        
//        let nVc = CommonFunctions.getTopMostViewController() as! UINavigationController
//        let tvc = nVc.viewControllers[nVc.viewControllers.count - 1]
        
//        if tvc.isKind(of: orderDetailVC.self){
//            let oVc = tvc as! orderDetailVC
//            if oVc.id == userInfo["order_id"] as! String{
//                oVc.getOrderDetail()
//            }
//        }
        
        completionHandler([UNNotificationPresentationOptions.alert, UNNotificationPresentationOptions.badge, UNNotificationPresentationOptions.sound])
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            if profileAPI.lat != nil{
                let loca = locNew.init(profileAPI.lat ?? 0.0, lng: profileAPI.lng ?? 0.0, name: profileAPI.address ?? "", fullAddress: profileAPI.address ?? "", Appartment: profileAPI.apartment ?? "",landMark: profileAPI.landmark ?? "")
                globalAddedLocation = loca
            }
            if profileAPI.tagType == "Home"{
                locationTagType = 1
            }else if profileAPI.tagType == "Work"{
                locationTagType = 2
            }else{
                locationTagType = 3
            }
            coredata.shared.phone_no = "\(profileAPI.phone ?? 0)"
            coredata.shared.email = profileAPI.email ?? ""
            coredata.shared.name =  profileAPI.name ?? ""
            coredata.shared.countryCode = profileAPI.countryCode ?? ""
            coredata.shared.isEmailVerified = profileAPI.isEmailVerified ?? 0
            coredata.shared.walletBalance = profileAPI.walletBalance ?? 0
            coredata.shared.dataSave()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }

}

