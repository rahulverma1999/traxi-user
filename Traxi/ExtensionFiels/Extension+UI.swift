import UIKit


internal var topStatusHeight : CGFloat{
    if UIDevice.isBigIphone{
        return 44
    }else{
        return 22
    }
}

internal var bottomStatusHeight : CGFloat{
    if UIDevice.isBigIphone{
        return 34
    }else{
        return 4
    }
}

internal struct Languages {
    static let Arabic = "ar"
    static let English = "en"
}

// Screen width
public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width // current screen width
}

// Screen height
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height // current screen height
}

//var industries : [IndustryAPIElement] = []
//var categoriesNew : [getCategories] = []
//var schoolStatic : [SchoolElement] = []
//var benifitsNew : [Benefit] = []



//struct Fonts {
//    struct ProximaNova {
//        internal static let Regular = "ProximaNova-regular"
//        internal static let Bold = "ProximaNova-bold"
//        internal static let Light = "ProximaNova-light"
//        internal static let SemiBold = "ProximaNova-Semibold"
//    }
//}


internal enum Colors : Int {
    case tfTextColor = 0x424142ff
    case RedColor = 0xF54540f
    //Horsebnb Color
    case btnborderColorbnb = 0x343A40ff
    case redColorLoginbnb = 0xDF3C47ff
    case colorChatBgColorbnb = 0xFAFAFAff
    case tfBorderColorbnb = 0xD8D8D8ff
    case btnBorderBlueColorBnb = 0x008489ff
    case btnBorderGrayBnb = 0x8C8E9099ff
    case bookingPendingColor = 0xCEAF08ff
    case bookingBlueColor = 0x3597DCff
    case bookingGreenColor = 0x15AA09ff
    case pendinfColorStatusBnb = 0xffc107ff
    case reviewExpireStatusBnb = 0xdc3545ff
    
    
    //Traxi App Colors
    case pinkColorTraxi = 0xFF148Eff
    case borderYellowColorTraxi = 0xE8C857ff
    case greyColorTraxi = 0x404040ff
    case traxiShadowColor = 0xB38C00ff
    case traxiBlueColor = 0x0031A3ff
    case traxiGreenColor = 0x009957ff
    case traxiredColor = 0xFC441Fff
    case traxiSkinColor = 0xFFF6C7ff
    case traxiThemeColor = 0xFFEBA2ff
    
    case imagePinkColor = 0xffadadff
    case imageOrangeColor = 0xffd6a5ff
    case imageYellowColor = 0xfdffb6ff
    case imageGreenColor = 0xcaffbfff
    case imageBlueColor = 0xa0c4ffff
    case imagePurpleColor = 0xffc6ffff
    case traxiRedColor = 0xFF0000ff
//    case traxiPinkColorOpacity20 = 0x33FF148Eff
    
    func color() -> UIColor {
        return UIColor(netHex: self.rawValue)
    }
}



