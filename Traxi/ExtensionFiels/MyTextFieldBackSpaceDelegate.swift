//
//  MyTextFieldBackSpaceDelegate.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 11/12/20.
//

// MyTextField.swift
import UIKit

protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {

    var myDelegate: MyTextFieldDelegate?

    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }

}

