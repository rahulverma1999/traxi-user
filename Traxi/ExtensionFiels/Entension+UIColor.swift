//
//  Extension+UIColor.swift
//  NewApp
//
//  Created by Bhanu Dhiman on 27/08/19.
//  Copyright © 2019 Bhanu Dhiman. All rights reserved.
//

import UIKit

extension UIColor {
    // help in fetching color code throught Int
    convenience init(red: Int, green: Int, blue: Int, alpha: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        assert(alpha >= 0 && alpha <= 255, "Invalid alpha component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: CGFloat(alpha) / 255.0)
    }
    
    // help in fetching color code throught Int
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 24) & 0xff, green:(netHex >> 16) & 0xff, blue:(netHex >> 8) & 0xff, alpha: netHex & 0xff)
    }
    
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}

