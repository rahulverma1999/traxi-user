//
//  Fonts.swift
//  Lowicks
//
//  Created by MAC on 03/09/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

extension UIFont {
//    class func poppinsMedium(_ size:CGFloat) -> UIFont {
//        return UIFont(name: "Poppins-Medium", size: size)!
//    }
//    class func poppinsRegular(_ size:CGFloat) -> UIFont {
//        return UIFont(name: "Poppins-Regular", size: size)!
//    }
    
    class func NunitoRegular(_ size:CGFloat) -> UIFont {
        return UIFont(name: "Nunito-Regular", size: size)!
    }

    class func NunitoSemiBold(_ size:CGFloat) -> UIFont {
        return UIFont(name: "Nunito-SemiBold", size: size)!
    }

    class func NunitoBold(_ size:CGFloat) -> UIFont {
        return UIFont(name: "Nunito-Bold", size: size)!
    }
//
//    class func SfProSemiBold(_ size:CGFloat) -> UIFont {
//        return UIFont(name: "SFProDisplay-Semibold", size: size)!
//    }
    
//    class func NunitoRegular(_ size:CGFloat) -> UIFont {
//        return UIFont(name: "Nunito-Regular", size: size)!
//    }
//    
//    class func MontserratSemiBold(_ size:CGFloat) -> UIFont {
//        return UIFont(name: "Montserrat-SemiBold", size: size)!
//    }
}
