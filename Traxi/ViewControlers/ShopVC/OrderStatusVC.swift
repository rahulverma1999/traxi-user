//
//  OrderStatusVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 10/12/20.
//

import UIKit
import GoogleMaps
import GooglePlaces

class OrderStatusVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var deiverImage: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDriverNmaeButom: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblReview: UILabel!
    
    //MARK:- Variables
    var orderId : Int!
    var activeOrderData : ActiveOrdersAPIElement?
    var orderDetailData : GetOrderAPI?
    
    //MARK:- VAriables
    var DriverLatLng : CLLocationCoordinate2D?
    var driverMarker : GMSMarker?
    var previousCoordinate: CLLocationCoordinate2D?
    var destinationLatLng : CLLocationCoordinate2D?
    var drivrId : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
        setMapSetting()
        getOrderDetailApi()
    }

    //MARK:- UIbutton Actions
    @IBAction func btnActPhone(_ sender: Any) {
//        let vc = RateToShopVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
        if orderDetailData?.driverPhone != "" && orderDetailData?.driverPhone != nil{
            self.callMethod(number: orderDetailData?.driverPhone ?? "")
        }else{
            CommonFunctions.toster("Number not found.")
        }
    }
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension OrderStatusVC{
    //MARK:- SetUpUI
    func setupUi(){
        deiverImage.layer.cornerRadius = 4.0
        DispatchQueue.main.async {
            self.scrollViewHeight.constant = self.view.bounds.height/2.8
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.vwCorner.roundCorners([.topRight, .topLeft], radius: 24)
        }
        


    }
    
    
    func setDataAfterResponse(){
        self.destinationLatLng = CLLocationCoordinate2D(latitude: orderDetailData?.userLat ?? 0.0, longitude: orderDetailData?.userLng ?? 0.0)
        
        SocketIOManager.sharedInstance.getListenLatLng{ (data)  in
            print(data)
//            self.DriverLatLng = CLLocationCoordinate2D(latitude: ((data["lat"] as? Double) ?? 0.0), longitude: ((data["lng"] as? Double) ?? 0.0))
            self.DriverLatLng = CLLocationCoordinate2D(latitude: ((data["lat"] as? NSString)?.doubleValue ?? 0.0), longitude: ((data["lng"] as? NSString)?.doubleValue ?? 0.0))
            self.drivrId = (data["driver_id"] as? Int) ?? 0
            if self.drivrId == self.orderDetailData?.driverID{
                print("Driver id matched")
                self.tracking(currentLat: self.DriverLatLng?.latitude ?? 0.0, currentLong: self.DriverLatLng?.longitude ?? 0.0)
            }else{
                print("Driver id not matched")
            }
        }
        
        if DriverLatLng == nil{
            self.tracking(currentLat: self.orderDetailData?.driverLat ?? 0.0, currentLong: self.orderDetailData?.driverLng ?? 0.0)
        }
    }
    
    func setDataApi(_ data : GetOrderAPI?){
        lblDriverName.text = "\(data?.driverName ?? "N/A") picked up your order, he is on the way"
        lblTime.text = data?.estimatedDeliveryTime ?? ""
        deiverImage.yy_setImage(with: URL(string: CommonFunctions.getImage(data?.driverImage ?? "", quality: .large)), placeholder: UIImage(named: "image_large"))
        lblDriverNmaeButom.text = "\(data?.driverName ?? "N/A")"
    }
    
    //MARK:- Set map setting
    func setMapSetting(){
        mapView.animate(toZoom: 15)
        mapView.settings.zoomGestures = true
        mapView.settings.rotateGestures = true
        mapView.settings.scrollGestures = true
        mapView.isUserInteractionEnabled = true
        LocationServices.shared.getAdress { (valueName, stringError) in
            if valueName != nil {
                self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0.0, longitude: valueName?.location?.coordinate.longitude ?? 0.0))
             }else{
             }
        }
    }
    
    func callMethod(number: String) {
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

//MARK:- Hit Api
extension OrderStatusVC{
    func getOrderDetailApi(){
        let paarams : [String : AnyObject] = [
            "order_id"  : self.orderId as AnyObject,
            "job_type" : 2 as AnyObject
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getOrderDetail, withParameters: paarams as [String: AnyObject], ofType: GetOrderAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.orderDetailData = profileAPI
            self.setDataApi(profileAPI)
            self.setDataAfterResponse()
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}


extension OrderStatusVC{
    //MARK:- Tracking methods
    func pathFromStartToEndPoint(startLat: Double, startLong:Double, endLat:Double, endLong:Double, isLoader: Bool) {
            let startPoint = CLLocationCoordinate2DMake(startLat, startLong)
            let end = CLLocationCoordinate2DMake(endLat, endLong)
            let allpoints = [startPoint,end]
            var bounds = GMSCoordinateBounds()
            for i in 0..<allpoints.count {
                bounds = bounds.includingCoordinate(allpoints[i])
            }
            if (isLoader) == true{
                let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
                self.mapView.animate(with: update)
            }
        CommonFunctions.getPolylineRoute(from: startPoint, to: end, isLoader: isLoader) { (status, polyline,time,distance,TimeValue)  in
                print(status, polyline,time,distance)
                if !(polyline.isEmpty){
                    self.mapView.clear()
                    let coordinate1 = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
                    self.mapView.isMyLocationEnabled = false
                    self.driverMarker?.position = coordinate1
                    self.driverMarker?.icon = UIImage(named: "car_bottom")
                    self.driverMarker?.map = self.mapView
                }
                self.showPath(polyStr: polyline)
                let marker = GMSMarker(position: startPoint)
                marker.icon = #imageLiteral(resourceName: "car_bottom")
                marker.map = self.mapView
                let marker1 = GMSMarker(position: end)
                marker1.icon = #imageLiteral(resourceName: "map_pin_blue")
                marker1.map = self.mapView
            }
        }
    
    
    func showPath(polyStr :String){
            guard let path = GMSPath(fromEncodedPath: polyStr) else { return }
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
        polyline.strokeColor = .black
            polyline.map = self.mapView
            var bounds = GMSCoordinateBounds()
            for index in 0...path.count() {
                bounds = bounds.includingCoordinate(path.coordinate(at: UInt(index)))
            }
            mapView.animate(with: GMSCameraUpdate.fit(bounds))
        }
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
        func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / Double.pi }
        func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
            let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
            let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
            let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
            let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
            let dLon = lon2 - lon1
            let y = sin(dLon) * cos(lat2)
            let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
            let radiansBearing = atan2(y, x)
            return radiansToDegrees(radians: radiansBearing)
    }
    
    func tracking(currentLat: Double, currentLong: Double) {
            //        self.mapView.clear()
        let coordinate1 = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLong)
        self.mapView.isMyLocationEnabled = false
        self.driverMarker?.position = coordinate1
        self.driverMarker?.icon = #imageLiteral(resourceName: "car_bottom")
        self.driverMarker?.map = self.mapView
        if self.previousCoordinate != nil {
            self.driverMarker?.rotation = self.getBearingBetweenTwoPoints1(point1: CLLocation(latitude: currentLat, longitude: currentLong), point2: CLLocation(latitude: self.previousCoordinate?.latitude ?? 0, longitude: self.previousCoordinate?.longitude ?? 0))
//            print("************\(self.driverMarker?.rotation)")
            self.pathFromStartToEndPoint(startLat: currentLat, startLong: currentLong, endLat: (self.destinationLatLng?.latitude ?? 0.0), endLong: (self.destinationLatLng?.longitude ?? 0.0), isLoader: false)
        }else{
            self.pathFromStartToEndPoint(startLat: currentLat, startLong: currentLong, endLat: (self.destinationLatLng?.latitude ?? 0.0), endLong: (self.destinationLatLng?.longitude ?? 0.0), isLoader: false)
        }
        self.previousCoordinate = coordinate1
    }
}
