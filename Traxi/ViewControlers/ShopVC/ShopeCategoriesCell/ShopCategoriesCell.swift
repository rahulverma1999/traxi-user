//
//  ShopCategoriesCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/12/20.
//

import UIKit

class ShopCategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //shadow to view
        bgView.layer.cornerRadius = 4
        bgView.clipsToBounds = true
        bgView.layer.masksToBounds = false
        bgView.layer.shadowColor = UIColor.black.cgColor
        bgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        bgView.layer.shadowOpacity = 0.5
        bgView.layer.shadowRadius = 5.0
        
        imgView.layer.cornerRadius = 4.0
    }
}
