//
//  CartDetailVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 10/12/20.
//

import UIKit

struct UserDataStruct{
    let name : String!
    let countryCode : String!
    let phoneNumber : String!
}

class CartDetailVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnChangeAddress: UIButton!
    @IBOutlet weak var tfCoupon: paddingLeftAndRightSide!
    @IBOutlet weak var btnApplyCoupon: UIButton!
    @IBOutlet weak var btnAddMponey: UIButton!
    @IBOutlet weak var btnChangeNameNumber: UIButton!
    @IBOutlet weak var bottomContraintOfBottomCartView: NSLayoutConstraint!
    @IBOutlet weak var lblItemCount: UILabel!
    @IBOutlet weak var lblPriceBottom: UILabel!
    @IBOutlet weak var btnPlaceOrder: UIButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDeliveryTagType: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPreprationTime: UILabel!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var lblUserNameAndNumber: UILabel!
    @IBOutlet weak var lblItemTotalPrice: UILabel!
    @IBOutlet weak var lblDeliveryChargesAmount: UILabel!
    @IBOutlet weak var lblCoupnAndDisscountPrice: UILabel!
    @IBOutlet weak var vwCoupnAndDisscount: UIView!
    @IBOutlet weak var lblAllTotalCaluclatedPrice: UILabel!
    @IBOutlet weak var amountInWallet: UILabel!
    @IBOutlet weak var lblGSTCharges: UILabel!
    
    //MARK:- Variables
    var totalCount = 0
    var itemsAddedCount = 0
    var pricePerUnit = 0.0
    var deliveryTimeCalaucted : Int = 0
    var distanceDriverFromShopToHome = 0.0
    var storeId : Int!
    var deliveyActualTime = ""
    
    var userData : UserDataStruct?
    var counpDetail : GetCoupnDetailAPI?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        lblNoDataFound.isHidden = true
        self.lblCoupnAndDisscountPrice.text = ""
//        self.setDataOfButtonView(addedProductData)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if globalAddedLocation == nil{
        }else{
            self.lblAddress.text = globalAddedLocation?.fullAddress ?? ""
            if locationTagType == 1{
                self.lblDeliveryTagType.text = "Delivery to Home"
            }else if locationTagType == 2{
                self.lblDeliveryTagType.text = "Delivery to Work"
            }else{
                self.lblDeliveryTagType.text = "Delivery to Others"
            }
            EditProfileApiHit()
        }
        self.GetProfileApiHit()
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActChangeAddress(_ sender: Any) {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .cartDetail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActApplyCoupon(_ sender: Any) {
        if tfCoupon.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfCoupon.shake()
            CommonFunctions.toster("Please enter coupon")
        }else{
            self.getCounpDisscount()
        }
    }
    @IBAction func btnActAddMoney(_ sender: Any) {
        let vc = TraxiCreditsVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
        vc.fromOrderScreen = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActChangeNameNumber(_ sender: Any) {
        let vc = ContactDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        vc.delegate = self
        vc.userData = userData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActPlaceOrder(_ sender: Any) {
        if self.btnAddMponey.isHidden{
            self.createOrderApi()
        }else{
            CommonFunctions.toster("Your Traxi Credit balance is low. Please add Credits.")
        }
        
    }
}

//MARK:- Delegate methods
extension CartDetailVC: ContactDetailVCDelegate{
    func sendUserDetail(_ userData: UserDataStruct) {
        self.userData = userData
        setUserData()
    }
}

extension CartDetailVC{
    //MARK:- setupUI
    func setUpUI(){
        self.getPrepareTime()
        let appendData = UserDataStruct.init(name: coredata.shared.name, countryCode: coredata.shared.countryCode, phoneNumber: coredata.shared.phone_no)
        self.userData = appendData
        setUserData()
        self.vwCoupnAndDisscount.isHidden = true
    }
    
    func getPrepareTime(){
        if addedProductData.count == 0{
            self.lblPreprationTime.isHidden = true
        }else{
            self.lblPreprationTime.isHidden = false
        }
        var calculatedMaxTimeForPrePration : [Int] = []
        for time in addedProductData{
            calculatedMaxTimeForPrePration.append(time.timeToMake ?? 0)
        }
        let sortedData = calculatedMaxTimeForPrePration.sorted(by: { $0.self > $1.self})
        let intTime = CommonFunctions.decimalPlaceFromDouble((Double((sortedData.first ?? 0)*60)), decimalPlace: 0)
        let totalTime = (self.deliveryTimeCalaucted) + ((intTime as NSString).integerValue)
        let actualTime = "\(CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: Int(totalTime)))"
        self.lblPreprationTime.text = "Prepared & Delivered in \(actualTime)"
        self.deliveyActualTime = actualTime
    }
    
    func setUserData(){
        self.lblUserNameAndNumber.text = "\(userData?.name ?? "N/A"), \(userData?.countryCode ?? "N/A")\(userData?.phoneNumber ?? "N/A")"
    }
}

//MARK:- UItable view delegate methods
extension CartDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if addedProductData.count == 0{
            lblNoDataFound.isHidden = false
            self.setDataOfButtonView(addedProductData)
            self.navigationController?.popViewController(animated: true)
            return 0
        }else{
            lblNoDataFound.isHidden = true
            return addedProductData.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartDetailItemCell", for: indexPath) as! CartDetailItemCell
        DispatchQueue.main.async {
            self.tableView.layoutIfNeeded()
            let contentSize = self.tableView.contentSize.height
            self.tableViewHeight.constant = contentSize
        }
        cell.lblTotalPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble((addedProductData[indexPath.row].price ?? 0.0), decimalPlace: 2))"
        let calculatePrice = CommonFunctions.decimalPlaceFromDouble(((Double(CommonFunctions.decimalPlaceFromDouble((addedProductData[indexPath.row].price ?? 0.0), decimalPlace: 2)) ?? 0.0)/Double(addedProductData[indexPath.row].itemCount ?? 0)), decimalPlace: 2)
        cell.lblPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble((Double(calculatePrice) ?? 0.0), decimalPlace: 2))"
        cell.lblName.text = addedProductData[indexPath.row].name ?? ""
        cell.lblDes.text = addedProductData[indexPath.row].description ?? "N/A"
        cell.btnMinus.setTitle("\(addedProductData[indexPath.row].timeToMake ?? 0)", for: .normal)
        cell.imgFood.yy_setImage(with: URL(string: CommonFunctions.getImage(addedProductData[indexPath.row].imageOfProduct ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
        cell.btnAdd.tag = indexPath.row
        cell.btnMinus.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(btnActAddHit(_:)), for: .touchUpInside)
        cell.btnMinus.addTarget(self, action: #selector(btnActMinusHit(_:)), for: .touchUpInside)
        cell.delegate = self
        var idPresent = false
        print("Strut array count:",addedProductData.count)
        for dt in addedProductData{
            if dt.productId == addedProductData[indexPath.row].productId {
                idPresent = true
                if dt.itemCount > 0{
                    cell.vwMinus.isHidden = false
                    cell.vwCount.isHidden = false
                    cell.lblCount.text = "\(dt.itemCount ?? 0)"
                }else{
                    cell.vwMinus.isHidden = true
                    cell.vwCount.isHidden = true
                }
            }
        }
        if !idPresent{
            cell.vwMinus.isHidden = true
            cell.vwCount.isHidden = true
        }
        self.setDataOfButtonView(addedProductData)
        self.GetProfileApiHit()
        return cell
    }
}

extension CartDetailVC{
    @objc func btnActAddHit(_ sender : UIButton){
        self.itemsAddedCount = 0
        self.pricePerUnit = 0.0
        var countPresnet = false
        for dt in addedProductData{
            if dt.productId == addedProductData[sender.tag].productId ?? 0{
                self.itemsAddedCount = dt.itemCount+1
                self.pricePerUnit = ((addedProductData[sender.tag].price ?? 0.0)/Double(addedProductData[sender.tag].itemCount ?? 0)) * Double(self.itemsAddedCount)
                countPresnet = true
            }
        }
        if !countPresnet{
            self.itemsAddedCount += 1
            self.pricePerUnit = ((addedProductData[sender.tag].price ?? 0.0)/Double(addedProductData[sender.tag].itemCount ?? 0))
        }
        let addedData = StructProductsAddedData.init(name: addedProductData[sender.tag].name ?? "", description: addedProductData[sender.tag].description ?? "", price: self.pricePerUnit, timeToMake: addedProductData[sender.tag].timeToMake ?? 0, itemCount: itemsAddedCount, imageOfProduct: addedProductData[sender.tag].imageOfProduct ?? "", productId: addedProductData[sender.tag].productId ?? 0, catProductId: addedProductData[sender.tag].catProductId ?? 0)
        var changed = false
        if addedProductData.count > 0{
            for (num, data) in addedProductData.enumerated(){
                if (data.catProductId == addedData.catProductId) && (data.productId == addedData.productId){
                    changed = true
                    addedProductData[num] = addedData
                }
            }
        }
        if !changed{
            addedProductData.append(addedData)
        }
        setMinusAndCountHideUnHide()
    }
    
    @objc func btnActMinusHit(_ sender : UIButton){
        var countPresnet = false
        for dt in addedProductData{
            if dt.productId == addedProductData[sender.tag].productId ?? 0{
                self.itemsAddedCount = dt.itemCount-1
                self.pricePerUnit = (dt.price) - ((addedProductData[sender.tag].price ?? 0.0)/Double(addedProductData[sender.tag].itemCount ?? 0))
                countPresnet = true
            }
        }
        if !countPresnet{
            self.itemsAddedCount -= 1
            self.pricePerUnit -= ((addedProductData[sender.tag].price ?? 0.0)/Double(addedProductData[sender.tag].itemCount ?? 0))
        }
        
        let removeData = StructProductsAddedData.init(name: addedProductData[sender.tag].name ?? "", description: addedProductData[sender.tag].description ?? "", price: self.pricePerUnit, timeToMake: addedProductData[sender.tag].timeToMake ?? 0, itemCount: itemsAddedCount, imageOfProduct: addedProductData[sender.tag].imageOfProduct ?? "", productId: addedProductData[sender.tag].productId ?? 0, catProductId: addedProductData[sender.tag].catProductId ?? 0)
        print("tag: ", sender.tag)
        for (num, data) in addedProductData.enumerated(){
            if data.productId == addedProductData[sender.tag].productId ?? 0{
                if self.itemsAddedCount > 0{
                    addedProductData[num] = removeData
                }else{
                    addedProductData.remove(at: num)
                }
                break
            }
        }
        setMinusAndCountHideUnHide()
    }
    
    func setMinusAndCountHideUnHide(){
        tableView.reloadData()
    }
}

extension CartDetailVC{
    func setDataOfButtonView(_ data : [StructProductsAddedData]){
        var tempCount = 0
        var totalPrice = 0.0
        for count in data{
            tempCount += count.itemCount
            totalPrice += count.price
        }
        self.totalCount = tempCount
        if self.totalCount > 0{
            bottomContraintOfBottomCartView.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblPriceBottom.text = "$\(CommonFunctions.decimalPlaceFromDouble(totalPrice, decimalPlace: 2))"
        }else{
            bottomContraintOfBottomCartView.constant = -90
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        if self.totalCount > 1{
            lblItemCount.text = "\(self.totalCount) ITEMS"
        }else{
            lblItemCount.text = "\(self.totalCount) ITEM"
        }
        if counpDetail == nil{
            self.lblItemTotalPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble(totalPrice, decimalPlace: 2))"
            let temp1 = CommonFunctions.decimalPlaceFromDouble(distanceDriverFromShopToHome, decimalPlace: 2)
            let temp2 = CommonFunctions.decimalPlaceFromDouble(totalPrice, decimalPlace: 2)
            let totalChargeTemp = ((temp1 as NSString).doubleValue + (temp2 as NSString).doubleValue)
//            let totalCountCharges = CommonFunctions.decimalPlaceFromDouble(totalChargeTemp, decimalPlace: 2)
            let gstCharges = totalChargeTemp * 0.10
            self.lblGSTCharges.text = "$\(CommonFunctions.decimalPlaceFromDouble(gstCharges, decimalPlace: 2))"
            self.lblAllTotalCaluclatedPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble(totalChargeTemp + gstCharges, decimalPlace: 2))"
        }else{
            let disscountAmount = ((Double(counpDetail?.amountOff ?? 0))/100)*totalPrice
            let finalAmountAfterDiscount = totalPrice-disscountAmount
            self.lblItemTotalPrice.text = "$\((CommonFunctions.decimalPlaceFromDouble(totalPrice, decimalPlace: 2)))"
            self.lblCoupnAndDisscountPrice.text = "-$\((CommonFunctions.decimalPlaceFromDouble(disscountAmount, decimalPlace: 2)))"
            
            let temp1 = CommonFunctions.decimalPlaceFromDouble(distanceDriverFromShopToHome, decimalPlace: 2)
            let temp2 = CommonFunctions.decimalPlaceFromDouble(finalAmountAfterDiscount, decimalPlace: 2)
            let totalChargeTemp = ((temp1 as NSString).doubleValue + (temp2 as NSString).doubleValue)
            //let totalCountCharges = CommonFunctions.decimalPlaceFromDouble(totalChargeTemp, decimalPlace: 2)
            let gstCharges = totalChargeTemp * 0.10
            self.lblGSTCharges.text = "$\(CommonFunctions.decimalPlaceFromDouble(gstCharges, decimalPlace: 2))"
            self.lblAllTotalCaluclatedPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble(totalChargeTemp + gstCharges, decimalPlace: 2))"
        }
        
        self.lblDeliveryChargesAmount.text = "$\(CommonFunctions.decimalPlaceFromDouble(distanceDriverFromShopToHome, decimalPlace: 2))"

        self.getPrepareTime()
    }
}

//MARK:- Delegate methods
extension CartDetailVC: CartDetailItemCellDelegate{
    func getAddOrMinusHit(hit: Int, count: Int, minusCall: Bool) {
//        if count == 0{
//            if minusCall{
//                if totalCount > 0{
//                    totalCount-=1
//                }
//            }
//        }else{
//            if minusCall{
//                totalCount-=1
//            }else{
//                totalCount+=1
//            }
//        }
//        print(totalCount)
//        if totalCount > 0{
//            bottomContraintOfBottomCartView.constant = 0
//            UIView.animate(withDuration: 0.3) {
//                self.view.layoutIfNeeded()
//            }
//        }else{
//            bottomContraintOfBottomCartView.constant = -90
//            UIView.animate(withDuration: 0.3) {
//                self.view.layoutIfNeeded()
//            }
//        }
//        if totalCount > 1{
//            lblItemCount.text = "\(totalCount) ITEMS"
//        }else{
//            lblItemCount.text = "\(totalCount) ITEM"
//        }
    }
}

//MARK:- Hit Api
extension CartDetailVC{
    func createOrderApi(){
        
        var SelectedProducts : [[String: AnyObject]] = [[:]]
        SelectedProducts.removeAll()
        
        for data in addedProductData{
            let parm : [String : AnyObject] = [
                "product_id" : data.productId as AnyObject,
                "quantity" : data.itemCount as AnyObject,
                "price" : data.price as AnyObject
            ]
            SelectedProducts.append(parm)
        }
        
        var paarams : [String : AnyObject] = [
            "store_id": self.storeId as AnyObject,
            "delivery_charges" : ((self.lblDeliveryChargesAmount.text)?.replacingOccurrences(of: "$", with: "")) as AnyObject,
            "tax_charges" : ((self.lblGSTCharges.text)?.replacingOccurrences(of: "$", with: "")) as AnyObject,
            "estimated_delivery_time" : self.deliveyActualTime as AnyObject,
            "order_total" : ((self.lblAllTotalCaluclatedPrice.text)?.replacingOccurrences(of: "$", with: "")) as AnyObject,
            "address" : globalAddedLocation?.fullAddress as AnyObject,
            "user_name" : userData?.name as AnyObject,
            "user_phone" : userData?.phoneNumber as AnyObject,
            "user_apartment" : globalAddedLocation?.Appartment as AnyObject,
            "user_landmark" : globalAddedLocation?.landMark as AnyObject,
            "lat" : globalAddedLocation?.lat as AnyObject,
            "lng" : globalAddedLocation?.lng as AnyObject,
            "products" : SelectedProducts as AnyObject,
         ]
        
        if self.counpDetail != nil{
            paarams["coupon"] = self.counpDetail?.couponID as AnyObject
        }
        
        if lblCoupnAndDisscountPrice.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            paarams["discount"] = ((self.lblCoupnAndDisscountPrice.text)?.replacingOccurrences(of: "-$", with: "")) as AnyObject
        }
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.createOrder, withParameters: paarams as [String: AnyObject], ofType: CreateOrderAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: HomeVC.self){
                    self.navigationController?.popToViewController(vc, animated: false)
                }
            }
            CommonFunctions.hideLoader()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func getCounpDisscount(){
        let paarams : [String : AnyObject] = [
            "coupon" : tfCoupon.text as AnyObject,
            "store_id" : self.storeId as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getCoupn, withParameters: paarams as [String: AnyObject], ofType: GetCoupnDetailAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster("Coupon applied sucessfully")
            CommonFunctions.hideLoader()
            self.vwCoupnAndDisscount.isHidden = false
            self.counpDetail = profileAPI
            self.setDataOfButtonView(addedProductData)
            self.GetProfileApiHit()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
//            CommonFunctions.hideLoader()
            print(profileAPI)
            let amountbalance = CommonFunctions.decimalPlaceFromDouble(profileAPI.walletBalance ?? 0, decimalPlace: 2)
            self.amountInWallet.text = "$\(amountbalance)"
            if (profileAPI.walletBalance ?? 0.0) >= ((self.lblAllTotalCaluclatedPrice.text)?.replacingOccurrences(of: "$", with: "") as NSString?)?.doubleValue ?? 0.0{
                self.btnAddMponey.isHidden = true
            }else{
                self.btnAddMponey.isHidden = false
            }
        }, failure: { (false, string) in
//            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
