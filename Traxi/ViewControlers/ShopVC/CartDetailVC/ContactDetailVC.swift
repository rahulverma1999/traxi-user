//
//  ContactDetailVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 10/12/20.
//

import UIKit
import FlagPhoneNumber

protocol ContactDetailVCDelegate : class {
    func sendUserDetail(_ userData : UserDataStruct)
}

class ContactDetailVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tfName: UITextFieldWxtension!
    @IBOutlet weak var tfPhoneNumber: FPNTextField!
    @IBOutlet weak var btnConfirm: UIButton!
    
    //MARK:- Variables
    var selectedValue = "+61"
    var countryCode = "+61"
    weak var delegate : ContactDetailVCDelegate?
    var userData : UserDataStruct?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActConfirm(_ sender: Any) {
        if tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            CommonFunctions.toster("Please enter name.")
            tfName.shake()
        }else if tfPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            CommonFunctions.toster("Please enter phone number.")
            tfPhoneNumber.shake()
        }else{
            self.navigationController?.popViewController(animated: true)
            let appendData = UserDataStruct.init(name: tfName.text, countryCode: countryCode, phoneNumber: tfPhoneNumber.text)
            self.delegate?.sendUserDetail(appendData)
        }
    }
}

extension ContactDetailVC{
    //MARK:- setUpUI
    func setUpUI(){
        //MARK:- Implement country code in textfield
        tfName.text = userData?.name
        tfPhoneNumber.text = userData?.phoneNumber
        countryCode = userData?.countryCode ?? ""
        
        tfPhoneNumber.delegate = self
        tfPhoneNumber.displayMode = .list
        self.tfPhoneNumber.setFlag(countryCode: .AU)
        self.tfPhoneNumber.setFlag(countryNumCode: userData?.countryCode ?? "")
        tfPhoneNumber.flagButton.isUserInteractionEnabled = true
        tfPhoneNumber.flagButton.addTarget(self, action: #selector(btnActCountryCode(_:)), for: .touchUpInside)
    }
}

//MARK:- Objective C methods
extension ContactDetailVC{
    @objc func btnActCountryCode(_ sender: UIButton){
        self.fpnDisplayCountryList()
    }
}


//MARK:- Delegate method of country code flag
extension ContactDetailVC: FPNTextFieldDelegate{
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
         print(name, dialCode, code) // Output "France", "+33", "FR"
        countryCode = dialCode
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
    }
    
    func fpnDisplayCountryList() {
        print("sad")
        
        let listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
        tfPhoneNumber.displayMode = .list // .picker by default
        
        listController.setup(repository: tfPhoneNumber.countryRepository)
        listController.title = "Countries"
        

        let navigationViewController = UINavigationController(rootViewController: listController)
                
        self.present(navigationViewController, animated: true, completion: nil)
        
        
        listController.didSelect = {[weak self] country in
            self?.tfPhoneNumber.setFlag(countryCode: country.code)
            self?.selectedValue = country.phoneCode
        }
    }
}
