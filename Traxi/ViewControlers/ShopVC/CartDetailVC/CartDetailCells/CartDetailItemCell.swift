//
//  CartDetailItemCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 10/12/20.
//

import UIKit

protocol CartDetailItemCellDelegate : class {
    func getAddOrMinusHit(hit : Int, count : Int, minusCall : Bool) // hit: 1-Add, 2-Minus
}

class CartDetailItemCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var vwMinus: UIView!
    @IBOutlet weak var vwCount: UIView!
    @IBOutlet weak var btnMinus: UIButton!
    
    //MARK:- variables
    var itemCount = 1
    var delegate : CartDetailItemCellDelegate?
    var totalCount = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgFood.layer.cornerRadius = 6.0
        vwMinus.isHidden = true
        vwCount.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
