//
//  RateToShopVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 10/12/20.
//

import UIKit
import Cosmos
import GrowingTextView

protocol RateToShopVCDelegate : class {
    func getSunccessRating()
}

class RateToShopVC: UIViewController {
    
    //MARK:- outlets
    @IBOutlet weak var btnSubmit: LoadingButton!
    @IBOutlet weak var cosmoasView: CosmosView!
    @IBOutlet weak var tvDes: GrowingTextView!
    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblReviewCOunt: UILabel!
    @IBOutlet weak var lblStoreLocation: UILabel!
    
    //MARK:- Variables
    var orderDetailData : GetOrderAPI?
    weak var delegate : RateToShopVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    //MARK:- Button Actions
    @IBAction func btnActSubmit(_ sender: Any) {
        if cosmoasView.rating == 0{
            CommonFunctions.toster("Please give rating")
            cosmoasView.shake()
        }else if tvDes.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            CommonFunctions.toster("Please enter feedback")
            tvDes.shake()
        }else{
            self.GiveRatingApi()
        }
    }
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- setUp viewDidLoadData
extension RateToShopVC{
    func setUpUI(){
        imgShop.layer.cornerRadius = 6
        tvDes.layer.cornerRadius = 4
        tvDes.layer.borderWidth = 1
        tvDes.layer.borderColor = Colors.borderYellowColorTraxi.color().cgColor
        tvDes.textContainerInset = UIEdgeInsets(top: 8,left: 12,bottom: 8,right: 12)
        
        self.imgShop.yy_setImage(with: URL(string: CommonFunctions.getImage(orderDetailData?.image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
        self.lblShopName.text = orderDetailData?.storeName ?? ""
        self.lblStoreLocation.text = orderDetailData?.storeAddress ?? ""
        self.lblRating.text = "\(orderDetailData?.storeRatings ?? 0.0)"
        self.lblReviewCOunt.text = "(\(orderDetailData?.reviewsCount ?? 0) reviews)"
    }
    
    
    func GiveRatingApi(){
        let paarams : [String : AnyObject] = [
            "order_id" : self.orderDetailData?.orderID as AnyObject,
            "user_ratings" : self.cosmoasView.rating as AnyObject,
            "feedback" : tvDes.text as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.rating, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            print(profileAPI)
            self.delegate?.getSunccessRating()
            self.navigationController?.popViewController(animated: true)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PUT, img: nil, imageParamater: "", headerPresent: true)
    }
}
