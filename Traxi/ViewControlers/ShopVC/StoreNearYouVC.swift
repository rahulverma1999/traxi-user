//
//  StoreNearYouVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/12/20.
//

import UIKit
import ESPullToRefresh

class StoreNearYouVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnTopCurrentLocation: UIButton!
    @IBOutlet weak var lblStoreCount: UILabel!
    
    //MARK:- Variables
    var storesNearYouData : StoreNearYouAPI?
    var catId : Int!
    var canPaginate : Bool = true
    var isApiHit = false
    
    //MARK:- Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if catId != nil{
            self.GetStoreNearApiHit(false)
        }
        
        tableView.alwaysBounceVertical = true
        tableView.es.addPullToRefresh {
            self.canPaginate = true
            self.GetStoreNearApiHit(false)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addedProductData.removeAll()
        if globalAddedLocation == nil{
//            self.setDataViewWillAppear()
        }else{
            self.btnTopCurrentLocation.setTitle(globalAddedLocation?.fullAddress, for: .normal)
            EditProfileApiHit()
        }
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActSelectTopCurrentLocation(_ sender: Any) {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .StoreNearYou
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK:- UItable view delegate methods
extension StoreNearYouVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storesNearYouData?.stores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        vc.storeId = self.storesNearYouData?.stores?[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreNearYouCell", for: indexPath) as! StoreNearYouCell
        cell.lblTitle.text = self.storesNearYouData?.stores?[indexPath.row].name ?? ""
        cell.lbldes.text = self.storesNearYouData?.stores?[indexPath.row].categories ?? ""
        cell.lblLocation.text = self.storesNearYouData?.stores?[indexPath.row].address ?? ""
        cell.lblrating.text = "\(self.storesNearYouData?.stores?[indexPath.row].ratings ?? 0.0)"
        cell.imgView.yy_setImage(with: URL(string: CommonFunctions.getImage(self.storesNearYouData?.stores?[indexPath.row].image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
        cell.lbldes.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (self.storesNearYouData?.stores?.count ?? 0) - 3 && self.canPaginate{
            self.GetStoreNearApiHit(true)
        }
    }
}

extension StoreNearYouVC{
    //MARK:- Hit Api
    func GetStoreNearApiHit(_ pagination: Bool){
        
        var page = 1
        if pagination{
            page+=1
        }
        
        let paarams : [String : AnyObject] = [
            "category_id"  : catId as AnyObject,
            "limit" : 10 as AnyObject,
            "page_id" : page as AnyObject,
            "current_time" : CommonFunctions.getDateFormatTraxi3(Date().description) as AnyObject
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.storeNearYou, withParameters: paarams as [String: AnyObject], ofType: StoreNearYouAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            if profileAPI.stores?.count != 10{
                self.canPaginate = false
            }
//            if self.storesNearYouData.count == profileAPI.count{
//                self.canPaginate = false
//            }
            if pagination{
                self.storesNearYouData?.stores?.append(contentsOf: profileAPI.stores ?? [])
            }else{
                self.storesNearYouData = profileAPI
            }
            if (profileAPI.stores?.count ?? 0) > 1{
                self.lblStoreCount.text = "\(profileAPI.storeCount ?? 0) stores"
            }else if profileAPI.stores?.count == 1{
                self.lblStoreCount.text = "\(profileAPI.storeCount ?? 0) store"
            }else{
                self.lblStoreCount.text = "No store found"
            }
            self.tableView.es.stopPullToRefresh()
            self.tableView.reloadData()
            print(profileAPI)
        }, failure: { (false, string) in
            self.tableView.es.stopPullToRefresh()
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
