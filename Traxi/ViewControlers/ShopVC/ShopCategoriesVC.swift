//
//  ShopCategoriesVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/12/20.
//

import UIKit

class ShopCategoriesVC: baseVC {

    //MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBookDelivery: UIButton!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnTopCurrentLocation: UIButton!
    
    //MARK:- variables
    var categoriesData : [GetShopCategory] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViewDidLoadData()
        self.GetShopCategoriesApiHit()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if globalAddedLocation == nil{
        }else{
            self.btnTopCurrentLocation.setTitle(globalAddedLocation?.fullAddress, for: .normal)
            EditProfileApiHit()
        }
    }
    
    //MARK:- Button Action
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActBookDelivery(_ sender: Any) {
        let vc = DeliveryTypeVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActSelectTopCurrentLocation(_ sender: Any) {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .shopeByCat
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ShopCategoriesVC{
    //MARK:- Set Collection view layout
    fileprivate func settingUPTheLayout() -> UICollectionViewFlowLayout {
        let width = UIScreen.main.bounds.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: (width-30) / 2, height: width/1.6-10)//self.collectionView.bounds.height/2)
        return layout
    }
    
    //MARK:- SetViewDidLoadData
    func setViewDidLoadData(){
        collectionView.collectionViewLayout = self.settingUPTheLayout()
        collectionView.reloadData()
        //        DispatchQueue.main.async {
        //            self.collectionView.layoutIfNeeded()
        //            let contentSize = self.collectionView.contentSize.height
        //            self.collectionViewHeight.constant = contentSize
        //        }
    }
}

//MARK:- CollectionView Delegates
extension ShopCategoriesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.categoriesData.count
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = StoreNearYouVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        vc.catId = self.categoriesData[indexPath.row].id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopCategoriesCell", for: indexPath) as! ShopCategoriesCell
        cell.lblTitle.text = self.categoriesData[indexPath.row].name ?? ""
        cell.lblDes.text = "\(self.categoriesData[indexPath.row].storeCounts ?? 0) store near by you"
        cell.bgView.backgroundColor = CommonFunctions.hexStringToUIColor(hex: self.categoriesData[indexPath.row].bgColor ?? "")
        cell.imgView.yy_setImage(with: URL(string: CommonFunctions.getImage(self.categoriesData[indexPath.row].image ?? "", quality: .large)), placeholder: UIImage(named: "categroies_placeholder"))
        return cell
    }
}

extension ShopCategoriesVC{
    //MARK:- Hit Api
    func GetShopCategoriesApiHit(){
        let paarams : [String : String] = [
            :
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.shopCategories, withParameters: paarams as [String: AnyObject], ofType: GetShopCategories.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.categoriesData = profileAPI
            self.collectionView.reloadData()
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
