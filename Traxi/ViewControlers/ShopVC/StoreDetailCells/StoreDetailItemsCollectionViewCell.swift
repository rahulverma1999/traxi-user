//
//  StoreDetailItemsCollectionViewCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 14/12/20.
//

import UIKit

struct StructProductsAddedData{
    let name : String!
    let description :String!
    let price : Double!
    let timeToMake : Int!
    let itemCount : Int!
    let imageOfProduct : String!
    let productId : Int!
    let catProductId : Int!
}

protocol StoreDetailItemsCollectionViewCellDelegate : class{
    func getPlusMinus(bottomViewHide : Bool, totalcount: [StructProductsAddedData])
    func sendTableViewHeight(_ height : CGFloat,_ path : Int)
    func getMinusDetect()
}

class StoreDetailItemsCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    //MARK:- Variables
    var totalCount = 0
    weak var delegate : StoreDetailItemsCollectionViewCellDelegate?
    var productsAllData : [[Product]] = []
    var catId : Int!
    var itemsAddedCount = 0
    var pricePerUnit = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        tableView.contentInset.bottom = 100
        let rowHeight = 80
        let heightDependentSpace = rowHeight
        let customViewFooter = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.bounds.width), height: heightDependentSpace))
        customViewFooter.backgroundColor = .clear
        tableView.tableFooterView = customViewFooter
        tableView.reloadData()
    }
    
    func makeChkThings(_ dataIndex : Int){
        tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.delegate?.sendTableViewHeight(self.tableView.contentSize.height, dataIndex)
        }
    }
    
}

extension StoreDetailItemsCollectionViewCell: UITableViewDelegate, UITableViewDataSource{
    //MARK:- UItable view delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if catId != nil{
            if productsAllData[catId].count == 0{
                return 1
            }else{
                return productsAllData[catId].count
            }
        }else{
            return 0 
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if productsAllData[catId].count == 0{
            return 200
        }else{
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if productsAllData[catId].count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataFoundStoreDetailCell", for: indexPath) as! NoDataFoundStoreDetailCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodItemsTableViewCell", for: indexPath) as! FoodItemsTableViewCell
            cell.btnAdd.tag = indexPath.row
            cell.btnMinus.tag = indexPath.row
            cell.delegate = self
            cell.lblName.text = productsAllData[catId][indexPath.row].name ?? ""
            cell.lblDes.text  = productsAllData[catId][indexPath.row].productDescription ?? "N/A"
            cell.lblPrice.text = "$\(productsAllData[catId][indexPath.row].unitPrice ?? 0)"
            cell.btnTime.setTitle("\(productsAllData[catId][indexPath.row].preprationTime ?? 0) MINS", for: .normal)
            cell.imgFood.yy_setImage(with: URL(string: CommonFunctions.getImage(productsAllData[catId][indexPath.row].image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
            cell.btnAdd.addTarget(self, action: #selector(btnActAddHit(_:)), for: .touchUpInside)
            cell.btnMinus.addTarget(self, action: #selector(btnActMinusHit(_:)), for: .touchUpInside)
            var idPresent = false
            print("Strut array count:",addedProductData.count)
            for dt in addedProductData{
                if dt.productId == productsAllData[catId][indexPath.row].id {
                    idPresent = true
                    if dt.itemCount > 0{
                        cell.vwMinus.isHidden = false
                        cell.vwCount.isHidden = false
                        cell.lblCount.text = "\(dt.itemCount ?? 0)"
                    }else{
                        cell.vwMinus.isHidden = true
                        cell.vwCount.isHidden = true
//                        self.delegate?.getPlusMinus(bottomViewHide: true, totalcount: addedProductData)
                    }
                }
            }
            if !idPresent{
                cell.vwMinus.isHidden = true
                cell.vwCount.isHidden = true
            }
            self.delegate?.getPlusMinus(bottomViewHide: false, totalcount: addedProductData)
            return cell
        }
    }
}



extension StoreDetailItemsCollectionViewCell{
    @objc func btnActAddHit(_ sender : UIButton){
        self.itemsAddedCount = 0
        self.pricePerUnit = 0.0
        var countPresnet = false
        for dt in addedProductData{
            if dt.productId == productsAllData[catId][sender.tag].id ?? 0{
                self.itemsAddedCount = dt.itemCount+1
                self.pricePerUnit = (productsAllData[catId][sender.tag].unitPrice ?? 0.0) * Double(self.itemsAddedCount)
                countPresnet = true
            }
        }
        if !countPresnet{
            self.itemsAddedCount += 1
            self.pricePerUnit = productsAllData[catId][sender.tag].unitPrice ?? 0
        }
        let addedData = StructProductsAddedData.init(name: productsAllData[catId][sender.tag].name ?? "", description: productsAllData[catId][sender.tag].productDescription ?? "", price: self.pricePerUnit, timeToMake: productsAllData[catId][sender.tag].preprationTime ?? 0, itemCount: itemsAddedCount, imageOfProduct: productsAllData[catId][sender.tag].image ?? "", productId: productsAllData[catId][sender.tag].id ?? 0, catProductId: productsAllData[catId][sender.tag].categoryID ?? 0)
        var changed = false
        if addedProductData.count > 0{
            for (num, data) in addedProductData.enumerated(){
                if (data.catProductId == addedData.catProductId) && (data.productId == addedData.productId){
                    changed = true
                    addedProductData[num] = addedData
                }
            }
        }
        if !changed{
            addedProductData.append(addedData)
        }
        setMinusAndCountHideUnHide()
    }
    
    @objc func btnActMinusHit(_ sender : UIButton){
//        self.itemsAddedCount-=1
        var countPresnet = false
        for dt in addedProductData{
            if dt.productId == productsAllData[catId][sender.tag].id ?? 0{
                self.itemsAddedCount = dt.itemCount-1
                self.pricePerUnit = (dt.price) - (productsAllData[catId][sender.tag].unitPrice ?? 0.0)
                countPresnet = true
            }
        }
        if !countPresnet{
            self.itemsAddedCount -= 1
            self.pricePerUnit -= productsAllData[catId][sender.tag].unitPrice ?? 0.0
        }
        
        let removeData = StructProductsAddedData.init(name: productsAllData[catId][sender.tag].name ?? "", description: productsAllData[catId][sender.tag].productDescription ?? "", price: self.pricePerUnit, timeToMake: productsAllData[catId][sender.tag].preprationTime ?? 0, itemCount: itemsAddedCount, imageOfProduct: productsAllData[catId][sender.tag].image ?? "", productId: productsAllData[catId][sender.tag].id ?? 0, catProductId: productsAllData[catId][sender.tag].categoryID ?? 0)
//        var changes = false
        for (num, data) in addedProductData.enumerated(){
            if data.catProductId == productsAllData[catId][sender.tag].categoryID ?? 0 && data.productId == productsAllData[catId][sender.tag].id ?? 0{
                if self.itemsAddedCount > 0{
                    addedProductData[num] = removeData
                }else{
                    addedProductData.remove(at: num)
                }
            }
        }
        setMinusAndCountHideUnHide()
        self.delegate?.getMinusDetect()
    }
    
    func setMinusAndCountHideUnHide(){
        tableView.reloadData()
    }
}

//MARK:- Delegate methods
extension StoreDetailItemsCollectionViewCell: FoodItemsTableViewCellDelegate{
    func getAddOrMinusHit(hit: Int, count: Int, minusCall: Bool) {
//        if count == 0{
//            if minusCall{
//                totalCount-=1
//            }
//        }else{
//            if minusCall{
//                totalCount-=1
//            }else{
//                totalCount+=1
//            }
//        }
//        print(totalCount)
//        if totalCount > 0{
//            self.delegate?.getPlusMinus(bottomViewHide: false, totalcount: totalCount)
//        }else{
//            self.delegate?.getPlusMinus(bottomViewHide: true, totalcount: totalCount)
//        }
    }
}

