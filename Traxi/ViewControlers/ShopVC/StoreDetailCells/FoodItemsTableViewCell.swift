//
//  FoodItemsTableViewCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 09/12/20.
//

import UIKit

protocol FoodItemsTableViewCellDelegate : class {
    func getAddOrMinusHit(hit : Int, count : Int, minusCall : Bool) // hit: 1-Add, 2-Minus
}

class FoodItemsTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var vwMinus: UIView!
    @IBOutlet weak var vwCount: UIView!
    
    //MARK:- variables
    var itemCount = 0
    var delegate : FoodItemsTableViewCellDelegate?
    var totalCount = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgFood.layer.cornerRadius = 6.0
        btnTime.layer.cornerRadius = 2.0
        vwMinus.isHidden = true
        vwCount.isHidden = true
        // Initialization code
//        btnAdd.addTarget(self, action: #selector(btnActAddHit(_:)), for: .touchUpInside)
//        btnMinus.addTarget(self, action: #selector(btnActMinusHit(_:)), for: .touchUpInside)
//        lblCount.text = "\(itemCount)"
        setMinusAndCountHideUnHide()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//MARK:- Objective C methods
extension FoodItemsTableViewCell{
    @objc func btnActAddHit(_ sender : UIButton){
        itemCount+=1
        lblCount.text = "\(itemCount)"
        setMinusAndCountHideUnHide()
        self.delegate?.getAddOrMinusHit(hit: 1, count: itemCount, minusCall: false)
    }
    
    @objc func btnActMinusHit(_ sender : UIButton){
        if itemCount == 0{
        }else{
            itemCount-=1
            lblCount.text = "\(itemCount)"
        }
        self.delegate?.getAddOrMinusHit(hit: 1, count: itemCount, minusCall: true)
        setMinusAndCountHideUnHide()
    }
    
    func setMinusAndCountHideUnHide(){
        if itemCount == 0{
            vwMinus.isHidden = true
            vwCount.isHidden = true
        }else{
            vwMinus.isHidden = false
            vwCount.isHidden = false
        }
    }
}
