//
//  FoodTypeTopCollectionViewCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 09/12/20.
//

import UIKit

class FoodTypeTopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var vwButtomCooloe: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        vwButtomCooloe.isHidden =  true
    }
    
    override var isHighlighted: Bool {
      didSet{
        lblText.textColor = isHighlighted ? UIColor.white : UIColor.white
        lblText.alpha = isHighlighted ? 1.0 : 0.8
        lblText.font = isHighlighted ? UIFont.NunitoSemiBold(14) : UIFont.NunitoRegular(14)
        vwButtomCooloe.backgroundColor = isHighlighted ? UIColor.white : UIColor.clear
        vwButtomCooloe.isHidden = isSelected ? false : true
      }
    }
     
    override var isSelected: Bool {
      didSet{
        lblText.textColor = isSelected ? UIColor.white : UIColor.white
        lblText.alpha = isSelected ? 1.0 : 0.8
        lblText.font = isSelected ? UIFont.NunitoSemiBold(14) : UIFont.NunitoRegular(14)
        vwButtomCooloe.backgroundColor = isSelected ? UIColor.white : UIColor.clear
        vwButtomCooloe.isHidden = isSelected ? false : true
      }
    }
    
}
