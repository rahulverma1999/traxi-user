//
//  StoreNearYouCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/12/20.
//

import UIKit

class StoreNearYouCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblrating: UILabel!
    @IBOutlet weak var lbldes: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
