//
//  StoreDetailVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/12/20.
//

import UIKit
import GooglePlaces
import GoogleMaps

class StoreDetailVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgStore: UIImageView!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var vwBottomShowSelectedItems: GradientView!
    @IBOutlet weak var buttonViewCart: UIButton!
    @IBOutlet weak var bottomConstantScroolView: NSLayoutConstraint!
    @IBOutlet weak var collectionViewTop: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomContraintOfBottomCartView: NSLayoutConstraint!
    @IBOutlet weak var lblItemCount: UILabel!
    @IBOutlet weak var collectionViewDetail: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblShopRating: UILabel!
    @IBOutlet weak var lblShopReviewsa: UILabel!
    @IBOutlet weak var lblShopLocation: UILabel!
    @IBOutlet weak var lblUserTagTypeAddress: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var lblPreparingTime: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    //MARK:- Variables
    var itemsSelected : [String] = []
    var totalCount = 0
    var height : [CGFloat] = []
    var storeId : Int!
    var storeDetailData : StoreDetailAPI?
    var productsAllData : [[Product]] = []
    var catId : Int!
    var deliveryTimeCalaucted : Int!
    
    var startCoordinates : CLLocationCoordinate2D!
    var endCoordinates : CLLocationCoordinate2D!
    var distanceDriverFromShopToHome = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if storeId != nil{
            self.GetStoreDetailApiHit()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setDataViewWillAppear()
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        if addedProductData.count > 0{
            let popOverVC = CancelOrderPopUpVC()
            self.addChild(popOverVC)
            popOverVC.sType = .cancelCart
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParent: self)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnActChange(_ sender: Any) {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .storeDetail
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActViewCart(_ sender: Any) {
        let vc = CartDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        vc.deliveryTimeCalaucted = deliveryTimeCalaucted
        vc.distanceDriverFromShopToHome = self.distanceDriverFromShopToHome
        vc.storeId = storeId
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension StoreDetailVC{
    //MARK:- SetUp viewDidLoadData
    func setViewDidLoadData(){
        self.imgStore.layer.cornerRadius = 6.0
        if (storeDetailData?.categories?.count ?? 0) > 0{
            self.GetStoreCategoriesProductsApiHit(0)
            collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: [.centeredHorizontally])
            collectionViewTop.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: [.centeredHorizontally])
            self.catId = 0//storeDetailData?.categories?[0].id ?? 0
        }
        collectionViewTop.isHidden = true
        collectionView.tag = 1
        collectionViewTop.tag = 2
        collectionViewDetail.tag = 3
        height = []
        for _ in storeDetailData?.categories ?? []{
            height.append(660)
        }
        self.lblPreparingTime.isHidden = false
        collectionViewDetail.reloadData()
    }
    
    func setDataViewWillAppear(){
        if globalAddedLocation == nil{
        }else{
            self.startCoordinates = CLLocationCoordinate2D(latitude: globalAddedLocation?.lat ?? 0.0, longitude: globalAddedLocation?.lng ?? 0.0)
            self.lblUserAddress.text = globalAddedLocation?.fullAddress ?? ""
            if locationTagType == 1{
                self.lblUserTagTypeAddress.text = "Delivery to Home"
            }else if locationTagType == 2{
                self.lblUserTagTypeAddress.text = "Delivery to Work"
            }else{
                self.lblUserTagTypeAddress.text = "Delivery to Others"
            }
            EditProfileApiHit()
        }
        self.setDataOfButtonView(addedProductData, isViewWillAppearCalls: true)
        if addedProductData.count == 0{
            self.lblPreparingTime.isHidden = false
        }else{
            self.lblPreparingTime.isHidden = false
        }
    }
    
    func setStoreDataApi(_ data : StoreDetailAPI){
        self.lblShopName.text = data.store?.storeName ?? ""
        if data.store?.reviewsCount == 0{
            self.lblShopReviewsa.text = "No review"
        }else if data.store?.reviewsCount == 1{
            self.lblShopReviewsa.text = "\(data.store?.reviewsCount ?? 0) review"
        }else{
            self.lblShopReviewsa.text = "\(data.store?.reviewsCount ?? 0) reviews"
        }
        self.lblShopRating.text = "\(data.store?.storeRatings ?? 0.0)"
        self.lblShopLocation.text = data.store?.storeAddress ?? ""
        self.imgStore.yy_setImage(with: URL(string: CommonFunctions.getImage(data.store?.image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
    }
    
    
    func setDataOfButtonView(_ data : [StructProductsAddedData], isViewWillAppearCalls : Bool){
        var tempCount = 0
        var totalPrice = 0.0
        for count in data{
            tempCount += count.itemCount
            totalPrice += count.price
        }
        self.totalCount = tempCount
        if self.totalCount > 0{
            vwBottomShowSelectedItems.isHidden = false
            bottomContraintOfBottomCartView.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.lblTotalPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble(totalPrice, decimalPlace: 2))"
        }else{
            vwBottomShowSelectedItems.isHidden = false
            bottomContraintOfBottomCartView.constant = -90
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        if self.totalCount > 1{
            lblItemCount.text = "\(self.totalCount) ITEMS"
        }else{
            lblItemCount.text = "\(self.totalCount) ITEM"
        }
        if isViewWillAppearCalls{
            collectionViewDetail.reloadData()
        }
    }
    
    func getDeliveryTime(){
        CommonFunctions.getPolylineRouteTimeInSecs(from: startCoordinates, to: endCoordinates, isLoader: true) { [self] (status, polyline,time,distance) in
            print("Status, pol, time, distance",status, polyline,time,distance)
            if !(polyline.isEmpty){
            }
            self.deliveryTimeCalaucted = time
            let tempCheck = (distance.components(separatedBy: " ").first as NSString?)?.replacingOccurrences(of: ",", with: "")
            let tempdistanceInKm = (tempCheck as NSString?)?.doubleValue
            
            self.distanceDriverFromShopToHome = CommonFunctions.getPriceAcordingToDistance(tempdistanceInKm ?? 0.0)
            if addedProductData.count == 0{
                self.lblPreparingTime.text = "Delivered in \("\(CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: Int(self.deliveryTimeCalaucted)))")"
            }
        }
    }
    
    func getPrepareTime(){
        var calculatedMaxTimeForPrePration : [Int] = []
        for time in addedProductData{
            calculatedMaxTimeForPrePration.append(time.timeToMake ?? 0)
        }
        let sortedData = calculatedMaxTimeForPrePration.sorted(by: { $0.self > $1.self})
        let intTime = CommonFunctions.decimalPlaceFromDouble((Double((sortedData.first ?? 0)*60)), decimalPlace: 0)
        let totalTime = (self.deliveryTimeCalaucted) + ((intTime as NSString).integerValue)
        let actualTime = "\(CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: Int(totalTime)))"
        if addedProductData.count == 0{
            self.lblPreparingTime.text = "Delivered in \("\(CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: Int(self.deliveryTimeCalaucted)))")"
        }else{
            self.lblPreparingTime.text = "Prepared & Delivered in \(actualTime)"
        }
        
    }
}

//MARK:- CollectionView Delegate methods
extension StoreDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if collectionView == self.collectionViewDetail{
            return storeDetailData?.categories?.count ?? 0
        }else{
            return storeDetailData?.categories?.count ?? 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionViewDetail{
            return CGSize(width: self.collectionViewDetail.frame.width , height: self.height[indexPath.row])
        }else{
            return CGSize(width: UIScreen.main.bounds.width/4.7, height: 38)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK:- Scroll View Method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.tag != 1 && scrollView.tag != 2 && scrollView.tag != 3{
            self.scrollView = scrollView
            if (scrollView.contentOffset.y > 240){
                collectionViewTop.isHidden = false
            }else{
                collectionViewTop.isHidden = true
            }
        }else{
            
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView.tag == 3{
            let index = targetContentOffset.move().x / view.frame.width
            let indexPath = NSIndexPath(item: Int(index), section: 0)
            collectionView.selectItem(at: indexPath as IndexPath, animated: true, scrollPosition: [ .centeredHorizontally])
            collectionViewTop.selectItem(at: indexPath as IndexPath, animated: true, scrollPosition: [ .centeredHorizontally])
            self.GetStoreCategoriesProductsApiHit(indexPath.row)
            self.catId = indexPath.row
        }
       }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionViewDetail{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreDetailItemsCollectionViewCell", for: indexPath) as! StoreDetailItemsCollectionViewCell
            cell.delegate = self
            cell.productsAllData = self.productsAllData
            cell.catId = self.catId
            if let kData = self.storeDetailData?.categories{
                for (ind, _) in (kData.enumerated()){
                    if indexPath.row == ind{
                        cell.makeChkThings(indexPath.row)
                    }
                }
            }
            return cell
        }else if collectionView == self.collectionViewTop{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodTypeTopCollectionViewCell", for: indexPath) as! FoodTypeTopCollectionViewCell
            cell.lblText.text = storeDetailData?.categories?[indexPath.row].categoryName ?? ""
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodTypeCollectionViewCell", for: indexPath) as! FoodTypeCollectionViewCell
            cell.lblText.text = storeDetailData?.categories?[indexPath.row].categoryName ?? ""//titleHeader[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.scrollToMenuIndex(indexPath.row)
        if collectionView == self.collectionView{
            collectionViewTop.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
            self.scrollToMenuIndex(indexPath.row)
            self.GetStoreCategoriesProductsApiHit(indexPath.row)
            self.catId = indexPath.row
        }else{
            self.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
            self.scrollToMenuIndex(indexPath.row)
            self.GetStoreCategoriesProductsApiHit(indexPath.row)
            self.catId = indexPath.row
        }
    }
        
    fileprivate func scrollToMenuIndex(_ menuIndex:Int) {
        let indexPath = NSIndexPath(item: menuIndex, section: 0)
        self.collectionViewDetail.selectItem(at: indexPath as IndexPath, animated: true, scrollPosition: [ .centeredHorizontally])
    }

}



//MARK:- Objective C methods
extension StoreDetailVC{
    @objc func btnActAddItems(_ sender : UIButton){
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
        }
        vwBottomShowSelectedItems.isHidden = false
    }
    
    @objc func btnActRemoveItems(_ sender : UIButton){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.vwBottomShowSelectedItems.isHidden = true
        }
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK:- Delegate Method
extension StoreDetailVC : StoreDetailItemsCollectionViewCellDelegate{
    
    func sendTableViewHeight(_ height: CGFloat, _ path: Int) {
//        print("height", height)
        if self.height[path] == height{
            return
        }
        self.height = []
        for _ in self.storeDetailData?.categories ?? []{
            if height < 200{
                self.height.append(200)
            }else{
                self.height.append(height)
            }
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+0.25, execute: {
            self.collectionViewDetail.reloadData()
            self.collectionViewDetail.layoutIfNeeded()
            var contentSize = CGFloat(0)
            if height < 200{
                contentSize = 200
            }else{
                contentSize = height
            }
            self.collectionViewHeight.constant = contentSize
        })
    }
    
    func getPlusMinus(bottomViewHide: Bool, totalcount: [StructProductsAddedData]) {
        self.setDataOfButtonView(totalcount, isViewWillAppearCalls: false)
        if addedProductData.count > 0{
            if deliveryTimeCalaucted != nil{
                self.getPrepareTime()
            }
        }
    }
    
    func getMinusDetect() {
        if addedProductData.count == 0{
            if addedProductData.count == 0{
                self.lblPreparingTime.text = "Delivered in \("\(CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: Int(self.deliveryTimeCalaucted)))")"
            }
        }else{
            self.lblPreparingTime.isHidden = false
        }
    }
}

extension StoreDetailVC{
    //MARK:- Hit Api
    func GetStoreDetailApiHit(){
        
        let paarams : [String : AnyObject] = [
            "store_id"  : storeId as AnyObject
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.storeDetails, withParameters: paarams as [String: AnyObject], ofType: StoreDetailAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.storeDetailData = profileAPI
            self.endCoordinates = CLLocationCoordinate2D(latitude: profileAPI.store?.lat ?? 0.0, longitude: profileAPI.store?.lng ?? 0.0)
            self.setStoreDataApi(profileAPI)
            print(profileAPI)
            self.productsAllData = []
            for _ in profileAPI.categories ?? []{
                self.productsAllData.append([])
            }
            self.collectionViewTop.reloadData()
            self.collectionView.reloadData()
            self.collectionViewDetail.reloadData()
            self.setViewDidLoadData()
            self.getDeliveryTime()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetStoreCategoriesProductsApiHit(_ cat1Id : Int){
        
        if self.productsAllData[cat1Id].count != 0{
            self.collectionViewDetail.reloadData()
            return
        }
        
        let paarams : [String : AnyObject] = [
            "store_category_id"  : (storeDetailData?.categories?[cat1Id].id ?? 0) as AnyObject
        ]
        
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.categoriesProducts, withParameters: paarams as [String: AnyObject], ofType: CategoriesProductsAPI.self, success2: { (profileAPI) in
//            CommonFunctions.hideLoader()
            self.productsAllData[cat1Id] = profileAPI.products ?? []
            self.collectionViewDetail.reloadData()
            print(profileAPI)
        }, failure: { (false, string) in
//            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
}
