//
//  NotificationVC.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 17/02/21.
//

import UIKit
import ESPullToRefresh
import ENSwiftSideMenu

class NotificationVC: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var vwnotificationCount: setCornerRadiusCircle!
    
    //MARK:- Vairables
    var canPaginate : Bool = true
    var isApiHit = false
    var notificationData : NotificationAPI?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwnotificationCount.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        GetProfileApiHit()
    }
    //MARK:- IBAction
    @IBAction func btnActBack(_ sender: Any) {
        toggleSideMenuView()
    }
}

extension NotificationVC{
    //MARK:- SetUpUI
    func setUpUI(){
        self.GetNotificaitonsApiHit(false)
        self.NotificationSeenApiHit()
        tableView.alwaysBounceVertical = true
        tableView.es.addPullToRefresh {
            self.canPaginate = true
            self.GetNotificaitonsApiHit(false)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            if self.navigationController != nil{
                self.sideMenuController()?.sideMenu?.delegate = self
                self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
                self.sideMenuController()?.sideMenu?.allowPanGesture = true
                self.sideMenuController()?.sideMenu?.allowRightSwipe = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
            }
        }
        
    }
}

//MARK:- UItable view delegate methods
extension NotificationVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (notificationData?.notifications?.count ?? 0) == 0{
            self.lblNoDataFound.isHidden = false
            return 0
        }else{
            self.lblNoDataFound.isHidden = true
            return (notificationData?.notifications?.count ?? 0)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        if notificationData?.notifications?[indexPath.row].isRead == 0{
            cell.unreadView.isHidden = false
            cell.imgView.layer.cornerRadius = (cell.imgView.bounds.height)/2
            cell.imgView.layer.borderWidth = 1
            cell.imgView.layer.borderColor = Colors.pinkColorTraxi.color().cgColor
            cell.imgView.clipsToBounds = true
        }else{
            cell.unreadView.isHidden = true
            cell.imgView.layer.borderWidth = 0
        }
        cell.lblTime.text = CommonFunctions.gettingDifferenceInDate(notificationData?.notifications?[indexPath.row].createdAt ?? "")
        cell.lblTitle.text = notificationData?.notifications?[indexPath.row].title ?? ""
        cell.lblSubTitle.text = notificationData?.notifications?[indexPath.row].message ?? ""
        if indexPath.row == (notificationData?.notifications?.count ?? 0) - 1{
            cell.vwLayer.isHidden = true
        }else{
            cell.vwLayer.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (self.notificationData?.notifications?.count ?? 0) - 3 && self.canPaginate{
            self.GetNotificaitonsApiHit(true)
        }
    }
}

// MARK: - DELEGATE FUNCTIONS
extension NotificationVC : ENSideMenuDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
            
            self.view.bringSubviewToFront(fullView)
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        toggleSideMenuView()
    }
}

extension NotificationVC{
    //MARK:- hit APi
    func GetNotificaitonsApiHit(_ pagination: Bool){
        var page = 1
        if pagination{
            //page+=1
            page += (self.notificationData?.notifications?.count ?? 0) / 10
        }
        
        let paarams : [String : AnyObject] = [
            "limit" : 10 as AnyObject,
            "page" : page as AnyObject
        ]
        if !isApiHit{
            CommonFunctions.showLoader()
        }
        ApiHandler.callApiWithParameters(url: APIConstants.notificationUrl, withParameters: paarams as [String: AnyObject], ofType: NotificationAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.isApiHit = true
            if profileAPI.notifications?.count != 10{
                self.canPaginate = false
            }
            if pagination{
                self.notificationData?.notifications?.append(contentsOf: profileAPI.notifications ?? [])
            }else{
                self.notificationData = profileAPI
            }
            self.tableView.es.stopPullToRefresh()
            self.tableView.reloadData()
        }, failure: { (false, string) in
            print(string)
            self.isApiHit = false
            CommonFunctions.hideLoader()
            self.tableView.es.stopPullToRefresh()
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func NotificationSeenApiHit(){
        let paarams : [String : AnyObject] = [
            "user_type": 1 as AnyObject
        ]
        
        ApiHandler.callApiWithParameters(url: APIConstants.notificationUrl, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            print(profileAPI)
            UIApplication.shared.applicationIconBadgeNumber = 0
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.PUT, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            if (profileAPI.unreadComplains ?? 0) > 0 || (profileAPI.unreadNotifications ?? 0) > 0{
                self.vwnotificationCount.isHidden = false
            }else{
                self.vwnotificationCount.isHidden = true
            }
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
}
