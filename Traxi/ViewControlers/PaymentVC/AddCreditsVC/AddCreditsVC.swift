//
//  AddCreditsVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 15/12/20.
//

import UIKit
import Stripe
import ESPullToRefresh

class AddCreditsVC: baseVC {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblAmountAdded: UILabel!
    @IBOutlet weak var vwNoDataFound: UIView!
    
    //MARK:- variables
    var addedAmount = ""
    var cardsData : GetSavedCardAPI?
    var cardDataFromCoreData = coredata.shared.cardNumber
    var cardIdFromCoreData = coredata.shared.cardIds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        GetSavedCardApi()
        tableView.es.addPullToRefresh {
            self.GetSavedCardApi()
        }
        lblAmountAdded.text = "$ \(addedAmount)"
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActAddCard(_ sender: Any) {
        let vc = AddNewCardVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
        vc.sType = .AddCredits
        vc.addedAmount = self.addedAmount
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension AddCreditsVC{
    //MARK:- setUpUi
    func setUpUI(){
        
    }
}

//MARK:- UItable view delegate methods
extension AddCreditsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (cardDataFromCoreData.count) == 0{
            self.vwNoDataFound.isHidden = false
            return 0
        }else{
            self.vwNoDataFound.isHidden = true
            return (cardDataFromCoreData.count)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
        let alert = UIAlertController(title: "Pay $\(addedAmount)", message: "Do you want to pay using \(self.cardsData?.data?[indexPath.row].last4 ?? "") card?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Pay", style: .default, handler: { (_) in
            self.getPaymentToken(self.cardIdFromCoreData[indexPath.row])
        }))
        alert.addAction(UIAlertAction(title: "Discard", style: .cancel, handler: { (_) in }))
        CommonFunctions.getTopMostViewController()?.present(alert, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddCreditsCell", for: indexPath) as! AddCreditsCell
        cell.lblCardNumber.text = "XXXX - XXXX - XXXX - \(self.cardDataFromCoreData[indexPath.row])"
        return cell
    }
}

//MARK:- Hit Api
extension AddCreditsVC{
    func GetSavedCardApi(){
        let paarams : [String : String] = [
            :
        ]
        ApiHandler.callApiWithParameters(url: APIConstants.getSavedCards, withParameters: paarams as [String: AnyObject], ofType: GetSavedCardAPI.self, success2: { (profileAPI) in
            self.cardsData = profileAPI
            print(profileAPI)
            self.tableView.reloadData()
            self.tableView.es.stopPullToRefresh()
            
            self.tableView.layoutIfNeeded()
            let contentSize = self.tableView.contentSize.height
            self.tableViewHeight.constant = contentSize
        }, failure: { (false, string) in
            self.tableView.es.stopPullToRefresh()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func getPaymentToken(_ cardId : String) {
        let paarams : [String: AnyObject] = [
            "card_id": cardId as AnyObject,
            "amount" : addedAmount as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.addCard, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            CommonFunctions.hideLoader()
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: TraxiCreditsVC.self){
                    self.navigationController?.popToViewController(vc, animated: false)
                }
            }
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }

}
