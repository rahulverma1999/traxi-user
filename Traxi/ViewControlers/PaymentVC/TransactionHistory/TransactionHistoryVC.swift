//
//  TransactionHistoryVC.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 16/02/21.
//

import UIKit

class TransactionHistoryVC: baseVC {
    
    //MARK:- OutLets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    //MARK:- variables
    var TransactionHistory : [TransactionHistoryAPIElement] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        GetTransactionHistoryApiHit()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- IBAction
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:- UItable view delegate methods
extension TransactionHistoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (TransactionHistory.count) == 0{
            self.tableView.separatorStyle = .none
            self.lblNoDataFound.isHidden = false
            return 0
        }else{
            self.tableView.separatorStyle = .singleLine
            self.lblNoDataFound.isHidden = true
            return (TransactionHistory.count)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionHistoryNewCell", for: indexPath) as! TransactionHistoryNewCell
        if TransactionHistory[indexPath.row].status == "Added"{
            cell.lblTransactionName.text = "Added into wallet"
            cell.lbltransactionAmount.textColor = Colors.traxiGreenColor.color()
            cell.lbltransactionAmount.text = TransactionHistory[indexPath.row].amount ?? ""
        }else if TransactionHistory[indexPath.row].status == "Refunded"{
            cell.lblTransactionName.text = "Added into wallet"
            cell.lbltransactionAmount.textColor = Colors.traxiGreenColor.color()
            cell.lbltransactionAmount.text = TransactionHistory[indexPath.row].amount ?? ""
        }else{
            cell.lblTransactionName.text = "Deducted"
            cell.lbltransactionAmount.textColor = Colors.traxiredColor.color()
            cell.lbltransactionAmount.text = TransactionHistory[indexPath.row].amount ?? ""
        }
        if indexPath.row == TransactionHistory.count - 1{
            self.tableView.separatorStyle = .none
        }else{
            self.tableView.separatorStyle = .singleLine
        }
        return cell
    }
}

extension TransactionHistoryVC{
    //MARK:- hit APi
    func GetTransactionHistoryApiHit(){
        let paarams : [String : String] = [
            :
        ]
        ApiHandler.callApiWithParameters(url: APIConstants.transactionHistory, withParameters: paarams as [String: AnyObject], ofType: TransactionHistoryAPI.self, success2: { (profileAPI) in
            self.TransactionHistory = profileAPI
            self.tableView.reloadData()
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
