//
//  TransactionsCell.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 16/02/21.
//

import UIKit

class TransactionsCell: UITableViewCell {

    @IBOutlet weak var lblTransactionName: UILabel!
    @IBOutlet weak var lbltransactionAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
