//
//  TraxiCreditsVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 15/12/20.
//

import UIKit
import ENSwiftSideMenu
import ESPullToRefresh

class TraxiCreditsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnAddCard: UIButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddCredit: LoadingButton!
    @IBOutlet weak var lblWalletAmount: UILabel!
    @IBOutlet weak var lblNocardFound: UILabel!
    @IBOutlet weak var vwNoCardFound: UIView!
    @IBOutlet weak var tfAmount: paddingLeftSide!
    @IBOutlet weak var tableViewTransactionHistory: UITableView!
    @IBOutlet weak var tableViewTransactionHeight: NSLayoutConstraint!
    @IBOutlet weak var btnViewAllTransactions: UIButton!
    @IBOutlet weak var lblNodataFOund: UILabel!
    @IBOutlet weak var vwnotificationCount: setCornerRadiusCircle!
    
    //MARK:- Variables
    var fromOrderScreen = false
    var cardsData : GetSavedCardAPI?
    var cardDataFromCoreData = coredata.shared.cardNumber
    var cardIdFromCoreData = coredata.shared.cardIds
    var TransactionHistory : [TransactionHistoryAPIElement] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUi()
//        self.GetSavedCardApi()
//        self.tableView.es.addPullToRefresh {
//            self.GetSavedCardApi()
//        }
        self.lblWalletAmount.text = "$ \(CommonFunctions.decimalPlaceFromDouble(coredata.shared.walletBalance, decimalPlace: 2))"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwnotificationCount.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        self.GetProfileApiHit()
        self.lblNocardFound.text = "Save your Credit/Debit cards for faster payments. Your cards are secure with us"
        self.tfAmount.text = ""
        self.tfAmount.resignFirstResponder()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "SavedCard"), object: nil)
        
        GetTransactionHistoryApiHit()
    }

    //MARK:- Button Actions
    @IBAction func btnActSideMenu(_ sender: Any) {
        self.tfAmount.resignFirstResponder()
        if fromOrderScreen{
            self.navigationController?.popViewController(animated: true)
        }else{
            toggleSideMenuView()
        }
    }
    @IBAction func btnActAddCard(_ sender: Any) {
        let vc = AddNewCardVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActAddCredits(_ sender: Any) {
//        let vc = AddNewCardVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
//        vc.sType = .AddCredits
//        self.navigationController?.pushViewController(vc, animated: true)
        if tfAmount.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfAmount.shake()
            CommonFunctions.toster("Please enter amount")
        }else{
            let vc = AddCreditsVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
            vc.addedAmount = tfAmount.text ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnActViewAllTransactions(_ sender: Any) {
        let vc = TransactionHistoryVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TraxiCreditsVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        let aSet = NSCharacterSet(charactersIn:"0123456789.").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        if (textField.text?.contains("."))!{
            let aSet1 = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet1 = string.components(separatedBy: aSet1)
            let numberFiltered1 = compSepByCharInSet1.joined(separator: "")
            let numStr = newString.components(separatedBy: ".")
            if numStr.count>1{
                let decStr = numStr[1]
                if decStr.count > 2{
                    return false
                }
            }
            return string == numberFiltered1 && newString.length <= 6
        }else{
            return string == numberFiltered && newString.length <= 4
        }
    }
}


extension TraxiCreditsVC{
    //MARK:- SetUpUI
    func setUpUi(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            if self.navigationController != nil{
                self.sideMenuController()?.sideMenu?.delegate = self
                if self.fromOrderScreen{
                    self.sideMenuController()?.sideMenu?.allowLeftSwipe = false
                    self.sideMenuController()?.sideMenu?.allowPanGesture = false
                    self.sideMenuController()?.sideMenu?.allowRightSwipe = false
                }else{
                    self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
                    self.sideMenuController()?.sideMenu?.allowPanGesture = true
                    self.sideMenuController()?.sideMenu?.allowRightSwipe = true
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
                }
                
            }
        }
        
        if fromOrderScreen{
            btnSideMenu.setImage(UIImage(named: "back_black"), for: .normal)
        }else{
            btnSideMenu.setImage(UIImage(named: "menu"), for: .normal)
        }
    }
    
    
    
    @objc func loadList(notification: NSNotification){
        cardDataFromCoreData = coredata.shared.cardNumber
        cardIdFromCoreData = coredata.shared.cardIds

        self.tableView.reloadData()
    }
}

//MARK:- UItable view delegate methods
extension TraxiCreditsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView{
            if (cardDataFromCoreData.count) == 0{
                self.vwNoCardFound.isHidden = false
                return 0
            }else{
                self.vwNoCardFound.isHidden = true
                return (cardDataFromCoreData.count)
            }
        }else{
            if (TransactionHistory.count) == 0{
                self.btnViewAllTransactions.isHidden = true
                self.lblNodataFOund.isHidden = false
                return 0
            }else if (TransactionHistory.count) > 5{
                self.lblNodataFOund.isHidden = true
                self.btnViewAllTransactions.isHidden = false
                return 5
            }else{
                self.btnViewAllTransactions.isHidden = true
                self.lblNodataFOund.isHidden = true
                return (TransactionHistory.count)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardCells", for: indexPath) as! CardCells
            cell.lblCardNumber.text = "XXXX - XXXX - XXXX - \(cardDataFromCoreData[indexPath.row])"//\(cardsData?.data?[indexPath.row].last4 ?? "XXXX")"
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnActCardDelete(_:)), for: .touchUpInside)
            self.tableView.layoutIfNeeded()
            let contentSize = self.tableView.contentSize.height
            self.tableViewHeight.constant = contentSize
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionsCell", for: indexPath) as! TransactionsCell
            if TransactionHistory[indexPath.row].status == "Added"{
                cell.lblTransactionName.text = "Added into wallet"
                cell.lbltransactionAmount.textColor = Colors.traxiGreenColor.color()
                cell.lbltransactionAmount.text = TransactionHistory[indexPath.row].amount ?? ""
            }else if TransactionHistory[indexPath.row].status == "Refunded"{
                cell.lblTransactionName.text = "Added into wallet"
                cell.lbltransactionAmount.textColor = Colors.traxiGreenColor.color()
                cell.lbltransactionAmount.text = TransactionHistory[indexPath.row].amount ?? ""
            }else{
                cell.lblTransactionName.text = "Deducted"
                cell.lbltransactionAmount.textColor = Colors.traxiredColor.color()
                cell.lbltransactionAmount.text = TransactionHistory[indexPath.row].amount ?? ""
            }
            
            return cell
        }
    }
}

// MARK: - DELEGATE FUNCTIONS
extension TraxiCreditsVC : ENSideMenuDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
            
            self.view.bringSubviewToFront(fullView)
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        toggleSideMenuView()
    }
}

//MARK:- object C methods
extension TraxiCreditsVC{
    @objc func btnActCardDelete(_ sender : UIButton){
        let alert = UIAlertController(title: "Remove Card?", message: "Are you sure you want to remove this card?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
            self.deleteDelivery(self.cardIdFromCoreData[sender.tag])//(self.cardsData?.data?[sender.tag].id ?? "")
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (_) in }))
        CommonFunctions.getTopMostViewController()?.present(alert, animated: true, completion: nil)
    }
}

//MARK:- Hit Api
extension TraxiCreditsVC{
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
//            CommonFunctions.hideLoader()
            print(profileAPI)
            coredata.shared.walletBalance = profileAPI.walletBalance ?? 0
            coredata.shared.dataSave()
            self.lblWalletAmount.text = "$ \(CommonFunctions.decimalPlaceFromDouble((profileAPI.walletBalance ?? 0), decimalPlace: 2))"
            if !self.fromOrderScreen{
                if (profileAPI.unreadComplains ?? 0) > 0 || (profileAPI.unreadNotifications ?? 0) > 0{
                    self.vwnotificationCount.isHidden = false
                }else{
                    self.vwnotificationCount.isHidden = true
                }
            }
        }, failure: { (false, string) in
//            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
//    func GetSavedCardApi(){
//        let paarams : [String : String] = [
//            :
//        ]
////        CommonFunctions.showLoader()
//        ApiHandler.callApiWithParameters(url: APIConstants.getSavedCards, withParameters: paarams as [String: AnyObject], ofType: GetSavedCardAPI.self, success2: { (profileAPI) in
//            CommonFunctions.hideLoader()
//            coredata.shared.deleteSavedCardData()
//            coredata.shared.addDataCards(data: profileAPI)
//            print(coredata.shared.cardNumber)
//            self.cardsData = profileAPI
//            print(profileAPI)
//            self.tableView.reloadData()
//            self.tableView.es.stopPullToRefresh()
//
//            self.tableView.layoutIfNeeded()
//            let contentSize = self.tableView.contentSize.height
//            self.tableViewHeight.constant = contentSize
//        }, failure: { (false, string) in
////            CommonFunctions.hideLoader()
//            self.tableView.es.stopPullToRefresh()
//            print(string)
//        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
//    }
    
    //MARK:- HitApi
    func deleteDelivery(_ cardId : String){
        let paarams : [String : AnyObject] = [
           "card_id" : cardId as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.deleteCard, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            if self.cardDataFromCoreData.count == 1{
                self.cardDataFromCoreData = []
                self.tableView.reloadData()
            }
            CommonFunctions.toster(profileAPI.message ?? "")
            coredata.shared.newlyAddedCardData = []
            coredata.shared.cardNumber = []
            coredata.shared.cardIds = []
            coredata.shared.deleteSavedCardData()
            GetSavedCardApi(self.tableView)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.DELETEWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetTransactionHistoryApiHit(){
        let paarams : [String : String] = [
            :
        ]
        ApiHandler.callApiWithParameters(url: APIConstants.transactionHistory, withParameters: paarams as [String: AnyObject], ofType: TransactionHistoryAPI.self, success2: { (profileAPI) in
            self.TransactionHistory = profileAPI
            self.tableViewTransactionHistory.reloadData()
            
            
            DispatchQueue.main.async {
                self.tableViewTransactionHistory.layoutIfNeeded()
                let contentSize = self.tableViewTransactionHistory.contentSize.height
                self.tableViewTransactionHeight.constant = contentSize
            }
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
