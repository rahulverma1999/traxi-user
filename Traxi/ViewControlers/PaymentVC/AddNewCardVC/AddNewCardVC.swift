//
//  AddNewCardVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 15/12/20.
//

import UIKit
import Stripe

enum AddNewCardVCScreentype {
    case AddCard, AddCredits
}

class AddNewCardVC: baseVC {

    //MARK:- OutLets
    @IBOutlet weak var btnDebitCard: UIButton!
    @IBOutlet weak var imgDebitCard: UIImageView!
    @IBOutlet weak var vwDebitCard: cornerRadiusView!
    @IBOutlet weak var tfDebitCardNumber: UITextField!
    @IBOutlet weak var tfExpDateDebit: UITextField!
    @IBOutlet weak var tfCvvDebit: UITextField!
    @IBOutlet weak var tfCardHolderName: UITextField!
    @IBOutlet weak var btnCredit: UIButton!
    @IBOutlet weak var tfCvvCredit: UITextField!
    @IBOutlet weak var tfExpDateCredit: UITextField!
    @IBOutlet weak var tfCardHolderNameCredit: UITextField!
    @IBOutlet weak var vwHideCredit: cornerRadiusView!
    @IBOutlet weak var imgCredit: UIImageView!
    @IBOutlet weak var tfCardNumberCredit: UITextField!
    @IBOutlet weak var btnSave: LoadingButton!
    @IBOutlet weak var vwAddMoney: cornerRadiusView!
    @IBOutlet weak var lblAddCard: UILabel!
    @IBOutlet weak var constantAddCard: NSLayoutConstraint!
    @IBOutlet weak var vwSaveForFuture: UIView!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var lblAddedAmount: UILabel!
    
    //MARK:- Variables
    var isDebitCard = true
    var cardSelected = false
    var sType : AddNewCardVCScreentype = .AddCard
    var addedAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUi()
        self.lblAddedAmount.text = "$ \(addedAmount)"
    }
    
    //MARK:- Button Actions
    @IBAction func btnActCreditcard(_ sender: Any) {
        self.isDebitCard = false
        self.setCardOpetions()
        
    }
    
    @IBAction func btnActDebitCard(_ sender: Any) {
        self.isDebitCard = true
        self.setCardOpetions()
    }
    
    @IBAction func btnActSave(_ sender: Any) {
        if isDebitCard{
            if tfDebitCardNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter card number")
                tfDebitCardNumber.shake()
            }else if tfExpDateDebit.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter expire month and year")
                tfExpDateDebit.shake()
            }else if tfCvvDebit.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter cvv number")
                tfCvvDebit.shake()
            }else if tfCardHolderName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter card holder name")
                tfCardHolderName.shake()
            }else{
                self.getPaymentToken()
            }
        }else{
            if tfCardNumberCredit.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter card number")
                tfCardNumberCredit.shake()
            }else if tfExpDateCredit.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter expire month and year")
                tfExpDateCredit.shake()
            }else if tfCvvCredit.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter cvv number")
                tfCvvCredit.shake()
            }else if tfCardHolderNameCredit.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please enter card holder name")
                tfCardHolderNameCredit.shake()
            }else{
                self.getPaymentToken()
            }
        }
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActCheckBox(_ sender: Any) {
        cardSelected = !cardSelected
        btnCheckBox.setImage(cardSelected ? Asset.ic_checked.image() : Asset.ic_notChecked.image(), for: .normal)
    }
    
    @IBAction func btnActCheckBoxFull(_ sender: Any) {
        cardSelected = !cardSelected
        btnCheckBox.setImage(cardSelected ? Asset.ic_checked.image() : Asset.ic_notChecked.image(), for: .normal)
    }
    
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddNewCardVC{
    //MARK:- SetUpUI
    func setUpUi(){
        setCardOpetions()
        switch sType{
        case .AddCard:
            vwAddMoney.isHidden = true
            constantAddCard.constant = 22
            lblAddCard.text = "ADD CARD"
            btnSave.setTitle("SAVE", for: .normal)
            vwSaveForFuture.isHidden = true
            lblTopTitle.text = "Add New Card"
        case .AddCredits:
            vwAddMoney.isHidden = false
            lblTopTitle.text = "Add Credit"
            constantAddCard.constant = 96
            lblAddCard.text = "SELECT AN OPTION TO PAY"
            btnSave.setTitle("PAY $\(addedAmount)", for: .normal)
            vwSaveForFuture.isHidden = false
        }
    }
    
    //MARK:- Radio Button Actions
    func setCardOpetions(){
        if self.isDebitCard{
            self.vwDebitCard.isHidden = false
            self.vwHideCredit.isHidden = true
            self.imgDebitCard.image = UIImage(named: "radio_button_checked")
            self.imgCredit.image = UIImage(named: "radio_button_unchecked")
        }else{
            self.vwDebitCard.isHidden = true
            self.vwHideCredit.isHidden = false
            self.imgDebitCard.image = UIImage(named: "radio_button_unchecked")
            self.imgCredit.image = UIImage(named: "radio_button_checked")
        }
    }
}

//MARK:- UItextField Delegates
extension AddNewCardVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        var maxLength = 120
        if textField == tfExpDateDebit || textField == tfExpDateCredit {
          guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
          }
          let updatedText = oldText.replacingCharacters(in: r, with: string)
          if string == "" {
            if updatedText.count == 2 {
              textField.text = "\(updatedText.prefix(1))"
              return false
            }
          } else if updatedText.count == 1 {
            if updatedText > "1" {
              textField.text = "0\(updatedText)/"
              return false
            }
          } else if updatedText.count == 2 {
            if updatedText <= "12" { //Prevent user to not enter month more than 12
              textField.text = "\(updatedText)/" //This will add “/” when user enters 2nd digit of month
            }
            return false
          } else if updatedText.count > 5 {
            return false
          }
        }
        else if (tfCardNumberCredit == textField) || (tfDebitCardNumber == textField){
            maxLength = 16
        }
        else if(textField == tfCvvDebit) || (textField == tfCvvCredit){
          maxLength = 4
        }
        else if(textField == tfCardHolderName) || (textField == tfCardHolderNameCredit){
          maxLength = 40
        }
        else if(textField == tfDebitCardNumber) || (textField == tfCardNumberCredit){
          maxLength = 16
        }
        return newString.length <= maxLength
      }
}

//MARK:- Hit Api
extension AddNewCardVC{
    func getPaymentToken() {
        CommonFunctions.showLoader()
        let cardParams = STPCardParams()
        if isDebitCard{
            cardParams.number = tfDebitCardNumber.text!
            cardParams.expMonth = UInt(tfExpDateDebit.text!.prefix(2)) ?? 0
            cardParams.expYear = UInt(tfExpDateDebit.text!.suffix(2)) ?? 0
            cardParams.cvc = tfCvvDebit.text!
        }else{
            cardParams.number = tfCardNumberCredit.text!
            cardParams.expMonth = UInt(tfExpDateCredit.text!.prefix(2)) ?? 0
            cardParams.expYear = UInt(tfExpDateCredit.text!.suffix(2)) ?? 0
            cardParams.cvc = tfCvvCredit.text!
        }
        STPAPIClient.shared.createToken(withCard: cardParams) { (token, error) in
            print(token as Any, error as Any)
            if error != nil{
                CommonFunctions.toster(error?.localizedDescription ?? "Wrong detail")
                CommonFunctions.hideLoader()
            }else{
                var paarams : [String: AnyObject] = [
                    "stripe_token": (token?.tokenId ?? "") as AnyObject
                ]
                
                if self.sType == .AddCredits{
                    paarams["amount"]  = self.addedAmount as AnyObject
                }
                
                if self.cardSelected || self.sType == .AddCard{
                    paarams["is_save_card"]  = 1 as AnyObject
                }else{
                    paarams["is_save_card"]  = 0 as AnyObject
                }
            ApiHandler.callApiWithParameters(url: APIConstants.addCard, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
                CommonFunctions.toster(profileAPI.message ?? "")
                CommonFunctions.hideLoader()
//                if self.sType == .AddCredits{
                    let allVcs = self.navigationController?.viewControllers
                    for vc in allVcs ?? []{
                        if vc.isKind(of: TraxiCreditsVC.self){
                            let kVC = vc as! TraxiCreditsVC
                            GetSavedCardApi(kVC.tableView)
                            self.navigationController?.popToViewController(vc, animated: false)
                        }
                    }
//                }else{
//                    self.navigationController?.popViewController(animated: true)
//                }
            }, failure: { (false, string) in
                CommonFunctions.hideLoader()
                print(string)
            }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
          }
        }
    }
}
