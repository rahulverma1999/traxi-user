//
//  OrderDisputeCell.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

import UIKit

class OrderComplaintsCell: UITableViewCell {

    @IBOutlet weak var imgStore: UIImageView!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblrating: UILabel!
    @IBOutlet weak var lblStoreLocation: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    
    var complaintData : [GetComplainsAPIElement] = []
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ indexPath : IndexPath){
        if self.complaintData[indexPath.row].orderStatus == 0{
            self.lblOrderStatus.text = "PENDING"
            self.lblOrderStatus.textColor = Colors.pinkColorTraxi.color()
        }else if self.complaintData[indexPath.row].orderStatus == 6 || self.complaintData[indexPath.row].orderStatus == 7 || self.complaintData[indexPath.row].orderStatus == 8{
            self.lblOrderStatus.text = "CANCELLED"
            self.lblOrderStatus.textColor = Colors.traxiredColor.color()
        }else if self.complaintData[indexPath.row].orderStatus == 5{
            self.lblOrderStatus.text = "DELIVERED"
            self.lblOrderStatus.textColor = Colors.traxiGreenColor.color()
        }else{
            self.lblOrderStatus.text = "ACCEPTED"
            self.lblOrderStatus.textColor = Colors.traxiGreenColor.color()
        }
        self.imgStore.yy_setImage(with: URL(string: CommonFunctions.getImage(self.complaintData[indexPath.row].storeImage ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
        self.lblStoreName.text = self.complaintData[indexPath.row].storeName ?? ""
        self.lblCreateDate.text = self.complaintData[indexPath.row].pickupDate ?? ""
        self.lblrating.text = "\(self.complaintData[indexPath.row].storeRatings ?? 0)"
        self.lblMsg.text = self.complaintData[indexPath.row].message ?? ""
        self.lblStoreLocation.text = self.complaintData[indexPath.row].storeAddress ?? ""
    }

}
