//
//  DeliveryDisputeCell.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

import UIKit

class DeliveryComplaintsCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var lblDeliveryType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTimeSenstive: UILabel!
    @IBOutlet weak var lblDeliveryStatus: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var lblPickupTime: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    
    var complaintData : [GetComplainsAPIElement] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func setData(_ indexPath : IndexPath){
        if self.complaintData[indexPath.row].deliverTypeID == 1{// 1 = Single, 2 = Return, 3 = Traxi
            self.lblDeliveryType.text = "Single Delivery"
        }else if self.complaintData[indexPath.row].deliverTypeID == 2{
            self.lblDeliveryType.text = "Return Delivery"
        }else{
            self.lblDeliveryType.text = "Traxi Run"
        }
        if self.complaintData[indexPath.row].timeSensitive == 1{
            self.lblTimeSenstive.text = "Time Sensitive"
        }else{
            self.lblTimeSenstive.text = "Time Flexible"
        }
        self.lblMsg.text = self.complaintData[indexPath.row].message ?? ""
        self.lblPickupTime.text = self.complaintData[indexPath.row].pickupTime ?? ""
        self.lblPickUpDate.text = self.complaintData[indexPath.row].pickupDate ?? ""
        self.lblPrice.text = "$\(self.complaintData[indexPath.row].deliveredCharges ?? 0)"
        
        if self.complaintData[indexPath.row].deliveryStatus == 1{
            self.lblDeliveryStatus.text = "PENDING"
            self.lblDeliveryStatus.textColor = Colors.pinkColorTraxi.color()
        }else if (self.complaintData[indexPath.row].deliveryStatus == 2) || (self.complaintData[indexPath.row].deliveryStatus == 3){
            self.lblDeliveryStatus.text = "ONGOING"
            self.lblDeliveryStatus.textColor = Colors.pinkColorTraxi.color()
        }else if self.complaintData[indexPath.row].deliveryStatus == 4{
            self.lblDeliveryStatus.text = "COMPLETED"
            self.lblDeliveryStatus.textColor = Colors.traxiGreenColor.color()
        }else if (self.complaintData[indexPath.row].deliveryStatus == 5) || (self.complaintData[indexPath.row].deliveryStatus == 6){
            self.lblDeliveryStatus.text = "CANCELLED"
            self.lblDeliveryStatus.textColor = Colors.traxiredColor.color()
        }
    }
}
