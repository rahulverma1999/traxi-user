//
//  ChatReciveCell.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

import UIKit

class ChatReciveCell: UITableViewCell {
    
    @IBOutlet weak var imgBubble: UIImageView!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgBubble.clipsToBounds = true
        self.imgBubble.layer.masksToBounds = false
        self.imgBubble.layer.shadowColor = UIColor.black.cgColor
        self.imgBubble.layer.shadowOffset = CGSize(width: 1.0, height: 6.0)
        self.imgBubble.layer.shadowOpacity = 0.2
        self.imgBubble.layer.shadowRadius = 3.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.imgBubble.roundCorners([.topRight, .topLeft, .bottomRight], radius: 12)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
