//
//  ChatVC.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

import UIKit
import GrowingTextView
import IQKeyboardManagerSwift
import ESPullToRefresh

class ChatVC: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tvMsg: GrowingTextView!
    @IBOutlet weak var btnSendMsg: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vwButtomContant: NSLayoutConstraint!
    @IBOutlet weak var vwShadow: UIView!
    
    
    //MARK:- Variables
    var chatResponse : [GetChatDataAPIElement] = []
    var keyboardEmoji: Bool = true
    var deliveryId : Int!
    var messageText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetUpUi()
        self.getChatsApi(false)
        self.tableView.es.addPullToRefresh {
            self.getChatsApi(true)
        }
        self.getSeenMsgsApi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
//        self.willScrollToBottom(true)
//        self.tableView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActSendMsg(_ sender: UIButton) {
        if tvMsg.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
//            CommonFunctions.toster("Please type message")
            tvMsg.shake()
        }else{
            self.messageText = tvMsg.text
            self.chatResponse.append(GetChatDataAPIElement.init(msg: self.tvMsg.text))
            self.tvMsg.text = ""
            self.postComplainApi()
            scrollingToBottom(true)
            tableView.reloadData()
            
        }
    }
}

extension ChatVC{
    //MARK:- SetUpUi
    func SetUpUi(){
        self.btnSendMsg.clipsToBounds = true
        self.btnSendMsg.layer.masksToBounds = false
        self.btnSendMsg.layer.shadowColor = UIColor.black.cgColor
        self.btnSendMsg.layer.shadowOffset = CGSize(width: 1.0, height: 6.0)
        self.btnSendMsg.layer.shadowOpacity = 0.2
        self.btnSendMsg.layer.shadowRadius = 3.0
        self.btnSendMsg.layer.cornerRadius = self.btnSendMsg.bounds.height/2
        
        self.tvMsg.textContainerInset = UIEdgeInsets(top: 12,left: 12,bottom: 12,right: 12)
        self.vwShadow.clipsToBounds = true
        self.vwShadow.layer.masksToBounds = false
        self.vwShadow.layer.shadowColor = UIColor.black.cgColor
        self.vwShadow.layer.shadowOffset = CGSize(width: 1.0, height: 6.0)
        self.vwShadow.layer.shadowOpacity = 0.2
        self.vwShadow.layer.shadowRadius = 3.0
        DispatchQueue.main.async {
            self.tvMsg.layer.cornerRadius = 24
            self.vwShadow.layer.cornerRadius = 24
        }
       
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.scrollingToBottom(true)
    }
    
    //MARK:- Method for scrolling chat to bottom
    func willScrollToBottom(_ animation: Bool){
//            scrollingToBottom(animation)
        let indexes = self.tableView.indexPathsForVisibleRows
        var kIndex : IndexPath!
        if self.chatResponse.count > 0{
            kIndex = IndexPath(row: (self.chatResponse.count ) - 1, section: 0)
        }else{
            kIndex = IndexPath(row: (self.chatResponse.count ) - 1, section: 0)
        }
        if indexes?.contains(kIndex) ?? false{
            scrollingToBottom(animation)
        }
    }
    
    //MARK:- Method for scrolling chat to bottom
    func scrollingToBottom(_ animation: Bool){
        if self.chatResponse.count > 0{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                let indexPath = IndexPath(row: (self.chatResponse.count ) - 1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .top, animated: animation)
            }
        }
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
//        if (tableView.contentSize.height) > (tableView.frame.size.height) - keyboardSize.height{
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("keyboardSize.height", keyboardSize.height as Any)
            self.vwButtomContant.constant = keyboardSize.height// + CGFloat(tempCheckValue)
            print(self.vwButtomContant.constant)
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            if self.keyboardEmoji{
                if self.isLastCellVisisble(){
                    self.tableView.contentOffset = CGPoint(x: 0, y: self.tableView.contentOffset.y + keyboardSize.height)// - CGFloat(tempCheckValue))
                }else{
                   self.tableView.contentOffset = CGPoint(x: 0, y: self.tableView.contentOffset.y + keyboardSize.height)// - CGFloat(tempCheckValue))
                }
            }
            self.keyboardEmoji = false
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        keyboardEmoji = true
        //tempCheckValue = 0
        self.vwButtomContant.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK:- Method for see tha last chat of chatting
    func isLastCellVisisble() -> Bool{
        let indexes = self.tableView.indexPathsForVisibleRows
        var kIndex : IndexPath!
        if self.chatResponse.count == 0{
          return false
        }
          kIndex = IndexPath(row: (self.chatResponse.count) - 1, section: 0)
        if indexes?.contains(kIndex) ?? false{
          return true
        }
        return false
    }
}

//MARK:- UItableView DelegateMethods
extension ChatVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if chatResponse.count != 0{
            return chatResponse.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tvMsg.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if chatResponse[indexPath.row].admin == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatSendCell", for: indexPath) as! ChatSendCell
        cell.lblMsg.text = chatResponse[indexPath.row].message ?? ""
            cell.lblTime.text = CommonFunctions.getTraxiTime(chatResponse[indexPath.row].createdAt ?? "")
            cell.imgProfile.tintColor = Colors.traxiThemeColor.color()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                cell.imgProfile.roundCorners([.topRight, .topLeft, .bottomLeft], radius: 12)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatReciveCell", for: indexPath) as! ChatReciveCell
            cell.lblMsg.text = chatResponse[indexPath.row].message ?? ""
            cell.lblTime.text = CommonFunctions.getTraxiTime(chatResponse[indexPath.row].createdAt ?? "")
            cell.imgBubble.tintColor = UIColor.white
            return cell
        }
    }
}

//MARK:- HitApi
extension ChatVC{
    //MARK:- Hit Api
    func getChatsApi(_ pagination: Bool){
        
//        var page = 1
//        if pagination{
//            page+=1
//        }
        
        var paarams : [String : AnyObject] = [
            "delivery_id" : deliveryId as AnyObject,
            "limit" : 30 as AnyObject,
        ]
        
        if pagination{
            if chatResponse.count > 0 {
                paarams["last_id"] =  chatResponse[0].id as AnyObject
            }
        }
        
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getChats, withParameters: paarams as [String: AnyObject], ofType: GetChatDataAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            if pagination{
                for ind in profileAPI.reversed(){
                    self.chatResponse.insert(ind, at: 0)
                }
            }else{
                self.chatResponse = profileAPI.reversed()
            }
            if !pagination{
                 self.scrollingToBottom(true)
            }
            self.tableView.reloadData()
            self.tableView.es.stopPullToRefresh()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            self.tableView.reloadData()
            self.tableView.es.stopPullToRefresh()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    //MARK:- Hit Api
    func getSeenMsgsApi(){
        var paarams : [String:AnyObject] = [
            :
        ]
        
        if deliveryId != nil{
            paarams["delivery_id"] = deliveryId as AnyObject
        }
        ApiHandler.callApiWithParameters(url: APIConstants.seenMsgs, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.PUTWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func postComplainApi(){
        var paarams : [String:AnyObject] = [
            "message" : messageText as AnyObject
        ]
        
        if deliveryId != nil{
            paarams["deliver_id"] = deliveryId as AnyObject
        }
        
        ApiHandler.callApiWithParameters(url: APIConstants.postComplain, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
//            self.getChatsApi(false)
            self.tvMsg.text = ""
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
}
