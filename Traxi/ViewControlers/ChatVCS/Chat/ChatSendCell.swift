//
//  ChatSendCell.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

import UIKit

class ChatSendCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgProfile.clipsToBounds = true
        self.imgProfile.layer.masksToBounds = false
        self.imgProfile.layer.shadowColor = UIColor.black.cgColor
        self.imgProfile.layer.shadowOffset = CGSize(width: 1.0, height: 6.0)
        self.imgProfile.layer.shadowOpacity = 0.2
        self.imgProfile.layer.shadowRadius = 3.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
