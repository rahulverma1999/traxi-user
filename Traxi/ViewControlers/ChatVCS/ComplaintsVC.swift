//
//  DisputesVC.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 13/01/21.
//

import UIKit
import ENSwiftSideMenu

class ComplaintsVC: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var vwnotificationCount: setCornerRadiusCircle!
    
    
    //MARK:- Variables
    var complaintData : [GetComplainsAPIElement] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwnotificationCount.isHidden = true
        self.getOrderDetailApi()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        GetProfileApiHit()
    }
    
    //MARK:- Button Actions
    @IBAction func btnActSideMenu(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        toggleSideMenuView()
    }
    
}


extension ComplaintsVC{
    //MARK:- setUp viewDid load
    func setUpUI(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            if self.navigationController != nil{
                self.sideMenuController()?.sideMenu?.delegate = self
                self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
                self.sideMenuController()?.sideMenu?.allowPanGesture = true
                self.sideMenuController()?.sideMenu?.allowRightSwipe = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
            }
        }
    }
}


//MARK:- TableView delegate methods
extension ComplaintsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.complaintData.count == 0{
            self.lblNoDataFound.isHidden = false
            return 0
        }else{
            self.lblNoDataFound.isHidden = true
            return self.complaintData.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ChatVC.instantiateFromAppStoryboard(appStoryboard: .Chat)
        vc.deliveryId = self.complaintData[indexPath.row].deliveryID ?? 0
        CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.complaintData[indexPath.row].jobType == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryComplaintsCell", for: indexPath) as! DeliveryComplaintsCell
            cell.complaintData = self.complaintData
            cell.setData(indexPath)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderComplaintsCell", for: indexPath) as! OrderComplaintsCell
            cell.complaintData = self.complaintData
            cell.setData(indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
}

extension ComplaintsVC{
    //MARK:- Hit Api
    func getOrderDetailApi(){
        let paarams : [String : AnyObject] = [
            :
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getComplains, withParameters: paarams as [String: AnyObject], ofType: GetComplainsAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.complaintData = profileAPI
            self.tableView.reloadData()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            if (profileAPI.unreadComplains ?? 0) > 0 || (profileAPI.unreadNotifications ?? 0) > 0{
                self.vwnotificationCount.isHidden = false
            }else{
                self.vwnotificationCount.isHidden = true
            }
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}

// MARK: - DELEGATE FUNCTIONS
extension ComplaintsVC : ENSideMenuDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
            
            self.view.bringSubviewToFront(fullView)
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        toggleSideMenuView()
    }
}
