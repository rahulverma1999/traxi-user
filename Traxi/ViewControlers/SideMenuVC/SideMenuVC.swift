//
//  SideMenuVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit
import ENSwiftSideMenu
import YYWebImage

struct SideMenuStruct {
    var isProfile: Bool?
    var title: String?
    var image: UIImage?
    var isLine: Bool?
    var isLang: Bool?
}

class SideMenuVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - VARIABLES
    
    var sideMenuNew : SideMenuVC!
    var sideData : [SideMenuStruct] = [
        SideMenuStruct.init(isProfile: true, title: nil, image: nil,
                            isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Home", image: UIImage(named: "home-1"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Booking History", image: UIImage(named: "booking_history"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Wallet", image: UIImage(named: "wallet"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Complaints", image: UIImage(named: "complaint"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Notifications", image: UIImage(named: "notifications_icon"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: nil, image: nil, isLine: true, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Contact Us", image: UIImage(named: "contact_us"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Share", image: UIImage(named: "share"), isLine: false, isLang: false),
//        SideMenuStruct.init(isProfile: false, title: "Help & Feedback", image: UIImage(named: "help_feedback"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: nil, image: nil, isLine: true, isLang: false),
        SideMenuStruct.init(isProfile: false, title: "Log Out", image: UIImage(named: "logout"), isLine: false, isLang: false),
        SideMenuStruct.init(isProfile: false, title: nil, image: nil, isLine: true, isLang: false)
    ]
    var CELL_IDENTIFIER_TOP = "sideMenuTopTVC"
    var CELL_IDENTIFIER_CELL = "sideMenuDataTVC"
    var CELL_IDENTIFIER_LINE = "sideMenuLineTVC"
    var UserData : GetProfile?

    override func viewDidLoad() {
        super.viewDidLoad()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tableView.reloadData()
    }
    
    @objc func loadList(notification: NSNotification){
        //load data here
        self.GetProfileApiHit()
        self.tableView.reloadData()
    }

}


// MARK: - SET UP UI
extension SideMenuVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if sideData[indexPath.row].isProfile ?? false{
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER_TOP, for: indexPath) as! sideMenuTopTVC
            cell.lblUserData.attributedText = CommonFunctions.sendAttString([UIFont.NunitoSemiBold(18), UIFont.NunitoRegular(14)], colors: [UIColor.black, Colors.greyColorTraxi.color()], texts: [UserData?.name ?? "N/A", "\n\(UserData?.email ?? "N/A")"], align: .left, lineSpacing: 0)
//            cell.imgView.yy_setImage(with: URL(string: CommonFunctions.getImage(UserData?.profileImage ?? "", quality: .large), relativeTo: nil))
            if UserData?.profileImage != "" && UserData?.profileImage != nil{
                cell.imgView.yy_setImage(with: URL(string: CommonFunctions.getImage(UserData?.profileImage ?? "", quality: .large)), placeholder: UIImage(named: "image_large"))
            }else{
                CommonFunctions.getImageWithLbl(imageView: cell.imgView)
            }

            return cell
        }
        
        if sideData[indexPath.row].isLine ?? false{
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER_LINE, for: indexPath) as! sideMenuLineTVC
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER_CELL, for: indexPath) as! sideMenuDataTVC
        if (UserData?.unreadNotifications ?? 0) > 0{
            sideData[5].image = UIImage(named: "notifications_icon_red")
        }else{
            sideData[5].image = UIImage(named: "notifications_icon")
        }
        if (UserData?.unreadComplains ?? 0) > 0{
            sideData[4].image = UIImage(named: "complaint_red")
        }else{
            sideData[4].image = UIImage(named: "complaint")
        }
        cell.imgView.image = sideData[indexPath.row].image
        cell.lblData.numberOfLines = 1
        cell.lblData.attributedText = nil
        cell.lblData.text = sideData[indexPath.row].title ?? ""
        if indexPath.row == SelectedIndexValueOfSideMenu{
            cell.contentView.backgroundColor = Colors.borderYellowColorTraxi.color()//.withAlphaComponent(0.40)
        }else{
            cell.contentView.backgroundColor = .clear
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if sideData[indexPath.row].isLine ?? false || sideData[indexPath.row].isLang ?? false{
            toggleSideMenuView()
            return
        }
        toggleSideMenuView()
        if SelectedIndexValueOfSideMenu == indexPath.row{
//            toggleSideMenuView()
        }else{
            if sideData[indexPath.row].isProfile ?? false{
                let vc = ProfileVC.instantiateFromAppStoryboard(appStoryboard: .Profile)
                CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
                SelectedIndexValueOfSideMenu = indexPath.row
            }else{
                
                if indexPath.row == 1{
                    //Home
                    SelectedIndexValueOfSideMenu = indexPath.row
//                    let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: .Home)
//                    CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
                    self.sideMenuNew = SideMenuVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
                    let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
                    
                    let navVC : navController = navController(menuViewController: self.sideMenuNew, contentViewController: vc)
                    SelectedIndexValueOfSideMenu = 1
        //            let navigationController = UINavigationController(rootViewController: vc)
                    if UIApplication.shared.windows.count > 0 {
                        UIApplication.shared.windows[0].rootViewController = navVC
                        navVC.setNavigationBarHidden(true, animated: false)
                    }
                }else if indexPath.row == 2{
                    //Booking History
                    SelectedIndexValueOfSideMenu = indexPath.row
                    let vc = BookingHistoryVC.instantiateFromAppStoryboard(appStoryboard: .BookingHistory)
                    CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
                }else if indexPath.row == 3{
                    //Wallet
                    SelectedIndexValueOfSideMenu = indexPath.row
                    let vc = TraxiCreditsVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
                    CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
                }else if indexPath.row == 4{
                    //Complaints
                    SelectedIndexValueOfSideMenu = indexPath.row
                    let vc = ComplaintsVC.instantiateFromAppStoryboard(appStoryboard: .Chat)
                    CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
                }else if indexPath.row == 5{
                    //Complaints
                    SelectedIndexValueOfSideMenu = indexPath.row
                    let vc = NotificationVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
                    CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
                }else if indexPath.row == 7{
                    //contact Us
                    SelectedIndexValueOfSideMenu = indexPath.row
                    let vc = ContactUsVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
                    CommonFunctions.getNavigationController().pushViewController(vc, animated: true)
                }else if indexPath.row == 8{
                    //Share
                    let firstActivityItem = "Hey friends,\nPlease use traxi for deliveres and order booking. https://thetraxi.com/ \nThanks!"
                    let items = [NSURL(string: "https://thetraxi.com/")!]
                    let ac = UIActivityViewController(activityItems: [firstActivityItem, items], applicationActivities: nil)
                    present(ac, animated: true)
                }else if indexPath.row == 10{
                    let alert = UIAlertController(title: "Log Out?", message: "Are you sure you want to log out", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
                        //print("logout")
                        self.LogoutApiHit()
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (_) in }))
                    CommonFunctions.getTopMostViewController()?.present(alert, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if sideData[indexPath.row].isProfile ?? false{
            return 152
        }else if sideData[indexPath.row].isLine ?? false{
            return 36
        }else if sideData[indexPath.row].isLang ?? false{
            return 106
        }else{
            return 52
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let hView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 24))
        hView.backgroundColor = .clear
        return hView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
}

//MARK:- ApiHit
extension SideMenuVC{
    func LogoutApiHit(){
        let paarams : [String : String] = [
            "device_id" : device_id
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.logOut, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
//            CommonFunctions.toster(profileAPI.message ?? "")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                coredata.shared.deleteProfile()
            }
            coredata.shared.newlyAddedCardData = []
            coredata.shared.cardNumber = []
            coredata.shared.cardIds = []
            SelectedIndexValueOfSideMenu = 1
            coredata.shared.deleteSavedCardData()
            let vc = SocialLoginVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Main)
            CommonFunctions.getNavigationController().viewControllers.insert(vc, at: 0)
            CommonFunctions.getNavigationController().popToRootViewController(animated: true)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
//            CommonFunctions.hideLoader()
            self.UserData = profileAPI
            print(profileAPI)
            self.tableView.reloadData()
        }, failure: { (false, string) in
//            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}

