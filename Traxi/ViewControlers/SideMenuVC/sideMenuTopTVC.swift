//
//  sideMenuTopTVC.swift
//  RxHub
//
//  Created by MAC on 07/04/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

class sideMenuTopTVC: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblUserData: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.cornerRadius = imgView.bounds.height/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
