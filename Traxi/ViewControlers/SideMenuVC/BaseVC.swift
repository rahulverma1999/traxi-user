//
//  BaseVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//


import UIKit
import ENSwiftSideMenu

class baseVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        swipeToPop()
        self.sideMenuController()?.sideMenu?.allowLeftSwipe = false
        self.sideMenuController()?.sideMenu?.allowPanGesture = false
        self.sideMenuController()?.sideMenu?.allowRightSwipe = false
    }
    
    func swipeToPop() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
}


// MARK: - DELEGATE FUNCTIONS
extension baseVC: ENSideMenuDelegate, UIGestureRecognizerDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return false
        }
        return true
    }
}

