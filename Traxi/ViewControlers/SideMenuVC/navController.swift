//
//  navController.swift
//  RxHub
//
//  Created by MAC on 07/04/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import ENSwiftSideMenu

class navController: ENSideMenuNavigationController {//,ENSideMenuDelegate{
    
    var sideMenuNew : SideMenuVC!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.bringSubviewToFront(navigationBar)
        navigationController?.isNavigationBarHidden = true
//        sideMenu?.delegate = self //optional
        sideMenu?.menuWidth = 4*screenWidth/5 // optional, default is 160
        sideMenu?.bouncingEnabled = false


        // Do any additional setup after loading the view.
    }
    
    func reloadMenuView(_ homeView:HomeVC){
        if sideMenuNew != nil{
            sideMenuNew.tableView.reloadData()
            return
        }
//        sideMenuNew = sideMenuVC()
        sideMenuNew = SideMenuVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
        //sideMenuNew.chkValues()
        navigationController?.isNavigationBarHidden = true
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: sideMenuNew, menuPosition:.left)
//        sideMenuNew = homeView
//        sideMenu?.delegate = self //optional
        sideMenu?.menuWidth = 4*screenWidth/5 // optional, default is 160
        sideMenu?.bouncingEnabled = false
        
        view.bringSubviewToFront(navigationBar)
    }
    
//    func sideMenuWillOpen() {
//
//    }
//
//    func sideMenuWillClose() {
//
//    }
//
//    func sideMenuShouldOpenSideMenu() -> Bool {
//
//    }
//
//    func sideMenuDidOpen() {
//
//    }
//
//    func sideMenuDidClose() {
//
//    }
}
