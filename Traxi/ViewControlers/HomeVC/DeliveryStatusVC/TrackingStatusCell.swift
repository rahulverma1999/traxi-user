//
//  TrackingStatusCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit

class TrackingStatusCell: UITableViewCell {
    
    @IBOutlet weak var lblReq: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTimeImage: UIImageView!
    @IBOutlet weak var lblReqImage: UIImageView!
    @IBOutlet weak var vwDotted: UIView!
    @IBOutlet weak var bottomConstaint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:- Add Doted Layer to view
    func addDotedLayer(View : UIView, color: CGColor){
        DispatchQueue.main.async {
            let lineLayer = CAShapeLayer()
            lineLayer.strokeColor = color
            lineLayer.lineWidth = 2
            lineLayer.lineDashPattern = [2,2]
            let path = CGMutablePath()
            path.addLines(between: [CGPoint(x: View.bounds.minX, y: View.bounds.minY),
                                    CGPoint(x: View.bounds.minX, y: View.bounds.maxY)])
            lineLayer.path = path
            View.layer.addSublayer(lineLayer)
        }
    }
    
    func setData(_ indexPath : IndexPath, _ DeliveryData : GetDeliveryStatus?, _ sType : DeliveryStatusVCScreentype){
        if indexPath.row == 0{
            self.addDotedLayer(View: self.vwDotted, color: Colors.traxiGreenColor.color().cgColor)
            self.lblReqImage.image = UIImage(named: "request_accepted")
            self.lblReq.text = "Job Posted"
            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.requestAcceptedAt ?? "")
            self.vwDotted.isHidden = false
            self.bottomConstaint.constant = 46
            self.lblTimeImage.isHidden = false
        }else if indexPath.row == 1{
            if DeliveryData?.driverAssignedAt != nil && DeliveryData?.driverAssignedAt != ""{
                self.lblReqImage.image = UIImage(named: "driver_assigned")
                self.lblReq.text = "Driver Accepted"
                self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.driverAssignedAt ?? "")
                self.addDotedLayer(View: self.vwDotted, color: Colors.traxiBlueColor.color().cgColor)
                self.vwDotted.isHidden = false
                self.bottomConstaint.constant = 46
                self.lblTimeImage.isHidden = false
            }else{
                self.lblReqImage.image = UIImage(named: "outline_circle")
                self.lblReq.text = "Driver Accepted"
                self.lblTime.text = ""
                self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                self.vwDotted.isHidden = false
                self.bottomConstaint.constant = 46
                self.lblTimeImage.isHidden = true
            }
        }else{
            switch sType {
            case .singleDelivery:
                if indexPath.row == 2{
                    if (DeliveryData?.pickup?.count ?? 0) > 0{
                        if DeliveryData?.pickup?[0].pickedUpAt != nil && DeliveryData?.pickup?[0].pickedUpAt != ""{
                            self.lblReqImage.image = UIImage(named: "package_collected")
                            self.lblReq.text = "Package Collected"
                            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.pickup?[0].pickedUpAt ?? "")
                            self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = false
                        }else{
                            self.lblReqImage.image = UIImage(named: "outline_circle")
                            self.lblReq.text = "Package Collected"
                            self.lblTime.text = ""
                            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                            self.vwDotted.isHidden = false
                            self.lblTimeImage.isHidden = true
                            self.bottomConstaint.constant = 46
                        }
                    }
                }else{
                    if (DeliveryData?.dropoff?.count ?? 0) > 0{
                        if DeliveryData?.dropoff?[0].deliveredAt != nil && DeliveryData?.dropoff?[0].deliveredAt != ""{
                            self.lblReqImage.image = UIImage(named: "package_delivered")
                            self.lblReq.text = "Package Delivered"
                            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.dropoff?[0].deliveredAt ?? "")
                            self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                            self.vwDotted.isHidden = true
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = false
                        }else{
                            self.lblReqImage.image = UIImage(named: "outline_circle")
                            self.lblReq.text = "Package Delivered"
                            self.lblTime.text = ""
                            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                            self.vwDotted.isHidden = true
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = true
                        }
                    }
                }
            case .returnDelivery:
                if indexPath.row == 2{
                    if (DeliveryData?.pickup?.count ?? 0) > 0{
                        if DeliveryData?.pickup?[0].pickedUpAt != nil && DeliveryData?.pickup?[0].pickedUpAt != ""{
                            self.lblReqImage.image = UIImage(named: "package_collected")
                            self.lblReq.text = "Package Collected"
                            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.pickup?[0].pickedUpAt ?? "")
                            self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = false
                        }else{
                            self.lblReqImage.image = UIImage(named: "outline_circle")
                            self.lblReq.text = "Package Collected"
                            self.lblTime.text = ""
                            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = true
                        }
                    }
                }else if indexPath.row == 3{
                    if (DeliveryData?.dropoff?.count ?? 0) > 0{
                        if DeliveryData?.dropoff?[0].deliveredAt != nil && DeliveryData?.dropoff?[0].deliveredAt != ""{
                            self.lblReqImage.image = UIImage(named: "package_delivered")
                            self.lblReq.text = "Package Delivered"
                            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.dropoff?[0].deliveredAt ?? "")
                            self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = false
                        }else{
                            self.lblReqImage.image = UIImage(named: "outline_circle")
                            self.lblReq.text = "Package Delivered"
                            self.lblTime.text = ""
                            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = true
                        }
                    }
                }else if indexPath.row == 4{
                    if (DeliveryData?.pickup?.count ?? 0) > 1{
                        if DeliveryData?.pickup?[1].pickedUpAt != nil && DeliveryData?.pickup?[1].pickedUpAt != ""{
                            self.lblReqImage.image = UIImage(named: "package_collected")
                            self.lblReq.text = "Return Package Collected"
                            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.pickup?[1].pickedUpAt ?? "")
                            self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = false
                        }else{
                            self.lblReqImage.image = UIImage(named: "outline_circle")
                            self.lblReq.text = "Return Package Collected"
                            self.lblTime.text = ""
                            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = true
                        }
                    }
                }else{
                    if (DeliveryData?.dropoff?.count ?? 0) > 1{
                        if DeliveryData?.dropoff?[1].deliveredAt != nil && DeliveryData?.dropoff?[1].deliveredAt != ""{
                            self.lblReqImage.image = UIImage(named: "package_delivered")
                            self.lblReq.text = "Return Package Delivered"
                            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.dropoff?[1].deliveredAt ?? "")
                            self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                            self.vwDotted.isHidden = true
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = false
                        }else{
                            self.lblReqImage.image = UIImage(named: "outline_circle")
                            self.lblReq.text = "Return Package Delivered"
                            self.lblTime.text = ""
                            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                            self.vwDotted.isHidden = true
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = true
                        }
                    }
                }
            case .multipleDelivery:
                if indexPath.row == 2{
                    if (DeliveryData?.pickup?.count ?? 0) > 0{
                        if DeliveryData?.pickup?[0].pickedUpAt != nil && DeliveryData?.pickup?[0].pickedUpAt != ""{
                            self.lblReqImage.image = UIImage(named: "package_collected")
                            self.lblReq.text = "Package Collected"
                            self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.pickup?[0].pickedUpAt ?? "")
                            self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = false
                        }else{
                            self.lblReqImage.image = UIImage(named: "outline_circle")
                            self.lblReq.text = "Package Collected"
                            self.lblTime.text = ""
                            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                            self.vwDotted.isHidden = false
                            self.bottomConstaint.constant = 46
                            self.lblTimeImage.isHidden = true
                        }
                    }
                }else{
                    if DeliveryData?.dropoff?[indexPath.row-3].deliveredAt != nil && DeliveryData?.dropoff?[indexPath.row-3].deliveredAt != ""{
                        self.lblReqImage.image = UIImage(named: "package_delivered")
                        self.lblReq.text = "Package \(indexPath.row-2) Delivered"
                        self.lblTime.text = CommonFunctions.getTraxiTime(DeliveryData?.dropoff?[indexPath.row-3].deliveredAt ?? "")
                        self.addDotedLayer(View: self.vwDotted, color: Colors.pinkColorTraxi.color().cgColor)
                        self.vwDotted.isHidden = false
                        self.bottomConstaint.constant = 46
                        self.lblTimeImage.isHidden = false
                    }else{
                        self.lblReqImage.image = UIImage(named: "outline_circle")
                        self.lblReq.text = "Package \(indexPath.row-2) Delivered"
                        self.lblTime.text = ""
                        self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
                        self.vwDotted.isHidden = false
                        self.bottomConstaint.constant = 46
                        self.lblTimeImage.isHidden = true
                    }
                    if indexPath.row-3 == ((DeliveryData?.dropoff?.count ?? 0)-1){
                        self.vwDotted.isHidden = true
                    }
                }
            }
        }
    }
}
