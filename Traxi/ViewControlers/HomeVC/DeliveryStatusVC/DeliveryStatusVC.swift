//
//  DeliveryStatusVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit
import GoogleMaps
import GooglePlaces

enum DeliveryStatusVCScreentype {
    case singleDelivery, returnDelivery, multipleDelivery
}

class DeliveryStatusVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var btnConfirmDriverLocation: LoadingButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var vwTrackDeliveryButton: GradientView!
    @IBOutlet weak var lblEstimateTIme: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDriverReview: UILabel!
    @IBOutlet weak var lblDriverTotalReview: UILabel!
    @IBOutlet weak var lblCarNumber: UILabel!
    @IBOutlet weak var lblDriverCarNam: UILabel!
    @IBOutlet weak var imgDriverCar: UIImageView!
    @IBOutlet weak var vwReturnEstimatedTime: UIView!
    @IBOutlet weak var lblReturnEstimatedTime: UILabel!
    
    //MARK:- Variables
    var deliveryId : Int!
    var DeliveryData : GetDeliveryStatus?
    var fromPostDelivery = false
    var sType : DeliveryStatusVCScreentype = .singleDelivery
    var estimateDeliveryTime = ""
    var fromListingScreen = false
    
    var desArrayLocation : [CLLocationCoordinate2D] = []
    var soucseLocation : CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        if deliveryId != nil{
            getDeliveryStatus()
        }
        
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "PushNotifReloadData"), object: nil)
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: UIButton) {
        if fromPostDelivery{
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: HomeVC.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnActConfirmDeiverLocation(_ sender: Any) {
//        let vc = ReviewDeleiveredVC.instantiateFromAppStoryboard(appStoryboard: .Home)
//        self.navigationController?.pushViewController(vc, animated: true)
        let vc = TrackDriverLoactionMapVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.DeliveryData = DeliveryData
        if self.sType == .multipleDelivery{
            vc.isMultipleType = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnActCall(_ sender: Any) {
        if self.DeliveryData?.driverPhone != "" && self.DeliveryData?.driverPhone != nil{
            self.callMethod(number: self.DeliveryData?.driverPhone ?? "")
        }else{
            CommonFunctions.toster("Number not found.")
        }
    }
    
    @IBAction func btnActCancel(_ sender: Any) {
        let popOverVC = CancelOrderPopUpVC()
        self.addChild(popOverVC)
        if fromListingScreen{
            popOverVC.fromListing = true
        }
        popOverVC.deliveryId = deliveryId
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
}


extension DeliveryStatusVC{
    //MARK:- SetUpUI
    func setUpUI(){
        tableView.layer.cornerRadius = 6.0
        imgDriver.layer.cornerRadius = 4.0
        vwCorner.layer.cornerRadius = 6.0
        
        vwCorner.isHidden = true
        vwTrackDeliveryButton.isHidden = true
    }
    
    //MARK:- Driver Number Call
    func callMethod(number: String) {
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func loadList(notification: NSNotification){
        self.getDeliveryStatus()
    }
}

//MARK:- UItable view delegate methods
extension DeliveryStatusVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (2) + (DeliveryData?.pickup?.count ?? 0) + (DeliveryData?.dropoff?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackingStatusCell", for: indexPath) as! TrackingStatusCell
        cell.setData(indexPath, DeliveryData, self.sType)
        DispatchQueue.main.async {
            self.tableView.layoutIfNeeded()
            let contentSize = self.tableView.contentSize.height
            self.tableViewHeight.constant = contentSize
        }
        return cell
    }
    
    
}

extension DeliveryStatusVC{
    func setDataFromApi(_ data : GetDeliveryStatus){
        if data.driverAssignedAt != nil && data.driverAssignedAt != ""{
            self.vwCorner.isHidden = false
        }else{
            self.vwCorner.isHidden = true
        }
        
        if (data.dropoff?.count ?? 0) > 0{
            if data.dropoff?[0].deliveredAt != nil && data.dropoff?[0].deliveredAt != nil{
                self.btnCancel.isHidden = true
            }else{
                self.btnCancel.isHidden = false
            }
        }else{
            self.btnCancel.isHidden = false
        }
        self.lblDriverName.text = data.driverName ?? "N/A"
        self.lblCarNumber.text = data.vehicleNo ?? "N/A"
        self.lblDriverCarNam.text = data.vehicleName ?? "N/A"
        self.lblDriverReview.text = "\(data.driverRatings ?? 0.0)"
        self.lblDriverTotalReview.text = "\(data.driverReviews ?? 0) reviews"
        self.imgDriver.yy_setImage(with: URL(string: CommonFunctions.getImage(data.driverImage ?? "", quality: .large)), placeholder: UIImage(named: "image_large"))
        self.imgDriverCar.yy_setImage(with: URL(string: CommonFunctions.getImage(data.vehicleImage ?? "", quality: .large)), placeholder: UIImage(named: "car"))
        if (data.pickup?.count ?? 0) > 0{
            if self.sType == .multipleDelivery{
                if data.pickup?[0].pickedUpAt != nil && data.pickup?[0].pickedUpAt != ""{
                    self.vwTrackDeliveryButton.isHidden = false
                }else{
                    self.vwTrackDeliveryButton.isHidden = true
                }
            }else{
                if (data.pickup?[0].pickedUpAt != nil && data.pickup?[0].pickedUpAt != "") && (data.timeSensitive == 1){
                    self.vwTrackDeliveryButton.isHidden = false
                }else{
                    self.vwTrackDeliveryButton.isHidden = true
                }
            }
        }
    }
}

extension DeliveryStatusVC{
    //MARK:- Function for Get Api response of google
    func getTimeFromGoogleApi(_ profileAPI : GetDeliveryStatus){
        for data in profileAPI.pickup ?? []{
            if data.pickedUpAt != nil && data.pickedUpAt != ""{
                self.soucseLocation = CLLocationCoordinate2D(latitude: profileAPI.driverLat ?? 0.0, longitude: profileAPI.driverLng ?? 0.0)
            }else{
                self.soucseLocation = CLLocationCoordinate2D(latitude: data.lat ?? (profileAPI.driverLat ?? 0.0), longitude: data.lng ?? (profileAPI.driverLng ?? 0.0))
            }
        }
        let sortedData = profileAPI.dropoff?.sorted(by: { $0.lat ?? 0.0 > $1.lat ?? 0.0})
        var count = 0
        for data in sortedData ?? []{
            count+=1
            self.desArrayLocation.append(CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0))
            if count == profileAPI.dropoff?.count{
                CommonFunctions.getPolylineRoute(from: self.soucseLocation!, to: self.desArrayLocation.last!, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                    print("Status, pol, time, distance",status, polyline,time,distance)
                    if !(polyline.isEmpty){
                    }
                    if time == ""{
                        self.lblEstimateTIme.text = "0 min"
                    }else{
                        self.lblEstimateTIme.text = "\(time)"
                    }
                }
            }
        }
    }
    
    func getTimeFromGoogleApiReturn(_ profileAPI : GetDeliveryStatus, isReturn : Bool){
        if isReturn{
            if (profileAPI.pickup?.count ?? 0) > 1{
                if profileAPI.pickup?[1].pickedUpAt != nil && profileAPI.pickup?[1].pickedUpAt != ""{
                    self.soucseLocation = CLLocationCoordinate2D(latitude: profileAPI.driverLat ?? 0.0, longitude: profileAPI.driverLng ?? 0.0)
                }else{
                    self.soucseLocation = CLLocationCoordinate2D(latitude: profileAPI.pickup?[1].lat ?? (profileAPI.driverLat ?? 0.0), longitude: profileAPI.pickup?[1].lng ?? (profileAPI.driverLng ?? 0.0))
                }
            }
            if (profileAPI.dropoff?.count ?? 0) > 1{
                if profileAPI.dropoff?[1].deliveredAt != nil && profileAPI.dropoff?[1].deliveredAt != ""{
                    self.desArrayLocation.append(CLLocationCoordinate2D(latitude: profileAPI.driverLat ?? 0.0, longitude: profileAPI.driverLng ?? 0.0))
                }else{
                    self.desArrayLocation.append(CLLocationCoordinate2D(latitude: profileAPI.dropoff?[1].lat ?? (profileAPI.driverLat ?? 0.0), longitude: profileAPI.dropoff?[1].lng ?? (profileAPI.driverLng ?? 0.0)))
                }
            }
            CommonFunctions.getPolylineRoute(from: self.soucseLocation!, to: self.desArrayLocation.last!, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                print("Status, pol, time, distance",status, polyline,time,distance)
                if !(polyline.isEmpty){
                }
                self.lblReturnEstimatedTime.text = "\(time)"
            }
        }else{
            if (profileAPI.pickup?.count ?? 0) > 0{
                if profileAPI.pickup?[0].pickedUpAt != nil && profileAPI.pickup?[0].pickedUpAt != ""{
                    self.soucseLocation = CLLocationCoordinate2D(latitude: profileAPI.driverLat ?? 0.0, longitude: profileAPI.driverLng ?? 0.0)
                }else{
                    self.soucseLocation = CLLocationCoordinate2D(latitude: profileAPI.pickup?[0].lat ?? (profileAPI.driverLat ?? 0.0), longitude: profileAPI.pickup?[0].lng ?? (profileAPI.driverLng ?? 0.0))
                }
            }
            if (profileAPI.dropoff?.count ?? 0) > 0{
                if profileAPI.dropoff?[0].deliveredAt != nil && profileAPI.dropoff?[0].deliveredAt != ""{
                    self.desArrayLocation.append(CLLocationCoordinate2D(latitude: profileAPI.driverLat ?? 0.0, longitude: profileAPI.driverLng ?? 0.0))
                }else{
                    self.desArrayLocation.append(CLLocationCoordinate2D(latitude: profileAPI.dropoff?[0].lat ?? (profileAPI.driverLat ?? 0.0), longitude: profileAPI.dropoff?[0].lng ?? (profileAPI.driverLng ?? 0.0)))
                }
            }
            CommonFunctions.getPolylineRoute(from: self.soucseLocation!, to: self.desArrayLocation.last!, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                print("Status, pol, time, distance",status, polyline,time,distance)
                if !(polyline.isEmpty){
                }
                self.lblEstimateTIme.text = "\(time)"
            }
        }
    }
    
    //MARK:- HitApi
    func getDeliveryStatus(){
        let paarams : [String : AnyObject] = [
           "delivery_id" : deliveryId as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getDeliveryStatus, withParameters: paarams as [String: AnyObject], ofType: GetDeliveryStatus.self, success2: { (profileAPI) in
            self.DeliveryData = profileAPI
            if (profileAPI.pickup?.count ?? 0) > 1{
                self.sType = .returnDelivery
                self.vwReturnEstimatedTime.isHidden = false
                self.getTimeFromGoogleApiReturn(profileAPI, isReturn: false)
                self.getTimeFromGoogleApiReturn(profileAPI, isReturn: true)
            }else if (profileAPI.dropoff?.count ?? 0) > 1{
                self.sType = .multipleDelivery
                self.vwReturnEstimatedTime.isHidden = true
                self.getTimeFromGoogleApi(profileAPI)
            }else{
                self.getTimeFromGoogleApi(profileAPI)
                self.vwReturnEstimatedTime.isHidden = true
                self.sType = .singleDelivery
            }
            self.setDataFromApi(profileAPI)
            print(self.DeliveryData ?? "")
            self.tableView.reloadData()
            CommonFunctions.hideLoader()
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
