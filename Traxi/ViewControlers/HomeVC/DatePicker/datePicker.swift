//
//  datePicker.swift
//  Lowicks
//
//  Created by Bhanu Dhiman on 09/09/19.
//  Copyright © 2019 MAC. All rights reserved.
//

import UIKit

protocol logs {
    func datepic(_ date: Date, isDate : Bool, isReturn : Bool)
}

class datePicker: UIViewController {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var vwdissmiss: UIView!
    
    var delegate : logs?
    var date : Date?
    var dateReturn : Date?
    var isDate = false
    var isReturn = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        showDatePicker()
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dissmissPOP(_:)))
        view.addGestureRecognizer(tap1)
    }
    
    
    func showAnimate()
    {
        self.outerView.transform = CGAffineTransform(scaleX: 0.02, y: 0.02)
        //
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.2, animations: {
            self.view.alpha = 1.0
        }) { _ in
            UIView.animate(withDuration: 0.3, animations: {
                //
                self.outerView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            }) { _ in
                UIView.animate(withDuration: 0.2, animations: {
                    self.outerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
            }
        }
        
    }
    
    func stopAnimation()
    {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.outerView.transform = CGAffineTransform(scaleX: 0.02, y: 0.02)
            
        }) { (success) in
            
            self.view.removeFromSuperview()
        }
    }
    
    func showDatePicker(){
        //Formate Date
        if isDate{
            datePicker.datePickerMode = .date
            self.datePicker.minimumDate = Date()
            self.datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 15, to: Date())!
            datePicker.date = Date()
//            if date != nil{
//                datePicker.setDate(date!, animated: false)
//            }
        }else{
            datePicker.datePickerMode = .time
            datePicker.minuteInterval = 30
            self.datePicker.date = Calendar.current.date(byAdding: .hour, value: 1, to: Date())!
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat =  "HH:mm"
//
//            let min = dateFormatter.date(from: "0:00")      //createing min time
//            let max = dateFormatter.date(from: "23:59") //creating max time
//            datePicker.minimumDate = min  //setting min time to picker
//            datePicker.maximumDate = max  //setting max time to picker
        }
        
    }
    
    
    
    @IBAction func btnActCancel(_ sencder: Any) {
        self.stopAnimation()
        
    }
    
    @IBAction func btnActSubmit(_ sender: Any) {
        if !isDate{
            if isReturn{
                if CommonFunctions.getDateFormatTraxi5(self.dateReturn?.description ?? "") == CommonFunctions.getDateFormatTraxi5(Date().description){
                    if datePicker.date == Date() || datePicker.date < Date(){
                        CommonFunctions.toster("Selected time should be greater then currrent time")
                    }else{
                        delegate?.datepic(datePicker.date, isDate: isDate, isReturn: isReturn)
                        self.stopAnimation()
                    }
                }else{
                    delegate?.datepic(datePicker.date, isDate: isDate, isReturn: isReturn)
                    self.stopAnimation()
                }
            }else{
                if CommonFunctions.getDateFormatTraxi5(self.date?.description ?? "") == CommonFunctions.getDateFormatTraxi5(Date().description){
                    if datePicker.date == Date() || datePicker.date < Date(){
                        CommonFunctions.toster("Selected time should be greater then currrent time")
                    }else{
                        delegate?.datepic(datePicker.date, isDate: isDate, isReturn: isReturn)
                        self.stopAnimation()
                    }
                }else{
                    delegate?.datepic(datePicker.date, isDate: isDate, isReturn: isReturn)
                    self.stopAnimation()
                }
            }
        }else{
            delegate?.datepic(datePicker.date, isDate: isDate, isReturn: isReturn)
            self.stopAnimation()
        }
//        self.showDatePicker()
    }
}

extension datePicker{
    @objc func dissmissPOP(_ sender: UITapGestureRecognizer) {
       stopAnimation()
    }
}
