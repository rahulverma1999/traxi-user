//
//  CourierEstimateDetailsVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire

enum CourierEstimateDetailsVCScreenType {
    case signleDelivery, returnDelivery, multipleDelivery
}

class CourierEstimateDetailsVC: UIViewController {

    //MARK:- OUTLETS
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var btnTimeFlexible: UIButton!
    @IBOutlet weak var btnConfirm: LoadingButton!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var vwCalculateHeight: UIView!
    @IBOutlet weak var MapView: GMSMapView!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDeliveryDataAndTime: UILabel!
    @IBOutlet weak var lblEstimateTime: UILabel!
    @IBOutlet weak var lblPickToDropLocatoinCharge: UILabel!
    @IBOutlet weak var lblTaxAndServiceFee: UILabel!
    @IBOutlet weak var lblTotalCharges: UILabel!
    
    //MARK:- Variables
    var addedLocation : loc?
    var pickLoaction : loc?
    var dropLocation : loc?
    
    var pickLoactionReturn : loc?
    var dropLocationReturn : loc?
    
    var startCoordinates : CLLocationCoordinate2D!
    var endCoordinates : CLLocationCoordinate2D!
    
    var startCoordinatesReturn : CLLocationCoordinate2D!
    var endCoordinatesReturn : CLLocationCoordinate2D!
    
    var deliveryId : Int!
    var DeliveryData : DeliveryDetailAPi?
    var distanceInKm : String?
    var sType : CourierEstimateDetailsVCScreenType = .signleDelivery
    
    var customInfoWindow : CustomMarkerViewInfo?
    var getMarker : GMSMarker!
    
    var dropLoactionsStructMultipleLocations : [DropLoactions] = []
    var estimateDeliveryTime = ""
    var estimateDeliveryTimeValue : Int!
    var estimateTimeReturn = ""
    var returnDeliveryTimeValue : Int!
    var distanceForDelivery = ""
    var distanceForDeliveryReturn = ""
    var balanceAvaliable = false

    var paarams : [String : AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunctions.hideLoader()
        self.setUpUi()
        self.setMapSetting()
//        self.getDeliveryDetail()
        self.setDataApi(DeliveryData)
        setDataOfMultipleAddress()
        self.customInfoWindow = CustomMarkerViewInfo().loadView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.GetProfileApiHit()
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActConfirm(_ sender: Any) {
        if balanceAvaliable{
            self.PostDelivery()
        }else{
            let popOverVC = NotePopUpVc()
            popOverVC.sType = .TraxiCredits
            self.addChild(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParent: self)
        }
    }
}

extension CourierEstimateDetailsVC{
    
    func showPath(polyStr :String, _ color : UIColor){
        guard let path = GMSPath(fromEncodedPath: polyStr) else {return }
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = color
        polyline.map = self.MapView
        var bounds = GMSCoordinateBounds()
        for index in 0...path.count() {
          bounds = bounds.includingCoordinate(path.coordinate(at: UInt(index)))
        }
        MapView.animate(with: GMSCameraUpdate.fit(bounds))
      }

    
    //MARK:- set viewDidLoadData
    func setUpUi(){
        btnBack.layer.cornerRadius = btnBack.bounds.height/2
        btnBack.clipsToBounds = true
        btnBack.layer.masksToBounds = false
        btnBack.layer.shadowColor = UIColor.black.cgColor
        btnBack.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//            imgs.layer.shadowPath = shadowPath.cgPath
        btnBack.layer.shadowOpacity = 0.5
        btnBack.layer.shadowRadius = 5.0
        
        btnTimeFlexible.layer.cornerRadius = 2.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.vwCorner.roundCorners([.topRight, .topLeft], radius: 24)
        }
        
        DispatchQueue.main.async {
            self.scrollViewHeight.constant = self.view.bounds.height/2-10
        }
        
        startCoordinates = CLLocationCoordinate2D(latitude: pickLoaction?.lat ?? 0.0, longitude: pickLoaction?.lng ?? 0.0)
        endCoordinates = CLLocationCoordinate2D(latitude: dropLocation?.lat ?? 0.0, longitude: dropLocation?.lng ?? 0.0)
        
        self.getGoogleMapApi()
        
    }
    
    //MARK:- Call GoogleMapApis
    func getGoogleMapApi(){
        if sType != .multipleDelivery{
            CommonFunctions.getPolylineRoute(from: startCoordinates, to: endCoordinates, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                print("Status, pol, time, distance",status, polyline,time,distance)
                if !(polyline.isEmpty){
    //                self.MapView.clear()
                }
                self.lblEstimateTime.text = "Estimated delivery time \(time), \(distance)"
                self.estimateDeliveryTime = time
                self.estimateDeliveryTimeValue = TimeValue
                self.distanceForDelivery = distance
                self.showPath(polyStr: polyline, .black)
                let marker = GMSMarker(position: endCoordinates)
                marker.icon = UIImage(named: "map_pin_blue-1")
                marker.map = self.MapView
                marker.title = pickLoaction?.fullAddress ?? ""
//                setCustomMarkerView(marker, 2)
                marker.zIndex = 2
                let markerS = GMSMarker(position: startCoordinates)
                markerS.title = dropLocation?.fullAddress ?? ""
                markerS.icon = #imageLiteral(resourceName: "green_map_dot")
                markerS.map = self.MapView
                markerS.zIndex = 1
//                setCustomMarkerView(marker, 1)
            }
            
            startCoordinatesReturn = CLLocationCoordinate2D(latitude: pickLoactionReturn?.lat ?? 0.0, longitude: pickLoactionReturn?.lng ?? 0.0)
            endCoordinatesReturn = CLLocationCoordinate2D(latitude: dropLocationReturn?.lat ?? 0.0, longitude: dropLocationReturn?.lng ?? 0.0)
            
            if (startCoordinates.latitude == startCoordinatesReturn.latitude && startCoordinates.longitude == startCoordinatesReturn.longitude) && (endCoordinates.latitude == endCoordinatesReturn.latitude && endCoordinates.longitude == endCoordinatesReturn.longitude){
                //Return route not draw
                print("Coordinates matched")
            }else if (startCoordinates.latitude == endCoordinatesReturn.latitude && startCoordinates.longitude == endCoordinatesReturn.longitude) && (endCoordinates.latitude == startCoordinatesReturn.latitude && endCoordinates.longitude == startCoordinatesReturn.longitude){
                //Return route not draw
                print("Coordinates 2 matched")
            }else{
                CommonFunctions.getPolylineRoute(from: startCoordinatesReturn, to: endCoordinatesReturn, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                    print("Status, pol, time, distance",status, polyline,time,distance)
                    if !(polyline.isEmpty){
        //                self.MapView.clear()
                    }
                    self.lblEstimateTime.text = "Estimated delivery time \(time), \(distance)"
                    self.estimateDeliveryTime = time
                    self.estimateDeliveryTimeValue = TimeValue
                    self.distanceForDelivery = distance
                    self.showPath(polyStr: polyline, .blue)
                    let marker = GMSMarker(position: endCoordinatesReturn)
                    marker.icon = UIImage(named: "map_pin_blue-1")
                    marker.map = self.MapView
                    marker.title = pickLoactionReturn?.fullAddress ?? ""
                    marker.zIndex = 2
                    let markerS = GMSMarker(position: startCoordinatesReturn)
                    markerS.icon = #imageLiteral(resourceName: "green_map_dot")
                    markerS.title = dropLocationReturn?.fullAddress ?? ""
                    markerS.map = self.MapView
                    markerS.zIndex = 1
                }
            }
        }
    }
    
    //MARK:- Set map setting
    func setMapSetting(){
        self.MapView.delegate = self
        MapView.animate(toZoom: 15)
        MapView.settings.zoomGestures = true
        MapView.settings.rotateGestures = true
        MapView.settings.scrollGestures = true
        MapView.isUserInteractionEnabled = true
        LocationServices.shared.getAdress { (valueName, stringError) in
            if valueName != nil {
                self.MapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0.0, longitude: valueName?.location?.coordinate.longitude ?? 0.0))
             }else{
             }
        }
    }
    
    func setDataApi(_ dataResponse : DeliveryDetailAPi?){
        if (dataResponse?.pickupDetails?.count ?? 0) > 0{
            self.lblDeliveryDataAndTime.text = "\(CommonFunctions.getDateFormatTraxi2(dataResponse?.pickupDetails?[0].pickupDate ?? ""))"//, \(dataResponse?.pickupDetails?[0].pickupTime ?? "")"
            }
        if dataResponse?.timeSensitive == 1{
            self.btnTimeFlexible.setTitle("Time Sensitive", for: .normal)
        }else{
            self.btnTimeFlexible.setTitle("Time Flexible", for: .normal)
        }
        self.lblPickToDropLocatoinCharge.text = "$\(dataResponse?.deliveredCharges ?? 0.0)"
        self.lblTaxAndServiceFee.text = "$\(dataResponse?.taxCharges ?? 0.0)"
        let totalAmount = (dataResponse?.deliveredCharges ?? 0.0) + (dataResponse?.taxCharges ?? 0.0)
        self.lblTotalCharges.text = "$\(totalAmount)"
    }
}

//MARK:- Map delegate methods
extension CourierEstimateDetailsVC : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        getMarker = marker
        print(marker.zIndex)
        customInfoWindow?.lblStatus.text = marker.title
        customInfoWindow?.imgRightArrow.isHidden = true
        if marker.zIndex == 1{
            self.customInfoWindow?.lblName.text = "Your Pick Up"
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
        }else {
            self.customInfoWindow?.lblName.textColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.lblName.text = "Your Drop Off"
        }
        DispatchQueue.main.async {
            let position = marker.position
            self.customInfoWindow?.center = mapView.projection.point(for: position)
            self.customInfoWindow?.center.y = (self.customInfoWindow?.center.y ?? 0) - 62
            self.customInfoWindow?.center.x = (self.customInfoWindow?.center.x ?? 0)
        }
        self.customInfoWindow?.frame.size.height = 54
        self.customInfoWindow?.frame.size.width = 180
        self.MapView.addSubview(self.customInfoWindow!)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return self.customInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        customInfoWindow?.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (getMarker != nil){
            guard let location = getMarker?.position else {
                print("locationMarker is nil")
                return
            }
            customInfoWindow?.center = self.MapView.projection.point(for: location)
            customInfoWindow?.center.y = (customInfoWindow?.center.y ?? 0)-62
            customInfoWindow?.center.x = (customInfoWindow?.center.x ?? 0)
        }
    }
}


extension CourierEstimateDetailsVC{
    //MARK:- HitApi
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
//            CommonFunctions.hideLoader()
            print(profileAPI)
            if (profileAPI.walletBalance ?? 0.0) >= ((self.lblTotalCharges.text)?.replacingOccurrences(of: "$", with: "") as NSString?)?.doubleValue ?? 0.0{
                self.balanceAvaliable = true
            }else{
                self.balanceAvaliable = false
            }
        }, failure: { (false, string) in
//            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func PostDelivery(){
        let tempCheckDistance = (distanceForDelivery.components(separatedBy: " ").first as NSString?)?.replacingOccurrences(of: ",", with: "")
        let tempCheckDistanceReturn = (distanceForDeliveryReturn.components(separatedBy: " ").first as NSString?)?.replacingOccurrences(of: ",", with: "")
        switch sType{
        case .signleDelivery:
            self.paarams["estimated_delivery_time"] = (estimateDeliveryTime) as AnyObject
            self.paarams["delivery_distance"] = tempCheckDistance as AnyObject
            self.paarams["estimated_delivery_minute"] = self.estimateDeliveryTimeValue/60 as AnyObject
        case .returnDelivery:
            self.paarams["estimated_delivery_time"] = estimateDeliveryTime as AnyObject
            self.paarams["estimated_delivery_time_return"] = estimateTimeReturn as AnyObject
            self.paarams["delivery_distance"] = "\(((tempCheckDistance as NSString?)?.doubleValue ?? 0.0) + ((tempCheckDistanceReturn as NSString?)?.doubleValue ?? 0.0))" as AnyObject
            self.paarams["estimated_delivery_minute"] = self.estimateDeliveryTimeValue/60 as AnyObject
            self.paarams["estimated_delivery_return_minute"] = self.returnDeliveryTimeValue/60 as AnyObject
        case .multipleDelivery:
            self.paarams["delivery_distance"] = tempCheckDistance as AnyObject
            self.paarams["estimated_delivery_time"] = estimateDeliveryTime as AnyObject
            self.paarams["estimated_delivery_minute"] = self.estimateDeliveryTimeValue/60 as AnyObject
        }
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.postDelivers, withParameters: self.paarams as [String: AnyObject], ofType: AddDeliveriesAPI.self, success2: { (profileAPI) in
//            CommonFunctions.toster(profileAPI.message ?? "")
            let vc = DeliveryStatusVC.instantiateFromAppStoryboard(appStoryboard: .Home)
            vc.deliveryId = profileAPI.deliverID ?? 0
            vc.fromPostDelivery = true
            vc.estimateDeliveryTime = self.estimateDeliveryTime
            self.navigationController?.pushViewController(vc, animated: true)
            CommonFunctions.hideLoader()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    
    func setDataOfMultipleAddress(){
        if self.sType == .multipleDelivery{
            var desArrayLocation : [CLLocationCoordinate2D] = []
            var soucseLocation : CLLocationCoordinate2D?
            
            soucseLocation = CLLocationCoordinate2D(latitude: pickLoaction?.lat ?? 0.0, longitude: pickLoaction?.lng ?? 0.0)
            
            let markerS = GMSMarker(position: soucseLocation!)
            markerS.icon = #imageLiteral(resourceName: "green_map_dot")
            markerS.map = self.MapView
            markerS.zIndex = 1 // 1- Source, 2- Desitnation
            markerS.title = pickLoaction?.fullAddress ?? ""
            
            var count = 0
            let sortedData = self.dropLoactionsStructMultipleLocations.sorted(by: { $0.lat ?? 0.0 > $1.lat ?? 0.0})
            for data in sortedData {
                count+=1
                desArrayLocation.append(CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0))
                if count == self.dropLoactionsStructMultipleLocations.count{
                    self.getPolylineRoute(from: soucseLocation!, to: desArrayLocation) { (status, polyline,time,distance) in
                        print("status, polyline,time,distance",status, polyline,time,distance)
                        if status{
                            DispatchQueue.main.async {
                                self.drawPolyline(withMapView: self.MapView, withPolylinePoints: polyline)
                                var marker : GMSMarker!
                                for location in desArrayLocation{
                                    marker = GMSMarker(position: location)
                                    marker.icon = UIImage(named: "map_pin_blue-1")
                                    marker.map = self.MapView
                                    marker.title = data.fullAddress ?? ""
                                    marker.zIndex = 2 // 1- Source, 2- Desitnation
                                }
                            }
                        }else{
                            print(status)
                        }
                    }
                }
            }
        }
    }
    
    func getDeliveryDetail(){
        let paarams : [String : AnyObject] = [
           "delivery_id" : deliveryId as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.postDelivers, withParameters: paarams as [String: AnyObject], ofType: DeliveryDetailAPi.self, success2: { (profileAPI) in
            self.DeliveryData = profileAPI
            self.setDataApi(profileAPI)
            print(self.DeliveryData ?? "")
            if self.sType == .multipleDelivery{
                var desArrayLocation : [CLLocationCoordinate2D] = []
                var soucseLocation : CLLocationCoordinate2D?
                for data in profileAPI.pickupDetails ?? []{
                    soucseLocation = CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0)
                    let markerS = GMSMarker(position: soucseLocation!)
                    markerS.icon = #imageLiteral(resourceName: "green_map_dot")
                    markerS.map = self.MapView
                    markerS.zIndex = 1 // 1- Source, 2- Desitnation
                    markerS.title = data.address ?? ""
                }
                var count = 0
                let sortedData = profileAPI.dropoffDetails?.sorted(by: { $0.lat ?? 0.0 > $1.lat ?? 0.0})
//                let sortedData = profileAPI.dropoffDetails
                for data in sortedData ?? []{
                    count+=1
                    desArrayLocation.append(CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0))
                    if count == profileAPI.dropoffDetails?.count{
                        self.getPolylineRoute(from: soucseLocation!, to: desArrayLocation) { (status, polyline,time,distance) in
                            print("status, polyline,time,distance",status, polyline,time,distance)
                            if status{
                                DispatchQueue.main.async {
                                    self.drawPolyline(withMapView: self.MapView, withPolylinePoints: polyline)
                                    var marker : GMSMarker!
                                    for location in desArrayLocation{
                                        marker = GMSMarker(position: location)
                                        marker.icon = UIImage(named: "map_pin_blue-1")
                                        marker.map = self.MapView
                                        marker.title = data.address ?? ""
                                        marker.zIndex = 2 // 1- Source, 2- Desitnation
//                                        self.setCustomImage(marker)
                                    }
                                }
                            }else{
                                print(status)
                            }
                        }
                    }
                }
            }
            CommonFunctions.hideLoader()
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destinations: [CLLocationCoordinate2D], completionHandler: @escaping (Bool, String, String, String) -> ()) {

        guard let destination = destinations.last else {
            return
        }
        var wayPoints = ""
        for (_, point) in destinations.enumerated() {
//            if index == 0 { // Skipping first location that is current location.
//                continue
//            }
            wayPoints = wayPoints.count == 0 ? "\(point.latitude),\(point.longitude)" : "\(wayPoints)%7C\(point.latitude),\(point.longitude)"
        }

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&waypoints=\(wayPoints)&key=\(GOOGLEAPIKEY)")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Failed : \(String(describing: error?.localizedDescription))")
                return
            } else {
                do {
                    if let json: [String: Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        guard let routes = json["routes"] as? [[String: Any]] else { return }
                        if (routes.count > 0) {
                            let overview_polyline = routes[0]
                            let dictPolyline = overview_polyline["overview_polyline"] as? NSDictionary
                            let points = dictPolyline?.object(forKey: "points") as? String
                            let legsArr = overview_polyline["legs"] as? [Any]
                            let legfirstDict = legsArr?.first as? [String:Any]
                            let distanceDict = legfirstDict?["distance"]as?[String:Any]
                            let distanceValue = distanceDict?["text"]as?String
                            var pointss = ""
                            var time = ""
                            var timeTemp = 0
                            for kData in overview_polyline["legs"] as! [Any]{
                                let nData = kData as! [String:Any]
                                for pData in nData["steps"] as! [[String:Any]]{
                                    let poData = pData
                                    let plData = poData["polyline"] as! [String:Any]
                                    let ptData = plData["points"] as! String
                                    pointss.append(ptData)
                                }
                                let durationtemp = nData["duration"]as?[String:Any]
                                let xDataTime = durationtemp?["value"]as?Int
                                timeTemp+=xDataTime ?? 0
                                print("TimeCheck", timeTemp )
                            }
                            time = "\(CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: timeTemp))"
                            DispatchQueue.main.async {
                                completionHandler(true, points!, time, distanceValue ?? "")
                                self.lblEstimateTime.text = "Estimated delivery time \(time), \(distanceValue ?? "")"
                                self.estimateDeliveryTime = time
                                self.estimateDeliveryTimeValue = timeTemp
                                self.distanceForDelivery = distanceValue ?? ""
                            }
                        } else {
                            completionHandler(false, "", "", "")
                        }
                    }
                } catch {
                    print("Error : \(error)")
                }
            }
        }
        task.resume()
    }
    
    func drawPolyline(withMapView googleMapView: GMSMapView, withPolylinePoints polylinePoints: String){
        guard let path = GMSPath(fromEncodedPath: polylinePoints) else {return }
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 4.0
        polyline.strokeColor = .black
        polyline.map = googleMapView
        var bounds = GMSCoordinateBounds()
        for index in 0...path.count() {
          bounds = bounds.includingCoordinate(path.coordinate(at: UInt(index)))
        }
        googleMapView.animate(with: GMSCameraUpdate.fit(bounds))
    }
}
