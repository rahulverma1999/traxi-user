//
//  TrackDriverLoactionMapVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit
import GoogleMaps
import GooglePlaces

class TrackDriverLoactionMapVC: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    
    //MARK:- VAriables
    var DriverLatLng : CLLocationCoordinate2D?
    var driverMarker : GMSMarker?
    var previousCoordinate: CLLocationCoordinate2D?
    var destinationLatLng : CLLocationCoordinate2D?
    var DeliveryData : GetDeliveryStatus?
    var drivrId : Int!
    
    var destinationMulipleLatLng : [CLLocationCoordinate2D] = []
    
    var isMultipleType = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMapSetting()
        setUpUi()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SocketIOManager.sharedInstance.closeSocket()
        SocketIOManager.sharedInstance.closeConnection()
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension TrackDriverLoactionMapVC{
    
    //MARK:- set viewDidLoadData
    func setUpUi(){
        
        btnBack.layer.cornerRadius = btnBack.bounds.height/2
        btnBack.clipsToBounds = true
        btnBack.layer.masksToBounds = false
        btnBack.layer.shadowColor = UIColor.black.cgColor
        btnBack.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        btnBack.layer.shadowOpacity = 0.5
        btnBack.layer.shadowRadius = 5.0
        

        self.setDataOfMap()
    }
    
    
    //MARK:- Set map setting
    func setMapSetting(){
        mapView.animate(toZoom: 15)
        mapView.settings.zoomGestures = true
        mapView.settings.rotateGestures = true
        mapView.settings.scrollGestures = true
        mapView.isUserInteractionEnabled = true
    }
    
    func setDataOfMap(){
        for data in self.DeliveryData?.dropoff ?? []{
            self.destinationLatLng = CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0)
        }
        
        let sortedData = self.DeliveryData?.dropoff?.sorted(by: { $0.lat ?? 0.0 < $1.lat ?? 0.0})
        for data in sortedData ?? []{
            destinationMulipleLatLng.append(CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0))
        }
        
        
        SocketIOManager.sharedInstance.getListenLatLng{ (data)  in
            print(data)
            if self.isMultipleType{
                
        //            self.DriverLatLng = CLLocationCoordinate2D(latitude: ((data["lat"] as? Double) ?? 0.0), longitude: ((data["lng"] as? Double) ?? 0.0))
                self.DriverLatLng = CLLocationCoordinate2D(latitude: ((data["lat"] as? NSString)?.doubleValue ?? 0.0), longitude: ((data["lng"] as? NSString)?.doubleValue ?? 0.0))
                self.drivrId = (data["driver_id"] as? Int) ?? 0
                if self.drivrId == self.DeliveryData?.driverID{
                    print("Driver id matched")
                    self.trackingMultiple(currentLat: self.DriverLatLng?.latitude ?? 0.0, currentLong: self.DriverLatLng?.longitude ?? 0.0)
                }else{
                    print("Driver id not matched")
                }
            }else{
    //            self.DriverLatLng = CLLocationCoordinate2D(latitude: ((data["lat"] as? Double) ?? 0.0), longitude: ((data["lng"] as? Double) ?? 0.0))
                self.DriverLatLng = CLLocationCoordinate2D(latitude: ((data["lat"] as? NSString)?.doubleValue ?? 0.0), longitude: ((data["lng"] as? NSString)?.doubleValue ?? 0.0))
                self.drivrId = (data["driver_id"] as? Int) ?? 0
                if self.drivrId == self.DeliveryData?.driverID{
                    print("Driver id matched")
                    self.tracking(currentLat: self.DriverLatLng?.latitude ?? 0.0, currentLong: self.DriverLatLng?.longitude ?? 0.0)
                }else{
                    print("Driver id not matched")
                }
            }
        }
        
        if DriverLatLng == nil{
            if self.isMultipleType{
                self.trackingMultiple(currentLat: self.DeliveryData?.driverLat ?? 0.0, currentLong: self.DeliveryData?.driverLng ?? 0.0)
            }else{
                self.tracking(currentLat: self.DeliveryData?.driverLat ?? 0.0, currentLong: self.DeliveryData?.driverLng ?? 0.0)
            }
        }
    }
}

extension TrackDriverLoactionMapVC{
    //MARK:- Tracking methods
    func pathFromStartToEndPoint(startLat: Double, startLong:Double, endLat:Double, endLong:Double, isLoader: Bool) {
            let startPoint = CLLocationCoordinate2DMake(startLat, startLong)
            let end = CLLocationCoordinate2DMake(endLat, endLong)
            let allpoints = [startPoint,end]
            var bounds = GMSCoordinateBounds()
            for i in 0..<allpoints.count {
                bounds = bounds.includingCoordinate(allpoints[i])
            }
            if (isLoader) == true{
                let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
                self.mapView.animate(with: update)
            }
        CommonFunctions.getPolylineRoute(from: startPoint, to: end, isLoader: isLoader) { (status, polyline,time,distance,TimeValue) in
                print(status, polyline,time,distance)
                if !(polyline.isEmpty){
                    self.mapView.clear()
                    let coordinate1 = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
                    self.mapView.isMyLocationEnabled = false
                    self.driverMarker?.position = coordinate1
                    self.driverMarker?.icon = UIImage(named: "car_bottom")
                    self.driverMarker?.map = self.mapView
                }
                self.showPath(polyStr: polyline)
                let marker = GMSMarker(position: startPoint)
                marker.icon = #imageLiteral(resourceName: "car_bottom")
                marker.map = self.mapView
                let marker1 = GMSMarker(position: end)
                marker1.icon = #imageLiteral(resourceName: "map_pin_blue")
                marker1.map = self.mapView
            }
    }
    
    //MARK:- Tracking methods
    func pathFromStartToEndPointMultiple(startLat: Double, startLong:Double, endLocations : [CLLocationCoordinate2D], isLoader: Bool) {
        let startPoint = CLLocationCoordinate2DMake(startLat, startLong)
//            let end = CLLocationCoordinate2DMake(endLat, endLong)
        for end in endLocations{
            let allpoints = [startPoint,end]
            var bounds = GMSCoordinateBounds()
            for i in 0..<allpoints.count {
                bounds = bounds.includingCoordinate(allpoints[i])
            }
            if (isLoader) == true{
                let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
                self.mapView.animate(with: update)
            }
        }
        self.getPolylineRouteForMultiples(from: startPoint, to: endLocations) { (status, polyline,time,distance) in
            print("status, polyline,time,distance",status, polyline,time,distance)
            if status{
                DispatchQueue.main.async {
                    if !(polyline.isEmpty){
                        self.mapView.clear()
                        let coordinate1 = CLLocationCoordinate2D(latitude: startLat, longitude: startLong)
                        self.mapView.isMyLocationEnabled = false
                        self.driverMarker?.position = coordinate1
                        self.driverMarker?.icon = UIImage(named: "car_bottom")
                        self.driverMarker?.map = self.mapView
                    }
                    self.showPath(polyStr: polyline)
                    let marker = GMSMarker(position: startPoint)
                    marker.icon = #imageLiteral(resourceName: "car_bottom")
                    marker.map = self.mapView
                    for location in endLocations{
                        let marker1 = GMSMarker(position: location)
                        marker1.icon = #imageLiteral(resourceName: "map_pin_blue")
                        marker1.map = self.mapView
                    }
                }
            }else{
                print(status)
            }
        }
    }
    
    
    func showPath(polyStr :String){
            guard let path = GMSPath(fromEncodedPath: polyStr) else { return }
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 4.0
        polyline.strokeColor = .black
            polyline.map = self.mapView
            var bounds = GMSCoordinateBounds()
            for index in 0...path.count() {
                bounds = bounds.includingCoordinate(path.coordinate(at: UInt(index)))
            }
            mapView.animate(with: GMSCameraUpdate.fit(bounds))
        }
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
        func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / Double.pi }
        func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
            let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
            let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
            let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
            let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
            let dLon = lon2 - lon1
            let y = sin(dLon) * cos(lat2)
            let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
            let radiansBearing = atan2(y, x)
            return radiansToDegrees(radians: radiansBearing)
    }
    
    func tracking(currentLat: Double, currentLong: Double) {
            //        self.mapView.clear()
        let coordinate1 = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLong)
        self.mapView.isMyLocationEnabled = false
        self.driverMarker?.position = coordinate1
        self.driverMarker?.icon = #imageLiteral(resourceName: "car_bottom")
        self.driverMarker?.map = self.mapView
        if self.previousCoordinate != nil {
            self.driverMarker?.rotation = self.getBearingBetweenTwoPoints1(point1: CLLocation(latitude: currentLat, longitude: currentLong), point2: CLLocation(latitude: self.previousCoordinate?.latitude ?? 0, longitude: self.previousCoordinate?.longitude ?? 0))
//            print("************\(self.driverMarker?.rotation)")
            self.pathFromStartToEndPoint(startLat: currentLat, startLong: currentLong, endLat: (self.destinationLatLng?.latitude ?? 0.0), endLong: (self.destinationLatLng?.longitude ?? 0.0), isLoader: false)
        }else{
            self.pathFromStartToEndPoint(startLat: currentLat, startLong: currentLong, endLat: (self.destinationLatLng?.latitude ?? 0.0), endLong: (self.destinationLatLng?.longitude ?? 0.0), isLoader: false)
        }
        self.previousCoordinate = coordinate1
    }
    
    func trackingMultiple(currentLat: Double, currentLong: Double) {
        let coordinate1 = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLong)
        self.mapView.isMyLocationEnabled = false
        self.driverMarker?.position = coordinate1
        self.driverMarker?.icon = #imageLiteral(resourceName: "car_bottom")
        self.driverMarker?.map = self.mapView
        if self.previousCoordinate != nil {
            self.driverMarker?.rotation = self.getBearingBetweenTwoPoints1(point1: CLLocation(latitude: currentLat, longitude: currentLong), point2: CLLocation(latitude: self.previousCoordinate?.latitude ?? 0, longitude: self.previousCoordinate?.longitude ?? 0))
            self.pathFromStartToEndPointMultiple(startLat: currentLat, startLong: currentLong, endLocations: self.destinationMulipleLatLng, isLoader: false)
        }else{
            self.pathFromStartToEndPointMultiple(startLat: currentLat, startLong: currentLong, endLocations: self.destinationMulipleLatLng, isLoader: false)
        }
        self.previousCoordinate = coordinate1
    }
}

extension TrackDriverLoactionMapVC{
    func getPolylineRouteForMultiples(from source: CLLocationCoordinate2D, to destinations: [CLLocationCoordinate2D], completionHandler: @escaping (Bool, String, String, String) -> ()) {
        
        guard let destination = destinations.last else {
            return
        }
        var wayPoints = ""
        for (_, point) in destinations.enumerated() {
//            if index == 0 { // Skipping first location that is current location.
//                continue
//            }
            wayPoints = wayPoints.count == 0 ? "\(point.latitude),\(point.longitude)" : "\(wayPoints)%7C\(point.latitude),\(point.longitude)"
        }

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&waypoints=\(wayPoints)&key=\(GOOGLEAPIKEY)")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Failed : \(String(describing: error?.localizedDescription))")
                return
            } else {
                do {
                    if let json: [String: Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] {
                        guard let routes = json["routes"] as? [[String: Any]] else { return }
                        if (routes.count > 0) {
                            let overview_polyline = routes[0]
                            let dictPolyline = overview_polyline["overview_polyline"] as? NSDictionary
                            let points = dictPolyline?.object(forKey: "points") as? String
                            let legsArr = overview_polyline["legs"] as? [Any]
                            let legfirstDict = legsArr?.first as? [String:Any]
                            let distanceDict = legfirstDict?["distance"]as?[String:Any]
                            let distanceValue = distanceDict?["text"]as?String
                            var pointss = ""
                            var time = ""
                            var timeTemp = 0
                            for kData in overview_polyline["legs"] as! [Any]{
                                let nData = kData as! [String:Any]
                                for pData in nData["steps"] as! [[String:Any]]{
                                    let poData = pData
                                    let plData = poData["polyline"] as! [String:Any]
                                    let ptData = plData["points"] as! String
                                    pointss.append(ptData)
                                }
                                let durationtemp = nData["duration"]as?[String:Any]
                                let xDataTime = durationtemp?["value"]as?Int
                                timeTemp+=xDataTime ?? 0
                                print("TimeCheck", timeTemp )
                            }
                            time = "\(CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: timeTemp))"
                            completionHandler(true, points ?? "", distanceValue ?? "", time)
                        } else {
                            completionHandler(false, "", "", "")
                        }
                    }
                } catch {
                    print("Error : \(error)")
                }
            }
        }
        task.resume()
    }
}
