//
//  NotePopUpVc.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit

enum NotePopUpVcScreentype{
    case notePopUp, EmailVerification, TraxiCredits, cancelCart
}

class NotePopUpVc: BasePopUp {

    //MARK:- Outlets
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var innerView: cornerRadiusView!
    @IBOutlet weak var topViewCorners: UIView!
    @IBOutlet weak var bottomViewCorners: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    
    //MARK:- Varaibles
    var catType : Int! //1-SINGLE Delivery, 2-Return Delivery, 3-TRAXI RUN, 4-cooperate delivery
    var sType : NotePopUpVcScreentype = .notePopUp
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewDidLoadData()
    }
    
    //MARK:- Button Actions
    @IBAction func btnActCancel(_ sender: Any) {
        stopAnimation()
    }
    
    @IBAction func btnActOk(_ sender: Any) {
        switch sType {
        case .notePopUp:
            if catType == 1{
                let vc = AddCourierDetailVC.instantiateFromAppStoryboard(appStoryboard: .Home)
                vc.sType = .singleType
                self.navigationController?.pushViewController(vc, animated: true)
            }else if catType ==  2{
                let vc = AddCourierDetailVC.instantiateFromAppStoryboard(appStoryboard: .Home)
                vc.sType = .returnType
                self.navigationController?.pushViewController(vc, animated: true)
            }else if catType ==  3{
                let vc = AddCourierDetailVC.instantiateFromAppStoryboard(appStoryboard: .Home)
                vc.sType = .multipleDelivery
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = ContactUsVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
                vc.sType = .CorporateDelivery
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case .EmailVerification:
            self.ResentEmailApiHit()
        case .TraxiCredits:
            let vc = TraxiCreditsVC.instantiateFromAppStoryboard(appStoryboard: .Payment)
            vc.fromOrderScreen = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .cancelCart:
            self.navigationController?.popViewController(animated: true)
        }
        stopAnimation()
    }
}


//MARK:- SetViewDidLoadData
extension NotePopUpVc{
    func setViewDidLoadData(){
        outerVw = outerView
        innerVw = innerView
        showAnimate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.topViewCorners.roundCorners([.topRight, .topLeft], radius: 4)
            self.bottomViewCorners.roundCorners([.bottomRight, .bottomLeft], radius: 4)
        }
        switch sType {
        case .notePopUp:
            lblTitle.text = "Please Note"
            lblDescription.text = "Delivery Package(s) must fit inside a standard vehicle."
            btnOk.setTitle("OK", for: .normal)
            btnCancel.isHidden = true
        case .EmailVerification:
            lblTitle.text = "Email Verification"
            lblDescription.text = "Please verify your email so that you may get discount coupons and updates from Traxi.\nThanks"
            btnOk.setTitle("SEND LINK", for: .normal)
            btnCancel.isHidden = false
        case .TraxiCredits:
            lblTitle.text = "Traxi Credits"
            lblDescription.text = "Your Traxi Credit balance is low. Please add Credits."
            btnOk.setTitle("OK", for: .normal)
            btnCancel.isHidden = false
        case .cancelCart:
            lblTitle.text = "Cancel"
            lblDescription.text = "Are you sure you want to cancel order"
            btnOk.setTitle("OK", for: .normal)
            btnCancel.isHidden = false
        }
    }
}

extension NotePopUpVc{
    //MARK:- Hit Api
    func ResentEmailApiHit(){
        let paarams : [String : String] = [:]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.VerificationEmailSendLink, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            CommonFunctions.hideLoader()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.POST, img: nil, imageParamater: "", headerPresent: true)
    }
}
