//
//  DeliveryTypeVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit

class DeliveryTypeVC: baseVC {
    
    //MARK:- Outlets
    
    
    //MARK:- Variables

    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnActSingleDelivery(_ sender: Any) {
        let popOverVC = NotePopUpVc()
        popOverVC.catType = 1
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    @IBAction func btnActReturnDelivery(_ sender: Any) {
        let popOverVC = NotePopUpVc()
        popOverVC.catType = 2
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    @IBAction func btnActTraxiRun(_ sender: Any) {
        let popOverVC = NotePopUpVc()
        popOverVC.catType = 3
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    @IBAction func btnActCooperateDelivery(_ sender: Any) {
        let popOverVC = NotePopUpVc()
        popOverVC.catType = 4
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
}
