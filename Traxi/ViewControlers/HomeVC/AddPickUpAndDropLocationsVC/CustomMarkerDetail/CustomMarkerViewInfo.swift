//
//  CustomMarkerViewInfo.swift
//  iclsProject
//
//  Created by Bhanu Dhiman on 23/06/20.
//  Copyright © 2020 Bhanu Dhiman. All rights reserved.
//

import UIKit

class CustomMarkerViewInfo: UIView {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnSendDetailScreen: UIButton!
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var imgRightArrow: UIImageView!
    @IBOutlet weak var vwColor: setCornerRadiusCircle!
    
    override init(frame: CGRect) {
     super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
     super.init(coder: aDecoder)
    }
    
    func loadView() -> CustomMarkerViewInfo{
     var customInfoWindow = Bundle.main.loadNibNamed("CustomMarkerViewInfo", owner: self, options: nil)?[0] as! CustomMarkerViewInfo
     return customInfoWindow
    }

}
