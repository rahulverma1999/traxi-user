//
//  AddPickUpLoactionVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 02/12/20.
//

import UIKit
import GrowingTextView
import GoogleMaps
import GooglePlaces

//MARK:-  Sacreen type Enum
enum Button_Handler {
    case None
    case home
    case work
    case other
}

enum AddPickUpLoactionVCScreentyep {
    case pickUpLocation, DropLocation, pickUpReturn, DropReturn, homelocation, shopeByCat, StoreNearYou, storeDetail, cartDetail
}

protocol AddPickUpLoactionVCDelegate : class {
    func getPichUpData(_ data : PickLoactions)
    func getDropLoaction(_ data : DropLoactions)
    func getPichUpDataReturn(_ data : PickLoactionsReturn)
    func getDropLoactionReturn(_ data : DropLoactionsReturn)
}

class AddPickUpLoactionVC: UIViewController {

    //MARK:- OUTLETS
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tfName: paddingLeftSide!
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var tfApartmentNumber: paddingLeftSide!
    @IBOutlet weak var tfLandMark: paddingLeftSide!
    @IBOutlet weak var btnWork: UIButton!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var twProvideNameForAddress: UITextView!
    @IBOutlet weak var tfPhoneNumber: paddingLeftSide!
    @IBOutlet weak var btnSaveAndContinue: LoadingButton!
    @IBOutlet weak var vwTags: UIView!
    @IBOutlet weak var VwAddressname: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwLandMark: cornerRadiusView!
    @IBOutlet weak var imgLoaction: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:- VARIABLES
    var btnhandler : Button_Handler = .None
    var sType : AddPickUpLoactionVCScreentyep = .pickUpLocation
    var marker : GMSMarker!
    var addedLocation : loc?
    var customInfoWindow : CustomMarkerViewInfo?
    var getMarker : GMSMarker!
    var tagId : Int!
    weak var delegate : AddPickUpLoactionVCDelegate?
    
    var dataFilledPickUp : PickLoactions?
    var dataFilledDropUp : DropLoactions?
    
    var dataFilledPickUpReturn : PickLoactionsReturn?
    var dataFilledDropUpReturn : DropLoactionsReturn?
    
    var notPopWork = false
    var fromHomeLocation = false
    var fromShopeByCat = false
    var fromStoreNearYou = false
    var fromStoreDetail = false
    var formCartDetail = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
        btnDisplayHandle()
        setMapSetting()
        setEditData()
        self.customInfoWindow = CustomMarkerViewInfo().loadView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if addedLocation != nil{
            self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: addedLocation?.lat ?? 0.0, longitude: addedLocation?.lng ?? 0.0))
            self.marker = GMSMarker(position: CLLocationCoordinate2D.init(latitude: addedLocation?.lat ?? 0.0, longitude: addedLocation?.lng ?? 0.0))
            self.marker.map = self.mapView
            self.mapView.delegate = self
            self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: addedLocation?.lat ?? 0.0, longitude: addedLocation?.lng ?? 0.0))
            if self.sType == .pickUpLocation{
                self.marker.icon = UIImage(named: "pickup_pin")
            }else{
                self.marker.icon = UIImage(named: "drop_off_pin")
            }
            self.marker.title = addedLocation?.fullAddress ?? ""
            
            DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
                self.setCustomMarkerView()
            })
        }
    }

    //MARK:- BUTTON ACTIONS
    @IBAction func btnActBack(_ sender: Any) {
//        if notPopWork{
//            let allVcs = self.navigationController!.viewControllers
//            for vc in allVcs{
//                if vc.isKind(of: AddCourierDetailVC.self){
//                    self.navigationController?.popToViewController(vc, animated: false)
//                }
//            }
//        }else{
            self.navigationController?.popViewController(animated: true)
//        }
    }
    
    @IBAction func btnActWork(_ sender: Any) {
        if btnhandler == .work{
            btnhandler = .None
        }else{
            btnhandler = .work
        }
        btnDisplayHandle()
    }
    
    @IBAction func btnActHome(_ sender: Any) {
        if btnhandler == .home{
            btnhandler = .None
        }else{
            btnhandler = .home
        }
        btnDisplayHandle()
    }
    
    @IBAction func btnActOther(_ sender: Any) {
        if btnhandler == .other{
            btnhandler = .None
        }else{
            btnhandler = .other
        }
        btnDisplayHandle()
    }
    
    @IBAction func btnActSaveAndContinue(_ sender: Any) {
        if tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfName.shake()
            CommonFunctions.toster("Please enter name")
        }else if tfPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfPhoneNumber.shake()
            CommonFunctions.toster("Please enter phone number")
        }else if tagId == nil{
            vwTags.shake()
            CommonFunctions.toster("Please select tag as")
        }else{
            self.AddAddressApiHit()
        }
    }
}

extension AddPickUpLoactionVC: UITextFieldDelegate{
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if tfPhoneNumber == textField{
            return newString.length <= 15
        }else{
            return newString.length <= 30
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case tfName:
            tfPhoneNumber.becomeFirstResponder()
        case tfPhoneNumber:
            tfApartmentNumber.becomeFirstResponder()
        case tfApartmentNumber:
            tfLandMark.becomeFirstResponder()
        case tfLandMark:
            tfLandMark.resignFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
}

extension AddPickUpLoactionVC{
    //MARK:- set viewDidLoadData
    func setUpUi(){
        btnBack.layer.cornerRadius = btnBack.bounds.height/2
        btnBack.clipsToBounds = true
        btnBack.layer.masksToBounds = false
        btnBack.layer.shadowColor = UIColor.black.cgColor
        btnBack.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//            imgs.layer.shadowPath = shadowPath.cgPath
        btnBack.layer.shadowOpacity = 0.5
        btnBack.layer.shadowRadius = 5.0
        
        tfName.openLeftViewWithImagenew(UIImage(named: "name")!, height: tfName.frame.height)
        tfPhoneNumber.openLeftViewWithImagenew(UIImage(named: "phone_number")!, height: tfPhoneNumber.frame.height)
        tfLandMark.openLeftViewWithImagenew(UIImage(named: "landmark")!, height: tfLandMark.frame.height)
        tfApartmentNumber.openLeftViewWithImagenew(UIImage(named: "apartment")!, height: tfApartmentNumber.frame.height)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.vwCorner.roundCorners([.topRight, .topLeft], radius: 24)
        }
        scrollView.layer.cornerRadius = 24
        
        twProvideNameForAddress.textContainerInset = UIEdgeInsets(top: 8,left: 0,bottom: 8,right: 0)
        
        switch sType {
        case .pickUpLocation:
            lblTitle.text = "Add Pickup Details"
            imgLoaction.image = UIImage(named: "add_pickup_details")
        case .DropLocation:
            lblTitle.text = "Add Drop Off Details"
            imgLoaction.image = UIImage(named: "add_drop_details")
        case .pickUpReturn:
            lblTitle.text = "Add Pickup Details"
            imgLoaction.image = UIImage(named: "add_pickup_details")
        case .DropReturn:
            lblTitle.text = "Add Drop Off Details"
//            vwTags.isHidden = true
//            VwAddressname.isHidden = true
//            vwLandMark.isHidden = true
            imgLoaction.image = UIImage(named: "add_drop_details")
        case .homelocation:
            lblTitle.text = "Add Your Location"
            imgLoaction.image = UIImage(named: "add_pickup_details")
        case .shopeByCat:
            lblTitle.text = "Add Your Location"
            imgLoaction.image = UIImage(named: "add_pickup_details")
        case .StoreNearYou:
            lblTitle.text = "Add Your Location"
            imgLoaction.image = UIImage(named: "add_pickup_details")
        case .storeDetail:
            lblTitle.text = "Add Your Location"
            imgLoaction.image = UIImage(named: "add_pickup_details")
        case .cartDetail:
            lblTitle.text = "Add Your Location"
            imgLoaction.image = UIImage(named: "add_pickup_details")
        }
        self.VwAddressname.isHidden = true
        setscrollViewHeight()
        
        self.tfName.text = coredata.shared.name
        self.tfPhoneNumber.text = coredata.shared.phone_no
    }
    
    func setEditData(){
        //Signle Delivery Data
        print("FilledPickUp",dataFilledPickUp ?? "")
        print("FilledDropUp",dataFilledDropUp ?? "")
        if dataFilledPickUp != nil{
            tfName.text = dataFilledPickUp?.Name ?? ""
            tfPhoneNumber.text = dataFilledPickUp?.phoneNumber ?? ""
            tfApartmentNumber.text = dataFilledPickUp?.Appartment ?? ""
            tfLandMark.text = dataFilledPickUp?.landmark ?? ""
            twProvideNameForAddress.text = dataFilledPickUp?.otherAddress ?? ""
            if dataFilledPickUp?.tagId == 2{
                if btnhandler == .work{
                    btnhandler = .None
                }else{
                    btnhandler = .work
                }
                btnDisplayHandle()
            }else if dataFilledPickUp?.tagId == 1{
                if btnhandler == .home{
                    btnhandler = .None
                }else{
                    btnhandler = .home
                }
                btnDisplayHandle()
            }else{
                if btnhandler == .other{
                    btnhandler = .None
                }else{
                    btnhandler = .other
                }
                btnDisplayHandle()
            }
        }
        if dataFilledDropUp != nil{
            tfName.text = dataFilledDropUp?.Name ?? ""
            tfPhoneNumber.text = dataFilledDropUp?.phoneNumber ?? ""
            tfApartmentNumber.text = dataFilledDropUp?.Appartment ?? ""
            tfLandMark.text = dataFilledDropUp?.landmark ?? ""
            twProvideNameForAddress.text = dataFilledDropUp?.otherAddress ?? ""
            if dataFilledDropUp?.tagId == 2{
                if btnhandler == .work{
                    btnhandler = .None
                }else{
                    btnhandler = .work
                }
                btnDisplayHandle()
            }else if dataFilledDropUp?.tagId == 1{
                if btnhandler == .home{
                    btnhandler = .None
                }else{
                    btnhandler = .home
                }
                btnDisplayHandle()
            }else{
                if btnhandler == .other{
                    btnhandler = .None
                }else{
                    btnhandler = .other
                }
                btnDisplayHandle()
            }
        }
        
        //Return Delivery Data
        print("FilledPickUpReturn",dataFilledPickUpReturn ?? "")
        print("FilledDropUpReturn",dataFilledDropUpReturn ?? "")
        if dataFilledPickUpReturn != nil{
            tfName.text = dataFilledPickUpReturn?.Name ?? ""
            tfPhoneNumber.text = dataFilledPickUpReturn?.phoneNumber ?? ""
            tfApartmentNumber.text = dataFilledPickUpReturn?.Appartment ?? ""
            tfLandMark.text = dataFilledPickUpReturn?.landmark ?? ""
            twProvideNameForAddress.text = dataFilledPickUpReturn?.otherAddress ?? ""
            if dataFilledPickUpReturn?.tagId == 2{
                if btnhandler == .work{
                    btnhandler = .None
                }else{
                    btnhandler = .work
                }
                btnDisplayHandle()
            }else if dataFilledPickUpReturn?.tagId == 1{
                if btnhandler == .home{
                    btnhandler = .None
                }else{
                    btnhandler = .home
                }
                btnDisplayHandle()
            }else{
                if btnhandler == .other{
                    btnhandler = .None
                }else{
                    btnhandler = .other
                }
                btnDisplayHandle()
            }
        }
        
        if dataFilledDropUpReturn != nil{
            tfName.text = dataFilledDropUpReturn?.Name ?? ""
            tfPhoneNumber.text = dataFilledDropUpReturn?.phoneNumber ?? ""
            tfApartmentNumber.text = dataFilledDropUpReturn?.Appartment ?? ""
            tfLandMark.text = dataFilledDropUpReturn?.landmark ?? ""
            twProvideNameForAddress.text = dataFilledDropUpReturn?.otherAddress ?? ""
            if dataFilledDropUpReturn?.tagId == 2{
                if btnhandler == .work{
                    btnhandler = .None
                }else{
                    btnhandler = .work
                }
                btnDisplayHandle()
            }else if dataFilledDropUpReturn?.tagId == 1{
                if btnhandler == .home{
                    btnhandler = .None
                }else{
                    btnhandler = .home
                }
                btnDisplayHandle()
            }else{
                if btnhandler == .other{
                    btnhandler = .None
                }else{
                    btnhandler = .other
                }
                btnDisplayHandle()
            }
        }
    }
    
    func setscrollViewHeight(){
        DispatchQueue.main.async {
            self.scrollViewHeight.constant = self.view.bounds.height/2-10
            
        }
    }
    
    //MARK:- Buttons unSelected
    func btnUnSelected(btns : [UIButton]){
        for btn in btns{
            btn.layer.backgroundColor = UIColor.white.cgColor
            btn.layer.cornerRadius = 16.0
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.clipsToBounds = true
            btn.layer.masksToBounds = false
            btn.layer.shadowColor = UIColor.black.cgColor
            btn.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            btn.layer.shadowOpacity = 0.3
            btn.layer.shadowRadius = 5.0
        }
    }
    
    //MARK:- Buttons Selected
    func btnSelected(btns : [UIButton]){
        for btn in btns{
            btn.layer.backgroundColor = Colors.traxiBlueColor.color().cgColor
            btn.layer.cornerRadius = 16.0
            btn.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
}

extension AddPickUpLoactionVC{
    //MARK:- handle buttons
    func btnDisplayHandle(){
        var shouldHide = false
        switch self.btnhandler{
        case .None:
            self.VwAddressname.isHidden = true
            self.btnUnSelected(btns: [self.btnHome, self.btnWork, self.btnOther])
            shouldHide = true
        case .home:
            self.tagId = 1
            self.VwAddressname.isHidden = true
            self.btnUnSelected(btns: [self.btnWork, self.btnOther])
            self.btnSelected(btns: [self.btnHome])
            shouldHide = true
        case .work:
            self.tagId = 2
            self.VwAddressname.isHidden = true
            self.btnUnSelected(btns: [self.btnHome, self.btnOther])
            self.btnSelected(btns: [self.btnWork])
            shouldHide = true
        case .other:
            self.tagId = 3
            self.VwAddressname.isHidden = false
            self.btnUnSelected(btns: [self.btnHome, self.btnWork])
            self.btnSelected(btns: [self.btnOther])
            shouldHide = true
        }
        self.setscrollViewHeight()
        print(shouldHide)
    }
    
    //MARK:- Set map setting
    func setMapSetting(){
        mapView.animate(toZoom: 15)
        mapView.settings.zoomGestures = true
        mapView.settings.rotateGestures = true
        mapView.settings.scrollGestures = true
        mapView.isUserInteractionEnabled = true
        if addedLocation == nil{
            LocationServices.shared.getAdress { (valueName, stringError) in
                if valueName != nil {
                    self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0.0, longitude: valueName?.location?.coordinate.longitude ?? 0.0))
                    self.marker = GMSMarker(position: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0.0, longitude: valueName?.location?.coordinate.longitude ?? 0.0))
                    self.marker.map = self.mapView
                    self.mapView.delegate = self
                    self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0.0, longitude: valueName?.location?.coordinate.longitude ?? 0.0))
                    if self.sType == .pickUpLocation{
                        self.marker.icon = UIImage(named: "pickup_pin")
                    }else if self.sType == .homelocation{
                        self.marker.icon = UIImage(named: "pickup_pin")
                    }else{
                        self.marker.icon = UIImage(named: "drop_off_pin")
                    }
                    self.marker.title = "\(valueName?.name ?? ""),\(valueName?.subAdministrativeArea ?? ""),\(valueName?.locality ?? ""),\(valueName?.administrativeArea ?? "")"
                    
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
                        self.setCustomMarkerView()
                    })
                    self.setLoca(valueName!)

                 }else{
                 }
            }
        }
    }
    
    func setLoca(_ valueName : CLPlacemark){
        let loca = loc.init(valueName.location?.coordinate.latitude ?? 0, lng: valueName.location?.coordinate.longitude ?? 0, name: valueName.subAdministrativeArea ?? "", fullAddress: "\(valueName.name ?? ""), \(valueName.subAdministrativeArea ?? ""),\(valueName.locality ?? ""), \(valueName.administrativeArea ?? ""), \(valueName.country ?? "")")
        self.addedLocation = loca
    }
    
    func setCustomMarkerView(){
        getMarker = self.marker
        self.customInfoWindow?.lblStatus.text = self.marker.title
        self.customInfoWindow?.btnSendDetailScreen.addTarget(self, action: #selector(self.btnActSendDetailScreen(_:)), for: .touchUpInside)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            self.customInfoWindow?.vwCorner.layer.cornerRadius = 4.0
        })
        switch self.sType {
        case .pickUpLocation:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
        case .DropLocation:
            self.customInfoWindow?.lblName.textColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiBlueColor.color()
        case .pickUpReturn:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
        case .DropReturn:
            self.customInfoWindow?.lblName.textColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiBlueColor.color()
        case .homelocation:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .shopeByCat:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .StoreNearYou:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .storeDetail:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .cartDetail:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        }
        DispatchQueue.main.async {
            let position = self.marker.position
            self.customInfoWindow?.center = self.mapView.projection.point(for: position)
            self.customInfoWindow?.center.y = (self.customInfoWindow?.center.y ?? 0)-86
            self.customInfoWindow?.center.x = (self.customInfoWindow?.center.x ?? 0)
        }
        self.customInfoWindow?.frame.size.height = 54
        self.customInfoWindow?.frame.size.width = 180
        self.mapView.addSubview(self.customInfoWindow!)
    }
    
    @objc func btnActSendDetailScreen(_ sender: UIButton){
       
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        switch sType {
        case .pickUpLocation:
            vc.isPickupLocation = true
            vc.sType = .PikUpSingle
        case .DropLocation:
            vc.sType = .PikUpSingle
            vc.isPickupLocation = false
        case .pickUpReturn:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = true
        case .DropReturn:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = false
        case .homelocation:
            vc.isPickupLocation = true
            vc.sType = .HomeLocationSelect
        case .shopeByCat:
            vc.isPickupLocation = true
            vc.sType = .HomeLocationSelect
        case .StoreNearYou:
            vc.isPickupLocation = true
            vc.sType = .HomeLocationSelect
        case .storeDetail:
            vc.isPickupLocation = true
            vc.sType = .HomeLocationSelect
        case .cartDetail:
            vc.isPickupLocation = true
            vc.sType = .HomeLocationSelect
        }
        self.navigationController?.pushViewController(vc, animated: true)
        tfLandMark.resignFirstResponder()
    }
}

//MARK:- Map delegate methods
extension AddPickUpLoactionVC : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        getMarker = self.marker
        customInfoWindow?.lblStatus.text = self.marker.title
        customInfoWindow?.btnSendDetailScreen.addTarget(self, action: #selector(btnActSendDetailScreen(_:)), for: .touchUpInside)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            self.customInfoWindow?.vwCorner.layer.cornerRadius = 4.0
        })
        switch self.sType {
        case .pickUpLocation:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Pick Up"
        case .DropLocation:
            self.customInfoWindow?.lblName.textColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.lblName.text = "Your Drop Off"
        case .pickUpReturn:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Pick Up"
        case .DropReturn:
            self.customInfoWindow?.lblName.textColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiBlueColor.color()
            self.customInfoWindow?.lblName.text = "Your Drop Off"
        case .homelocation:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .shopeByCat:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .StoreNearYou:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .storeDetail:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        case .cartDetail:
            self.customInfoWindow?.lblName.textColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.vwColor.backgroundColor = Colors.traxiGreenColor.color()
            self.customInfoWindow?.lblName.text = "Your Location"
        }
        DispatchQueue.main.async {
            let position = self.marker.position
            self.customInfoWindow?.center = self.mapView.projection.point(for: position)
            self.customInfoWindow?.center.y = (self.customInfoWindow?.center.y ?? 0)-86
            self.customInfoWindow?.center.x = (self.customInfoWindow?.center.x ?? 0)
        }
        self.customInfoWindow?.frame.size.height = 54
        self.customInfoWindow?.frame.size.width = 180
        self.mapView.addSubview(self.customInfoWindow!)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return self.customInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        customInfoWindow?.removeFromSuperview()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (marker != nil){
            guard let location = getMarker?.position else {
                print("locationMarker is nil")
                return
            }
            customInfoWindow?.center = self.mapView.projection.point(for: location)
            customInfoWindow?.center.y = (customInfoWindow?.center.y ?? 0)-86
            customInfoWindow?.center.x = (customInfoWindow?.center.x ?? 0)
        }
    }
}

//MARK:- Api Hit
extension AddPickUpLoactionVC{
    func AddAddressApiHit(){
        var paarams : [String : AnyObject] = [
            :
        ]
        switch sType {
        case .pickUpLocation:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .DropLocation:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .pickUpReturn:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .DropReturn:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .homelocation:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .shopeByCat:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .StoreNearYou:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .storeDetail:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        case .cartDetail:
            paarams["name"] = tfName.text as AnyObject
            paarams["phone"] = tfPhoneNumber.text as AnyObject
            paarams["apartment"] = tfApartmentNumber.text as AnyObject
            paarams["landmark"] = tfLandMark.text as AnyObject
            paarams["lat"] = addedLocation?.lat as AnyObject
            paarams["lng"] = addedLocation?.lng as AnyObject
            paarams["address"] = addedLocation?.fullAddress as AnyObject
            if self.tagId != nil{
                paarams["tag_id"] = tagId as AnyObject
            }
            if self.twProvideNameForAddress.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                paarams["other_address"] = twProvideNameForAddress.text as AnyObject
            }
        }
        var tempDataPickUp : PickLoactions!
        var tempAddressDrop : DropLoactions!
        var tempDataPickUpReturn : PickLoactionsReturn!
        var tempAddressDropReturn : DropLoactionsReturn!
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.addAddress, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            CommonFunctions.hideLoader()
            locationTagType = self.tagId
            switch self.sType{
            case .pickUpLocation:
                tempDataPickUp = PickLoactions.init(fullAddress: self.addedLocation?.fullAddress ?? "", Name: self.tfName.text ?? "", phoneNumber: self.tfPhoneNumber.text ?? "", Appartment: self.tfApartmentNumber.text ?? "", landmark: self.tfLandMark.text ?? "", lat : self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, tagId: self.tagId, otherAddress: self.twProvideNameForAddress.text ?? "", id: UUID().uuidString)
                self.delegate?.getPichUpData(tempDataPickUp)
            case .DropLocation:
                var isEditAddress = false
                if self.dataFilledDropUp?.id != nil && self.dataFilledDropUp?.id != ""{
                    isEditAddress = true
                }
                tempAddressDrop = DropLoactions.init(fullAddress: self.addedLocation?.fullAddress ?? "", Name: self.tfName.text ?? "", phoneNumber: self.tfPhoneNumber.text ?? "", Appartment: self.tfApartmentNumber.text ?? "", landmark: self.tfLandMark.text ?? "", tagId: self.tagId, lat: self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, otherAddress: self.twProvideNameForAddress.text ?? "", id: isEditAddress ? self.dataFilledDropUp?.id : UUID().uuidString)
                self.delegate?.getDropLoaction(tempAddressDrop)
            case .pickUpReturn:
                tempDataPickUpReturn = PickLoactionsReturn.init(fullAddress: self.addedLocation?.fullAddress ?? "", Name: self.tfName.text ?? "", phoneNumber: self.tfPhoneNumber.text ?? "", Appartment: self.tfApartmentNumber.text ?? "", landmark: self.tfLandMark.text ?? "", lat : self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, tagId: self.tagId, otherAddress: self.twProvideNameForAddress.text ?? "", id: UUID().uuidString)
                self.delegate?.getPichUpDataReturn(tempDataPickUpReturn)
            case .DropReturn:
                tempAddressDropReturn = DropLoactionsReturn.init(fullAddress: self.addedLocation?.fullAddress ?? "", Name: self.tfName.text ?? "", phoneNumber: self.tfPhoneNumber.text ?? "", Appartment: self.tfApartmentNumber.text ?? "", landmark: self.tfLandMark.text ?? "", tagId: self.tagId, lat: self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, otherAddress: self.twProvideNameForAddress.text ?? "", id: UUID().uuidString)
                self.delegate?.getDropLoactionReturn(tempAddressDropReturn)
            case .homelocation:
                let loca = locNew.init(self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, name: self.addedLocation?.fullAddress ?? "", fullAddress: self.addedLocation?.fullAddress ?? "", Appartment: self.tfApartmentNumber.text ?? "", landMark: self.tfLandMark.text ?? "")
                globalAddedLocation = loca
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            case .shopeByCat:
                let loca = locNew.init(self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, name: self.addedLocation?.fullAddress ?? "", fullAddress: self.addedLocation?.fullAddress ?? "", Appartment: self.tfApartmentNumber.text ?? "", landMark: self.tfLandMark.text ?? "")
                globalAddedLocation = loca
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            case .StoreNearYou:
                let loca = locNew.init(self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, name: self.addedLocation?.fullAddress ?? "", fullAddress: self.addedLocation?.fullAddress ?? "", Appartment: self.tfApartmentNumber.text ?? "", landMark: self.tfLandMark.text ?? "")
                globalAddedLocation = loca
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            case .storeDetail:
                let loca = locNew.init(self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, name: self.addedLocation?.fullAddress ?? "", fullAddress: self.addedLocation?.fullAddress ?? "", Appartment: self.tfApartmentNumber.text ?? "", landMark: self.tfLandMark.text ?? "")
                globalAddedLocation = loca
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            case .cartDetail:
                let loca = locNew.init(self.addedLocation?.lat ?? 0.0, lng: self.addedLocation?.lng ?? 0.0, name: self.addedLocation?.fullAddress ?? "", fullAddress: self.addedLocation?.fullAddress ?? "", Appartment: self.tfApartmentNumber.text ?? "", landMark: self.tfLandMark.text ?? "")
                globalAddedLocation = loca
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            }
            if self.notPopWork{
                let allVcs = self.navigationController!.viewControllers
                if self.fromHomeLocation{
                    for vc in allVcs{
                        if vc.isKind(of: HomeVC.self){
                            self.navigationController?.popToViewController(vc, animated: false)
                        }
                    }
                }else if self.fromShopeByCat{
                    for vc in allVcs{
                        if vc.isKind(of: ShopCategoriesVC.self){
                            self.navigationController?.popToViewController(vc, animated: false)
                        }
                    }
                }else if self.fromStoreNearYou{
                    for vc in allVcs{
                        if vc.isKind(of: StoreNearYouVC.self){
                            self.navigationController?.popToViewController(vc, animated: false)
                        }
                    }
                }else if self.formCartDetail{
                    for vc in allVcs{
                        if vc.isKind(of: CartDetailVC.self){
                            self.navigationController?.popToViewController(vc, animated: false)
                        }
                    }
                }else{
                    for vc in allVcs{
                        if vc.isKind(of: AddCourierDetailVC.self){
                            let kVC = vc as! AddCourierDetailVC
                            switch self.sType {
                            case .pickUpLocation:
                                kVC.pickLocationStrct = tempDataPickUp
                            case .DropLocation:
                                if kVC.sType == .multipleDelivery{
                                    kVC.dropLoactionsStruct.append(tempAddressDrop)
                                }else{
                                    kVC.dropLoactionsStruct.removeAll()
                                    kVC.dropLoactionsStruct.append(tempAddressDrop)
                                }
                            case .pickUpReturn:
                                kVC.pickLocationStrctReturn = tempDataPickUpReturn
                            case .DropReturn:
                                kVC.dropLoactionsStructReturn = tempAddressDropReturn
                            case .homelocation:
                                break
                            case .shopeByCat:
                                break
                            case .StoreNearYou:
                                break
                            case .storeDetail:
                                break
                            case .cartDetail:
                                break
                            }
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetAddressData"), object: nil)
                            self.navigationController?.popToViewController(vc, animated: false)
                        }
                    }
                }
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.POST, img: nil, imageParamater: "", headerPresent: true)
    }
}
