//
//  SelectCategoriesCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 02/12/20.
//

import UIKit

class SelectCategoriesCell: UITableViewCell {

    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
