//
//  AddCourierDetailVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit
import GrowingTextView
import GoogleMaps
import GooglePlaces

enum AddCourierDetailVCScreenType{
    case returnType, singleType, multipleDelivery
}

struct DropLoactions{
    var fullAddress : String?
    var Name : String?
    var phoneNumber : String?
    var Appartment: String?
    var landmark : String?
    var tagId : Int?
    var lat : Double?
    var lng : Double?
    var otherAddress : String?
    var id : String?
}

struct PickLoactions{
    var fullAddress : String?
    var Name : String?
    var phoneNumber : String?
    var Appartment: String?
    var landmark : String?
    var lat : Double?
    var lng : Double?
    var tagId : Int?
    var otherAddress : String?
    var id : String?
}

struct DropLoactionsReturn{
    var fullAddress : String?
    var Name : String?
    var phoneNumber : String?
    var Appartment: String?
    var landmark : String?
    var tagId : Int?
    var lat : Double?
    var lng : Double?
    var otherAddress : String?
    var id : String?
}

struct PickLoactionsReturn{
    var fullAddress : String?
    var Name : String?
    var phoneNumber : String?
    var Appartment: String?
    var landmark : String?
    var lat : Double?
    var lng : Double?
    var tagId : Int?
    var otherAddress : String?
    var id : String?
}

class AddCourierDetailVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var vwDotted: UIView!
    @IBOutlet weak var vwCorner1: UIView!
    @IBOutlet weak var vwCorner2: UIView!
    @IBOutlet weak var vwCorner3: UIView!
    @IBOutlet weak var vwCorner4: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSelectDate: LoadingButton!
    @IBOutlet weak var btnSelectTime: LoadingButton!
    @IBOutlet weak var btnAddPickUpPoint: UIButton!
    @IBOutlet weak var btnAddDropPoint: UIButton!
    @IBOutlet weak var btnSelectCategories: UIButton!
    @IBOutlet weak var tvMsg: GrowingTextView!
    @IBOutlet weak var btnGetStart: LoadingButton!
    @IBOutlet weak var lblPickUpAddress: UILabel!
    @IBOutlet weak var lblDropAddress: UILabel!
    @IBOutlet weak var switchRqTime: UISwitch!
    @IBOutlet weak var imgTimeSensitive: UIImageView!
    @IBOutlet weak var vwHideUnHide: UIView!
    @IBOutlet weak var btnSelectTimeReturn: LoadingButton!
    @IBOutlet weak var btnSelectDateReturn: LoadingButton!
    @IBOutlet weak var btnAddLocationPickupReturn: UIButton!
    @IBOutlet weak var lblPickUpAddressReturn: UILabel!
    @IBOutlet weak var lblDropAddressReturn: UILabel!
    @IBOutlet weak var vwDottedReturn: UIView!
    @IBOutlet weak var btnEditPickLoaction: UIButton!
    @IBOutlet weak var btnEditDropLocation: UIButton!
    @IBOutlet weak var imgPlusePickUp: UIImageView!
    @IBOutlet weak var imgPlusDrop: UIImageView!
    @IBOutlet weak var btnAddpickupLeftConstant: NSLayoutConstraint!
    @IBOutlet weak var btnAddDropLeftConstant: NSLayoutConstraint!
    @IBOutlet weak var vwRqType: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var multipleAddressView: UIView!
    @IBOutlet weak var vwMuliopleLocationLayer: UIView!
    @IBOutlet weak var btnEditReturnPichLocation: UIButton!
    @IBOutlet weak var addImgPickReturnLocation: UIImageView!
    @IBOutlet weak var btnAddPickReturnLocationLeftConstant: NSLayoutConstraint!
    @IBOutlet weak var btnEditDropLocationReturn: UIButton!
    @IBOutlet weak var btnAddReturnDropLoaction: UIButton!
    @IBOutlet weak var imgAddDropreturnLocation: UIImageView!
    @IBOutlet weak var btnAddReturnDropLocationLedtConstant: NSLayoutConstraint!
    
    //MARK:- Variables
    var selectDate = Date()
    var selectDateReturn = Date()
    var sType : AddCourierDetailVCScreenType = .singleType
    var getDataOfCategoriesTitle : [String] = []
    var getDataOfCategoriesId : [Int] = []
    var addedPickUpAddress : String!
    var addedPickUpUserDetail : String!
    var dropLoactionsStruct : [DropLoactions] = []
    var pickLocationStrct : PickLoactions?
    
    var dropLoactionsStructReturn : DropLoactionsReturn?
    var pickLocationStrctReturn : PickLoactionsReturn?
    
    var PickUpDate = ""
    var PickUpTime = ""
    var PickUpDateReturn = ""
    var PickUpTimeReturn = ""
    
    var deliveryCharges : Double! = 0.0
    var TaxesAndCharges : Double!
    var deliveryType = 0
    
    var returnDeliveryTime = ""
    var returnDeliveryTimeValue : Int!
    var distanceForDeliveryReturn = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDataViewDidLoad()
        switch sType{
        case .returnType:
            deliveryType = 2
        case .singleType:
            deliveryType = 1
        case .multipleDelivery:
            deliveryType = 3
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "GetAddressData"), object: nil)
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActSelectDate(_ sender: Any) {
        let popOverVC = datePicker()
        popOverVC.isDate = true
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        popOverVC.delegate = self
    }
    
    @IBAction func btnActSelectTime(_ sender: Any) {
        let popOverVC = datePicker()
        popOverVC.isDate = false
        popOverVC.date = self.selectDate
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        popOverVC.delegate = self
    }
    
    @IBAction func btnActSelectTimeReturn(_ sender: Any) {
        let popOverVC = datePicker()
        popOverVC.isDate = false
        popOverVC.isReturn = true
        popOverVC.dateReturn = self.selectDateReturn
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        popOverVC.delegate = self
    }
    
    @IBAction func btnActSelectDateReturn(_ sender: Any) {
        let popOverVC = datePicker()
        popOverVC.isDate = true
        popOverVC.isReturn = true
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        popOverVC.delegate = self
    }
    
    
    @IBAction func btnActAddPickUpPoint(_ sender: Any) {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sTypeDelivery = .pickup
        switch sType {
        case .singleType:
            vc.isPickupLocation = true
            vc.sType = .PikUpSingle
        case .returnType:
            vc.sType = .PikUpSingle
            vc.isPickupLocation = true
        case .multipleDelivery:
            vc.sType = .PikUpSingle
            vc.isPickupLocation = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
//        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
//        vc.delegate = self
//        vc.dataFilledPickUp = self.pickLocationStrct
//        if pickLocationStrct != nil{
//            let tempLoc = loc.init(self.pickLocationStrct?.lat ?? 0.0, lng: self.pickLocationStrct?.lng ?? 0.0, name: self.pickLocationStrct?.fullAddress ?? "", fullAddress: self.pickLocationStrct?.fullAddress ?? "")
//            vc.addedLocation = tempLoc
//        }
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActAddDropPoint(_ sender: Any) {
//        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
//        vc.sType = .DropLocation
//        if self.dropLoactionsStruct.count > 0{
//            if sType != .multipleDelivery{
//                vc.dataFilledDropUp = self.dropLoactionsStruct[0]
//                let tempLoc = loc.init(self.dropLoactionsStruct[0].lat ?? 0.0, lng: self.dropLoactionsStruct[0].lng ?? 0.0, name: self.dropLoactionsStruct[0].fullAddress ?? "", fullAddress: self.dropLoactionsStruct[0].fullAddress ?? "")
//                vc.addedLocation = tempLoc
//            }
//        }
//        vc.delegate = self
//        self.navigationController?.pushViewController(vc, animated: true)
        switch sType {
        case .singleType:
            let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
            vc.sTypeDelivery = .drop
            vc.isPickupLocation = false
            vc.sType = .PikUpSingle
            self.navigationController?.pushViewController(vc, animated: true)
        case .returnType:
            let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
            vc.sTypeDelivery = .drop
            vc.sType = .PikUpSingle
            vc.isPickupLocation = false
            self.navigationController?.pushViewController(vc, animated: true)
        case .multipleDelivery:
            if self.dropLoactionsStruct.count > 19{
                CommonFunctions.toster("You cann't add more than 20 drop off locations")
            }else{
                let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
                vc.sTypeDelivery = .drop
                vc.sType = .PikUpSingle
                vc.isPickupLocation = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func btnActSelectCategories(_ sender: Any) {
        let vc = SelectCategoriesVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.delegate = self
        vc.sendDataTitle = self.getDataOfCategoriesTitle
        vc.sendDataId = self.getDataOfCategoriesId
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnActGetStart(_ sender: Any) {
        switch sType{
        case .returnType:
            let pickUpDataAndTime = "\(self.PickUpDate) \(self.PickUpTime)"
            let pickUpDataAndTimeReturn = "\(self.PickUpDateReturn) \(self.PickUpTimeReturn)"
            
            let tempPickDate = CommonFunctions.getDateFromStringTraxi6(pickUpDataAndTime)
            let tempPickDateReturn = CommonFunctions.getDateFromStringTraxi6(pickUpDataAndTimeReturn)
            
            let maxDateReturnTime = Calendar.current.date(byAdding: .day, value: 1, to: tempPickDate ?? Date())!
            
            
            if PickUpDate == ""{
                CommonFunctions.toster("Please select pickup date")
            }else if PickUpTime == ""{
                CommonFunctions.toster("Please select pickup time")
            }else if lblPickUpAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please select pickup location")
            }else if getDataOfCategoriesId == []{
                CommonFunctions.toster("Please selct package categories")
//            }else if tvMsg.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
//                CommonFunctions.toster("Please enter message")
            }else if lblDropAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please select delivery location")
            }else if PickUpDateReturn == ""{
                CommonFunctions.toster("Please select return pickup date")
            }else if PickUpTimeReturn == ""{
                CommonFunctions.toster("Please select return pickup time")
            }else if lblPickUpAddressReturn.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please select return pickup location")
            }else if lblDropAddressReturn.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please select return delivery location")
            }else if tempPickDate == tempPickDateReturn{
                CommonFunctions.toster("PickUp time and pickUp return time should not same.")
            }else if (tempPickDateReturn ?? Date()) > maxDateReturnTime{
                CommonFunctions.toster("Return delivery max. of next day from pickUp date.")
            }else{
                AddAddressApiHit()
            }
        case .singleType:
            if PickUpDate == ""{
                CommonFunctions.toster("Please select pickup date")
            }else if PickUpTime == ""{
                CommonFunctions.toster("Please select pickup time")
            }else if lblPickUpAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please select pickup location")
            }else if getDataOfCategoriesId == []{
                CommonFunctions.toster("Please selct package categories")
//            }else if tvMsg.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
//                CommonFunctions.toster("Please enter message")
            }else if lblDropAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please select delivery location")
            }else{
                AddAddressApiHit()
            }
        case .multipleDelivery:
            if PickUpDate == ""{
                CommonFunctions.toster("Please select pickup date")
            }else if PickUpTime == ""{
                CommonFunctions.toster("Please select pickup time")
            }else if lblPickUpAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                CommonFunctions.toster("Please select pickup location")
            }else if getDataOfCategoriesId == []{
                CommonFunctions.toster("Please selct package categories")
//            }else if tvMsg.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
//                CommonFunctions.toster("Please enter message")
            }else if dropLoactionsStruct.count == 0{
                CommonFunctions.toster("Please select drop locations")
            }else if dropLoactionsStruct.count < 2{
                CommonFunctions.toster("Please select minimum 2 drop off points")
            }else{
                AddAddressApiHit()
            }
        }
        
    }
    
    @IBAction func btnActAddLocationPickupReturn(_ sender: Any) {
        
//        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
//        vc.delegate = self
//        vc.sType = .pickUpReturn
//        vc.dataFilledPickUpReturn = self.pickLocationStrctReturn
//        if pickLocationStrctReturn != nil{
//            let tempLoc = loc.init(self.pickLocationStrctReturn?.lat ?? 0.0, lng: self.pickLocationStrctReturn?.lng ?? 0.0, name: self.pickLocationStrctReturn?.fullAddress ?? "", fullAddress: self.pickLocationStrctReturn?.fullAddress ?? "")
//            vc.addedLocation = tempLoc
//        }
//        self.navigationController?.pushViewController(vc, animated: true)
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sTypeDelivery = .pickUpReturn
        switch sType {
        case .singleType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = true
        case .returnType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = true
        case .multipleDelivery:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActAddLocationDropReturn(_ sender: Any) {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sTypeDelivery = .dropReturn
        switch sType {
        case .singleType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = false
        case .returnType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = false
        case .multipleDelivery:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = false
        }
        self.navigationController?.pushViewController(vc, animated: true)
//        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
////        vc.sType = .DropLocation
//        vc.sType = .DropReturn
//        if self.dropLoactionsStructReturn != nil{
//            if sType != .multipleDelivery{
//                vc.dataFilledDropUpReturn = self.dropLoactionsStructReturn
//                let tempLoc = loc.init(self.dropLoactionsStructReturn?.lat ?? 0.0, lng: self.dropLoactionsStructReturn?.lng ?? 0.0, name: self.dropLoactionsStructReturn?.fullAddress ?? "", fullAddress: self.dropLoactionsStructReturn?.fullAddress ?? "")
//                vc.addedLocation = tempLoc
//            }
//        }
//        vc.delegate = self
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActReturnEditPickUpLocation(_ sender: Any) {
        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.delegate = self
        vc.sType = .pickUpReturn
        vc.dataFilledPickUpReturn = self.pickLocationStrctReturn
        if pickLocationStrctReturn != nil{
            let tempLoc = loc.init(self.pickLocationStrctReturn?.lat ?? 0.0, lng: self.pickLocationStrctReturn?.lng ?? 0.0, name: self.pickLocationStrctReturn?.fullAddress ?? "", fullAddress: self.pickLocationStrctReturn?.fullAddress ?? "")
            vc.addedLocation = tempLoc
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActEditReturnDropLocation(_ sender: Any) {
        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .DropReturn
        if self.dropLoactionsStructReturn != nil{
            if sType != .multipleDelivery{
                vc.dataFilledDropUpReturn = self.dropLoactionsStructReturn
                let tempLoc = loc.init(self.dropLoactionsStructReturn?.lat ?? 0.0, lng: self.dropLoactionsStructReturn?.lng ?? 0.0, name: self.dropLoactionsStructReturn?.fullAddress ?? "", fullAddress: self.dropLoactionsStructReturn?.fullAddress ?? "")
                vc.addedLocation = tempLoc
            }
        }
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActEditPickupLoadtion(_ sender: Any) {
        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.delegate = self
        vc.dataFilledPickUp = self.pickLocationStrct
        if pickLocationStrct != nil{
            let tempLoc = loc.init(self.pickLocationStrct?.lat ?? 0.0, lng: self.pickLocationStrct?.lng ?? 0.0, name: self.pickLocationStrct?.fullAddress ?? "", fullAddress: self.pickLocationStrct?.fullAddress ?? "")
            vc.addedLocation = tempLoc
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActEditDropLocation(_ sender: Any) {
        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .DropLocation
        if self.dropLoactionsStruct.count > 0{
            vc.dataFilledDropUp = self.dropLoactionsStruct[0]
            let tempLoc = loc.init(self.dropLoactionsStruct[0].lat ?? 0.0, lng: self.dropLoactionsStruct[0].lng ?? 0.0, name: self.dropLoactionsStruct[0].fullAddress ?? "", fullAddress: self.dropLoactionsStruct[0].fullAddress ?? "")
            vc.addedLocation = tempLoc
        }
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Delegate methods
extension AddCourierDetailVC : SelectCategoriesVCDelegate, AddPickUpLoactionVCDelegate{
    func getPichUpDataReturn(_ data: PickLoactionsReturn) {
        self.pickLocationStrctReturn = data
        lblPickUpAddressReturn.isHidden = false
//        lblPickUpAddressReturn.text = data.fullAddress ?? ""
//        btnAddLocationPickupReturn.setTitle("\(data.Name ?? "") (\(data.phoneNumber ?? "")), \(data.Appartment ?? ""), \(data.landmark ?? "")", for: .normal)
        var fullAddress = ""
        if data.Name != nil && data.Name != ""{
            fullAddress.append(data.Name ?? "")
        }
        if data.phoneNumber != nil && data.phoneNumber != ""{
            fullAddress.append(",\(data.phoneNumber ?? "")")
        }
        if data.Appartment != nil && data.Appartment != ""{
            fullAddress.append(",\(data.Appartment ?? "")")
        }
        if data.landmark != nil && data.landmark != ""{
            fullAddress.append(",\(data.landmark ?? "")")
        }
        lblPickUpAddressReturn.text = fullAddress//"\(data.Name ?? "") (\(data.phoneNumber ?? "")), \(data.Appartment ?? ""), \(data.landmark ?? "")"
        btnAddLocationPickupReturn.setTitle(data.fullAddress ?? "", for: .normal) 
        btnEditReturnPichLocation.isHidden = false
        self.addDotedLayer(View: vwDottedReturn)
        addImgPickReturnLocation.isHidden = true
        btnAddPickReturnLocationLeftConstant.constant = 0
    }
    
    func getDropLoactionReturn(_ data: DropLoactionsReturn) {
        self.dropLoactionsStructReturn = data
//        lblDropAddressReturn.text = data.fullAddress ?? ""
//        btnAddReturnDropLoaction.setTitle("\(data.Name ?? "") (\(data.phoneNumber ?? "")), \(data.Appartment ?? ""), \(data.landmark ?? "")", for: .normal)
        var fullAddress = ""
        if data.Name != nil && data.Name != ""{
            fullAddress.append(data.Name ?? "")
        }
        if data.phoneNumber != nil && data.phoneNumber != ""{
            fullAddress.append(",\(data.phoneNumber ?? "")")
        }
        if data.Appartment != nil && data.Appartment != ""{
            fullAddress.append(",\(data.Appartment ?? "")")
        }
        if data.landmark != nil && data.landmark != ""{
            fullAddress.append(",\(data.landmark ?? "")")
        }
        lblDropAddressReturn.text = fullAddress//"\(data.Name ?? "") (\(data.phoneNumber ?? "")), \(data.Appartment ?? ""), \(data.landmark ?? "")"
        btnAddReturnDropLoaction.setTitle(data.fullAddress ?? "", for: .normal)
        btnEditDropLocationReturn.isHidden = false
        self.addDotedLayer(View: vwDottedReturn)
        imgAddDropreturnLocation.isHidden = true
        btnAddReturnDropLocationLedtConstant.constant = 0
        lblDropAddressReturn.isHidden = false
    }
    
    func getDropLoaction(_ data: DropLoactions) {
        if dropLoactionsStruct.count == 0{
            self.dropLoactionsStruct.append(data)
        }
        for (num, getData) in dropLoactionsStruct.enumerated(){
            if data.id == getData.id{
                dropLoactionsStruct[num] = data
                break
            }else{
                if sType == .multipleDelivery{
                    self.dropLoactionsStruct.append(data)
                }else{
                    self.dropLoactionsStruct.removeAll()
                    self.dropLoactionsStruct.append(data)
                }
            }
        }
        if sType == .multipleDelivery{
            multipleAddressView.isHidden = false
            vwMuliopleLocationLayer.isHidden = true
            tableView.reloadData()
            DispatchQueue.main.async {
                self.tableView.layoutIfNeeded()
                let contentSize = self.tableView.contentSize.height
                self.tableViewHeight.constant = contentSize
            }
        }else{
            for dt in self.dropLoactionsStruct{
//                lblDropAddress.text = dt.fullAddress ?? ""
//                btnAddDropPoint.setTitle("\(dt.Name ?? "") (\(dt.phoneNumber ?? "")), \(dt.Appartment ?? ""), \(dt.landmark ?? "")", for: .normal)
                var fullAddress = ""
                if dt.Name != nil && dt.Name != ""{
                    fullAddress.append(dt.Name ?? "")
                }
                if dt.phoneNumber != nil && dt.phoneNumber != ""{
                    fullAddress.append(",\(dt.phoneNumber ?? "")")
                }
                if dt.Appartment != nil && dt.Appartment != ""{
                    fullAddress.append(",\(dt.Appartment ?? "")")
                }
                if dt.landmark != nil && dt.landmark != ""{
                    fullAddress.append(",\(dt.landmark ?? "")")
                }
                lblDropAddress.text = fullAddress//"\(dt.Name ?? "") (\(dt.phoneNumber ?? "")), \(dt.Appartment ?? ""), \(dt.landmark ?? "")"
                btnAddDropPoint.setTitle(dt.fullAddress ?? "", for: .normal)
            }
            btnEditDropLocation.isHidden = false
            self.addDotedLayer(View: vwDotted)
            imgPlusDrop.isHidden = true
            btnAddDropLeftConstant.constant = 0
            lblDropAddress.isHidden = false
        }
    }
    
    func getPichUpData(_ data: PickLoactions) {
        self.pickLocationStrct = data
        lblPickUpAddress.isHidden = false
//        lblPickUpAddress.text = data.fullAddress ?? ""
//        btnAddPickUpPoint.setTitle("\(data.Name ?? "") (\(data.phoneNumber ?? "")), \(data.Appartment ?? ""), \(data.landmark ?? "")", for: .normal)
        var fullAddress = ""
        if data.Name != nil && data.Name != ""{
            fullAddress.append(data.Name ?? "")
        }
        if data.phoneNumber != nil && data.phoneNumber != ""{
            fullAddress.append(",\(data.phoneNumber ?? "")")
        }
        if data.Appartment != nil && data.Appartment != ""{
            fullAddress.append(",\(data.Appartment ?? "")")
        }
        if data.landmark != nil && data.landmark != ""{
            fullAddress.append(",\(data.landmark ?? "")")
        }
        lblPickUpAddress.text = fullAddress//"\(data.Name ?? "") (\(data.phoneNumber ?? "")), \(data.Appartment ?? ""), \(data.landmark ?? "")"
        btnAddPickUpPoint.setTitle(data.fullAddress ?? "", for: .normal)
        btnEditPickLoaction.isHidden = false
        self.addDotedLayer(View: vwDotted)
        imgPlusePickUp.isHidden = true
        btnAddpickupLeftConstant.constant = 0
    }
    
    func setDataOfCat(_ title: [String], _ Id: [Int]) {
        self.getDataOfCategoriesTitle = title
        self.getDataOfCategoriesId = Id
//        print("Titles:-",title)
//        print("Ids:-",Id)
        btnSelectCategories.setTitle("Select Category", for: .normal)
        if title.count == 1{
            self.btnSelectCategories.setTitle(title[0], for: .normal)
        }
        if title.count > 1{
            self.btnSelectCategories.setTitle(title.joined(separator: " | "), for: .normal)
        }
        CommonFunctions.hideLoader()
    }
}

//MARK:- Set Data View Did load
extension AddCourierDetailVC{
    func setDataViewDidLoad(){
        
        self.addDotedLayer(View: vwDottedReturn)
        self.addDotedLayer(View: vwDotted)
        
        tvMsg.layer.cornerRadius = 4
        tvMsg.layer.borderWidth = 1
        tvMsg.layer.borderColor = Colors.borderYellowColorTraxi.color().cgColor
        tvMsg.textContainerInset = UIEdgeInsets(top: 8,left: 12,bottom: 8,right: 12)
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            self.tvMsg.tintColor = .black
            self.tvMsg.textColor = .black
        }
        
        
        setCornerRadius([vwCorner1, vwCorner2, vwCorner3, vwCorner4])
        
        switchRqTime.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        imgTimeSensitive.isUserInteractionEnabled = true
        imgTimeSensitive.addGestureRecognizer(singleTap)
        
        switch sType {
        case .returnType:
            lblPickUpAddressReturn.isHidden = true
            lblDropAddressReturn.isHidden = true
            vwHideUnHide.isHidden = false
            
            multipleAddressView.isHidden = true
            vwRqType.isHidden = false
            self.btnAddDropPoint.setTitle("Add details (Eg. Apartment no.)", for: .normal)
        case .singleType:
            vwHideUnHide.isHidden = true
            
            multipleAddressView.isHidden = true
            vwRqType.isHidden = false
            self.btnAddDropPoint.setTitle("Add details (Eg. Apartment no.)", for: .normal)
        case .multipleDelivery:
            vwHideUnHide.isHidden = true
            multipleAddressView.isHidden = true
            vwRqType.isHidden = true
            self.btnAddDropPoint.setTitle("Add more drop off points", for: .normal)
        }
        
        btnEditPickLoaction.isHidden = true
        btnEditDropLocation.isHidden = true
        lblPickUpAddress.isHidden = true
        lblDropAddress.isHidden = true
        
        btnEditReturnPichLocation.isHidden = true
        btnEditDropLocationReturn.isHidden = true
        lblDropAddressReturn.isHidden = true
        lblPickUpAddressReturn.isHidden = true
        
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(tapDetected2))
        imgPlusePickUp.isUserInteractionEnabled = true
        imgPlusePickUp.addGestureRecognizer(singleTap2)
        
        
        let singleTap3 = UITapGestureRecognizer(target: self, action: #selector(tapDetected3))
        imgPlusDrop.isUserInteractionEnabled = true
        imgPlusDrop.addGestureRecognizer(singleTap3)
        
        let singleTap4 = UITapGestureRecognizer(target: self, action: #selector(tapDetected4))
        addImgPickReturnLocation.isUserInteractionEnabled = true
        addImgPickReturnLocation.addGestureRecognizer(singleTap4)

        let singleTap5 = UITapGestureRecognizer(target: self, action: #selector(tapDetected5))
        imgAddDropreturnLocation.isUserInteractionEnabled = true
        imgAddDropreturnLocation.addGestureRecognizer(singleTap5)

    }
    
    //MARK:- Set views corner radius
    func setCornerRadius(_ vws : [UIView]){
        for vw in vws{
            vw.layer.cornerRadius = vw.bounds.height/2
        }
    }
    
    //MARK:- Add Doted Layer to view
    func addDotedLayer(View : UIView){
        DispatchQueue.main.async {
            let lineLayer = CAShapeLayer()
            lineLayer.strokeColor = UIColor.gray.cgColor
            lineLayer.lineWidth = 2
            lineLayer.lineDashPattern = [4,4]
            let path = CGMutablePath()
            path.addLines(between: [CGPoint(x: View.bounds.minX, y: View.bounds.minY),
                                    CGPoint(x: View.bounds.minX, y: View.bounds.maxY)])
            lineLayer.path = path
            View.layer.addSublayer(lineLayer)
        }
    }
}

//MARK:- get Date from delegate method
extension AddCourierDetailVC: logs{
    
    func datepic(_ date: Date, isDate: Bool, isReturn : Bool) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = isDate ? "dd MMM yyyy" : "hh:mm a"
        let stringDate = dateFormatter.string(from: date)
        if isDate{
            if isReturn{
                self.selectDateReturn = date
            }else{
                self.selectDate = date
            }
        }
        if isReturn{
            if isDate{
                self.PickUpDateReturn = ""
                self.PickUpTimeReturn = ""
                self.btnSelectDateReturn.setTitle(stringDate, for: .normal)
                self.btnSelectTimeReturn.setTitle("Select pickup time", for: .normal)
                self.PickUpDateReturn.append(stringDate)
            }else{
                let tempTime2 = stringDate.components(separatedBy: ":").first
                let tempTime = stringDate.components(separatedBy: ":").last
                if tempTime?.components(separatedBy: " ").first != "30" && tempTime?.components(separatedBy: " ").first != "00"{
                    self.PickUpTimeReturn = ""
                    if ((tempTime?.components(separatedBy: " ").first as NSString?)?.integerValue ?? 0) < 30{
                        self.btnSelectTimeReturn.setTitle("\(tempTime2 ?? ""):00 \(tempTime?.components(separatedBy: " ").last ?? "")", for: .normal)
                    }else{
                        self.btnSelectTimeReturn.setTitle("\(tempTime2 ?? ""):30 \(tempTime?.components(separatedBy: " ").last ?? "")", for: .normal)
                    }
                    self.PickUpTimeReturn.append(stringDate)
                }else{
                    self.PickUpTimeReturn = ""
                    self.btnSelectTimeReturn.setTitle(stringDate, for: .normal)
                    self.PickUpTimeReturn.append(stringDate)
                }
            }
        }else{
            if isDate{
                self.PickUpDate = ""
                self.PickUpTime = ""
                self.btnSelectTime.setTitle("Select pickup time", for: .normal)
                self.btnSelectDate.setTitle(stringDate, for: .normal)
                self.PickUpDate.append(stringDate)
            }else{
                let tempTime2 = stringDate.components(separatedBy: ":").first
                let tempTime = stringDate.components(separatedBy: ":").last
                if tempTime?.components(separatedBy: " ").first != "30" && tempTime?.components(separatedBy: " ").first != "00"{
                    self.PickUpTime = ""
                    if ((tempTime?.components(separatedBy: " ").first as NSString?)?.integerValue ?? 0) < 30{
                        self.btnSelectTime.setTitle("\(tempTime2 ?? ""):00 \(tempTime?.components(separatedBy: " ").last ?? "")", for: .normal)
                    }else{
                        self.btnSelectTime.setTitle("\(tempTime2 ?? ""):30 \(tempTime?.components(separatedBy: " ").last ?? "")", for: .normal)
                    }
                    self.PickUpTime.append(stringDate)
                }else{
                    self.PickUpTime = ""
                    self.btnSelectTime.setTitle(stringDate, for: .normal)
                    self.PickUpTime.append(stringDate)
                }
            }
        }
    }
}

//MARK:- UItable view delegate methods
extension AddCourierDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dropLoactionsStruct.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MultipleCourierAddressTableViewCell", for: indexPath) as! MultipleCourierAddressTableViewCell
        cell.lblCount.text = "\(indexPath.row + 1)"
        cell.lblAddress.text = "\(dropLoactionsStruct[indexPath.row].Name ?? "") (\(dropLoactionsStruct[indexPath.row].phoneNumber ?? "")), \(dropLoactionsStruct[indexPath.row].Appartment ?? ""), \(dropLoactionsStruct[indexPath.row].landmark ?? "")"
        cell.lblUserDetail.text = dropLoactionsStruct[indexPath.row].fullAddress ?? ""
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnActEditAddedAddress(_:)), for: .touchUpInside)
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnActDeleteAddedAddress(_:)), for: .touchUpInside)
        return cell
    }
}

//MARK:- Objective C methods
extension AddCourierDetailVC{
    @objc func tapDetected() {
        let popOverVC = TimeSensitiveVC()
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
    
    //MARK:- Tap Gesture
    @objc func tapDetected2() {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sTypeDelivery = .pickup
        switch sType {
        case .singleType:
            vc.isPickupLocation = true
            vc.sType = .PikUpSingle
        case .returnType:
            vc.sType = .PikUpSingle
            vc.isPickupLocation = true
        case .multipleDelivery:
            vc.sType = .PikUpSingle
            vc.isPickupLocation = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Tap Gesture
    @objc func tapDetected3() {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sTypeDelivery = .drop
        switch sType {
        case .singleType:
            vc.isPickupLocation = false
            vc.sType = .PikUpSingle
        case .returnType:
            vc.sType = .PikUpSingle
            vc.isPickupLocation = false
        case .multipleDelivery:
            vc.sType = .PikUpSingle
            vc.isPickupLocation = false
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func tapDetected4() {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sTypeDelivery = .pickUpReturn
        switch sType {
        case .singleType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = true
        case .returnType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = true
        case .multipleDelivery:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func tapDetected5() {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sTypeDelivery = .dropReturn
        switch sType {
        case .singleType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = false
        case .returnType:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = false
        case .multipleDelivery:
            vc.sType = .pickUpReturn
            vc.isPickupLocation = false
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func btnActDeleteAddedAddress(_ sender : UIButton){
        let alert = UIAlertController(title: "Remove Address", message: "Are you sure you want to remove address?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
            self.dropLoactionsStruct.remove(at: sender.tag)
            self.tableView.reloadData()
            DispatchQueue.main.async {
                self.tableView.layoutIfNeeded()
                let contentSize = self.tableView.contentSize.height
                self.tableViewHeight.constant = contentSize
            }
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (_) in }))
        CommonFunctions.getTopMostViewController()?.present(alert, animated: true, completion: nil)

    }
    
    @objc func btnActEditAddedAddress(_ sender : UIButton){
        let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .DropLocation
        if self.dropLoactionsStruct.count > 0{
            vc.dataFilledDropUp = self.dropLoactionsStruct[sender.tag]
            let tempLoc = loc.init(self.dropLoactionsStruct[sender.tag].lat ?? 0.0, lng: self.dropLoactionsStruct[sender.tag].lng ?? 0.0, name: self.dropLoactionsStruct[sender.tag].fullAddress ?? "", fullAddress: self.dropLoactionsStruct[sender.tag].fullAddress ?? "")
            vc.addedLocation = tempLoc
        }
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func loadList(notification: NSNotification){
        //load data here
//        print(pickLocationStrct ?? "")
//        print(pickLocationStrctReturn ?? "")
//        print(dropLoactionsStruct)
//        print(dropLoactionsStructReturn ?? "")
        
        if pickLocationStrctReturn != nil{
            lblPickUpAddressReturn.isHidden = false
//            lblPickUpAddressReturn.text = pickLocationStrctReturn?.fullAddress ?? ""
//            btnAddLocationPickupReturn.setTitle("\(pickLocationStrctReturn?.Name ?? "") (\(pickLocationStrctReturn?.phoneNumber ?? "")), \(pickLocationStrctReturn?.Appartment ?? ""), \(pickLocationStrctReturn?.landmark ?? "")", for: .normal)
            var fullAddress = ""
            if pickLocationStrctReturn?.Name != nil && pickLocationStrctReturn?.Name != ""{
                fullAddress.append(pickLocationStrctReturn?.Name ?? "")
            }
            if pickLocationStrctReturn?.phoneNumber != nil && pickLocationStrctReturn?.phoneNumber != ""{
                fullAddress.append(",\(pickLocationStrctReturn?.phoneNumber ?? "")")
            }
            if pickLocationStrctReturn?.Appartment != nil && pickLocationStrctReturn?.Appartment != ""{
                fullAddress.append(",\(pickLocationStrctReturn?.Appartment ?? "")")
            }
            if pickLocationStrctReturn?.landmark != nil && pickLocationStrctReturn?.landmark != ""{
                fullAddress.append(",\(pickLocationStrctReturn?.landmark ?? "")")
            }
            lblPickUpAddressReturn.text = fullAddress//"\(pickLocationStrctReturn?.Name ?? "") (\(pickLocationStrctReturn?.phoneNumber ?? "")), \(pickLocationStrctReturn?.Appartment ?? ""), \(pickLocationStrctReturn?.landmark ?? "")"
            btnAddLocationPickupReturn.setTitle(pickLocationStrctReturn?.fullAddress ?? "", for: .normal)
            btnEditReturnPichLocation.isHidden = false
            self.addDotedLayer(View: vwDottedReturn)
            addImgPickReturnLocation.isHidden = true
            btnAddPickReturnLocationLeftConstant.constant = 0
        }
        
        if pickLocationStrct != nil{
            lblPickUpAddress.isHidden = false
//            lblPickUpAddress.text = pickLocationStrct?.fullAddress ?? ""
//            btnAddPickUpPoint.setTitle("\(pickLocationStrct?.Name ?? "") (\(pickLocationStrct?.phoneNumber ?? "")), \(pickLocationStrct?.Appartment ?? ""), \(pickLocationStrct?.landmark ?? "")", for: .normal)
            var fullAddress = ""
            if pickLocationStrct?.Name != nil && pickLocationStrct?.Name != ""{
                fullAddress.append(pickLocationStrct?.Name ?? "")
            }
            if pickLocationStrct?.phoneNumber != nil && pickLocationStrct?.phoneNumber != ""{
                fullAddress.append(",\(pickLocationStrct?.phoneNumber ?? "")")
            }
            if pickLocationStrct?.Appartment != nil && pickLocationStrct?.Appartment != ""{
                fullAddress.append(",\(pickLocationStrct?.Appartment ?? "")")
            }
            if pickLocationStrct?.landmark != nil && pickLocationStrct?.landmark != ""{
                fullAddress.append(",\(pickLocationStrct?.landmark ?? "")")
            }
            lblPickUpAddress.text = fullAddress//"\(pickLocationStrct?.Name ?? "") (\(pickLocationStrct?.phoneNumber ?? "")), \(pickLocationStrct?.Appartment ?? ""), \(pickLocationStrct?.landmark ?? "")"
            btnAddPickUpPoint.setTitle(pickLocationStrct?.fullAddress ?? "", for: .normal)
            btnEditPickLoaction.isHidden = false
            self.addDotedLayer(View: vwDotted)
            imgPlusePickUp.isHidden = true
            btnAddpickupLeftConstant.constant = 0
        }
        
        if dropLoactionsStruct.count > 0{
            if sType == .multipleDelivery{
                multipleAddressView.isHidden = false
                vwMuliopleLocationLayer.isHidden = true
                tableView.reloadData()
                DispatchQueue.main.async {
                    self.tableView.layoutIfNeeded()
                    let contentSize = self.tableView.contentSize.height
                    self.tableViewHeight.constant = contentSize
                }
            }else{
                for dt in self.dropLoactionsStruct{
//                    lblDropAddress.text = dt.fullAddress ?? ""
//                    btnAddDropPoint.setTitle("\(dt.Name ?? "") (\(dt.phoneNumber ?? "")), \(dt.Appartment ?? ""), \(dt.landmark ?? "")", for: .normal)
                    var fullAddress = ""
                    if dt.Name != nil && dt.Name != ""{
                        fullAddress.append(dt.Name ?? "")
                    }
                    if dt.phoneNumber != nil && dt.phoneNumber != ""{
                        fullAddress.append(",\(dt.phoneNumber ?? "")")
                    }
                    if dt.Appartment != nil && dt.Appartment != ""{
                        fullAddress.append(",\(dt.Appartment ?? "")")
                    }
                    if dt.landmark != nil && dt.landmark != ""{
                        fullAddress.append(",\(dt.landmark ?? "")")
                    }
                    lblDropAddress.text = fullAddress//"\(dt.Name ?? "") (\(dt.phoneNumber ?? "")), \(dt.Appartment ?? ""), \(dt.landmark ?? "")"
                    btnAddDropPoint.setTitle(dt.fullAddress ?? "", for: .normal)
                }
                btnEditDropLocation.isHidden = false
                self.addDotedLayer(View: vwDotted)
                imgPlusDrop.isHidden = true
                btnAddDropLeftConstant.constant = 0
                lblDropAddress.isHidden = false
            }
        }
        
        if dropLoactionsStructReturn != nil{
//            lblDropAddressReturn.text = dropLoactionsStructReturn?.fullAddress ?? ""
//            btnAddReturnDropLoaction.setTitle("\(dropLoactionsStructReturn?.Name ?? "") (\(dropLoactionsStructReturn?.phoneNumber ?? "")), \(dropLoactionsStructReturn?.Appartment ?? ""), \(dropLoactionsStructReturn?.landmark ?? "")", for: .normal)
            var fullAddress = ""
            if dropLoactionsStructReturn?.Name != nil && dropLoactionsStructReturn?.Name != ""{
                fullAddress.append(dropLoactionsStructReturn?.Name ?? "")
            }
            if dropLoactionsStructReturn?.phoneNumber != nil && dropLoactionsStructReturn?.phoneNumber != ""{
                fullAddress.append(",\(dropLoactionsStructReturn?.phoneNumber ?? "")")
            }
            if dropLoactionsStructReturn?.Appartment != nil && dropLoactionsStructReturn?.Appartment != ""{
                fullAddress.append(",\(dropLoactionsStructReturn?.Appartment ?? "")")
            }
            if dropLoactionsStructReturn?.landmark != nil && dropLoactionsStructReturn?.landmark != ""{
                fullAddress.append(",\(dropLoactionsStructReturn?.landmark ?? "")")
            }
            lblDropAddressReturn.text = fullAddress//"\(dropLoactionsStructReturn?.Name ?? "") (\(dropLoactionsStructReturn?.phoneNumber ?? "")), \(dropLoactionsStructReturn?.Appartment ?? ""), \(dropLoactionsStructReturn?.landmark ?? "")"
            btnAddReturnDropLoaction.setTitle(dropLoactionsStructReturn?.fullAddress ?? "", for: .normal) 
            btnEditDropLocationReturn.isHidden = false
            self.addDotedLayer(View: vwDottedReturn)
            imgAddDropreturnLocation.isHidden = true
            btnAddReturnDropLocationLedtConstant.constant = 0
            lblDropAddressReturn.isHidden = false
        }
    }
    
    func gettaxCharges(){
        self.TaxesAndCharges = (CommonFunctions.decimalPlaceFromDouble(self.deliveryCharges * 0.10, decimalPlace: 2) as NSString).doubleValue
        print("deliveryCharges", self.deliveryCharges ?? 0.0)
    }
    
    func getReturnData(){
        var returnStartCoordinates : CLLocationCoordinate2D!
        var returnEndCoordinates : CLLocationCoordinate2D!
        
        returnStartCoordinates = CLLocationCoordinate2D(latitude: pickLocationStrctReturn?.lat ?? 0.0, longitude: pickLocationStrctReturn?.lng ?? 0.0)
        
        returnEndCoordinates = CLLocationCoordinate2D(latitude: dropLoactionsStructReturn?.lat ?? 0.0, longitude: dropLoactionsStructReturn?.lng ?? 0.0)
        
        CommonFunctions.getPolylineRoute(from: returnStartCoordinates, to: returnEndCoordinates, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
            print("Status, pol, time, distance",status, polyline,time,distance)
            if !(polyline.isEmpty){
            }
            let tempCheck = (distance.components(separatedBy: " ").first as NSString?)?.replacingOccurrences(of: ",", with: "")
            let tempdistanceInKm = (tempCheck as NSString?)?.doubleValue
            if self.switchRqTime.isOn{
                self.deliveryCharges = (self.deliveryCharges + 7.0) + CommonFunctions.getPriceAcordingToDistance(tempdistanceInKm ?? 0.0)
            }else{
                self.deliveryCharges = self.deliveryCharges + CommonFunctions.getPriceAcordingToDistance(tempdistanceInKm ?? 0.0)
            }
            self.TaxesAndCharges = (CommonFunctions.decimalPlaceFromDouble(self.deliveryCharges * 0.10, decimalPlace: 2) as NSString).doubleValue
            print("deliveryCharges", deliveryCharges ?? 0.0)
        }
    }
}

//MARK:- Hit Api
extension AddCourierDetailVC{
    func AddAddressApiHit(){
        self.deliveryCharges = 0.0
        
        CommonFunctions.showLoader()
        
        var startCoordinates : CLLocationCoordinate2D!
        var endCoordinates : CLLocationCoordinate2D!
        
        startCoordinates = CLLocationCoordinate2D(latitude: pickLocationStrct?.lat ?? 0.0, longitude: pickLocationStrct?.lng ?? 0.0)
        
        for data in dropLoactionsStruct{
            endCoordinates = CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0)
        }
        
        switch self.sType {
        case .returnType:
            CommonFunctions.getPolylineRoute(from: startCoordinates, to: endCoordinates, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                print("Status, pol, time, distance",status, polyline,time,distance)
                self.returnDeliveryTime = time
                self.returnDeliveryTimeValue = TimeValue
                self.distanceForDeliveryReturn = distance
                if !(polyline.isEmpty){
                }
                let tempCheck = (distance.components(separatedBy: " ").first as NSString?)?.replacingOccurrences(of: ",", with: "")
                let tempdistanceInKm = (tempCheck as NSString?)?.doubleValue
                
                self.deliveryCharges = CommonFunctions.getPriceAcordingToDistance(tempdistanceInKm ?? 0.0)
                self.getReturnData()
            }
            

        case .singleType:
            CommonFunctions.getPolylineRoute(from: startCoordinates, to: endCoordinates, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                print("Status, pol, time, distance",status, polyline,time,distance)
                if !(polyline.isEmpty){
                }
                let tempCheck = (distance.components(separatedBy: " ").first as NSString?)?.replacingOccurrences(of: ",", with: "")
                let tempdistanceInKm = (tempCheck as NSString?)?.doubleValue
                
                self.deliveryCharges = CommonFunctions.getPriceAcordingToDistance(tempdistanceInKm ?? 0.0)
                if self.switchRqTime.isOn{
                    self.TaxesAndCharges = (CommonFunctions.decimalPlaceFromDouble((self.deliveryCharges+7.0) * 0.10, decimalPlace: 2) as NSString).doubleValue
                }else{
                    self.TaxesAndCharges = (CommonFunctions.decimalPlaceFromDouble(self.deliveryCharges * 0.10, decimalPlace: 2) as NSString).doubleValue
                }
                print("deliveryCharges", deliveryCharges ?? 0.0)
            }
        case .multipleDelivery:
            var tempCount = 0
            for data in dropLoactionsStruct{
                endCoordinates = CLLocationCoordinate2D(latitude: data.lat ?? 0.0, longitude: data.lng ?? 0.0)
                CommonFunctions.getPolylineRoute(from: startCoordinates, to: endCoordinates, isLoader: true) { [self] (status, polyline,time,distance,TimeValue) in
                    tempCount+=1
                    print("Status, pol, time, distance",status, polyline,time,distance)
                    if !(polyline.isEmpty){
                    }
                    let tempCheck = (distance.components(separatedBy: " ").first as NSString?)?.replacingOccurrences(of: ",", with: "")
                    
                    let tempdistanceInKm = (tempCheck as NSString?)?.doubleValue
                    if (tempdistanceInKm ?? 0.0) < 11.0{
                        self.deliveryCharges = self.deliveryCharges + 1.25*(10)
                    }else{
                        self.deliveryCharges = self.deliveryCharges + 1.25*(tempdistanceInKm ?? 0.0)
                    }
                    if tempCount == self.dropLoactionsStruct.count{
                        self.gettaxCharges()
                    }
                }
            }
        }

        
        let pickUpDataAndTime = "\(self.PickUpDate) \(self.PickUpTime)"
        let pickUpDataAndTimeReturn = "\(self.PickUpDateReturn) \(self.PickUpTimeReturn)"
        
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            CommonFunctions.hideLoader()
            var pickupDetails : [[String: AnyObject]] = [[:]]
            pickupDetails.removeAll()
            let parm : [String : AnyObject] = [
                "pickup_time": CommonFunctions.getDateFormatTraxi(pickUpDataAndTime) as AnyObject,
                "address" : self.pickLocationStrct?.fullAddress as AnyObject,
                "user_name" : self.pickLocationStrct?.Name as AnyObject,
                "user_phone" : self.pickLocationStrct?.phoneNumber as AnyObject,
                "user_apartment" : self.pickLocationStrct?.Appartment as AnyObject,
                "user_landmark" : self.pickLocationStrct?.landmark as AnyObject,
                "lat" : self.pickLocationStrct?.lat as AnyObject,
                "lng" : self.pickLocationStrct?.lng as AnyObject
            ]
            pickupDetails.append(parm)
            
            if self.pickLocationStrctReturn != nil{
                let parm : [String : AnyObject] = [
                    "pickup_time": CommonFunctions.getDateFormatTraxi(pickUpDataAndTimeReturn) as AnyObject,
                    "address" : self.pickLocationStrctReturn?.fullAddress as AnyObject,
                    "user_name" : self.pickLocationStrctReturn?.Name as AnyObject,
                    "user_phone" : self.pickLocationStrctReturn?.phoneNumber as AnyObject,
                    "user_apartment" : self.pickLocationStrctReturn?.Appartment as AnyObject,
                    "user_landmark" : self.pickLocationStrctReturn?.landmark as AnyObject,
                    "lat" : self.pickLocationStrctReturn?.lat as AnyObject,
                    "lng" : self.pickLocationStrctReturn?.lng as AnyObject
                ]
                pickupDetails.append(parm)
            }
            
            var dropoffDetails : [[String: AnyObject]] = [[:]]
            dropoffDetails.removeAll()
            
            for data in self.dropLoactionsStruct{
                let parm : [String : AnyObject] = [
                    "address" : data.fullAddress as AnyObject,
                    "user_name" : data.Name as AnyObject,
                    "user_phone" : data.phoneNumber as AnyObject,
                    "user_apartment" : data.Appartment as AnyObject,
                    "user_landmark" : data.landmark as AnyObject,
                    "lat" : data.lat as AnyObject,
                    "lng" : data.lng as AnyObject
                ]
                dropoffDetails.append(parm)
            }
            
            if self.dropLoactionsStructReturn != nil{
                let parm : [String : AnyObject] = [
                    "address" : self.dropLoactionsStructReturn?.fullAddress as AnyObject,
                    "user_name" : self.dropLoactionsStructReturn?.Name as AnyObject,
                    "user_phone" : self.dropLoactionsStructReturn?.phoneNumber as AnyObject,
                    "user_apartment" : self.dropLoactionsStructReturn?.Appartment as AnyObject,
                    "user_landmark" : self.dropLoactionsStructReturn?.landmark as AnyObject,
                    "lat" : self.dropLoactionsStructReturn?.lat as AnyObject,
                    "lng" : self.dropLoactionsStructReturn?.lng as AnyObject
                ]
                dropoffDetails.append(parm)
            }

            
            var paarams : [String : AnyObject] = [
                "time_sensitive" : (self.switchRqTime.isOn ? 1 : 0) as AnyObject,
                "message": self.tvMsg.text as AnyObject,
                "package_categories" : self.getDataOfCategoriesId as AnyObject,
                "pickupDetails" : pickupDetails as AnyObject,
                "dropoffDetails" : dropoffDetails as AnyObject
             ]
            
            switch self.sType{
            case .returnType:
                paarams["deliver_type_id"] = 2 as AnyObject
                paarams["delivered_charges"] = (self.switchRqTime.isOn ? (self.deliveryCharges + 14.0) :  self.deliveryCharges) as AnyObject
                paarams["tax_charges"] = self.TaxesAndCharges as AnyObject
                
            case .singleType:
                paarams["deliver_type_id"] = 1 as AnyObject
                paarams["delivered_charges"] = (self.switchRqTime.isOn ? (self.deliveryCharges + 7.0) :  self.deliveryCharges) as AnyObject
                paarams["tax_charges"] = self.TaxesAndCharges as AnyObject
            case .multipleDelivery:
                paarams["deliver_type_id"] = 3 as AnyObject
                paarams["delivered_charges"] = (self.switchRqTime.isOn ? (self.deliveryCharges + 7.0) :  self.deliveryCharges) as AnyObject
                paarams["tax_charges"] = self.TaxesAndCharges as AnyObject
            }
            
            let vc = CourierEstimateDetailsVC.instantiateFromAppStoryboard(appStoryboard: .Home)
            let temp2 = Detail.init(id: nil, address: nil, userName: nil, userPhone: nil, userApartment: nil, userLandmark: nil, lat: nil, lng: nil, pickupTime: CommonFunctions.getDateFormatTraxi(pickUpDataAndTime), pickupDate: CommonFunctions.getDateFormatTraxi(pickUpDataAndTime), deliveredAt: nil, cancelledAt: nil, pickedUpAt: nil)
            let temp : DeliveryDetailAPi!
            if self.sType == .returnType{
                temp = DeliveryDetailAPi.init(id: nil, driverID: nil, deliverTypeID: self.deliveryType, timeSensitive: (self.switchRqTime.isOn ? 1 : 0), deliveryTime: nil, deliveryStatus: nil, message: nil, name: nil, profileImage: nil, userToDriverRatings: nil, driverToUserRatings: nil, deliveredCharges: (self.switchRqTime.isOn ? (self.deliveryCharges + 14.0) : self.deliveryCharges), taxCharges: self.TaxesAndCharges, estimatedDeliveryTime: nil, estimatedDeliveryTimeReturn: nil, distanceTravelled: nil, duration: nil, pickupDetails: [temp2], dropoffDetails: nil, deliveryPackage: nil)
            }else if self.sType == .singleType{
                temp = DeliveryDetailAPi.init(id: nil, driverID: nil, deliverTypeID: self.deliveryType, timeSensitive: (self.switchRqTime.isOn ? 1 : 0), deliveryTime: nil, deliveryStatus: nil, message: nil, name: nil, profileImage: nil, userToDriverRatings: nil, driverToUserRatings: nil, deliveredCharges: (self.switchRqTime.isOn ? (self.deliveryCharges + 7.0) : self.deliveryCharges), taxCharges: self.TaxesAndCharges, estimatedDeliveryTime: nil, estimatedDeliveryTimeReturn: nil, distanceTravelled: nil, duration: nil, pickupDetails: [temp2], dropoffDetails: nil, deliveryPackage: nil)
            }else{
                temp = DeliveryDetailAPi.init(id: nil, driverID: nil, deliverTypeID: self.deliveryType, timeSensitive: (self.switchRqTime.isOn ? 1 : 0), deliveryTime: nil, deliveryStatus: nil, message: nil, name: nil, profileImage: nil, userToDriverRatings: nil, driverToUserRatings: nil, deliveredCharges: self.deliveryCharges, taxCharges: self.TaxesAndCharges, estimatedDeliveryTime: nil, estimatedDeliveryTimeReturn: nil, distanceTravelled: nil, duration: nil, pickupDetails: [temp2], dropoffDetails: nil, deliveryPackage: nil)
            }
            vc.DeliveryData = temp
            vc.paarams = paarams
            vc.estimateTimeReturn = self.returnDeliveryTime
            vc.returnDeliveryTimeValue = self.returnDeliveryTimeValue
            vc.distanceForDeliveryReturn = self.distanceForDeliveryReturn
            let tempLatLngPickUp = loc.init(self.pickLocationStrct?.lat ?? 0.0, lng: self.pickLocationStrct?.lng ?? 0.0, name: self.pickLocationStrct?.fullAddress ?? "", fullAddress: self.pickLocationStrct?.fullAddress ?? "")
            vc.pickLoaction = tempLatLngPickUp
            let tempLatLngPickUpreturn = loc.init(self.pickLocationStrctReturn?.lat ?? 0.0, lng: self.pickLocationStrctReturn?.lng ?? 0.0, name: self.pickLocationStrctReturn?.fullAddress ?? "", fullAddress: self.pickLocationStrctReturn?.fullAddress ?? "")
            vc.pickLoactionReturn = tempLatLngPickUpreturn
            for data in self.dropLoactionsStruct{
                let tempLatLngDrop = loc.init(data.lat ?? 0.0, lng: data.lng ?? 0.0, name: data.fullAddress ?? "", fullAddress: data.fullAddress ?? "")
                vc.dropLocation = tempLatLngDrop
            }
            let tempLatLngDropReturn = loc.init(self.dropLoactionsStructReturn?.lat ?? 0.0, lng: self.dropLoactionsStructReturn?.lng ?? 0.0, name: self.dropLoactionsStructReturn?.fullAddress ?? "", fullAddress: self.dropLoactionsStructReturn?.fullAddress ?? "")
            vc.dropLocationReturn = tempLatLngDropReturn
            if self.sType == .multipleDelivery{
                vc.sType = .multipleDelivery
                vc.dropLoactionsStructMultipleLocations = self.dropLoactionsStruct
            }
            self.navigationController?.pushViewController(vc, animated: true)
            
            CommonFunctions.hideLoader()
//            ApiHandler.callApiWithParameters(url: APIConstants.postDelivers, withParameters: paarams as [String: AnyObject], ofType: AddDeliveriesAPI.self, success2: { (profileAPI) in
//                CommonFunctions.toster(profileAPI.message ?? "")
//                CommonFunctions.hideLoader()
//
//                self.navigationController?.pushViewController(vc, animated: true)
//            }, failure: { (false, string) in
//                CommonFunctions.hideLoader()
//                print(string)
//            }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
        }
    }
}
