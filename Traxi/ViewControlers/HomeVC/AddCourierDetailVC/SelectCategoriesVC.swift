//
//  SelectCategoriesVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 02/12/20.
//

import UIKit

protocol SelectCategoriesVCDelegate : class {
    func setDataOfCat(_ title : [String], _ Id : [Int])
}

class SelectCategoriesVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var UIviewGradient: GradientView!
    @IBOutlet weak var btnProcced: LoadingButton!
    
    //MARK:- Variables
//    var titilesName : [String] = []
//    var titileId : [Int] = []
    var sendDataTitle : [String] = []
    var sendDataId : [Int] = []
    var checkData = false
    weak var delegate : SelectCategoriesVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
//        GetCategoriesApiHit()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActProced(_ sender: Any) {
        print(sendDataTitle)
        print(sendDataId)
        self.delegate?.setDataOfCat(sendDataTitle, sendDataId)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- set Data viewDidload
extension SelectCategoriesVC{
    func setUpUI(){
        //MARK:- Shadow to button
        btnBack.clipsToBounds = true
        btnBack.layer.masksToBounds = false
        btnBack.layer.shadowColor = UIColor.black.cgColor
        btnBack.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//            imgs.layer.shadowPath = shadowPath.cgPath
        btnBack.layer.shadowOpacity = 0.5
        btnBack.layer.shadowRadius = 5.0
        btnBack.layer.cornerRadius = 4.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.cornerView.roundCorners([.topRight, .topLeft], radius: 24)
        }
    }
}

extension SelectCategoriesVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titilesName.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        checkData = !checkData
        if sendDataTitle.contains(titilesName[indexPath.row]){
            if let ind = sendDataTitle.firstIndex(of: titilesName[indexPath.row]){
                sendDataTitle.remove(at: ind)
                sendDataId.remove(at: ind)
            }
        }else{
            sendDataTitle.append(titilesName[indexPath.row])
            sendDataId.append(titileId[indexPath.row])
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCategoriesCell", for: indexPath) as! SelectCategoriesCell
        cell.lblTitle.text = titilesName[indexPath.row]
        cell.btnCheckBox.tag = indexPath.row
        cell.btnCheckBox.addTarget(self, action: #selector(btnActSelectedCat(_:)), for: .touchUpInside)
        if sendDataTitle.contains(titilesName[indexPath.row]){
            cell.btnCheckBox.setImage(Asset.ic_checked.image(), for: .normal)
        }else{
            cell.btnCheckBox.setImage(Asset.ic_notChecked.image(), for: .normal)
        }
        cell.btnCheckBox.isUserInteractionEnabled = false
        return cell
    }
}

//MARK:- Objective C methods
extension SelectCategoriesVC{
    @objc func btnActSelectedCat(_ sender: UIButton){
        checkData = !checkData
        if sendDataTitle.contains(titilesName[sender.tag]){
            if let ind = sendDataTitle.firstIndex(of: titilesName[sender.tag]){
                sendDataTitle.remove(at: ind)
            }
            sender.setImage(Asset.ic_notChecked.image(), for: .normal)
        }else{
            sendDataTitle.append(titilesName[sender.tag])
            sender.setImage(Asset.ic_checked.image(), for: .normal)
        }
        print(sendDataTitle)
    }
}

//MARK:- Hit Api
extension SelectCategoriesVC{
//    func GetCategoriesApiHit(){
//        let paarams : [String : String] = [
//            :
//        ]
//        CommonFunctions.showLoader()
//        ApiHandler.callApiWithParameters(url: APIConstants.getCategories, withParameters: paarams as [String: AnyObject], ofType: CategoriesApis.self, success2: { (profileAPI) in
//
//            self.titilesName.removeAll()
//            for data in profileAPI{
//                self.titilesName.append(data.name ?? "")
//                self.titileId.append(data.id ?? 0)
//            }
//            print(profileAPI)
//            self.tableView.reloadData()
//            CommonFunctions.hideLoader()
//        }, failure: { (false, string) in
//            CommonFunctions.hideLoader()
//            print(string)
//        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
//    }
}
