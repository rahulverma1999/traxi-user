//
//  MultipleCourierAddressTableViewCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 28/12/20.
//

import UIKit

class MultipleCourierAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblUserDetail: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
