//
//  CancelOrderPopUpVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit

enum CancelOrderPopUpVCScreenType {
    case normal, cancelCart
}

class CancelOrderPopUpVC: BasePopUp {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var innerView: cornerRadiusView!
    @IBOutlet weak var topViewCorners: UIView!
    @IBOutlet weak var bottomViewCorners: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnDiscard: UIButton!
    @IBOutlet weak var btnYesCancel: UIButton!
    
    var deliveryId : Int!
    var sType : CancelOrderPopUpVCScreenType = .normal
    var fromListing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewDidLoadData()
        // Do any additional setup after loading the view.
    }

    //MARK:- Button Outlets
    @IBAction func btnActDiscard(_ sender: Any) {
        stopAnimation()
    }
    @IBAction func btnActYesCancel(_ sender: Any) {
        switch sType {
        case .normal:
            self.deleteDelivery()
        case .cancelCart:
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK:- SetViewDidLoadData
extension CancelOrderPopUpVC{
    func setViewDidLoadData(){
        outerVw = outerView
        innerVw = innerView
        showAnimate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.topViewCorners.roundCorners([.topRight, .topLeft], radius: 4)
            self.bottomViewCorners.roundCorners([.bottomRight, .bottomLeft], radius: 4)
        }
        
        switch sType {
        case .normal:
            break
        case .cancelCart:
            break
        }
    }
    
    //MARK:- HitApi
    func deleteDelivery(){
        let paarams : [String : AnyObject] = [
           "delivery_id" : deliveryId as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.postDelivers, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            CommonFunctions.toster(profileAPI.message ?? "")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CancelOrder"), object: nil)
            if self.fromListing{
                let allVcs = self.navigationController!.viewControllers
                for vc in allVcs{
                    if vc.isKind(of: BookingHistoryVC.self){
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }else{
                let allVcs = self.navigationController!.viewControllers
                for vc in allVcs{
                    if vc.isKind(of: HomeVC.self){
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
            print(profileAPI)
            self.stopAnimation()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.DELETEWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
}
