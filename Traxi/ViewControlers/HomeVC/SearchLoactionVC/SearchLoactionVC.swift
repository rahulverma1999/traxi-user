//
//  SearchLoactionVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit

enum SearchLoactionVCScreenType{
    case PikUpSingle, pickUpReturn, HomeLocationSelect, shopeByCat, StoreNearYou, storeDetail, cartDetail
}

enum deliveryType{
    case pickup, drop, pickUpReturn, dropReturn
}

class SearchLoactionVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var btnSelectOnMap: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblSuggestionForYou: UILabel!
    @IBOutlet weak var btnSearchPickLocation: UIButton!
    
    //MARK:- Variable
    var suggestionAdressList : [GetAddressAPIElement] = []
    var isPickupLocation = false
    var addedLocation : loc?
    var sType : SearchLoactionVCScreenType = .PikUpSingle
    var sTypeDelivery : deliveryType = .pickup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetProfileApiHit()
        if isPickupLocation{
            btnSearchPickLocation.setTitle("Select pick location", for: .normal)
        }else{
            btnSearchPickLocation.setTitle("Select drop location", for: .normal)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func btnActSelectOnMap(_ sender: Any) {
        let vc = SelectOnMapVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        if self.sType == .HomeLocationSelect{
            vc.sType = .HomeLocationSelect
        }else if self.sType == .shopeByCat{
            vc.sType = .shopeByCat
        }else if self.sType == .StoreNearYou{
            vc.sType = .StoreNearYou
        }else if self.sType == .storeDetail{
            vc.sType = .storeDetail
        }else if self.sType == .cartDetail{
            vc.sType = .cartDetail
        }else{
            switch sTypeDelivery {
            case .pickup:
                vc.sType = .pickup
            case .drop:
                vc.sType = .drop
            case .pickUpReturn:
                vc.sType = .pickUpReturn
            case .dropReturn:
                vc.sType = .dropReturn
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActSearchLoaction(_ sender: Any) {
//        findLocation.sharedInstance.pickerLocation(presentInVc: self, countryUs: false, pickedListner: { loc in
//            print(loc)
//        }) {
//        }
    }
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK:- UItable view delegate methods
extension SearchLoactionVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestionAdressList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if suggestionAdressList[indexPath.row].tag ?? "" == "Work" {
            locationTagType = 2
        }else if suggestionAdressList[indexPath.row].tag ?? "" == "Home"{
            locationTagType = 1
        }else{
            locationTagType = 3
        }
        if sType == .HomeLocationSelect{
            let loca = locNew.init(suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, name: suggestionAdressList[indexPath.row].address ?? "", fullAddress: suggestionAdressList[indexPath.row].address ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landMark: suggestionAdressList[indexPath.row].landmark ?? "")
            globalAddedLocation = loca
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: HomeVC.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else if sType == .shopeByCat{
            let loca = locNew.init(suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, name: suggestionAdressList[indexPath.row].address ?? "", fullAddress: suggestionAdressList[indexPath.row].address ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landMark: suggestionAdressList[indexPath.row].landmark ?? "")
            globalAddedLocation = loca
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: ShopCategoriesVC.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else if sType == .StoreNearYou{
            let loca = locNew.init(suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, name: suggestionAdressList[indexPath.row].address ?? "", fullAddress: suggestionAdressList[indexPath.row].address ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landMark: suggestionAdressList[indexPath.row].landmark ?? "")
            globalAddedLocation = loca
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: StoreNearYouVC.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else if sType == .storeDetail{
            let loca = locNew.init(suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, name: suggestionAdressList[indexPath.row].address ?? "", fullAddress: suggestionAdressList[indexPath.row].address ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landMark: suggestionAdressList[indexPath.row].landmark ?? "")
            globalAddedLocation = loca
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: StoreDetailVC.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else if sType == .cartDetail{
            let loca = locNew.init(suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, name: suggestionAdressList[indexPath.row].address ?? "", fullAddress: suggestionAdressList[indexPath.row].address ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landMark: suggestionAdressList[indexPath.row].landmark ?? "")
            globalAddedLocation = loca
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: CartDetailVC.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }else{
            let allVcs = self.navigationController!.viewControllers
            for vc in allVcs{
                if vc.isKind(of: AddCourierDetailVC.self){
                    let kVC = vc as! AddCourierDetailVC
                    switch sType {
                    case .PikUpSingle:
                        if isPickupLocation{
                            var tagId = 0
                            if suggestionAdressList[indexPath.row].tag ?? "" == "Work" {
                                tagId = 1
                            }else if suggestionAdressList[indexPath.row].tag ?? "" == "Home"{
                                tagId = 2
                            }else{
                                tagId = 3
                            }
                            let tempAddress = PickLoactions.init(fullAddress: suggestionAdressList[indexPath.row].address ?? "", Name: suggestionAdressList[indexPath.row].name ?? "", phoneNumber: suggestionAdressList[indexPath.row].phone ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landmark: suggestionAdressList[indexPath.row].landmark ?? "", lat: suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, tagId: tagId, otherAddress: suggestionAdressList[indexPath.row].otherAddress ?? "", id: "\(suggestionAdressList[indexPath.row].id ?? 0)")
                            kVC.pickLocationStrct = tempAddress
                        }else{
                            var tagId = 0
                            if suggestionAdressList[indexPath.row].tag ?? "" == "Work" {
                                tagId = 1
                            }else if suggestionAdressList[indexPath.row].tag ?? "" == "Home"{
                                tagId = 2
                            }else{
                                tagId = 3
                            }
                            let tempAddress = DropLoactions.init(fullAddress: suggestionAdressList[indexPath.row].address ?? "", Name: suggestionAdressList[indexPath.row].name ?? "", phoneNumber: suggestionAdressList[indexPath.row].phone ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landmark: suggestionAdressList[indexPath.row].landmark ?? "", tagId: tagId, lat: suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, otherAddress: suggestionAdressList[indexPath.row].otherAddress ?? "", id: "\(suggestionAdressList[indexPath.row].id ?? 0)")
                            if kVC.sType == .multipleDelivery{
                                kVC.dropLoactionsStruct.append(tempAddress)
                            }else{
                                kVC.dropLoactionsStruct.removeAll()
                                kVC.dropLoactionsStruct.append(tempAddress)
                            }
                        }
                    case .pickUpReturn:
                        if isPickupLocation{
                            var tagId = 0
                            if suggestionAdressList[indexPath.row].tag ?? "" == "Work" {
                                tagId = 2
                            }else if suggestionAdressList[indexPath.row].tag ?? "" == "Home"{
                                tagId = 1
                            }else{
                                tagId = 3
                            }
                            let tempAddress = PickLoactionsReturn.init(fullAddress: suggestionAdressList[indexPath.row].address ?? "", Name: suggestionAdressList[indexPath.row].name ?? "", phoneNumber: suggestionAdressList[indexPath.row].phone ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landmark: suggestionAdressList[indexPath.row].landmark ?? "", lat: suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, tagId: tagId, otherAddress: suggestionAdressList[indexPath.row].otherAddress ?? "", id: "\(suggestionAdressList[indexPath.row].id ?? 0)")
                            kVC.pickLocationStrctReturn = tempAddress
                        }else{
                            var tagId = 0
                            if suggestionAdressList[indexPath.row].tag ?? "" == "Work" {
                                tagId = 1
                            }else if suggestionAdressList[indexPath.row].tag ?? "" == "Home"{
                                tagId = 2
                            }else{
                                tagId = 3
                            }
                            let tempAddress = DropLoactionsReturn.init(fullAddress: suggestionAdressList[indexPath.row].address ?? "", Name: suggestionAdressList[indexPath.row].name ?? "", phoneNumber: suggestionAdressList[indexPath.row].phone ?? "", Appartment: suggestionAdressList[indexPath.row].apartment ?? "", landmark: suggestionAdressList[indexPath.row].landmark ?? "", tagId: tagId, lat: suggestionAdressList[indexPath.row].lat ?? 0.0, lng: suggestionAdressList[indexPath.row].lng ?? 0.0, otherAddress: suggestionAdressList[indexPath.row].otherAddress ?? "", id: "\(suggestionAdressList[indexPath.row].id ?? 0)")
                            kVC.dropLoactionsStructReturn = tempAddress
                        }
                    case .HomeLocationSelect:
                        break
                    case .shopeByCat:
                        break
                    case .StoreNearYou:
                        break
                    case .storeDetail:
                        break
                    case .cartDetail:
                        break
                    }
                    self.navigationController?.popToViewController(vc, animated: false)
                }
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetAddressData"), object: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionLocationCell", for: indexPath) as! SuggestionLocationCell
        cell.lblLocation.text = "\(suggestionAdressList[indexPath.row].address ?? ""),\(suggestionAdressList[indexPath.row].landmark ?? ""),\(suggestionAdressList[indexPath.row].otherAddress ?? "")"
        if suggestionAdressList[indexPath.row].tag == "Home"{
            cell.imgLogo.image = UIImage(named: "home")
        }else if suggestionAdressList[indexPath.row].tag == "Work"{
            cell.imgLogo.image = UIImage(named: "work")
        }else{
            cell.imgLogo.image = UIImage(named: "Other")
        }
        if suggestionAdressList[indexPath.row].tag == "Other"{
            cell.lblTagType.text = suggestionAdressList[indexPath.row].otherAddress ?? ""
        }else{
            cell.lblTagType.text = suggestionAdressList[indexPath.row].tag ?? ""
        }
        return cell
    }
    
    
}

//MARK:- Hit Api
extension SearchLoactionVC{
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getAddresses, withParameters: paarams as [String: AnyObject], ofType: GetAddressAPI.self, success2: { (profileAPI) in
            self.suggestionAdressList = profileAPI
            CommonFunctions.hideLoader()
            print(profileAPI)
            self.tableView.reloadData()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
