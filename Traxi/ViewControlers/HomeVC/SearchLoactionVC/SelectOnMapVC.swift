//
//  SelectOnMapVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 23/12/20.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol SelectOnMapVCDelegate : class{
    func getCoordinates(_ cordinates : String, _ LocationName : String)
}

enum SelectOnMapVCSceenType{
    case pickup, drop, pickUpReturn, dropReturn, HomeLocationSelect, shopeByCat, StoreNearYou, storeDetail, cartDetail
}

class SelectOnMapVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnDone: LoadingButton!
    @IBOutlet weak var viewShowLocation: UIView!
    @IBOutlet weak var lblLocationName: UILabel!
    @IBOutlet weak var tfSearch: UITextFieldWxtension!
    
    //MARK:- Variables
    var addedLocation : loc?
    let locationManager = CLLocationManager()
    var marker : GMSMarker!
    var delegate : SelectOnMapVCDelegate?
    var sType : SelectOnMapVCSceenType = .pickup
    
    var isHitDraggingLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        // Do any additional setup after loading the view.
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActDone(_ sender: Any) {
//        delegate?.getCoordinates("\(addedLocation?.lat ?? 0),\(addedLocation?.lng ?? 0)", lblLocationName.text ?? "")
//        print("\(addedLocation?.lat ?? 0),\(addedLocation?.lng ?? 0)")
//        self.navigationController?.popViewController(animated: true)
        var isFindVc = false
        let allVcs = self.navigationController!.viewControllers
        for vc in allVcs{
            if vc.isKind(of: AddPickUpLoactionVC.self){
                let kVC = vc as! AddPickUpLoactionVC
                let tempAddedData = loc.init(addedLocation?.lat ?? 0, lng: addedLocation?.lng ?? 0, name: lblLocationName.text ?? "", fullAddress: lblLocationName.text ?? "")
                kVC.addedLocation = tempAddedData
                self.navigationController?.popToViewController(vc, animated: false)
                isFindVc = true
            }
        }
        if !isFindVc{
            let vc = AddPickUpLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
            let tempAddedData = loc.init(addedLocation?.lat ?? 0, lng: addedLocation?.lng ?? 0, name: lblLocationName.text ?? "", fullAddress: lblLocationName.text ?? "")
            switch sType {
            case .pickup:
                vc.sType = .pickUpLocation
            case .drop:
                vc.sType = .DropLocation
            case .pickUpReturn:
                vc.sType = .pickUpReturn
            case .dropReturn:
                vc.sType = .DropReturn
            case .HomeLocationSelect:
                vc.fromHomeLocation = true
                vc.sType = .homelocation
            case .shopeByCat:
                vc.fromShopeByCat = true
                vc.sType = .shopeByCat
            case .StoreNearYou:
                vc.fromStoreNearYou = true
                vc.sType = .StoreNearYou
            case .storeDetail:
                vc.fromStoreDetail = true
                vc.sType = .storeDetail
            case .cartDetail:
                vc.formCartDetail = true
                vc.sType = .cartDetail
            }
            vc.addedLocation = tempAddedData
            vc.notPopWork = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}

extension SelectOnMapVC{
    //MARK:-  function for get country from lat, long
    func getPlace(for location: CLLocation,
           completion: @escaping (CLPlacemark?) -> Void) {
      let geocoder = CLGeocoder()
      geocoder.reverseGeocodeLocation(location) { placemarks, error in
        guard error == nil else {
          print("*** Error in \(#function): \(error!.localizedDescription)")
          completion(nil)
          return
        }
        guard let placemark = placemarks?[0] else {
          print("*** Error in \(#function): placemark is nil")
          completion(nil)
          return
        }
        completion(placemark)
      }
    }
    
    //MARK:- SetUpUI
    func setUpUI(){
        setMapSetting()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true

        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 130, right: 20)
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        tfSearch.setPlaceHolderColor(UIColor.lightGray, placeholderText: NSLocalizedString("Search", comment: ""), placeholderFont: UIFont.NunitoRegular(15))
        tfSearch.layer.cornerRadius = 4.0
        tfSearch.openLeftViewWithImagenew(UIImage(named: "search")!, height: tfSearch.frame.height)
        viewShowLocation.isHidden = true
        
        LocationServices.shared.getAdress { (valueName, stringError) in
            if valueName != nil {
                self.viewShowLocation.isHidden = false
                self.lblLocationName.text = "\(valueName?.name ?? ""),\(valueName?.subAdministrativeArea ?? ""),\(valueName?.locality ?? ""),\(valueName?.administrativeArea ?? "")"
                self.marker = GMSMarker(position: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0, longitude: valueName?.location?.coordinate.longitude ?? 0))
                self.setLoca(valueName!)
                self.marker.map = self.mapView
                self.mapView.delegate = self
                self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0, longitude: valueName?.location?.coordinate.longitude ?? 0))
                self.isHitDraggingLocation = true
            }else{
            }
        }
    }
}

//MARK:- UItextField Delegates
extension SelectOnMapVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if textField == tfSearch{
            findLocation.sharedInstance.pickerLocation(presentInVc: self, countryUs: false, pickedListner: { loc in
                self.viewShowLocation.isHidden = false
                self.tfSearch.text = loc.fullAddress
                self.addedLocation = loc
                self.marker = GMSMarker(position: CLLocationCoordinate2D.init(latitude: loc.lat, longitude: loc.lng))
                self.marker.map = self.mapView
                self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: loc.lat, longitude: loc.lng))
            }) {
            }
        }
    }
}

extension SelectOnMapVC{
    //MARK:- SetUpMapUi
    func setMapSetting(){
        mapView.animate(toZoom: 15)
        mapView.settings.zoomGestures = true
        mapView.settings.rotateGestures = true
        mapView.settings.scrollGestures = true
    }
    
    //MARK:- Init location
    func setLoca(_ valueName : CLPlacemark){
        let loca = loc.init(valueName.location?.coordinate.latitude ?? 0, lng: valueName.location?.coordinate.longitude ?? 0, name: valueName.subAdministrativeArea ?? "", fullAddress: "\(valueName.name ?? ""), \(valueName.subAdministrativeArea ?? ""),\(valueName.locality ?? ""), \(valueName.administrativeArea ?? ""), \(valueName.country ?? "")")
        self.addedLocation = loca
    }
}

//MARK:- Map Delegate Methods
extension SelectOnMapVC: CLLocationManagerDelegate, GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        self.viewShowLocation.isHidden = false
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
//            self.getPlace(for: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { (country) in
//                self.lblLocationName.text = "\(country?.subLocality ?? ""),\(country?.locality ?? ""),\(country?.country ?? "")"
//            }
//        }
//        if marker != nil{
//            marker.position = CLLocationCoordinate2D.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
//            let loca = loc.init(coordinate.latitude, lng: coordinate.longitude, name: "", fullAddress: "")
//            self.addedLocation = loca
//        }else{
//
//            self.marker = GMSMarker(position: CLLocationCoordinate2D.init(latitude: coordinate.latitude, longitude: coordinate.longitude))
//            self.marker.map = self.mapView
//
//            self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: coordinate.latitude, longitude: coordinate.longitude))
//            let loca = loc.init(coordinate.latitude, lng: coordinate.longitude, name: "", fullAddress: "")
//            self.addedLocation = loca
//        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        self.viewShowLocation.isHidden = false
        let lat = mapView.camera.target.latitude
        print(lat)

        let lon = mapView.camera.target.longitude
        print(lon)
        if marker != nil{
            marker.position = CLLocationCoordinate2D.init(latitude: lat, longitude: lon)
            let loca = loc.init(lat, lng: lon, name: "", fullAddress: "")
            self.addedLocation = loca
        }else{

            self.marker = GMSMarker(position: CLLocationCoordinate2D.init(latitude: lat, longitude: lon))
            self.marker.map = self.mapView

            self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: lat, longitude: lon))
            let loca = loc.init(lat, lng: lon, name: "", fullAddress: "")
            self.addedLocation = loca
        }

        let loca = loc.init(lat, lng: lat, name: "", fullAddress: "")
        self.addedLocation = loca
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            self.returnPostionOfMapView(mapView: mapView)
        }
        
      }

    func returnPostionOfMapView(mapView:GMSMapView){
       let geocoder = GMSGeocoder()
       let latitute = mapView.camera.target.latitude
       let longitude = mapView.camera.target.longitude
       let position = CLLocationCoordinate2DMake(latitute, longitude)
       geocoder.reverseGeocodeCoordinate(position) { response , error in
         if error != nil {
           print("GMSReverseGeocode Error: \(String(describing: error?.localizedDescription))")
         }else {
           let result = response?.results()?.first
           let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
//            self.marker.position = CLLocationCoordinate2D.init(latitude: latitute, longitude: longitude)
            let loca = loc.init(latitute, lng: longitude, name: address ?? "", fullAddress: address ?? "")
            self.addedLocation = loca
            self.lblLocationName.text = address ?? ""
         }
       }
     }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        self.lblLocation.text = locations.description
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
//        googleMapView.clear()
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
//        self.lblLocation.text = marker.title
    }
    
}
