//
//  SuggestionLocationCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit

class SuggestionLocationCell: UITableViewCell {

    @IBOutlet weak var lblTagType: UILabel! //1-Home, 2-Work, 3-Others
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgLogo: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
