//
//  HomeVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit
import ENSwiftSideMenu
import GoogleMaps
import GooglePlaces

class HomeVC: UIViewController, UIGestureRecognizerDelegate {

    //MARK:- OUTLETS
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var btnTopCurrentLocation: UIButton!
    @IBOutlet weak var vwButtomDeliveries: UIView!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblOrderType: UILabel!
    @IBOutlet weak var btnTrackOrder: UIButton!
    @IBOutlet weak var circleNotificationView: setCornerRadiusCircle!
    
    //MARK:- Variables
    var addedLocation : loc?
    var activeOrderData : [ActiveOrdersAPIElement] = []
    
    //MARK:- override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        circleNotificationView.isHidden = true
        if coredata.shared.isEmailVerified == 0{
            //Call popUp if email not verified
            if isEmailPopShowOnce{
                let popOverVC = NotePopUpVc()
                popOverVC.sType = .EmailVerification
                self.addChild(popOverVC)
                popOverVC.view.frame = self.view.frame
                self.view.addSubview(popOverVC.view)
                popOverVC.didMove(toParent: self)
                isEmailPopShowOnce = false
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "CheckLocation"), object: nil)
        self.vwButtomDeliveries.isHidden = true
        self.getActiveOrderDetials()
        GetSavedCardApi(nil)
        GetProfileApiHit()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
        self.sideMenuController()?.sideMenu?.allowPanGesture = true
        self.sideMenuController()?.sideMenu?.allowRightSwipe = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    @IBAction func btnActBookDelivery(_ sender: Any) {
        let vc = DeliveryTypeVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActShopNow(_ sender: Any) {
        let vc = ShopCategoriesVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActSideMenu(_ sender: Any) {
        toggleSideMenuView()
    }
    
    @IBAction func btnActSelectTopCurrentLocation(_ sender: Any) {
        let vc = SearchLoactionVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.sType = .HomeLocationSelect
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActTrackOrder(_ sender: Any) {
        if self.activeOrderData.count > 0{
            if self.activeOrderData[0].jobType == 2{
                let vc = OrderStatusVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
                vc.activeOrderData = activeOrderData[0]
                vc.orderId = activeOrderData[0].id 
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = DeliveryStatusVC.instantiateFromAppStoryboard(appStoryboard: .Home)
                vc.deliveryId = activeOrderData[0].id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

//MARK:- setUp view Did Load Data
extension HomeVC{
    func setUpUi(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            if self.navigationController != nil{
                let nav = self.navigationController as? navController
                nav?.reloadMenuView(self)
                self.sideMenuController()?.sideMenu?.delegate = self
                self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
                self.sideMenuController()?.sideMenu?.allowPanGesture = true
                self.sideMenuController()?.sideMenu?.allowRightSwipe = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
                
            }
        }
        if globalAddedLocation == nil{
            self.setDataViewWillAppear()
        }else{
            self.btnTopCurrentLocation.setTitle(globalAddedLocation?.fullAddress, for: .normal)
            EditProfileApiHit()
        }
        self.GetCategoriesApiHit()
    }
    
    func setDataViewWillAppear(){
        LocationServices.shared.getAdress { (valueName, stringError) in
            if valueName != nil {
                self.setLoca(valueName!)
             }else{
             }
        }
    }
    
    func setLoca(_ valueName : CLPlacemark){
        let loca = locNew.init(valueName.location?.coordinate.latitude ?? 0, lng: valueName.location?.coordinate.longitude ?? 0, name: valueName.subAdministrativeArea ?? "", fullAddress: "\(valueName.name ?? ""), \(valueName.subAdministrativeArea ?? ""),\(valueName.locality ?? ""), \(valueName.administrativeArea ?? ""), \(valueName.country ?? "")", Appartment: valueName.subAdministrativeArea ?? "", landMark: valueName.locality ?? "")
        globalAddedLocation = loca
        self.btnTopCurrentLocation.setTitle(loca.fullAddress, for: .normal)
        EditProfileApiHit()
    }
    
    @objc func loadList(notification: NSNotification){
        //load data here
        if globalAddedLocation == nil{
            self.setDataViewWillAppear()
        }else{
            self.btnTopCurrentLocation.setTitle(globalAddedLocation?.fullAddress, for: .normal)
            EditProfileApiHit()
        }
        self.GetProfileApiHit()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
    }
}

//MARK:- Hit APi
extension HomeVC{
    func getActiveOrderDetials(){
        let paarams : [String : AnyObject] = [
            :
        ]
        
        ApiHandler.callApiWithParameters(url: APIConstants.activeOrders, withParameters: paarams as [String: AnyObject], ofType: ActiveOrdersAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.activeOrderData = profileAPI
            if profileAPI.count > 0{
                self.vwButtomDeliveries.isHidden = false
                if profileAPI[0].jobType == 2{
                    self.lblOrderType.text = "Order food"
                    self.lblShopName.text = profileAPI[0].storeName ?? ""
                }else{
                    self.lblShopName.text = "Delivery"
                    self.lblOrderType.text = "Order delivery"
                }
            }else{
                self.vwButtomDeliveries.isHidden = true
            }
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetCategoriesApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getCategories, withParameters: paarams as [String: AnyObject], ofType: CategoriesApis.self, success2: { (profileAPI) in
            
            titilesName.removeAll()
            for data in profileAPI{
                titilesName.append(data.name ?? "")
                titileId.append(data.id ?? 0)
            }
            print(profileAPI)
//            CommonFunctions.hideLoader()
        }, failure: { (false, string) in
//            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            if (profileAPI.unreadComplains ?? 0) > 0 || (profileAPI.unreadNotifications ?? 0) > 0{
                self.circleNotificationView.isHidden = false
            }else{
                self.circleNotificationView.isHidden = true
            }
            print(profileAPI)
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
}

// MARK: - DELEGATE FUNCTIONS
extension HomeVC: ENSideMenuDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
            
            self.view.bringSubviewToFront(fullView)
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        toggleSideMenuView()
    }
}
