//
//  ConfirmYourRequestVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ConfirmYourRequestVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var vwCalculateHeight: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    
    //MARK:- Variables
    var timer = Timer()
    var poseDuration = 3
    var indexProgressBar = 0
    var currentPoseIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
        progressBar.progress = 0.0
        // Do any additional setup after loading the view.
    }

    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActCancelRequest(_ sender: Any) {
        let popOverVC = CancelOrderPopUpVC()
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
    }
}

extension ConfirmYourRequestVC{
    //MARK:- set viewDidLoadData
    func setUpUi(){
        btnBack.layer.cornerRadius = btnBack.bounds.height/2
        btnBack.clipsToBounds = true
        btnBack.layer.masksToBounds = false
        btnBack.layer.shadowColor = UIColor.black.cgColor
        btnBack.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//            imgs.layer.shadowPath = shadowPath.cgPath
        btnBack.layer.shadowOpacity = 0.5
        btnBack.layer.shadowRadius = 5.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.vwCorner.roundCorners([.topRight, .topLeft], radius: 24)
            self.progressBar.clipsToBounds = true
            self.progressBar.roundCorners([.topRight, .topLeft], radius: 24)
        }
        
        DispatchQueue.main.async {
            self.scrollViewHeight.constant = self.view.bounds.height/2-10
        }
        
        progressBar.transform = progressBar.transform.scaledBy(x: 1, y: 1.5)
        
        // display the first pose
        getNextPoseData()

        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(setProgressBar), userInfo: nil, repeats: true)
    }
    
    //MARK:- Methods of progress bar
    func getNextPoseData(){
        // do next pose stuff
        currentPoseIndex += 1
        print(currentPoseIndex)
    }

    @objc func setProgressBar(){
        if indexProgressBar == poseDuration{
//                getNextPoseData()
//
//                // reset the progress counter
//                indexProgressBar = 0
//            CommonFunctions.toster("ProgressCompleted")
            let vc = DeliveryStatusVC.instantiateFromAppStoryboard(appStoryboard: .Home)
            self.navigationController?.pushViewController(vc, animated: true)
        }

        // update the display
        // use poseDuration - 1 so that you display 20 steps of the the progress bar, from 0...19
        progressBar.progress = Float(indexProgressBar) / Float(poseDuration - 1)

        // increment the counter
        indexProgressBar += 1
    }
    
    //MARK:- Set map setting
    func setMapSetting(){
        mapView.animate(toZoom: 15)
        mapView.settings.zoomGestures = true
        mapView.settings.rotateGestures = true
        mapView.settings.scrollGestures = true
        mapView.isUserInteractionEnabled = true
        LocationServices.shared.getAdress { (valueName, stringError) in
            if valueName != nil {
                self.mapView.animate(toLocation: CLLocationCoordinate2D.init(latitude: valueName?.location?.coordinate.latitude ?? 0.0, longitude: valueName?.location?.coordinate.longitude ?? 0.0))
             }else{
             }
        }
    }
}
