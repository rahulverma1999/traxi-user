//
//  ReviewDeleiveredVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/12/20.
//

import UIKit
import Cosmos
import GrowingTextView

class ReviewDeleiveredVC: UIViewController {
    
    //MARK:- outlets
    @IBOutlet weak var btnSubmit: LoadingButton!
    @IBOutlet weak var cosmoasView: CosmosView!
    @IBOutlet weak var tvDes: GrowingTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Button Actions
    @IBAction func btnActSubmit(_ sender: Any) {
    }
    
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- setUp viewDidLoadData
extension ReviewDeleiveredVC{
    func setUpUI(){
        tvDes.layer.cornerRadius = 4
        tvDes.layer.borderWidth = 1
        tvDes.layer.borderColor = Colors.borderYellowColorTraxi.color().cgColor
        tvDes.textContainerInset = UIEdgeInsets(top: 8,left: 12,bottom: 8,right: 12)
    }
}
