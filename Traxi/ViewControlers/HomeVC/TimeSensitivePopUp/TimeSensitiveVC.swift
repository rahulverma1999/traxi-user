//
//  TimeSensitiveVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 02/12/20.
//

import UIKit

class TimeSensitiveVC: BasePopUp {

    //MARK:- outlets
    @IBOutlet weak var bottomCorner: UIView!
    @IBOutlet weak var innerView: cornerRadiusView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var topCorner: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        outerVw = outerView
        innerVw = innerView
        showAnimate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.topCorner.roundCorners([.topRight, .topLeft], radius: 4)
            self.bottomCorner.roundCorners([.bottomRight, .bottomLeft], radius: 4)
        }
        // Do any additional setup after loading the view.
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        stopAnimation()
    }
}
