//
//  BookingHistoryVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 11/12/20.
//

import UIKit
import ENSwiftSideMenu
import ESPullToRefresh

enum buttonHandlerBooking{
    case upcoming, complete, canceled
}

class BookingHistoryVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDeliveries: LoadingButton!
    @IBOutlet weak var btnStore: LoadingButton!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var btnComlete: UIButton!
    @IBOutlet weak var tabkeView: UITableView!
    @IBOutlet weak var btnCancelled: UIButton!
    @IBOutlet weak var lblNoDataFOund: UILabel!
    @IBOutlet weak var vwnotificationCount: setCornerRadiusCircle!
    
    //MARK:- Variables
    var isDeleviers = true
    var isUpcoming = true
    var buttonHandler : buttonHandlerBooking =  .upcoming
    var DeliveryType = 1 //1:- Upcoming, 2:- completed, 3:- canceled
    var canPaginateDelivery : Bool = true
    var canPaginateOrder : Bool = true
    var allDeliveresData : [GetAllDeliveresAPIElement] = []
    var allOrdersData : GetAllOrdersAPI?//[GetAllOrdersAPIElement] = []
    var firstTimeClick = true
    
    var btnTypeStore = 1 //1:- Upcoming, 2:- completed, 3:- canceled
    var btnTypeDeliveres = 1 //1:- Upcoming, 2:- completed, 3:- canceled
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetUpUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwnotificationCount.isHidden = true
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "CancelOrder"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        
        GetProfileApiHit()
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        toggleSideMenuView()
    }
    @IBAction func btnActDeliveries(_ sender: Any) {
        isDeleviers = true
        setDeliuveriesAndCartButtons(false)
    }
    @IBAction func btnActStore(_ sender: Any) {
        isDeleviers = false
//        if firstTimeClick{
//            self.buttonHandler = .upcoming
//        }
        setDeliuveriesAndCartButtons(true)
    }
    @IBAction func btnActUpcoming(_ sender: Any) {
        self.buttonHandler = .upcoming
        btnDisplayHandle()
    }
    @IBAction func btnActComplete(_ sender: Any) {
        self.buttonHandler = .complete
        btnDisplayHandle()
    }
    @IBAction func btnActCancelled(_ sender: Any) {
        self.buttonHandler = .canceled
        btnDisplayHandle()
    }
}


extension BookingHistoryVC{
    //MARK:- setupUI
    func SetUpUI(){
        btnComlete.layer.borderWidth = 1
        btnComlete.layer.borderColor = Colors.pinkColorTraxi.color().cgColor
        btnComlete.setTitleColor(Colors.pinkColorTraxi.color(), for: .normal)
        btnComlete.backgroundColor = .clear
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            if self.navigationController != nil{
                self.sideMenuController()?.sideMenu?.delegate = self
                self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
                self.sideMenuController()?.sideMenu?.allowPanGesture = true
                self.sideMenuController()?.sideMenu?.allowRightSwipe = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
            }
        }
        btnDisplayHandle()
//        self.getAllDeliveresApi(1, false)
        tabkeView.alwaysBounceVertical = true
        tabkeView.es.addPullToRefresh {
            if self.isDeleviers{
                self.canPaginateDelivery = true
                if self.DeliveryType == 1{
                    self.getAllDeliveresApi(1, false)
                }else if self.DeliveryType == 2{
                    self.getAllDeliveresApi(4, false)
                }else{
                    self.getAllDeliveresApi(5, false)
                }
            }else{
                self.canPaginateOrder = true
                if self.DeliveryType == 1{
                    self.getAllOrdersApi(1, false)
                }else if self.DeliveryType == 2{
                    self.getAllOrdersApi(2, false)
                }else{
                    self.getAllOrdersApi(3, false)
                }
            }
        }
    }
    
    func setUpCompingAndCompletedButtons(){
        if isUpcoming{
            btnUpcoming.setTitleColor(.white, for: .normal)
            btnUpcoming.backgroundColor = Colors.pinkColorTraxi.color()
            btnComlete.layer.borderWidth = 1
            btnComlete.layer.borderColor = Colors.pinkColorTraxi.color().cgColor
            btnComlete.setTitleColor(Colors.pinkColorTraxi.color(), for: .normal)
            btnComlete.backgroundColor = .clear
        }else{
            btnComlete.setTitleColor(.white, for: .normal)
            btnComlete.backgroundColor = Colors.pinkColorTraxi.color()
            btnUpcoming.layer.borderWidth = 1
            btnUpcoming.layer.borderColor = Colors.pinkColorTraxi.color().cgColor
            btnUpcoming.setTitleColor(Colors.pinkColorTraxi.color(), for: .normal)
            btnUpcoming.backgroundColor = .clear
        }
        self.tabkeView.reloadData()
    }
    
    func setDeliuveriesAndCartButtons(_ isStore : Bool){
//        self.buttonHandler = .upcoming
        if isStore{
            self.firstTimeClick = false
        }
        if !isStore{
            if btnTypeDeliveres == 1{
                buttonHandler = .upcoming
            }else if btnTypeDeliveres == 2{
                buttonHandler = .complete
            }else{
                buttonHandler = .canceled
            }
        }else{
            if btnTypeStore == 1{
                buttonHandler = .upcoming
            }else if btnTypeStore == 2{
                buttonHandler = .complete
            }else{
                buttonHandler = .canceled
            }
        }
        self.btnDisplayHandle()
        if isDeleviers{
            btnDeliveries.setTitleColor(.white, for: .normal)
            btnDeliveries.backgroundColor = Colors.pinkColorTraxi.color()
            btnDeliveries.setImage(UIImage(named: "deliveries_w"), for: .normal)
            btnStore.setTitleColor(.black, for: .normal)
            btnStore.backgroundColor = Colors.traxiSkinColor.color()
            btnStore.setImage(UIImage(named: "stores_b"), for: .normal)
            btnDeliveries.setTitleColor(.white, for: .normal)
        }else{
            btnStore.setTitleColor(.white, for: .normal)
            btnStore.backgroundColor = Colors.pinkColorTraxi.color()
            btnStore.setImage(UIImage(named: "stores_w"), for: .normal)
            btnDeliveries.setTitleColor(.black, for: .normal)
            btnDeliveries.backgroundColor = Colors.traxiSkinColor.color()
            btnDeliveries.setImage(UIImage(named: "deliveries_b"), for: .normal)
        }
//        self.tabkeView.reloadData()
    }
    
    func setBackgorunfSelected(_ btns : [UIButton]){
        for btn in btns{
            btn.setTitleColor(.white, for: .normal)
            btn.backgroundColor = Colors.pinkColorTraxi.color()
        }
    }
    
    func setBorderUnSelected(_ btns : [UIButton]){
        for btn in btns{
            btn.layer.borderWidth = 1
            btn.layer.borderColor = Colors.pinkColorTraxi.color().cgColor
            btn.setTitleColor(Colors.pinkColorTraxi.color(), for: .normal)
            btn.backgroundColor = .clear
        }
    }
    
    //MARK:- handle buttons
    func btnDisplayHandle(){
        self.allDeliveresData = []//.removeAll()
        self.allOrdersData?.orders = []//.removeAll()
        self.canPaginateDelivery = true
        self.canPaginateOrder = true
        switch self.buttonHandler{
        case .upcoming:
            self.DeliveryType = 1
            self.setBackgorunfSelected([self.btnUpcoming])
            self.setBorderUnSelected([self.btnComlete, self.btnCancelled])
            if self.isDeleviers{
                btnTypeDeliveres = 1
                self.getAllDeliveresApi(1, false)
            }else{
                btnTypeStore = 1
                self.getAllOrdersApi(1, false)
            }
        case .complete:
            self.setBackgorunfSelected([self.btnComlete])
            self.setBorderUnSelected([self.btnUpcoming, self.btnCancelled])
            self.DeliveryType = 2
            if self.isDeleviers{
                btnTypeDeliveres = 2
                self.getAllDeliveresApi(4, false)
            }else{
                btnTypeStore = 2
                self.getAllOrdersApi(2, false)
            }
        case .canceled:
            self.setBackgorunfSelected([self.btnCancelled])
            self.setBorderUnSelected([self.btnComlete, self.btnUpcoming])
            self.DeliveryType = 3
            if self.isDeleviers{
                btnTypeDeliveres = 3
                self.getAllDeliveresApi(5, false)
            }else{
                btnTypeStore = 3
                self.getAllOrdersApi(3, false)
            }
        }
        print(buttonHandler)
        self.tabkeView.reloadData()
    }
}

//MARK:- TableView delegate methods
extension BookingHistoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.buttonHandler{
        case .upcoming:
            self.lblNoDataFOund.text = "No Upcoming Data Found"
        case .complete:
            self.lblNoDataFOund.text = "No Complete Data Found"
        case .canceled:
            self.lblNoDataFOund.text = "No Cancelled Data Found"
        }
        if isDeleviers{
            if (allDeliveresData.count) == 0{
                self.lblNoDataFOund.isHidden = false
                return 0
            }else{
                self.lblNoDataFOund.isHidden = true
                return allDeliveresData.count
            }
        }else{
            if (allOrdersData?.orders?.count ?? 0) == 0{
                self.lblNoDataFOund.isHidden = false
                return 0
            }else{
                self.lblNoDataFOund.isHidden = true
                return allOrdersData?.orders?.count ?? 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isDeleviers{
            let vc = BookingDetailVC.instantiateFromAppStoryboard(appStoryboard: .BookingHistory)
            vc.deliveryId = self.allDeliveresData[indexPath.row].id ?? 0
            if self.DeliveryType == 1{
                vc.sType = .ongoingBooking
            }else if self.DeliveryType == 2{
                vc.sType = .CompletedBooking
            }else{
                vc.sType = .cancelledbooking
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = OrderDetailVC.instantiateFromAppStoryboard(appStoryboard: .BookingHistory)
            vc.orderId = self.allOrdersData?.orders?[indexPath.row].orderID ?? 0
            if self.DeliveryType == 1{
                vc.sType = .ongoingOrder
            }else if self.DeliveryType == 2{
                vc.sType = .completedOrder
            }else{
                vc.sType = .cancelled
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isDeleviers{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingHistoryCell", for: indexPath) as! BookingHistoryCell
            cell.allDeliveresData = self.allDeliveresData
            cell.DeliveryType = self.DeliveryType
            cell.setData(indexPath)
            cell.btnTrackDeleviery.tag = indexPath.row
            cell.btnTrackDeleviery.addTarget(self, action: #selector(getClickOfTrackOrder(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingStoreTableViewCell", for: indexPath) as! BookingStoreTableViewCell
//            if self.DeliveryType == 1{
//                cell.vwButtonTrack.isHidden = false
//                cell.totalConstant.constant = 45
//                cell.vwHideUnHide.isHidden = false
//            }else if self.DeliveryType == 2{
//                cell.vwButtonTrack.isHidden = true
//                cell.totalConstant.constant = 12
//                cell.vwHideUnHide.isHidden = true
//            }else{
//                cell.vwButtonTrack.isHidden = true
//                cell.totalConstant.constant = 12
//                cell.vwHideUnHide.isHidden = true
//            }
            if self.allOrdersData?.orders?[indexPath.row].orderStatus == 0{
                cell.lblBookingStatus.text = "PENDING"
                cell.lblBookingStatus.textColor = Colors.pinkColorTraxi.color()
            }else if self.allOrdersData?.orders?[indexPath.row].orderStatus == 6 || self.allOrdersData?.orders?[indexPath.row].orderStatus == 7 || self.allOrdersData?.orders?[indexPath.row].orderStatus == 8{
                cell.lblBookingStatus.text = "CANCELLED"
                cell.lblBookingStatus.textColor = Colors.traxiredColor.color()
            }else if self.allOrdersData?.orders?[indexPath.row].orderStatus == 5{
                cell.lblBookingStatus.text = "DELIVERED"
                cell.lblBookingStatus.textColor = Colors.traxiGreenColor.color()
            }else{
                cell.lblBookingStatus.text = "ACCEPTED"
                cell.lblBookingStatus.textColor = Colors.traxiGreenColor.color()
            }
            if self.allOrdersData?.orders?[indexPath.row].orderStatus == 4 || self.allOrdersData?.orders?[indexPath.row].orderStatus == 3{
                cell.vwButtonTrack.isHidden = false
                cell.totalConstant.constant = 45
                cell.vwHideUnHide.isHidden = false
            }else{
                cell.vwButtonTrack.isHidden = true
                cell.totalConstant.constant = 12
                cell.vwHideUnHide.isHidden = true
            }
            cell.btnTrack.tag = indexPath.row
            cell.btnTrack.addTarget(self, action: #selector(getClickOfTrackStore(_:)), for: .touchUpInside)
            cell.imgShop.yy_setImage(with: URL(string: CommonFunctions.getImage(self.allOrdersData?.orders?[indexPath.row].storeImage ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
            cell.lblStoreNake.text = self.allOrdersData?.orders?[indexPath.row].storeName ?? ""
            cell.lblShopRating.text = "\(self.allOrdersData?.orders?[indexPath.row].storeRatings ?? 0.0)"
            cell.lblShopLocation.text = self.allOrdersData?.orders?[indexPath.row].storeAddress ?? ""
//            var totalPrice = 0.0
//            for data in self.allOrdersData[indexPath.row].products ?? []{
//                totalPrice+=(data.price ?? 0.0)
//            }
            cell.lblTotalPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble((self.allOrdersData?.orders?[indexPath.row].orderTotal ?? 0.0), decimalPlace: 2))"
            cell.lblOrderDate.text = CommonFunctions.getDateFormatTraxi4(self.allOrdersData?.orders?[indexPath.row].createdAt ?? "")
            cell.imgProduct1.layer.cornerRadius = 4.0
            cell.imgProduct2.layer.cornerRadius = 4.0
            if (self.allOrdersData?.orders?[indexPath.row].products?.count ?? 0) > 2{
                cell.vwViewMore.isHidden = false
                cell.vwScondOrder.isHidden = false
            }else if (self.allOrdersData?.orders?[indexPath.row].products?.count ?? 0) == 1{
                cell.vwViewMore.isHidden = true
                cell.vwScondOrder.isHidden = true
            }else {
                cell.vwScondOrder.isHidden = false
                cell.vwViewMore.isHidden = true
                
            }
           
            if (self.allOrdersData?.orders?[indexPath.row].products?.count ?? 0) == 1{
                cell.lblQtyProduct1.text = "Qty \(self.allOrdersData?.orders?[indexPath.row].products?[0].quantity ?? 1)"
                cell.lblPriceProduct1.text = "$\((self.allOrdersData?.orders?[indexPath.row].products?[0].price ?? 0.0)/(Double(self.allOrdersData?.orders?[indexPath.row].products?[0].quantity ?? 1)))"
                cell.imgProduct1.yy_setImage(with: URL(string: CommonFunctions.getImage(self.allOrdersData?.orders?[indexPath.row].products?[0].image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
                cell.lblTotalPriceProduct1.text = "$\((self.allOrdersData?.orders?[indexPath.row].products?[0].price ?? 0.0))"
                cell.lblNameProduct1.text = self.allOrdersData?.orders?[indexPath.row].products?[0].name ?? ""
            }else if (self.allOrdersData?.orders?[indexPath.row].products?.count ?? 0) > 0 && (self.allOrdersData?.orders?[indexPath.row].products?.count ?? 0) < ((self.allOrdersData?.orders?[indexPath.row].products?.count ?? 0) + 1){
                cell.lblQtyProduct1.text = "Qty \(self.allOrdersData?.orders?[indexPath.row].products?[0].quantity ?? 1)"
                cell.lblPriceProduct1.text = "$\((self.allOrdersData?.orders?[indexPath.row].products?[0].price ?? 0.0)/(Double(self.allOrdersData?.orders?[indexPath.row].products?[0].quantity ?? 1)))"
                cell.imgProduct1.yy_setImage(with: URL(string: CommonFunctions.getImage(self.allOrdersData?.orders?[indexPath.row].products?[0].image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
                cell.lblTotalPriceProduct1.text = "$\((self.allOrdersData?.orders?[indexPath.row].products?[0].price ?? 0.0))"
                cell.lblNameProduct1.text = self.allOrdersData?.orders?[indexPath.row].products?[0].name ?? ""
                
                cell.lblQtyProduct2.text = "Qty \(self.allOrdersData?.orders?[indexPath.row].products?[1].quantity ?? 1)"
                cell.lblPriceProduct2.text = "$\((self.allOrdersData?.orders?[indexPath.row].products?[1].price ?? 0.0)/(Double(self.allOrdersData?.orders?[indexPath.row].products?[1].quantity ?? 1)))"
                cell.imgProduct2.yy_setImage(with: URL(string: CommonFunctions.getImage(self.allOrdersData?.orders?[indexPath.row].products?[1].image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
                cell.lblTotalPriceProduct2.text = "$\((self.allOrdersData?.orders?[indexPath.row].products?[1].price ?? 0.0))"
                cell.lblNameProduct2.text = self.allOrdersData?.orders?[indexPath.row].products?[1].name ?? ""
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isDeleviers{
            if indexPath.row == (self.allDeliveresData.count) - 3 && self.canPaginateDelivery{
                if self.DeliveryType == 1{
                    self.getAllDeliveresApi(1, true)
                }else if self.DeliveryType == 2{
                    self.getAllDeliveresApi(4, true)
                }else{
                    self.getAllDeliveresApi(5, true)
                }
            }
        }else{
            if indexPath.row == (self.allOrdersData?.orders?.count ?? 0) - 3 && self.canPaginateOrder{
                if self.DeliveryType == 1{
                    self.getAllOrdersApi(1, false)
                }else if self.DeliveryType == 2{
                    self.getAllOrdersApi(2, false)
                }else{
                    self.getAllOrdersApi(3, false)
                }
            }
        }
    }
}

//MARK:- objective C methods
extension BookingHistoryVC{
    @objc func getClickOfTrackOrder(_ sender : UIButton){
        let vc = DeliveryStatusVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.deliveryId = self.allDeliveresData[sender.tag].id ?? 0
        vc.fromListingScreen = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func getClickOfTrackStore(_ sender : UIButton){
        let vc = OrderStatusVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        vc.orderId = self.allOrdersData?.orders?[sender.tag].orderID ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func loadList(notification: NSNotification){
        //load data here
        self.btnDisplayHandle()
    }
}


extension BookingHistoryVC{
    func getAllDeliveresApi(_ status : Int ,_ pagination: Bool){ // status:- 1 = upcoming, 3 = completed, 5 = cancelled by user
        
        var page = 1
        if pagination{
            page+=1
        }
        
        let paarams : [String : AnyObject] = [
            "status" : status as AnyObject,
            "page" : page as AnyObject,
            "limit" : 10 as AnyObject
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getAllDeliveres, withParameters: paarams as [String: AnyObject], ofType: GetAllDeliveresAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            if profileAPI.count != 10{
                self.canPaginateDelivery = false
            }
            if pagination{
                self.allDeliveresData.append(contentsOf: profileAPI)
            }else{
                self.allDeliveresData = profileAPI
            }
            self.tabkeView.es.stopPullToRefresh()
            self.tabkeView.reloadData()
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            self.tabkeView.es.stopPullToRefresh()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func getAllOrdersApi(_ status : Int ,_ pagination: Bool){ // status:- 1 = upcoming, 3 = completed, 5 = cancelled by user
        
        var page = 1
        if pagination{
            page+=1
        }
        
        let paarams : [String : AnyObject] = [
            "status" : status as AnyObject,
            "page" : page as AnyObject,
            "limit" : 10 as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getAllOrders, withParameters: paarams as [String: AnyObject], ofType: GetAllOrdersAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            var tempData = profileAPI
            var tempData2 : [GetAllOrdersAPIElement] = []
            for dt in profileAPI.orders ?? []{
                tempData2.append(dt)
            }
            if (profileAPI.orders?.count ?? 0) != 10{
                self.canPaginateOrder = false
            }
            if pagination{
                tempData.orders?.append(contentsOf: tempData2)
//                self.allOrdersData?.orders?.append(contentsOf: profileAPI.orders ?? [])
            }else{
                tempData.orders = tempData2
//                self.allOrdersData?.orders = profileAPI.orders
            }
            self.allOrdersData = tempData
            self.tabkeView.es.stopPullToRefresh()
            self.tabkeView.reloadData()
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            self.tabkeView.es.stopPullToRefresh()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            if (profileAPI.unreadComplains ?? 0) > 0 || (profileAPI.unreadNotifications ?? 0) > 0{
                self.vwnotificationCount.isHidden = false
            }else{
                self.vwnotificationCount.isHidden = true
            }
            print(profileAPI)
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}


// MARK: - DELEGATE FUNCTIONS
extension BookingHistoryVC : ENSideMenuDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
            
            self.view.bringSubviewToFront(fullView)
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        toggleSideMenuView()
    }
    
}
