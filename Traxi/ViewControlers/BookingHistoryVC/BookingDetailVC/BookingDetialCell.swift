//
//  BookingDetialCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 14/12/20.
//

import UIKit

class BookingDetialCell: UITableViewCell {
    
    @IBOutlet weak var vrDotted: UIView!
    @IBOutlet weak var lblDropLocation: UILabel!
    @IBOutlet weak var lblLocations: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblUserDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
