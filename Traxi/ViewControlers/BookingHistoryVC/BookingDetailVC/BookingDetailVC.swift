//
//  BookingDetailVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 14/12/20.
//

import UIKit
import Cosmos

enum BookingDetailVCScreenType {
    case ongoingBooking, CompletedBooking, cancelledbooking
}

class BookingDetailVC: baseVC {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var raingView: UIView!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var btnTrackYourDelivery: UIButton!
    @IBOutlet weak var bottomContraint: NSLayoutConstraint!
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var vwDotted: UIView!
    @IBOutlet weak var vwDotted2: UIView!
    @IBOutlet weak var vwTableView: UIView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vwSendMsgs: UIView!
    @IBOutlet weak var btnSendMsgs: LoadingButton!
    @IBOutlet weak var lblDropShortAddress: UILabel!
    @IBOutlet weak var vwDropAdrressFull: UIView!
    @IBOutlet weak var lblDropTitle: UILabel!
    @IBOutlet weak var lblDropTime: UILabel!
    @IBOutlet weak var vwDropLocationSeprator: UIView!
    @IBOutlet weak var lblDeliveryStatus: UILabel!
    @IBOutlet weak var lblRqTypeTitle: UILabel!
    @IBOutlet weak var lblRqType: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var lblPickUpTime: UILabel!
    @IBOutlet weak var lblDeliveryType: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDeliveryCost: UILabel!
    @IBOutlet weak var lblPickUpLocation: UILabel!
    @IBOutlet weak var lblPickUpUserDetail: UILabel!
    @IBOutlet weak var lblPickUpPointTime: UILabel!
    @IBOutlet weak var lblDropLocation: UILabel!
    @IBOutlet weak var lblDropUserDetail: UILabel!
    @IBOutlet weak var lblDropPointTime: UILabel!
    @IBOutlet weak var btnPackages: UIButton!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDurationTime: UILabel!
    @IBOutlet weak var vwReturnDeliverys: UIView!
    @IBOutlet weak var lblPickupLocationReturn: UILabel!
    @IBOutlet weak var lblUserDetailPickUpReturn: UILabel!
    @IBOutlet weak var lblTimePickupReturn: UILabel!
    @IBOutlet weak var vwAddLayerReturn: UIView!
    @IBOutlet weak var lblDropLocationReturn: UILabel!
    @IBOutlet weak var lblUserDetailDropUpReturn: UILabel!
    @IBOutlet weak var lblDropTimereturn: UILabel!
    @IBOutlet weak var deiverDetailView: UIView!
    
    //MARK:- Variables
    var sType : BookingDetailVCScreenType = .ongoingBooking
    var dropLoactions : [String] = ["1","2","3","4","5"]
    var deliveryId : Int!
    var deliveryDetailData : DeliveryDetailAPi?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        getDeliveryDetailApi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tableView.removeObserver(self, forKeyPath: "contentSize")
        super.viewWillDisappear(true)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if let newvalue = change?[.newKey]
            {
                DispatchQueue.main.async {
                    self.tableView.layoutIfNeeded()
                    let newsize  = newvalue as! CGSize
                    self.tableViewHeight.constant = newsize.height
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func btnActTrackYourDelivery(_ sender: Any) {
        let vc = DeliveryStatusVC.instantiateFromAppStoryboard(appStoryboard: .Home)
        vc.deliveryId = self.deliveryDetailData?.id ?? 0
        vc.fromListingScreen = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActSendMsgs(_ sender: Any) {
        let vc = RiseComplaintVC.instantiateFromAppStoryboard(appStoryboard: .BookingHistory)
        vc.deliveryId = self.deliveryDetailData?.id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension BookingDetailVC{
    //MARK:- setUpUI
    func setUpUI(){
        switch sType {
        case .ongoingBooking:
            break
//            lblDeliveryStatus.text = "ONGOING"
//            lblDeliveryStatus.textColor = Colors.pinkColorTraxi.color()
//            lblRqTypeTitle.text = "REQUEST TYPE"
//            lblRqType.text = "Time-Flexible"
//            lblRqType.textColor = Colors.traxiBlueColor.color()
        case .CompletedBooking:
            break
        case .cancelledbooking:
            break
//            bottomContraint.constant = 20
//            btnTrackYourDelivery.isHidden = true
//            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
//            self.addDotedLayer(View: self.vwDotted2, color: UIColor.gray.cgColor)
//            vwDropAdrressFull.isHidden = true
//            lblDropShortAddress.isHidden = true
//            lblDropTitle.text = "4 DROP OFF POINTS"
//            vwSendMsgs.isHidden = false
//            vwTableView.isHidden = false
//            raingView.isHidden = false
//            lblDropTime.isHidden = true
//            vwDropLocationSeprator.backgroundColor = .clear
//            lblDeliveryStatus.text = "CANCELLED"
//            lblDeliveryStatus.textColor = Colors.traxiredColor.color()
//            lblRqTypeTitle.text = "DROP-OFF POINTS"
//            lblRqType.text = "4"
//            lblRqType.textColor = .black
        }
        
        //Button shadow
        
        btnTrackYourDelivery.clipsToBounds = true
        btnTrackYourDelivery.layer.masksToBounds = false
        btnTrackYourDelivery.layer.shadowColor = UIColor.black.cgColor
        btnTrackYourDelivery.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        btnTrackYourDelivery.layer.shadowOpacity = 0.5
        btnTrackYourDelivery.layer.shadowRadius = 5.0
        btnTrackYourDelivery.layer.cornerRadius = 4.0
        
        imgDriver.layer.cornerRadius = imgDriver.bounds.height/2
        
        self.tableView.reloadData()
        
        self.tableView.layoutIfNeeded()
        let contentSize = self.tableView.contentSize.height
        self.tableViewHeight.constant = contentSize
    }
    
    //MARK:- Add Doted Layer to view
    func addDotedLayer(View : UIView, color: CGColor){
        DispatchQueue.main.async {
            let lineLayer = CAShapeLayer()
            lineLayer.strokeColor = color
            lineLayer.lineWidth = 2
            lineLayer.lineDashPattern = [2,2]
            let path = CGMutablePath()
            path.addLines(between: [CGPoint(x: View.bounds.minX, y: View.bounds.minY),
                                    CGPoint(x: View.bounds.minX, y: View.bounds.maxY)])
            lineLayer.path = path
            View.layer.addSublayer(lineLayer)
        }
    }
    
    func setDataApi(){
        if self.deliveryDetailData?.deliveryStatus == 1{
            self.lblDeliveryStatus.text = "PENDING"
            self.lblDeliveryStatus.textColor = Colors.pinkColorTraxi.color()
            self.vwSendMsgs.isHidden = true
            self.raingView.isHidden = true
            self.btnTrackYourDelivery.isHidden = false
            self.bottomContraint.constant = 72
        }else if (deliveryDetailData?.deliveryStatus == 2) || (self.deliveryDetailData?.deliveryStatus == 3){
            self.lblDeliveryStatus.text = "ONGOING"
            self.lblDeliveryStatus.textColor = Colors.pinkColorTraxi.color()
            self.vwSendMsgs.isHidden = true
            self.raingView.isHidden = true
            self.btnTrackYourDelivery.isHidden = false
            self.bottomContraint.constant = 72
        }else if deliveryDetailData?.deliveryStatus == 4{
            self.lblDeliveryStatus.text = "COMPLETED"
            self.lblDeliveryStatus.textColor = Colors.traxiGreenColor.color()
            self.vwSendMsgs.isHidden = false
            self.raingView.isHidden = true
            self.btnTrackYourDelivery.isHidden = true
            self.bottomContraint.constant = 20
        }else if (self.deliveryDetailData?.deliveryStatus == 5) || (self.deliveryDetailData?.deliveryStatus == 6) {
            self.lblDeliveryStatus.text = "CANCELLED"
            self.lblDeliveryStatus.textColor = Colors.traxiredColor.color()
            self.vwSendMsgs.isHidden = true
            self.raingView.isHidden = true
            self.btnTrackYourDelivery.isHidden = true
            self.bottomContraint.constant = 20
        }
        
        if self.deliveryDetailData?.deliverTypeID == 1{
            self.lblDeliveryType.text = "Single Delivery"
            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
            vwDropAdrressFull.isHidden = false
            lblDropShortAddress.isHidden = false
            lblDropTitle.text = "DROP OFF POINTS"
            vwTableView.isHidden = true
            lblDropTime.isHidden = false
            vwReturnDeliverys.isHidden = true
            vwDropLocationSeprator.backgroundColor = Colors.borderYellowColorTraxi.color()
        }else if self.deliveryDetailData?.deliverTypeID == 2{
            self.lblDeliveryType.text = "Return Delivery"
            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
            vwDropAdrressFull.isHidden = false
            lblDropShortAddress.isHidden = false
            lblDropTitle.text = "DROP OFF POINTS"
            vwTableView.isHidden = true
            lblDropTime.isHidden = false
            vwReturnDeliverys.isHidden = false
            vwDropLocationSeprator.backgroundColor = Colors.borderYellowColorTraxi.color()
            self.addDotedLayer(View: self.vwAddLayerReturn, color: UIColor.gray.cgColor)
        }else{
            self.lblDeliveryType.text = "Traxi Run"
            self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
            self.addDotedLayer(View: self.vwDotted2, color: UIColor.gray.cgColor)
            vwDropAdrressFull.isHidden = true
            lblDropShortAddress.isHidden = true
            lblDropTitle.text = "\(self.deliveryDetailData?.dropoffDetails?.count ?? 0) DROP OFF POINTS"
            vwTableView.isHidden = false
            lblDropTime.isHidden = true
            vwReturnDeliverys.isHidden = true
            vwDropLocationSeprator.backgroundColor = .clear
        }
        
        if (self.deliveryDetailData?.pickupDetails?.count ?? 0) > 0{
            self.lblPickUpLocation.text = self.deliveryDetailData?.pickupDetails?[0].address ?? ""
            self.lblPickUpDate.text =  self.deliveryDetailData?.pickupDetails?[0].pickupDate ?? ""
            self.lblPickUpTime.text =  self.deliveryDetailData?.pickupDetails?[0].pickupTime ?? ""
            let userDetail = "\(self.deliveryDetailData?.pickupDetails?[0].userName ?? "") (\(self.deliveryDetailData?.pickupDetails?[0].userPhone ?? "")), \(self.deliveryDetailData?.pickupDetails?[0].userApartment ?? ""), \(self.deliveryDetailData?.pickupDetails?[0].userLandmark ?? "")"
            self.lblPickUpUserDetail.text = userDetail
            self.lblPickUpPointTime.text = CommonFunctions.getTraxiTime(self.deliveryDetailData?.pickupDetails?[0].pickedUpAt ?? "")
        }
        
        if (self.deliveryDetailData?.pickupDetails?.count ?? 0) > 1{
            self.lblPickupLocationReturn.text = self.deliveryDetailData?.pickupDetails?[1].address ?? ""
            let userDetail = "\(self.deliveryDetailData?.pickupDetails?[1].userName ?? "") (\(self.deliveryDetailData?.pickupDetails?[1].userPhone ?? "")), \(self.deliveryDetailData?.pickupDetails?[1].userApartment ?? ""), \(self.deliveryDetailData?.pickupDetails?[1].userLandmark ?? "")"
            self.lblUserDetailPickUpReturn.text = userDetail
            self.lblTimePickupReturn.text = CommonFunctions.getTraxiTime(self.deliveryDetailData?.pickupDetails?[1].pickedUpAt ?? "")
        }
        
        if (self.deliveryDetailData?.dropoffDetails?.count ?? 0) > 0{
            self.lblDropLocation.text = self.deliveryDetailData?.dropoffDetails?[0].address ?? ""
            let userDetail = "\(self.deliveryDetailData?.dropoffDetails?[0].userName ?? "") (\(self.deliveryDetailData?.dropoffDetails?[0].userPhone ?? "")), \(self.deliveryDetailData?.dropoffDetails?[0].userApartment ?? ""), \(self.deliveryDetailData?.dropoffDetails?[0].userLandmark ?? "")"
            self.lblDropUserDetail.text = userDetail
            self.lblDropTime.text = CommonFunctions.getTraxiTime(self.deliveryDetailData?.dropoffDetails?[0].deliveredAt ?? "")
        }
        
        if (self.deliveryDetailData?.dropoffDetails?.count ?? 0) > 1{
            self.lblDropLocationReturn.text = self.deliveryDetailData?.dropoffDetails?[1].address ?? ""
            let userDetail = "\(self.deliveryDetailData?.dropoffDetails?[1].userName ?? "") (\(self.deliveryDetailData?.dropoffDetails?[1].userPhone ?? "")), \(self.deliveryDetailData?.dropoffDetails?[1].userApartment ?? ""), \(self.deliveryDetailData?.dropoffDetails?[1].userLandmark ?? "")"
            self.lblUserDetailDropUpReturn.text = userDetail
            self.lblDropTimereturn.text = CommonFunctions.getTraxiTime(self.deliveryDetailData?.dropoffDetails?[1].deliveredAt ?? "")
        }
        
        if self.deliveryDetailData?.driverID != nil{
            self.deiverDetailView.isHidden = false
        }else{
            self.deiverDetailView.isHidden = true
        }
        
        self.lblDeliveryCost.text = "$\((self.deliveryDetailData?.deliveredCharges ?? 0.0) + (self.deliveryDetailData?.taxCharges ?? 0.0))"
        self.lblDriverName.text = self.deliveryDetailData?.name ?? "N/A"
        self.imgDriver.yy_setImage(with: URL(string: CommonFunctions.getImage(self.deliveryDetailData?.profileImage ?? "", quality: .large)), placeholder: UIImage(named: "image_large"))
        
        if self.deliveryDetailData?.timeSensitive == 1{
            self.lblRqType.text = "Time Sensitive"
        }else{
            self.lblRqType.text = "Time Flexible"
        }
        
        var cats : [String] = []
        for data in self.deliveryDetailData?.deliveryPackage ?? []{
            cats.append(data.name ?? "")
        }
        self.btnPackages.setTitle(cats.joined(separator: " | "), for: .normal)
        let time = (self.deliveryDetailData?.duration ?? 0)*60
        self.lblDurationTime.text = CommonFunctions.secondsToDaysHoursMinutesSecons(seconds: time)
        self.lblDistance.text = self.deliveryDetailData?.distanceTravelled ?? ""
    }
}

//MARK:- UItable view delegate methods
extension BookingDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (deliveryDetailData?.dropoffDetails?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingDetialCell", for: indexPath) as! BookingDetialCell
        cell.vrDotted.layer.sublayers = nil
        self.addDotedLayer(View: cell.vrDotted, color: UIColor.gray.cgColor)
        cell.lblDropLocation.text = "\(indexPath.row+1)"
        cell.lblTime.text = self.deliveryDetailData?.dropoffDetails?[indexPath.row].pickupTime ?? ""
        cell.lblLocations.text = self.deliveryDetailData?.dropoffDetails?[indexPath.row].address ?? ""
        let userDetail = "\(self.deliveryDetailData?.dropoffDetails?[indexPath.row].userName ?? "") (\(self.deliveryDetailData?.dropoffDetails?[indexPath.row].userPhone ?? "")), \(self.deliveryDetailData?.dropoffDetails?[indexPath.row].userApartment ?? ""), \(self.deliveryDetailData?.dropoffDetails?[indexPath.row].userLandmark ?? "")"
        cell.lblUserDetail.text = userDetail
        if ((self.deliveryDetailData?.dropoffDetails?.count ?? 0) - 1) == indexPath.row{
            cell.vrDotted.isHidden = true
        }else{
            cell.vrDotted.isHidden = false
        }
        return cell
    }
}

//MARK:- Hit Api
extension BookingDetailVC{
    func getDeliveryDetailApi(){
        let paarams : [String : AnyObject] = [
            "delivery_id" : deliveryId as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getDeliveryDetail, withParameters: paarams as [String: AnyObject], ofType: DeliveryDetailAPi.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.deliveryDetailData = profileAPI
            print(profileAPI)
            self.setDataApi()
            self.tableView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                self.tableView.reloadData()
            })
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
