//
//  RiseComplaintVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 15/12/20.
//

import UIKit
import GrowingTextView

class RiseComplaintVC: UIViewController {

    @IBOutlet weak var twMsg: GrowingTextView!
    @IBOutlet weak var btnSubmit: LoadingButton!
    
    var orderId : Int!
    var deliveryId : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        twMsg.textContainerInset = UIEdgeInsets(top: 16,left: 16,bottom: 16,right: 16)
        twMsg.layer.cornerRadius = 4.0
        twMsg.layer.borderWidth  = 1.0
        twMsg.layer.borderColor = Colors.borderYellowColorTraxi.color().cgColor
        // Do any additional setup after loading the view.
    }

    @IBAction func btnActCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActSubMit(_ sender: Any) {
        if twMsg.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            twMsg.shake()
            CommonFunctions.toster("Please enter message")
        }else{
            self.postComplainApi()
        }
    }
}

extension RiseComplaintVC{
    func postComplainApi(){
        var paarams : [String:AnyObject] = [
            "message" : twMsg.text as AnyObject
        ]
        if orderId != nil{
            paarams["deliver_id"] = orderId as AnyObject
        }
        
        if deliveryId != nil{
            paarams["deliver_id"] = deliveryId as AnyObject
        }
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.postComplain, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            self.navigationController?.popViewController(animated: true)
            CommonFunctions.hideLoader()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
}
