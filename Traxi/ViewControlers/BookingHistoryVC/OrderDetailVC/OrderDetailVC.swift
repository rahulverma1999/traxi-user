//
//  OrderDetailVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 15/12/20.
//

import UIKit

enum OrderDetailVCScreenType{
    case ongoingOrder, completedOrder, cancelled
}

class OrderDetailVC: baseVC {
    
    //MARK:- Outlets
    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSendMsg: LoadingButton!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblShopNam: UILabel!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblItemTotal: UILabel!
    @IBOutlet weak var lblDeliveryCharges: UILabel!
    @IBOutlet weak var lblCountAndDiscountPrice: UILabel!
    @IBOutlet weak var vwCounpAndDiscount: UIView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblUserNameAndPhone: UILabel!
    @IBOutlet weak var lblUserLocation: UILabel!
    @IBOutlet weak var vwSendMsg: UIView!
    @IBOutlet weak var btnTrackYourDelivery: UIButton!
    @IBOutlet weak var scrollViewButtomConstant: NSLayoutConstraint!
    @IBOutlet weak var lblTaxChargesGST: UILabel!
    @IBOutlet weak var rateYourOrder: UIButton!
    
    //MARK:- Variables
    var sType : OrderDetailVCScreenType = .ongoingOrder
    var orderId : Int!
    var orderDetailData : GetOrderAPI?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
        getOrderDetailApi()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SocketIOManager.sharedInstance.closeSocket()
        SocketIOManager.sharedInstance.closeConnection()
    }

    //MARK:- Button Actions
    @IBAction func btnActSendMsg(_ sender: Any) {
        let vc = RiseComplaintVC.instantiateFromAppStoryboard(appStoryboard: .BookingHistory)
        vc.orderId = orderDetailData?.orderID ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActTrackDelivery(_ sender: Any) {
        let vc = OrderStatusVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        vc.orderId = orderDetailData?.orderID ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnActRateYourOrder(_ sender: Any) {
        let vc = RateToShopVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
        vc.orderDetailData = self.orderDetailData
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension OrderDetailVC{
    //MARK:- setup Ui
    func setUpUi(){
//        switch sType {
//        case .ongoingOrder:
//            lblOrderStatus.text = "ONGOING"
//            lblOrderStatus.textColor = Colors.pinkColorTraxi.color()
//        case .completedOrder:
//            lblOrderStatus.text = "DELIVERED"
//            lblOrderStatus.textColor = Colors.traxiGreenColor.color()
//        case .cancelled:
//            lblOrderStatus.text = "CANCELLED"
//            lblOrderStatus.textColor = Colors.traxiredColor.color()
//        }
        self.imgShop.layer.cornerRadius = 4.0
        
        btnTrackYourDelivery.clipsToBounds = true
        btnTrackYourDelivery.layer.masksToBounds = false
        btnTrackYourDelivery.layer.shadowColor = UIColor.black.cgColor
        btnTrackYourDelivery.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        btnTrackYourDelivery.layer.shadowOpacity = 0.5
        btnTrackYourDelivery.layer.shadowRadius = 5.0
        btnTrackYourDelivery.layer.cornerRadius = 4.0
    }
    
    func setDataFromApi(_ data : GetOrderAPI){
        if data.orderStatus == 0{//0 = pending, 1 = accepted, 2 = order prepared, 3 = driver assigned, 4 = item collected, 5 = item delivered, 6 = cancelled by store, 7 = cancelled by driver, 8 = cancelled by the user
            lblOrderStatus.text = "PENDING"
            lblOrderStatus.textColor = Colors.pinkColorTraxi.color()
            self.rateYourOrder.isHidden = true
            self.vwSendMsg.isHidden = true
        }else if data.orderStatus == 6 || data.orderStatus == 7 || data.orderStatus == 8{
            lblOrderStatus.text = "CANCELLED"
            self.rateYourOrder.isHidden = true
            self.vwSendMsg.isHidden = false
            lblOrderStatus.textColor = Colors.traxiredColor.color()
        }else if data.orderStatus == 5{
            lblOrderStatus.text = "DELIVERED"
            self.vwSendMsg.isHidden = false
            self.rateYourOrder.isHidden = false
            lblOrderStatus.textColor = Colors.traxiGreenColor.color()
        }else{
            lblOrderStatus.text = "ACCEPTED"
            self.rateYourOrder.isHidden = true
            self.vwSendMsg.isHidden = true
            lblOrderStatus.textColor = Colors.traxiGreenColor.color()
        }
        
        if self.orderDetailData?.userRated == 1{
            self.rateYourOrder.isHidden = true
        }else{
            if data.orderStatus == 5{
                self.rateYourOrder.isHidden = false
            }else{
                self.rateYourOrder.isHidden = true
            }
        }
        
        if data.orderStatus == 4 || data.orderStatus == 3{
            self.btnTrackYourDelivery.isHidden = false
            self.scrollViewButtomConstant.constant = 72
        }else{
            self.btnTrackYourDelivery.isHidden = true
            self.scrollViewButtomConstant.constant = 20
        }
        
//        if data.order?.orderStatus == 4{
//            cell.vwButtonTrack.isHidden = false
//        }else{
//            cell.vwButtonTrack.isHidden = true
//        }
        
        self.imgShop.yy_setImage(with: URL(string: CommonFunctions.getImage(data.image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
        self.lblShopNam.text = data.storeName ?? ""
        self.lblLocation.text = data.storeAddress ?? ""
        self.lblRating.text = "\(data.storeRatings ?? 0.0)"
        self.lblReviews.text = "(\(data.reviewsCount ?? 0) reviews)"
        if data.discount != nil && data.discount != 0.0{
            let itmTotal = (data.orderTotal ?? 0.0) - (data.deliveredCharges ?? 0.0) + (data.discount ?? 0.0)
            self.lblItemTotal.text = "$\((CommonFunctions.decimalPlaceFromDouble(itmTotal - (data.taxCharges ?? 0.0), decimalPlace: 2)))"
            self.vwCounpAndDiscount.isHidden = false
            self.lblCountAndDiscountPrice.text = "-$\(data.discount ?? 0.0)"
        }else{
            let itmTotal = ((data.orderTotal ?? 0.0) - (data.deliveredCharges ?? 0.0))  - (data.taxCharges ?? 0.0)
            self.lblItemTotal.text = "$\(CommonFunctions.decimalPlaceFromDouble(itmTotal, decimalPlace: 2))"
            self.vwCounpAndDiscount.isHidden = true
        }
        self.lblDeliveryCharges.text = "$\(data.deliveredCharges ?? 0.0)"
        let totalPrice = (data.orderTotal ?? 0.0)//+(data.order?.deliveredCharges ?? 0.0)
        self.lblTotalPrice.text = "$\(CommonFunctions.decimalPlaceFromDouble(totalPrice, decimalPlace: 2))"
        self.lblOrderDate.text = CommonFunctions.getDateFormatTraxi4(data.createdAt ?? "")
        var userData = ""
        if (data.userName != nil && data.userName != ""){
            userData.append("\(data.userName ?? "")")
        }
        if (data.userPhone != nil && data.userPhone != ""){
            userData.append("\(data.userPhone ?? "")")
        }
        self.lblUserNameAndPhone.text = userData
        self.lblUserLocation.text = "\(data.userApartment ?? "N/A"), \(data.userLandmark ?? "N/A")"
        self.lblTaxChargesGST.text = "$\(CommonFunctions.decimalPlaceFromDouble(data.taxCharges ?? 0.0, decimalPlace: 2))"
    }
}

//MARK:- Delegate Methods
extension OrderDetailVC: RateToShopVCDelegate{
    func getSunccessRating() {
        self.getOrderDetailApi()
    }
}

//MARK:- UItable view delegate methods
extension OrderDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orderDetailData?.products?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = StoreDetailVC.instantiateFromAppStoryboard(appStoryboard: .Shop)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailTableViewCell", for: indexPath) as! OrderDetailTableViewCell
        cell.lblProductName.text = self.orderDetailData?.products?[indexPath.row].name ?? ""
        cell.lblProductPrice.text = "$\((self.orderDetailData?.products?[indexPath.row].price ?? 0.0)/(Double(self.orderDetailData?.products?[indexPath.row].quantity ?? 0)))"
        cell.lblProductTotalPrice.text = "$\(self.orderDetailData?.products?[indexPath.row].price ?? 0.0)"
        cell.lblProductQuantity.text = "Qty \(self.orderDetailData?.products?[indexPath.row].quantity ?? 0)"
        cell.imgOrder.yy_setImage(with: URL(string: CommonFunctions.getImage(self.orderDetailData?.products?[indexPath.row].image ?? "", quality: .large)), placeholder: UIImage(named: "stores_placeholder"))
        return cell
    }
}

//MARK:- Hit Api
extension OrderDetailVC{
    func getOrderDetailApi(){
        let paarams : [String : AnyObject] = [
            "order_id"  : self.orderId as AnyObject,
            "job_type" : 2 as AnyObject
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getOrderDetail, withParameters: paarams as [String: AnyObject], ofType: GetOrderAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.orderDetailData = profileAPI
            self.setDataFromApi(profileAPI)
            print(profileAPI)
            self.tableView.reloadData()
            DispatchQueue.main.async {
                self.tableView.layoutIfNeeded()
                let contentSize = self.tableView.contentSize.height
                self.tableViewHeight.constant = contentSize
            }
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
