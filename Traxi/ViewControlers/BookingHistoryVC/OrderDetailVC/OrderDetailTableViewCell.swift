//
//  OrderDetailTableViewCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 15/12/20.
//

import UIKit

class OrderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imgOrder: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductTotalPrice: UILabel!
    @IBOutlet weak var lblProductQuantity: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgOrder.layer.cornerRadius = 4.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
