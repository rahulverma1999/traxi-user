//
//  BookingHistoryCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 11/12/20.
//

import UIKit

class BookingHistoryCell: UITableViewCell {
    
    
    //MARK:- Outlets
    @IBOutlet weak var vwDotted: UIView!
    @IBOutlet weak var vwTrackDeleviery: GradientView!
    @IBOutlet weak var btnTrackDeleviery: LoadingButton!
    @IBOutlet weak var vwHideUnHide: UIView!
    @IBOutlet weak var lblDeliveryType: UILabel!
    @IBOutlet weak var lblDelevieryStatus: UILabel!
    @IBOutlet weak var lblTimeFlexible: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPickUpDate: UILabel!
    @IBOutlet weak var lblPickUpTime: UILabel!
    @IBOutlet weak var lblPickUpLocation: UILabel!
    @IBOutlet weak var lblPickUpUserDetail: UILabel!
    @IBOutlet weak var lblDropLocation: UILabel!
    @IBOutlet weak var lblDropUserDetail: UILabel!
    @IBOutlet weak var vwUserPickLocation: UIView!
    @IBOutlet weak var vwUserDropLocation: UIView!
    @IBOutlet weak var lblPickUpTitle: UILabel!
    @IBOutlet weak var lblDropOffTitle: UILabel!
    
    
    var allDeliveresData : [GetAllDeliveresAPIElement] = []
    var DeliveryType : Int! //1:- Upcoming, 2:- completed, 3:- canceled
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:- Add Doted Layer to view
    func addDotedLayer(View : UIView, color: CGColor){
        DispatchQueue.main.async {
            let lineLayer = CAShapeLayer()
            lineLayer.strokeColor = color
            lineLayer.lineWidth = 2
            lineLayer.lineDashPattern = [2,2]
            let path = CGMutablePath()
            path.addLines(between: [CGPoint(x: View.bounds.minX, y: View.bounds.minY),
                                    CGPoint(x: View.bounds.minX, y: View.bounds.maxY)])
            lineLayer.path = path
            View.layer.addSublayer(lineLayer)
        }
    }
    
    func setData(_ indexPath : IndexPath){
        if self.DeliveryType == 1{
            self.vwTrackDeleviery.isHidden = false
            self.vwHideUnHide.isHidden = false
        }else if self.DeliveryType == 2{
            self.vwTrackDeleviery.isHidden = true
            self.vwHideUnHide.isHidden = true
        }else{
            self.vwTrackDeleviery.isHidden = true
            self.vwHideUnHide.isHidden = true
        }
        
        //delivery_Status: 1 = job posted, 2 = ongoing, 3 = package collected, 4 = job completed, 5 = cancelled by driver, 6 = cancelled by user
        if self.allDeliveresData[indexPath.row].deliveryStatus == 1{
            self.lblDelevieryStatus.text = "PENDING"
            self.lblDelevieryStatus.textColor = Colors.pinkColorTraxi.color()
        }else if (self.allDeliveresData[indexPath.row].deliveryStatus == 2) || (self.allDeliveresData[indexPath.row].deliveryStatus == 3){
            self.lblDelevieryStatus.text = "ONGOING"
            self.lblDelevieryStatus.textColor = Colors.pinkColorTraxi.color()
        }else if self.allDeliveresData[indexPath.row].deliveryStatus == 4{
            self.lblDelevieryStatus.text = "COMPLETED"
            self.lblDelevieryStatus.textColor = Colors.traxiGreenColor.color()
        }else if (self.allDeliveresData[indexPath.row].deliveryStatus == 5) || (self.allDeliveresData[indexPath.row].deliveryStatus == 6){
            self.lblDelevieryStatus.text = "CANCELLED"
            self.lblDelevieryStatus.textColor = Colors.traxiredColor.color()
        }
        if self.allDeliveresData[indexPath.row].timeSensitive == 1{
            self.lblTimeFlexible.text = "Time Sensitive"
        }else{
            self.lblTimeFlexible.text = "Time Flexible"
        }
//        if self.allDeliveresData[indexPath.row].deliverTypeID == 1 || self.allDeliveresData[indexPath.row].deliverTypeID == 2{
//            if self.allDeliveresData[indexPath.row].timeSensitive == 1{
//                self.vwTrackDeleviery.isHidden = false
//            }else{
//                self.vwTrackDeleviery.isHidden = true
//            }
//        }
        self.lblPrice.text = "$\((self.allDeliveresData[indexPath.row].deliveredCharges ?? 0.0)+(self.allDeliveresData[indexPath.row].taxCharges ?? 0.0))"
        if (self.allDeliveresData[indexPath.row].pickupDetails?.count ?? 0) > 0{
            self.lblPickUpDate.text = self.allDeliveresData[indexPath.row].pickupDetails?[0].pickupDate ?? ""
            self.lblPickUpTime.text = self.allDeliveresData[indexPath.row].pickupDetails?[0].pickupTime ?? ""
            self.lblPickUpLocation.text = self.allDeliveresData[indexPath.row].pickupDetails?[0].address ?? ""
            let userDetail = "\(self.allDeliveresData[indexPath.row].pickupDetails?[0].userName ?? "") (\(self.allDeliveresData[indexPath.row].pickupDetails?[0].userPhone ?? "")), \(self.allDeliveresData[indexPath.row].pickupDetails?[0].userApartment ?? ""), \(self.allDeliveresData[indexPath.row].pickupDetails?[0].userLandmark ?? "")"
            self.lblPickUpUserDetail.text = userDetail
        }
        if (self.allDeliveresData[indexPath.row].dropoffDetails?.count ?? 0) > 0{
            self.lblDropLocation.text = self.allDeliveresData[indexPath.row].dropoffDetails?[0].address ?? ""
            let userDetail = "\(self.allDeliveresData[indexPath.row].dropoffDetails?[0].userName ?? "") (\(self.allDeliveresData[indexPath.row].dropoffDetails?[0].userPhone ?? "")), \(self.allDeliveresData[indexPath.row].dropoffDetails?[0].userApartment ?? ""), \(self.allDeliveresData[indexPath.row].dropoffDetails?[0].userLandmark ?? "")"
            self.lblDropUserDetail.text = userDetail
        }
        
        if self.allDeliveresData[indexPath.row].deliverTypeID == 1{// 1 = Single, 2 = Return, 3 = Traxi run
            self.lblDeliveryType.text = "Single Delivery"
            self.lblPickUpLocation.isHidden = false
            self.vwUserPickLocation.isHidden = false
            self.lblDropLocation.isHidden = false
            self.vwUserDropLocation.isHidden = false
            self.lblPickUpTitle.text = "PICK UP POINT"
            self.lblDropOffTitle.text = "DROP OFF POINT"
        }else if self.allDeliveresData[indexPath.row].deliverTypeID == 2{
            self.lblPickUpTitle.text = "2 PICK UP POINTS"
            self.lblDeliveryType.text = "Return Delivery"
            self.lblDropOffTitle.text = "2 DROP OFF POINTS"
            self.lblPickUpLocation.isHidden = true
            self.vwUserPickLocation.isHidden = true
            self.lblDropLocation.isHidden = true
            self.vwUserDropLocation.isHidden = true
        }else{
            self.lblPickUpLocation.isHidden = false
            self.vwUserPickLocation.isHidden = false
            self.lblDropLocation.isHidden = true
            self.vwUserDropLocation.isHidden = true
            if (self.allDeliveresData[indexPath.row].dropoffDetails?.count ?? 0) > 1{
                self.lblDropOffTitle.text = "\(self.allDeliveresData[indexPath.row].dropoffDetails?.count ?? 0) DROP OFF POINTS"
            }else{
                self.lblDropOffTitle.text = "\(self.allDeliveresData[indexPath.row].dropoffDetails?.count ?? 0) DROP OFF POINT"
            }
            
            self.lblPickUpTitle.text = "PICK UP POINT"
            self.lblDeliveryType.text = "Traxi run"
        }
        self.vwDotted.layer.sublayers = nil
        self.addDotedLayer(View: self.vwDotted, color: UIColor.gray.cgColor)
        
        self.layoutSubviews()
    }
}

