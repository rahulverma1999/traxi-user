//
//  BookingStoreTableViewCell.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 14/12/20.
//

import UIKit

class BookingStoreTableViewCell: UITableViewCell {

    @IBOutlet weak var imgShop: UIImageView!
    @IBOutlet weak var btnTrack: LoadingButton!
    @IBOutlet weak var vwButtonTrack: GradientView!
    @IBOutlet weak var lblBookingStatus: UILabel!
    @IBOutlet weak var vwHideUnHide: UIView!
    @IBOutlet weak var totalConstant: NSLayoutConstraint!
    @IBOutlet weak var lblStoreNake: UILabel!
    @IBOutlet weak var lblShopLocation: UILabel!
    @IBOutlet weak var lblShopRating: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var vwScondOrder: UIView!
    @IBOutlet weak var vwViewMore: UIView!
    @IBOutlet weak var imgProduct1: UIImageView!
    @IBOutlet weak var lblNameProduct1: UILabel!
    @IBOutlet weak var lblPriceProduct1: UILabel!
    @IBOutlet weak var lblTotalPriceProduct1: UILabel!
    @IBOutlet weak var lblQtyProduct1: UILabel!
    @IBOutlet weak var imgProduct2: UIImageView!
    @IBOutlet weak var lblNameProduct2: UILabel!
    @IBOutlet weak var lblPriceProduct2: UILabel!
    @IBOutlet weak var lblTotalPriceProduct2: UILabel!
    @IBOutlet weak var lblQtyProduct2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgShop.layer.cornerRadius = 6.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
