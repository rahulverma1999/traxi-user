//
//  PhoneNumberVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 27/11/20.
//

import UIKit
import FlagPhoneNumber

enum PhoneNumberVCScreenType{
    case phoneNumber, EmailAddress, forgotPass
}

class PhoneNumberVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblCountryCodeAndName: UILabel!
    @IBOutlet weak var btnActOpenCountryCode: UIButton!
    @IBOutlet weak var tfMobileNumber: UITextFieldWxtension!
    @IBOutlet weak var btnContinue: LoadingButton!
    @IBOutlet weak var vwCorner: UIView!
    @IBOutlet weak var vwContinueBtn: GradientView!
    @IBOutlet weak var viewHideOrUnhide: UIView!
    @IBOutlet weak var lblTextChange: UILabel!
    @IBOutlet weak var lblWelecomeText: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK:- Variables
    var selectedValue = "+1"
    var countryCode = "+61"
    var slectedCountryName = ""
    var isFromSsocialLogin = false
    
    let sampleTextField : FPNTextField =  FPNTextField(frame: CGRect(x: 20, y: 100, width: 300, height: 40))
    
    var sType : PhoneNumberVCScreenType = .phoneNumber
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getViewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
    }

    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActCountryCode(_ sender: Any) {
        fpnDisplayCountryList()
    }
    
    @IBAction func btnActContinue(_ sender: Any) {
        if tfMobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfMobileNumber.shake()
            CommonFunctions.toster("Please enter mobile number")
        }else{
            if isFromSsocialLogin{
                EditUserApiHit()
            }else{
                SignUpApiHit()
            }
        }
    }
    
}

//MARK:- set Data viewDidLoad
extension PhoneNumberVC{
    func getViewDidLoad(){
        CommonFunctions.shadowView([vwContinueBtn])
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: {
            self.setViewBoarderAndRadius()
        })
        sampleTextField.delegate = self
        
        
        switch sType {
        case .phoneNumber:
            self.tfMobileNumber.setPlaceHolderColor(UIColor.lightGray, placeholderText: "Phone number", placeholderFont: UIFont.NunitoRegular(16))
            self.viewHideOrUnhide.isHidden = false
            self.lblTextChange.text = "Let's start with just your phone number."
            self.tfMobileNumber.keyboardType = .phonePad
            self.btnContinue.setTitle("CONTINUE", for: .normal)
        case .EmailAddress:
            self.tfMobileNumber.keyboardType = .emailAddress
            self.tfMobileNumber.setPlaceHolderColor(UIColor.lightGray, placeholderText: "Email address", placeholderFont: UIFont.NunitoRegular(16))
            self.viewHideOrUnhide.isHidden = true
            self.lblTextChange.text = "Let's start with just your email."
            self.btnContinue.setTitle("CONTINUE", for: .normal)
        case .forgotPass:
            self.tfMobileNumber.keyboardType = .emailAddress
            self.tfMobileNumber.setPlaceHolderColor(UIColor.lightGray, placeholderText: "Email address", placeholderFont: UIFont.NunitoRegular(16))
            self.viewHideOrUnhide.isHidden = true
            self.lblTextChange.text = "Please enter your email address"
            self.btnContinue.setTitle("SEND RESET LINK", for: .normal)
            self.lblWelecomeText.text = "Forgot Password?"
        }
        
        if isFromSsocialLogin{
            btnBack.isHidden = true
        }else{
            btnBack.isHidden = false
        }
    }
    
    //MARK:- set radius and border
    func setViewBoarderAndRadius(){
        vwCorner.layer.cornerRadius = 4
        vwCorner.layer.borderWidth = 1
        vwCorner.layer.borderColor = Colors.borderYellowColorTraxi.color().cgColor
        btnActOpenCountryCode.layer.cornerRadius = 4.0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.viewHideOrUnhide.roundCorners([.topRight, .topLeft], radius: 4)
           
        }
    }
    
    //MARK:- open country code
    func fpnDisplayCountryList() {
        print("sad")
        
        let listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
        listController.setup(repository: sampleTextField.countryRepository)
        listController.title = "Countries"

        let navigationViewController = UINavigationController(rootViewController: listController)
        self.present(navigationViewController, animated: true, completion: nil)
        listController.didSelect = {[weak self] country in
            self?.slectedCountryName = country.name
            self?.countryCode = country.phoneCode
            self?.selectedValue = country.phoneCode
            self?.lblCountryCodeAndName.text = "\(country.name) (\(country.phoneCode))"
        }
    }
}

//MARK:- Delegate method of country code flag
extension PhoneNumberVC: FPNTextFieldDelegate{
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
         print(name, dialCode, code) // Output "France", "+33", "FR"
        countryCode = dialCode
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
    }
    

}


//MARK:- UITextField Delegate
extension PhoneNumberVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        switch sType {
        case .phoneNumber:
            return newString.length <= 15
        case .EmailAddress:
            return newString.length <= 30
        case .forgotPass:
            return newString.length <= 30
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case tfMobileNumber:
            tfMobileNumber.resignFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
}

//MARK:- Hit Api

extension PhoneNumberVC{
    
    func SignUpApiHit(){
        let paarams : [String : String] = [
            
            "phone" : tfMobileNumber.text!.replacingOccurrences(of: " ", with: ""),
            "fcm_id" : device_token,
            "app_version": "1.0",
            "device_type": "2",
            "country_code" : countryCode
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.SignUp, withParameters: paarams as [String: AnyObject], ofType: PhoneNumberAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            switch self.sType {
            case .phoneNumber:
                let vc = PhoneVerificationVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                vc.phoneNumber = self.tfMobileNumber.text ?? ""
                vc.ContryCode = self.countryCode
                vc.firstTimeLogin = true
                self.navigationController?.pushViewController(vc, animated: true)
            break
            case .EmailAddress:
                let vc = VerifyEmailVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: true)
            case .forgotPass:
                break
            }
            coredata.shared.isEmailVerified = profileAPI.user?.isEmailVerified ?? 0
            coredata.shared.email = profileAPI.user?.email ?? ""
            coredata.shared.accesstoken = profileAPI.token ?? ""
            coredata.shared.dataSave()
            CommonFunctions.toster(profileAPI.message ?? "")
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.POST, img: nil, imageParamater: "", headerPresent: false)
    }
    
    func EditUserApiHit(){
        let paarams : [String : String] = [
            
            "phone" : tfMobileNumber.text!.replacingOccurrences(of: " ", with: ""),
            "country_code" : countryCode
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.editUser, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            switch self.sType {
            case .phoneNumber:
                let vc = PhoneVerificationVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                vc.phoneNumber = self.tfMobileNumber.text ?? ""
                vc.ContryCode = self.countryCode
                self.navigationController?.pushViewController(vc, animated: true)
            break
            case .EmailAddress:
                let vc = VerifyEmailVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: true)
            case .forgotPass:
                break
            }
            coredata.shared.dataSave()
            CommonFunctions.toster(profileAPI.message ?? "")
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PUT, img: nil, imageParamater: "", headerPresent: true)
    }
}

