//
//  TermsAndConditionsVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit
import WebKit


//MARK:- ScreenTypes
enum TermsAndConditionVCType{
    case termsAndCondtion
    case termsAndServices
    case about
    case Faq
    case cancellationPolicy
    case ContactUs
    case Privacypolicy
    case Facebook
    case Instagram
}

class TermsAndConditionsVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var webKitView: WKWebView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //MARK:- Variables
    var sType : TermsAndConditionVCType = .termsAndCondtion

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewDidLoadData()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnActback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Set View Did Load Data
extension TermsAndConditionsVC{
    func setViewDidLoadData(){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        webKitView.uiDelegate = self
        webKitView.navigationDelegate = self
        webKitView.tintColor = .black
        self.webKitView.allowsBackForwardNavigationGestures = true
        if sType == .termsAndCondtion{
            let myURL = URL(string: "https://app-transfer.com:8082/terms-conditions")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "Terms & Condtions"
            webKitView.load(myRequest)
        }else if sType == .about{
            let myURL = URL(string: "https://horsebnb.com/app-about-us")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "About Us"
            webKitView.load(myRequest)
        }else if sType == .Faq{
            let myURL = URL(string: "https://horsebnb.com/app-faq")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "FAQ"
            webKitView.load(myRequest)
        }else if sType == .cancellationPolicy {
            let myURL = URL(string: "https://horsebnb.com/app-cancellation-policy")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "Cancellation Policy"
            webKitView.load(myRequest)
        }else if sType == .ContactUs {
            let myURL = URL(string: "https://horsebnb.com/app-contact-us")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "Contact Us"
            webKitView.load(myRequest)
        }else if sType == .Privacypolicy {
            let myURL = URL(string: "https://horsebnb.com/app-privacy-policy")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "Privacy Policy"
            webKitView.load(myRequest)
        }else if sType == .Facebook {
            let myURL = URL(string: "https://www.facebook.com/horsebnb1")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "Facebook"
            webKitView.load(myRequest)
        }else if sType == .Instagram {
            let myURL = URL(string: "https://www.instagram.com/horse.bnb/")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "Instagram"
            webKitView.load(myRequest)
        }else{
            let myURL = URL(string: "https://horsebnb.com/app-terms-conditions")
            let myRequest = URLRequest(url: myURL!)
            lblTitle.text = "Terms of Service"
            webKitView.load(myRequest)
        }
    }
}

//MARK:- Web View Delegate Methods
extension TermsAndConditionsVC: WKNavigationDelegate{ //}, WKUIDelegate{
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        CustomLoader.instance.showLoaderView()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
//            CustomLoader.instance.hideLoaderView()
//        }
        CommonFunctions.showLoader()
        print("start loader here")
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
//        CustomLoader.instance.hideLoaderView()
        CommonFunctions.hideLoader()
        print("Fail")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Recive URL")
        CommonFunctions.hideLoader()
//        CustomLoader.instance.hideLoaderView()
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //MARK:- Remove Hyperlink click
           if navigationAction.navigationType == WKNavigationType.linkActivated {
               print("link")
            
               decisionHandler(WKNavigationActionPolicy.cancel)
               return
           }
           print("no link")
           decisionHandler(WKNavigationActionPolicy.allow)
    }
}

extension TermsAndConditionsVC: WKUIDelegate{
}
