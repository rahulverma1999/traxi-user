//
//  SignUpVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 27/11/20.
//

import UIKit
import FlagPhoneNumber

class SignUpVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tfFullName: UITextFieldWxtension!
    @IBOutlet weak var tfPhoneNumber: FPNTextField!
//    @IBOutlet weak var tfPassword: UITextFieldWxtension!
    @IBOutlet weak var TFEMAIL: UITextFieldWxtension!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblAttributedTemrsAndConditions: UILabel!
    @IBOutlet weak var btnSignUp: LoadingButton!
    @IBOutlet weak var lblAttributedSignIn: UILabel!
    
    
    //MARK:- Variables
    var termsSelected = false
    var selectedValue = "+61"
    var countryCode = ""
    var sideMenuNew : SideMenuVC!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViewDidLoadData()
        // Do any additional setup after loading the view.
    }

    //MARK:- Button Actions
    @IBAction func btnAcrSignUp(_ sender: Any) {
        if tfFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfFullName.shake()
            CommonFunctions.toster("Please enter full name ")
        }else if TFEMAIL.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            TFEMAIL.shake()
            CommonFunctions.toster("Please enter email")
        }else if !CommonFunctions.isValidEmail(TFEMAIL.text!){
            TFEMAIL.shake()
            CommonFunctions.toster("Please enter valid email")
        }else if !termsSelected{
            lblAttributedTemrsAndConditions.shake()
            btnCheckBox.shake()
            CommonFunctions.toster("Please agree Terms of Service.")
        }else{
            self.EditProfileApiHit()
        }
        
    }
    
    @IBAction func btnActCheckBox(_ sender: UIButton) {
        termsSelected = !termsSelected
        btnCheckBox.setImage(termsSelected ? Asset.ic_checked.image() : Asset.ic_notChecked.image(), for: .normal)
    }
    
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SignUpVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if TFEMAIL == textField{
            let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@/%$#!^&*"
            let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
            return (string == filtered) && newString.length <= 70
        }else if tfPhoneNumber == textField{
            return newString.length <= 15
        }else{
            return newString.length <= 30
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case tfFullName:
            TFEMAIL.becomeFirstResponder()
        case TFEMAIL:
            TFEMAIL.resignFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
}

//MARK:- set viewDidLoad Data
extension SignUpVC{
    func setViewDidLoadData(){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        getAttributedTextOfLables()
        
        //MARK:- Implement country code in textfield
        tfPhoneNumber.delegate = self
        tfPhoneNumber.displayMode = .list
        self.tfPhoneNumber.setFlag(countryCode: .AU)
        tfPhoneNumber.setPlaceHolderColor(UIColor.lightGray, placeholderText: "Phone Number", placeholderFont: UIFont.NunitoRegular(14))
        tfPhoneNumber.flagButton.isUserInteractionEnabled = true
        tfPhoneNumber.flagButton.addTarget(self, action: #selector(btnActCountryCode(_:)), for: .touchUpInside)
    }
    
    
    func getAttributedTextOfLables(){
        //MARK:- Attributed Text to lable
        let first = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.NunitoRegular(16)]
        let second = [NSAttributedString.Key.foregroundColor: Colors.pinkColorTraxi.color(), NSAttributedString.Key.font: UIFont.NunitoBold(16)] as [NSAttributedString.Key : Any]
        let One = NSMutableAttributedString(string: "Already have an account?", attributes: first)
        let Two = NSMutableAttributedString(string: " Sign In ", attributes: second)
        let tap = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap(_:)))
        One.append(Two)
        lblAttributedSignIn.attributedText = One
        lblAttributedSignIn.isUserInteractionEnabled = true
        lblAttributedSignIn.addGestureRecognizer(tap)
        lblAttributedSignIn.textAlignment = .center
        
        //MARK:- Attributed Text to lable
        let first2 = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.NunitoRegular(16)]
        let second2 = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor: Colors.pinkColorTraxi.color(), NSAttributedString.Key.font: UIFont.NunitoBold(16)] as [NSAttributedString.Key : Any]
        let One2 = NSMutableAttributedString(string: "I agree to The Traxi's ", attributes: first2)
        let Two2 = NSMutableAttributedString(string: "Terms of Service.", attributes: second2)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap2(_:)))
        One2.append(Two2)
        lblAttributedTemrsAndConditions.attributedText = One2
        lblAttributedTemrsAndConditions.isUserInteractionEnabled = true
        lblAttributedTemrsAndConditions.addGestureRecognizer(tap2)
        lblAttributedTemrsAndConditions.textAlignment = .center
        
//        let btneye = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
//        let btnImage = UIImage(named: "visibility_off")?.imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 16))
//        btneye.setImage(btnImage, for: .normal)
//        btneye.addTarget(self, action: #selector(btnActEye(_:)) , for: .touchUpInside)
//        tfPassword.rightViewMode = .always
//        tfPassword.rightView = btneye
        lblAttributedSignIn.isHidden = true
    }
}

//MARK:- Objective C methods
extension SignUpVC{
    @objc func myMethodToHandleTap(_ sender : UITapGestureRecognizer){
        let text = (lblAttributedSignIn.text)!
        let range = (text as NSString).range(of: " Sign In ")
        if sender.didTapAttributedTextlbl(textField: self.lblAttributedSignIn, inRange: range) {
//            let vc = SignInVC.instantiateFromAppStoryboard(appStoryboard: .Main)
//            self.navigationController?.pushViewController(vc, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func myMethodToHandleTap2(_ sender : UITapGestureRecognizer){
        let text = (lblAttributedTemrsAndConditions.text)!
        let range = (text as NSString).range(of: "Terms of Service.")
        if sender.didTapAttributedTextlbl(textField: self.lblAttributedTemrsAndConditions, inRange: range) {
            let vc = TermsAndConditionsVC.instantiateFromAppStoryboard(appStoryboard: .Main)
            vc.sType = .termsAndCondtion
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func btnActCountryCode(_ sender: UIButton){
        self.fpnDisplayCountryList()
    }
    
//    @objc func btnActEye(_ sender: UIButton){
//        tfPassword.isSecureTextEntry = !tfPassword.isSecureTextEntry
//        let btnImage1 = Asset.ic_hideeye_review.image().imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 16))
//        let btnImage2 = Asset.ic_eye_review.image().imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 16))
//        sender.setImage( tfPassword.isSecureTextEntry ? btnImage1 : btnImage2, for: .normal)
//    }
}

//MARK:- Delegate method of country code flag
extension SignUpVC: FPNTextFieldDelegate{
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
         print(name, dialCode, code) // Output "France", "+33", "FR"
        countryCode = dialCode
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
    }
    
    func fpnDisplayCountryList() {
        print("sad")
        
        let listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
        tfPhoneNumber.displayMode = .list // .picker by default
        
        listController.setup(repository: tfPhoneNumber.countryRepository)
        listController.title = "Countries"
        

        let navigationViewController = UINavigationController(rootViewController: listController)
                
        self.present(navigationViewController, animated: true, completion: nil)
        
        
        listController.didSelect = {[weak self] country in
            self?.tfPhoneNumber.setFlag(countryCode: country.code)
            self?.selectedValue = country.phoneCode
        }
    }
}

//MARK:- Hit Api
extension SignUpVC{
    func EditProfileApiHit(){
        let paarams : [String : String] = [
            "name" : tfFullName.text!.replacingOccurrences(of: " ", with: ""),
            "email" : TFEMAIL.text!.replacingOccurrences(of: " ", with: "")
        ]
        
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.editUser, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            coredata.shared.email = self.TFEMAIL.text ?? ""
            coredata.shared.name = self.tfFullName.text ?? ""
            coredata.shared.dataSave()
            CommonFunctions.toster(profileAPI.message ?? "")
//            let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: .Home)
//            self.navigationController?.pushViewController(vc, animated: true)
            self.sideMenuNew = SideMenuVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
            let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
            
            let navVC : navController = navController(menuViewController: self.sideMenuNew, contentViewController: vc)
            SelectedIndexValueOfSideMenu = 1
//            let navigationController = UINavigationController(rootViewController: vc)
            if UIApplication.shared.windows.count > 0 {
                UIApplication.shared.windows[0].rootViewController = navVC
                navVC.setNavigationBarHidden(true, animated: false)
            }
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PUT, img: nil, imageParamater: "", headerPresent: true)
    }
}
