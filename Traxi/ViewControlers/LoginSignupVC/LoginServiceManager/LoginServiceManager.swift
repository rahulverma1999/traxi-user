//
//  LoginServiceManager.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 21/12/20.
//


import Foundation

class LoginApiModal : NSObject{
    //MARK:- Login Api
    static func getLoginApi(_ params: [String: AnyObject],_ buttonSignUp : LoadingButton , success2:@escaping (PhoneNumberAPI)->()){
        var parrams = params
        
        parrams["device_type"] = device_type as AnyObject
        parrams["fcm_id"] = device_token as AnyObject
        parrams["device_id"] = device_id as AnyObject
        parrams["app_version"] = "1.0" as AnyObject
        
        ApiHandler.callApiWithParameters(url: APIConstants.SignUp, withParameters: parrams, ofType: PhoneNumberAPI.self, success2: { (profileAPI) in
            success2(profileAPI)
            isEmailPopShowOnce = true
            buttonSignUp.hideLoading()
            coredata.shared.isEmailVerified = profileAPI.user?.isEmailVerified ?? 0
            coredata.shared.phone_no = "\(profileAPI.user?.phone ?? 0)"
            coredata.shared.dataSave()
            CommonFunctions.toster(profileAPI.message ?? "")
            buttonSignUp.isUserInteractionEnabled = true
        }, failure: { (false, string) in
//            CustomLoader.instance.hideLoaderView()
            buttonSignUp.hideLoading()
            print(string)
            buttonSignUp.isUserInteractionEnabled = true
        }, method: ApiMethod.POST, img: nil, imageParamater: "", headerPresent: false)
    }
    
}

