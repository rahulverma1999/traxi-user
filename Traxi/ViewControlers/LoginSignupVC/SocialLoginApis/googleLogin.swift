//
//  googleLogin.swift
//  RxHub
//
//  Created by MAC on 30/04/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import Foundation
import GoogleSignIn

struct GoogleResult {
    var id: String?
    var name: String?
    var email: String?
    var profilePic: String?
    
    init(_ id: String?, name: String?, email: String?, profilePic: String?) {
        self.id = id
        self.name = name
        self.email = email
        self.profilePic = profilePic
    }
}

class googleLogin: NSObject, GIDSignInDelegate{
    
    var newVC : UIViewController!
    var googleListner : onGettingGoogleData?
    var canceledListner : onCanceled?

    
    typealias onGettingGoogleData = (GoogleResult) -> ()
    typealias onCanceled = () -> ()
    
    class var shared: googleLogin {
        struct Static {
            static let instance = googleLogin()
        }
        return Static.instance
    }
    
    func googleLoginHere(_ view: UIViewController, tgoogleListner: @escaping onGettingGoogleData, tonCanceled: @escaping onCanceled){
        newVC = view
        GIDSignIn.sharedInstance()?.presentingViewController = view
        GIDSignIn.sharedInstance()?.delegate = self
        
        canceledListner = tonCanceled
        googleListner = tgoogleListner
        
        GIDSignIn.sharedInstance().signIn()
        CommonFunctions.hideLoader()

    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            DispatchQueue.main.async {
                CommonFunctions.hideLoader()
            }
            CommonFunctions.toster(error.localizedDescription)
            self.canceledListner?()
            
        }else{
            var img = ""
            if user.profile.hasImage{
                img = user.profile.imageURL(withDimension: 120)!.absoluteString
            }
            let newResult = GoogleResult.init(user.userID, name: user.profile.name, email: user.profile.email, profilePic: img)
            
            self.googleListner?(newResult)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        CommonFunctions.toster(error.localizedDescription)
        DispatchQueue.main.async {
            CommonFunctions.hideLoader()
        }
        self.canceledListner?()
        
    }
}
