//
//  appleLogin.swift
//  RxHub
//
//  Created by MAC on 24/04/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import AuthenticationServices

@available(iOS 13.0, *)
class appleLogin : NSObject, ASAuthorizationControllerDelegate,
    ASAuthorizationControllerPresentationContextProviding{
    
    var newVC : UIViewController!
    var appleListner : onGettingAppleData?
    var canceledListner : onCanceled?

    
    typealias onGettingAppleData = (String?, String?, String?) -> ()
    typealias onCanceled = () -> ()
    
    class var shared: appleLogin {
        struct Static {
            static let instance = appleLogin()
        }
        return Static.instance
    }
    
    func appleLoginNow(_ vc: UIViewController, appleListner: @escaping onGettingAppleData, canceledListner: @escaping onCanceled){
        
        self.appleListner = appleListner
        self.canceledListner = canceledListner
        self.newVC = vc

        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]

        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()

    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        CommonFunctions.toster(error.localizedDescription)
        //print("error \(error)")
        self.canceledListner?()
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        //print("authorization \(authorization)")
        
        var id : String?
        var name : String?
        var email : String?
        var profilePic : String?

        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let userFirstName = appleIDCredential.fullName?.givenName
            let userLastName = appleIDCredential.fullName?.familyName
            let userEmail = appleIDCredential.email
            
            id = appleIDCredential.user
            name = appleIDCredential.fullName?.givenName
            email = appleIDCredential.email

            //print(userIdentifier, userFirstName, userLastName, userEmail)
            
            //Navigate to other view controller
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            //print(username, password)
            //Navigate to other view controller
        }
        self.appleListner?(id, name, email)
        
        
    }
        
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return newVC.view.window!
    }
}
