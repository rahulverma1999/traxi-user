//
//  VerifyEmailVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 27/11/20.
//

import UIKit

class VerifyEmailVC: baseVC {
    
    //MARK:- Outlets
    @IBOutlet weak var btnRelink: UIButton!
    @IBOutlet weak var btnDone: LoadingButton!
    
    //MARK:- Variables

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActDone(_ sender: Any) {
//        let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: .Home)
//        self.navigationController?.pushViewController(vc, animated: true)
        GetProfileApiHit()
    }
    
    @IBAction func btnActRelink(_ sender: Any) {
        self.ResentEmailApiHit()
    }
}

extension VerifyEmailVC{
    //MARK:- Hit Api
    func ResentEmailApiHit(){
        let paarams : [String : String] = [:]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.VerificationEmailSendLink, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            CommonFunctions.hideLoader()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.POST, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            if profileAPI.isEmailVerified == 1{
                self.navigationController?.popViewController(animated: true)
            }else{
                CommonFunctions.toster("Your email has not been verified")
            }
            coredata.shared.isEmailVerified = profileAPI.isEmailVerified ?? 0
            coredata.shared.dataSave()
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}
