//
//  SignInVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 01/12/20.
//

import UIKit

class SignInVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tfEmailOrPass: UITextFieldWxtension!
    @IBOutlet weak var tfPassword: UITextFieldWxtension!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet weak var btnSignIn: LoadingButton!
    @IBOutlet weak var lblAttributed: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDataViewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Button actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActSignIn(_ sender: Any) {
        
    }
    
    @IBAction func btnActForgotPassword(_ sender: Any) {
        let vc = PhoneNumberVC.instantiateFromAppStoryboard(appStoryboard: .Main)
        vc.sType = .forgotPass
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SignInVC{
    //MARK:- Set DataViewDidLoad
    func setDataViewDidLoad(){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        //MARK:- Attributed Text to lable
        let first = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.NunitoRegular(16)]
        let second = [NSAttributedString.Key.foregroundColor: Colors.pinkColorTraxi.color(), NSAttributedString.Key.font: UIFont.NunitoBold(16)] as [NSAttributedString.Key : Any]
        let One = NSMutableAttributedString(string: "Don't have an account?", attributes: first)
        let Two = NSMutableAttributedString(string: " Sign Up", attributes: second)
        let tap = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap(_:)))
        One.append(Two)
        lblAttributed.attributedText = One
        lblAttributed.isUserInteractionEnabled = true
        lblAttributed.addGestureRecognizer(tap)
        lblAttributed.textAlignment = .center
        
        //MARK:- Set Right image password
        let btneye = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let btnImage = UIImage(named: "visibility_off")?.imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 16))
        btneye.setImage(btnImage, for: .normal)
        btneye.addTarget(self, action: #selector(btnActEye(_:)) , for: .touchUpInside)
        tfPassword.rightViewMode = .always
        tfPassword.rightView = btneye
    }
    
    
}


//MARK:- Objective C methods
extension SignInVC{
    @objc func myMethodToHandleTap(_ sender : UITapGestureRecognizer){
        let text = (lblAttributed.text)!
        let range = (text as NSString).range(of: " Sign Up")
        if sender.didTapAttributedTextlbl(textField: self.lblAttributed, inRange: range) {
            let vc = SignUpVC.instantiateFromAppStoryboard(appStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func btnActEye(_ sender: UIButton){
        tfPassword.isSecureTextEntry = !tfPassword.isSecureTextEntry
        let btnImage1 = Asset.ic_hideeye_review.image().imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 16))
        let btnImage2 = Asset.ic_eye_review.image().imageWithInsets(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 16))
        sender.setImage( tfPassword.isSecureTextEntry ? btnImage1 : btnImage2, for: .normal)
    }
}
