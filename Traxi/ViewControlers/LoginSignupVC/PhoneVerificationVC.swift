//
//  PhoneVerificationVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 27/11/20.
//

import UIKit

class PhoneVerificationVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblAttributedSendAgain: UILabel!
    @IBOutlet weak var btnSubmit: LoadingButton!
    @IBOutlet weak var tfOtp1: MyTextField!
    @IBOutlet weak var tfOtp2: MyTextField!
    @IBOutlet weak var tfOtp3: MyTextField!
    @IBOutlet weak var tfOtp4: MyTextField!
    
    
    //MARK:- Variables
    var phoneNumber = ""
    var ContryCode = ""
    var Otp = ""
    var firstTimeLogin = false
    var fromEditProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDataViewDidLoad()
    }

    
    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnActSubmit(_ sender: Any) {
        if tfOtp1.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || tfOtp2.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || tfOtp3.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || tfOtp4.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfOtp1.shake()
            tfOtp2.shake()
            tfOtp3.shake()
            tfOtp4.shake()
            CommonFunctions.toster("Please enter full otp")
        }else{
            self.VerificationApiHit()
        }
    }
}

//MARK:- set data viewDidLoad
extension PhoneVerificationVC{
    func setDataViewDidLoad(){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        lblMobileNumber.textColor = UIColor.black
        
        tfOtp1.setPlaceHolderColor(UIColor.lightGray, placeholderText: "_", placeholderFont: UIFont.NunitoRegular(14))
        tfOtp2.setPlaceHolderColor(UIColor.lightGray, placeholderText: "_", placeholderFont: UIFont.NunitoRegular(14))
        tfOtp3.setPlaceHolderColor(UIColor.lightGray, placeholderText: "_", placeholderFont: UIFont.NunitoRegular(14))
        tfOtp4.setPlaceHolderColor(UIColor.lightGray, placeholderText: "_", placeholderFont: UIFont.NunitoRegular(14))
        
        self.setFTFont(tfs: [tfOtp1, tfOtp2, tfOtp3, tfOtp4])
        
        //MARK:- Attributed Text to lable
        let first = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.NunitoRegular(16)]
        let second = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor: Colors.pinkColorTraxi.color(), NSAttributedString.Key.font: UIFont.NunitoBold(16)] as [NSAttributedString.Key : Any]
        let One = NSMutableAttributedString(string: "Didn't get a code? ", attributes: first)
        let Two = NSMutableAttributedString(string: "Send again", attributes: second)
        let tap = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap(_:)))
        One.append(Two)
        lblAttributedSendAgain.attributedText = One
        lblAttributedSendAgain.isUserInteractionEnabled = true
        lblAttributedSendAgain.addGestureRecognizer(tap)
        lblAttributedSendAgain.textAlignment = .center
        
        tfOtp1.myDelegate = self
        tfOtp2.myDelegate = self
        tfOtp3.myDelegate = self
        tfOtp4.myDelegate = self
        if self.ContryCode.contains("+"){
            self.lblMobileNumber.text = "\(self.ContryCode)\(self.phoneNumber)"
        }else{
            self.lblMobileNumber.text = "+\(self.ContryCode)\(self.phoneNumber)"
        }
    }
    
    func setFTFont(tfs : [UITextField]){
        for tf in tfs{
            tf.font = UIFont.NunitoRegular(16)
        }
    }
}

//MARK:- Objective C methos
extension PhoneVerificationVC{
    @objc func myMethodToHandleTap(_ sender : UITapGestureRecognizer){
        let text = (lblAttributedSendAgain.text)!
        let range = (text as NSString).range(of: "Send again")
        if sender.didTapAttributedTextlbl(textField: self.lblAttributedSendAgain, inRange: range) {
            self.ResentOtpApiHit()
        }
    }
}

//MARK:- textField Delegates
extension PhoneVerificationVC: UITextFieldDelegate{
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
     if string != ""{
       textField.text = ""
       DispatchQueue.main.async {
         switch textField {
         case self.tfOtp1:
           self.tfOtp2.becomeFirstResponder()
         case self.tfOtp2:
           self.tfOtp3.becomeFirstResponder()
         case self.tfOtp3:
           self.tfOtp4.becomeFirstResponder()
         case self.tfOtp4:
           self.tfOtp4.resignFirstResponder()
         default:
           print("nothing will happen")
         }
       }
     }else{
//       DispatchQueue.main.async {
//         switch textField {
//         case self.tfOtp1:
//           print("nothing will happen")
//         case self.tfOtp2:
//           self.tfOtp1.becomeFirstResponder()
//         case self.tfOtp3:
//           self.tfOtp2.becomeFirstResponder()
//         case self.tfOtp4:
//           self.tfOtp3.becomeFirstResponder()
//         default:
//           print("nothing will happen")
//         }
//       }
     }
     return true
   }
}

extension PhoneVerificationVC: MyTextFieldDelegate{
    func textFieldDidDelete() {
        DispatchQueue.main.async {
            if self.tfOtp1.isEditing{
                self.tfOtp1.text = ""
            }else if self.tfOtp2.isEditing{
                self.tfOtp2.text = ""
                self.tfOtp1.becomeFirstResponder()
            }else if self.tfOtp3.isEditing{
                self.tfOtp3.text = ""
                self.tfOtp2.becomeFirstResponder()
            }else{
                self.tfOtp4.text = ""
                self.tfOtp3.becomeFirstResponder()
            }
        }
    }
}

extension PhoneVerificationVC{
    
    func VerificationApiHit(){
        self.Otp = "\(tfOtp1.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")\(tfOtp2.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")\(tfOtp3.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")\(tfOtp4.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")"
        
        let paarams : [String : String] = [
            "otp": self.Otp,
            "phone": phoneNumber,
            "country_code": ContryCode
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.PhoneVerification, withParameters: paarams as [String: AnyObject], ofType: PhoneVerificationAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            CommonFunctions.toster(profileAPI.message ?? "")
            print(profileAPI)
            coredata.shared.phone_no = self.phoneNumber
            coredata.shared.countryCode = self.ContryCode
            if self.firstTimeLogin{
                coredata.shared.accesstoken = profileAPI.token ?? ""
            }
            coredata.shared.dataSave()
            if self.fromEditProfile{
                self.navigationController?.popViewController(animated: true)
            }else{
                let vc = PhoneNumberVerifiedVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                vc.emailOrNot = profileAPI.isEmail
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func ResentOtpApiHit(){
        let paarams : [String : String] = [:]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.ResentOtp, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            CommonFunctions.hideLoader()
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PUT, img: nil, imageParamater: "", headerPresent: true)
    }
}
