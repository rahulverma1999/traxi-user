//
//  PhoneNumberVerifiedVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 27/11/20.
//

import UIKit

class PhoneNumberVerifiedVC: UIViewController {
    
    var emailOrNot : Int! // 0 = has no email & 1 = has email

    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK:- Set User to Booking list screen after 2 secs
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if self.emailOrNot == 0{
                let vc = SignUpVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: .Home)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        // Do any additional setup after loading the view.
    }

}
