//
//  ViewController.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 27/11/20.
//

import UIKit
import AuthenticationServices

class SocialLoginVC: baseVC, UINavigationControllerDelegate {

    //MARK:- Outlets
    @IBOutlet weak var btnGoogleLogin: LoadingButton!
    @IBOutlet weak var btnFacebookLogin: LoadingButton!
    @IBOutlet weak var btnEmailLogin: LoadingButton!
    @IBOutlet weak var lblAttributedLogin: UILabel!
    @IBOutlet weak var stackViewAddButtons: UIStackView!
    @IBOutlet weak var viewGoogleLogin: UIView!
    @IBOutlet weak var viewFaceBookLogin: UIView!
    @IBOutlet weak var viewEmailLogin: GradientView!
    
    //MARK:- Variables
    var loginManager = LoginApiModal()
    var facebookManager = FaceBookLoginManager()
    var loginGoogleManager = googleLogin()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewDidLoadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    //MARK:- Button Actions
    @IBAction func btnActGoogleLogin(_ sender: Any) {
        CommonFunctions.showLoader()
        btnGoogleLogin.showLoading()
        self.btnGoogleLogin.isUserInteractionEnabled = false
        googleLogin.shared.googleLoginHere(self, tgoogleListner: { (result) in
            self.btnGoogleLogin.hideLoading()
            self.view.isUserInteractionEnabled = true
            
            
            var params : [String : String] = [
                "email" : (result.email ?? ""),
                "name" : (result.name ?? ""),
                "google_id" : (result.id ?? "")
//                "image" : result.profilePic as AnyObject
            ]
            if result.profilePic != nil{
                params["profile_image"] = result.profilePic
            }
            coredata.shared.email = result.email ?? ""
            coredata.shared.name = result.name ?? ""
            self.getNectVc(params, btn: self.btnGoogleLogin, LoginType: 2)
        }) {
            CommonFunctions.hideLoader()
            self.btnGoogleLogin.hideLoading()
            self.btnGoogleLogin.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func btnActFacebookLogin(_ sender: Any) {
//        let vc = PhoneNumberVC.instantiateFromAppStoryboard(appStoryboard: .Main)
//        self.navigationController?.pushViewController(vc, animated: true)
        CommonFunctions.showLoader()
        self.btnFacebookLogin.showLoading()
        self.btnFacebookLogin.isUserInteractionEnabled = false
        self.facebookManager.facebookLogin(view: self, success: { (result) in
        print(result)
            var paarams : [String : String] = [
                "email" : result.email ?? "",
                "fb_id": result.id ?? "",
                "name" : result.name ?? ""
            ]
            if result.profilePic != ""{
                paarams["profile_image"] = result.profilePic
                coredata.shared.profileimage = result.profilePic ?? ""
            }
            coredata.shared.email = result.email ?? ""
            coredata.shared.name = result.name ?? ""
            self.getNectVc(paarams, btn: self.btnFacebookLogin, LoginType: 1)
        }) { (error) in
            print(error)
            CommonFunctions.hideLoader()
            self.btnFacebookLogin.hideLoading()
            self.btnFacebookLogin.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func btnActEmailLogin(_ sender: Any) {
        let vc = PhoneNumberVC.instantiateFromAppStoryboard(appStoryboard: .Main)
        vc.sType = .phoneNumber
        isEmailPopShowOnce = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- SetUp ViewDidLoad Data
extension SocialLoginVC{
    
    func setViewDidLoadData(){
        //MARK:- Tap recogniz gesture
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        //MARK:- Set Shadow and corner radius of views
        CommonFunctions.shadowView([viewGoogleLogin, viewFaceBookLogin, viewEmailLogin])
        
        //MARK:- Add apple login button programatically
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton()
            authorizationButton.addTarget(self, action: #selector(btnActApple(_:)), for: .touchUpInside)
            DispatchQueue.main.async {
                authorizationButton.clipsToBounds = true
                authorizationButton.layer.masksToBounds = false
                authorizationButton.layer.shadowColor = UIColor.black.cgColor
                authorizationButton.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    //            imgs.layer.shadowPath = shadowPath.cgPath
                authorizationButton.layer.shadowOpacity = 0.5
                authorizationButton.layer.shadowRadius = 5.0
                authorizationButton.layer.cornerRadius = 4.0
            }
            self.stackViewAddButtons.addArrangedSubview(authorizationButton)
        } else {
        }
        
        //MARK:- Attributed Text to lable 
        let first = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.NunitoRegular(16)]
        let second = [NSAttributedString.Key.foregroundColor: Colors.pinkColorTraxi.color(), NSAttributedString.Key.font: UIFont.NunitoBold(16)] as [NSAttributedString.Key : Any]
        let One = NSMutableAttributedString(string: "Already have an account?", attributes: first)
        let Two = NSMutableAttributedString(string: " Sign In ", attributes: second)
        let tap = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap(_:)))
        One.append(Two)
        lblAttributedLogin.attributedText = One
        lblAttributedLogin.isUserInteractionEnabled = true
        lblAttributedLogin.addGestureRecognizer(tap)
        lblAttributedLogin.textAlignment = .center
        lblAttributedLogin.isHidden = true
    }
}

//MARK:- Objective C methods
extension SocialLoginVC{
    @objc func btnActApple(_ sender: LoadingButton){
        if #available(iOS 13.0, *) {
            CommonFunctions.showLoader()
            var tempButton = LoadingButton()
            appleLogin.shared.appleLoginNow(self, appleListner: { (id, name, email) in
                tempButton.hideLoading()
                self.view.isUserInteractionEnabled = true
                
                var params : [String : String] = [
                    "apple_id" : id!,
                ]
                
                if name != nil{
                    params["name"] = name!
                    coredata.shared.name = name!
                }

                if email != nil{
                    params["email"] = email!
                    coredata.shared.email = email!
                }

                self.getNectVc(params, btn: tempButton, LoginType: 3)

            }) {
                sender.isUserInteractionEnabled = true
                CommonFunctions.hideLoader()
                }
        } else {
            CommonFunctions.hideLoader()
            sender.isUserInteractionEnabled = true
        }
    }
    
    @objc func myMethodToHandleTap(_ sender : UITapGestureRecognizer){
        let text = (lblAttributedLogin.text)!
        let range = (text as NSString).range(of: " Sign In ")
        if sender.didTapAttributedTextlbl(textField: self.lblAttributedLogin, inRange: range) {
            let vc = SignInVC.instantiateFromAppStoryboard(appStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- Api Hit
extension SocialLoginVC{
    // LoginType :- 1-FacebookLogin, 2-GoogleLogin, 3-AppleLogin
    func getNectVc(_ paarams : [String: String], btn : LoadingButton, LoginType : Int){
        LoginApiModal.getLoginApi(paarams as [String : AnyObject], btn) { (profileAPI) in
            btn.hideLoading()
            btn.isUserInteractionEnabled = true
            if profileAPI.user?.isPhoneVerifed == 0{
                let vc = PhoneNumberVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                vc.isFromSsocialLogin = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else if profileAPI.user?.isEmailVerified == 0{
                let vc = SignUpVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: .Home)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            coredata.shared.accesstoken = profileAPI.token ?? ""
            coredata.shared.dataSave()
            CommonFunctions.hideLoader()
        }
    }
}


//MARK:- Tap gesture
extension UITapGestureRecognizer {
  func didTapAttributedTextlbl(textField: UILabel, inRange targetRange: NSRange) -> Bool {
    let layoutManager = NSLayoutManager()
    let textContainer = NSTextContainer(size: CGSize.zero)
    let textStorage = NSTextStorage(attributedString: textField.attributedText!)
    layoutManager.addTextContainer(textContainer)
    textStorage.addLayoutManager(layoutManager)
    textContainer.lineFragmentPadding = 0.0
    let labelSize = textField.bounds.size
    textContainer.size = labelSize
    let locationOfTouchInLabel = self.location(in: textField)
    let textBoundingBox = layoutManager.usedRect(for: textContainer)
    let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
    let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
    let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
    return NSLocationInRange(indexOfCharacter, targetRange)
  }
   
}


