//
//  ContactUsVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 15/12/20.
//

import UIKit
import GrowingTextView
import FlagPhoneNumber
import ENSwiftSideMenu

enum ContactUsVCScreenType{
    case ContactUs, CorporateDelivery
}

class ContactUsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tfName: UITextFieldWxtension!
    @IBOutlet weak var tfEmail: UITextFieldWxtension!
    @IBOutlet weak var tfPhoneNumber: FPNTextField!
    @IBOutlet weak var btnSubmit: LoadingButton!
    @IBOutlet weak var tvMsg: GrowingTextView!
    @IBOutlet weak var lblContactUs: UILabel!
    @IBOutlet weak var btnSideMenu: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var stackViewConstant: NSLayoutConstraint!
    @IBOutlet weak var vwnotificationCount: setCornerRadiusCircle!
    
    //MARK:- Variables
    var selectedValue = "+61"
    var countryCode = "+61"
    var sType : ContactUsVCScreenType = .ContactUs
    var sideMenuNew : SideMenuVC!
    var isValidPhn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwnotificationCount.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        switch sType{
        case .ContactUs:
            GetProfileApiHit()
        case .CorporateDelivery:
            break
        }
    }

    @IBAction func btnActSideMenu(_ sender: Any) {
        switch sType {
        case .ContactUs:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
            toggleSideMenuView()
        case .CorporateDelivery:
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func btnActSubMit(_ sender: Any) {
        if tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            CommonFunctions.toster("Please enter name")
            tfName.shake()
        }else if tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            CommonFunctions.toster("Please enter email")
            tfEmail.shake()
        }else if !CommonFunctions.isValidEmail(tfEmail.text!){
            tfEmail.shake()
            CommonFunctions.toster("Please enter valid email")
        }else if tfPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            CommonFunctions.toster("Please enter phone number")
            tfPhoneNumber.shake()
        }else if !isValidPhn{
            CommonFunctions.toster("Please enter valid phone number")
            tfPhoneNumber.shake()
        }else if tvMsg.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            CommonFunctions.toster("Please enter message")
            tvMsg.shake()
        }else{
            switch self.sType {
            case .ContactUs:
                self.PostContactUsApi(2)
            case .CorporateDelivery:
                self.PostContactUsApi(1)
            }
        }
    }
}

extension ContactUsVC{
    //MARK:- setUpUI
    func setUpUI(){
        //MARK:- Implement country code in textfield
        tfPhoneNumber.delegate = self
        tfPhoneNumber.displayMode = .list
        self.tfPhoneNumber.setFlag(countryCode: .AU)
//        tfPhoneNumber.setPlaceHolderColor(UIColor.lightGray, placeholderText: " 00000 00000 ", placeholderFont: UIFont.NunitoRegular(14))
        tfPhoneNumber.flagButton.isUserInteractionEnabled = true
        tfPhoneNumber.flagButton.addTarget(self, action: #selector(btnActCountryCode(_:)), for: .touchUpInside)
        
        tvMsg.layer.cornerRadius = 4
        tvMsg.layer.borderWidth = 1
        tvMsg.layer.borderColor = Colors.borderYellowColorTraxi.color().cgColor
        tvMsg.textContainerInset = UIEdgeInsets(top: 8,left: 12,bottom: 8,right: 12)
        
        switch sType {
        case .ContactUs:
            lblContactUs.isHidden = true
            stackViewConstant.constant = 12
            lblTitle.text = "Contact Us"
            btnSideMenu.setImage(UIImage(named: "menu"), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                if self.navigationController != nil{
                    self.sideMenuController()?.sideMenu?.delegate = self
                    self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
                    self.sideMenuController()?.sideMenu?.allowPanGesture = true
                    self.sideMenuController()?.sideMenu?.allowRightSwipe = true
                }
            }
        case .CorporateDelivery:
            lblContactUs.isHidden = false
            stackViewConstant.constant = 60
            lblTitle.text = "Corporate Delivery"
            btnSideMenu.setImage(UIImage(named: "back_black"), for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                if self.navigationController != nil{
                    self.sideMenuController()?.sideMenu?.delegate = self
                    self.sideMenuController()?.sideMenu?.allowLeftSwipe = false
                    self.sideMenuController()?.sideMenu?.allowPanGesture = false
                    self.sideMenuController()?.sideMenu?.allowRightSwipe = false
                }
            }
        }
    }
}

//MARK:- Objective C methods
extension ContactUsVC{
    @objc func btnActCountryCode(_ sender: UIButton){
        self.fpnDisplayCountryList()
    }
}

extension ContactUsVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if tfEmail == textField{
            let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@/%$#!^&*"
            let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
            return (string == filtered) && newString.length <= 70
        }else if tfPhoneNumber == textField{
            return newString.length <= 15
        }else if tvMsg == textField{
            return newString.length <= 250
        }else{
            return newString.length <= 30
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case tfName:
            tfEmail.becomeFirstResponder()
        case tfEmail:
            tfPhoneNumber.becomeFirstResponder()
        case tfPhoneNumber:
            tvMsg.becomeFirstResponder()
        case tvMsg:
            tvMsg.resignFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
}

//MARK:- Delegate method of country code flag
extension ContactUsVC: FPNTextFieldDelegate{
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
         print(name, dialCode, code) // Output "France", "+33", "FR"
        countryCode = dialCode
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
          self.isValidPhn = true            // Output "600000001"
//          self.phone = textField.getRawPhoneNumber () ?? ""
        } else {
          self.isValidPhn = false
        }
    }
    
    func fpnDisplayCountryList() {
        print("sad")
        
        let listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
        tfPhoneNumber.displayMode = .list // .picker by default
        
        listController.setup(repository: tfPhoneNumber.countryRepository)
        listController.title = "Countries"
        

        let navigationViewController = UINavigationController(rootViewController: listController)
                
        self.present(navigationViewController, animated: true, completion: nil)
        
        
        listController.didSelect = {[weak self] country in
            self?.tfPhoneNumber.setFlag(countryCode: country.code)
            self?.selectedValue = country.phoneCode
        }
    }
}

extension ContactUsVC{
    //MARK:- HitApi
    func PostContactUsApi(_ notifType : Int){
        let paarams : [String : AnyObject] = [
            "name" : self.tfName.text as AnyObject,
            "message" : self.tvMsg.text as AnyObject,
            "phone" : self.tfPhoneNumber.text as AnyObject,
            "country_code" : countryCode as AnyObject,
            "notif_type" : notifType as AnyObject
        ]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.corporateDeliveryContactUs, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            CommonFunctions.hideLoader()
//            self.sideMenuNew = SideMenuVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
//                let allVcs = self.navigationController!.viewControllers
//                for vc in allVcs{
//                    if vc.isKind(of: HomeVC.self){
//                        let navVC : navController = navController(menuViewController: self.sideMenuNew, contentViewController: vc)
//                        SelectedIndexValueOfSideMenu = 1
//                        self.navigationController?.popToViewController(navVC, animated: false)
//                    }
//                }
            
            self.sideMenuNew = SideMenuVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
            let vc = HomeVC.instantiateFromAppStoryboard(appStoryboard: AppStoryboard.Home)
            
            let navVC : navController = navController(menuViewController: self.sideMenuNew, contentViewController: vc)
            SelectedIndexValueOfSideMenu = 1
//            let navigationController = UINavigationController(rootViewController: vc)
            if UIApplication.shared.windows.count > 0 {
                UIApplication.shared.windows[0].rootViewController = navVC
                navVC.setNavigationBarHidden(true, animated: false)
            }
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.PostWithJSON, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            if (profileAPI.unreadComplains ?? 0) > 0 || (profileAPI.unreadNotifications ?? 0) > 0{
                self.vwnotificationCount.isHidden = false
            }else{
                self.vwnotificationCount.isHidden = true
            }
        }, failure: { (false, string) in
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
}

// MARK: - DELEGATE FUNCTIONS
extension ContactUsVC : ENSideMenuDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
            
            self.view.bringSubviewToFront(fullView)
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        toggleSideMenuView()
    }
}

