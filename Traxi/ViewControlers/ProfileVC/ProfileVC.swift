//
//  ProfileVC.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 08/12/20.
//

import UIKit
import ENSwiftSideMenu
import FlagPhoneNumber

class ProfileVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnEditProfilePic: UIButton!
    @IBOutlet weak var btnSaveName: UIButton!
    @IBOutlet weak var btnEditName: UIButton!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var vwName: UIView!
    @IBOutlet weak var saveViewName: UIView!
    @IBOutlet weak var NameConstant: NSLayoutConstraint!
    
    @IBOutlet weak var btnEditEmail: UIButton!
    @IBOutlet weak var btnVerifyEmail: UIButton!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var vwEmail: UIView!
    @IBOutlet weak var saveViewEmail: UIView!
    @IBOutlet weak var emailConstant: NSLayoutConstraint!
    @IBOutlet weak var btnSaveEmail: UIButton!
    
    
    @IBOutlet weak var tfPhoneNumber: FPNTextField!
    @IBOutlet weak var btnVerifyPhone: UIButton!
    @IBOutlet weak var btnEditPhone: UIButton!
    @IBOutlet weak var btnSavePhone: UIButton!
    @IBOutlet weak var vwPhone: UIView!
    @IBOutlet weak var saveViewPhone: UIView!
    @IBOutlet weak var phoneConstant: NSLayoutConstraint!
    @IBOutlet weak var vwnotificationCount: setCornerRadiusCircle!
    
    
    //MARK:- Variables
    var UserData : GetProfile?
    var profileImage : String?
    var selectedValue = coredata.shared.countryCode
    var countryCode = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewDidloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vwnotificationCount.isHidden = true
        if coredata.shared.profileimage != ""{
            self.imgProfile.yy_setImage(with: URL(string: CommonFunctions.getImage(coredata.shared.profileimage, quality: .large)), placeholder: UIImage(named: "image_large"))
        }else{
            CommonFunctions.getImageWithLbl(imageView: self.imgProfile)
        }
        self.tfName.text = coredata.shared.name
        self.tfEmail.text = coredata.shared.email
        self.tfPhoneNumber.text = "\(coredata.shared.phone_no)"
        self.countryCode = coredata.shared.countryCode
        
        if coredata.shared.isEmailVerified == 1{
            self.btnVerifyEmail.isHidden = true
        }else{
            self.btnVerifyEmail.isHidden = false
        }
        
        self.tfPhoneNumber.setFlag(countryNumCode: coredata.shared.countryCode)
        self.GetProfileApiHit()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
    }
    
    //MARK:- Button Actions
    @IBAction func btnActBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        toggleSideMenuView()
    }
    
    @IBAction func btnActEditProfilePic(_ sender: Any) {
        self.getGallary()
    }
    
    @IBAction func btnActEditName(_ sender: Any) {
        vwName.isHidden = false
        saveViewName.isHidden = false
        tfName.isUserInteractionEnabled = true
        NameConstant.constant = 20
        btnEditName.isHidden = true
    }
    
    @IBAction func btnActSaveName(_ sender: Any) {
        if tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfName.shake()
            CommonFunctions.toster("Please enter name")
        }else{
            self.EditUserApiHit(false, updateType: 2)
        }
    }
    
    @IBAction func btnActEditEmail(_ sender: Any) {
        vwEmail.isHidden = false
        saveViewEmail.isHidden = false
        tfEmail.isUserInteractionEnabled = true
        emailConstant.constant = 20
        btnVerifyEmail.isHidden = true
        btnEditEmail.isHidden = true
    }
    
    @IBAction func btnActVerifiyEmail(_ sender: Any) {
        self.ResentEmailApiHit()
    }
    
    @IBAction func btnActSaveEmail(_ sender: Any) {
        if tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfEmail.shake()
            CommonFunctions.toster("Please enter email")
        }else if !CommonFunctions.isValidEmail(tfEmail.text!){
            tfEmail.shake()
            CommonFunctions.toster("Please enter valid email")
        }else{
            self.EditUserApiHit(false, updateType: 3)
        }
    }
    
    @IBAction func btnActVerifyPhone(_ sender: Any) {
    }
    
    @IBAction func btnActEditPhone(_ sender: Any) {
        vwPhone.isHidden = false
        saveViewPhone.isHidden = false
        tfPhoneNumber.isUserInteractionEnabled = true
        phoneConstant.constant = 20
        btnVerifyPhone.isHidden = true
        btnEditPhone.isHidden = true
        tfPhoneNumber.flagButton.isUserInteractionEnabled = true
    }
    
    @IBAction func btnActSavePhone(_ sender: Any) {
        if tfPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            tfPhoneNumber.shake()
            CommonFunctions.toster("Please enter phone number")
        }else{
            self.EditUserApiHit(false, updateType: 4)
        }
    }
    
}

//MARK:- UITextViewDelegateMethods
extension ProfileVC: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if tfEmail == textField{
            let ACCEPTABLE_CHARACTERS = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@/%$#!^&*"
            let cs = CharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered: String = (string.components(separatedBy: cs) as NSArray).componentsJoined(by: "")
            return (string == filtered) && newString.length <= 70
        }else if tfPhoneNumber == textField{
            return newString.length <= 15
        }else{
            return newString.length <= 30
        }
        
    }
}

//MARK:- Set ViewDidLoadData
extension ProfileVC{
    func setViewDidloadData(){
        self.setCornerRadiusButtons([btnSaveName, btnSaveEmail, btnVerifyEmail], radius: 2)
        self.setCornerRadiusButtons([btnEditProfilePic], radius: btnEditProfilePic.bounds.height/2)
        self.imgProfile.layer.cornerRadius = self.imgProfile.bounds.height/2
        
        vwName.isHidden = true
        saveViewName.isHidden = true
        tfName.isUserInteractionEnabled = false
        NameConstant.constant = 68
        btnEditName.isHidden = false
        
        vwEmail.isHidden = true
        saveViewEmail.isHidden = true
        tfEmail.isUserInteractionEnabled = false
        emailConstant.constant = 68
        btnVerifyEmail.isHidden = false
        btnEditEmail.isHidden = false
        
        vwPhone.isHidden = true
        saveViewPhone.isHidden = true
        tfPhoneNumber.isUserInteractionEnabled = false
        phoneConstant.constant = 68
        btnVerifyPhone.isHidden = false
        btnEditPhone.isHidden = false
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        imgProfile.isUserInteractionEnabled = true
        imgProfile.addGestureRecognizer(singleTap)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            if self.navigationController != nil{
                self.sideMenuController()?.sideMenu?.delegate = self
                self.sideMenuController()?.sideMenu?.allowLeftSwipe = true
                self.sideMenuController()?.sideMenu?.allowPanGesture = true
                self.sideMenuController()?.sideMenu?.allowRightSwipe = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
            }
        }
        tfPhoneNumber.flagButton.isUserInteractionEnabled = false
        tfPhoneNumber.delegate = self
        tfPhoneNumber.displayMode = .list
        self.tfPhoneNumber.setFlag(countryCode: .AU)
//        tfPhoneNumber.setPlaceHolderColor(UIColor.lightGray, placeholderText: " 00000 00000 ", placeholderFont: UIFont.NunitoRegular(14))
        tfPhoneNumber.flagButton.addTarget(self, action: #selector(btnActCountryCode(_:)), for: .touchUpInside)
    }
    
    func setCornerRadiusButtons(_ btns : [UIButton], radius : CGFloat){
        for btn in btns{
            btn.layer.cornerRadius = radius
        }
    }
    
    func getGallary(){
        var imageClicked = false
        CameraGalleryPicker.sharedInstance.pickerImage(mode: .All, presentInVc: self, pickedListner: { (Image) in
            if !imageClicked{
                imageClicked = true
                self.imgProfile.image = Image
                self.imgProfile.clipsToBounds = true
                self.PictureUpload(Image)
            }
        }, pickedMultipleListner: { (images) in
            if !imageClicked{
                imageClicked = true
                self.imgProfile.image = images[0]
                self.imgProfile.clipsToBounds = true
                self.PictureUpload(images[0])
            }
        }, canceledListner: {
            print("cencelled")
        }, maxLimit: 1)
    }
}

//MARK:- Objective C methods
extension ProfileVC{
    //MARK:- Tap Gesture
    @objc func tapDetected() {
        self.getGallary()
    }
    
    @objc func btnActCountryCode(_ sender: UIButton){
        self.fpnDisplayCountryList()
    }
}

//MARK:- Delegate method of country code flag
extension ProfileVC: FPNTextFieldDelegate{
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
         print(name, dialCode, code) // Output "France", "+33", "FR"
        countryCode = dialCode
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
    }
    
    func fpnDisplayCountryList() {
        print("sad")
        
        let listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
        tfPhoneNumber.displayMode = .list // .picker by default
        
        listController.setup(repository: tfPhoneNumber.countryRepository)
        listController.title = "Countries"
        

        let navigationViewController = UINavigationController(rootViewController: listController)
                
        self.present(navigationViewController, animated: true, completion: nil)
        
        
        listController.didSelect = {[weak self] country in
            self?.tfPhoneNumber.setFlag(countryCode: country.code)
            self?.selectedValue = country.phoneCode
        }
    }
}


// MARK: - DELEGATE FUNCTIONS
extension ProfileVC : ENSideMenuDelegate{
    func sideMenuWillOpen() {
        var present = false
        for cView in self.view.subviews{
            if cView.tag == 1000{
                present = true
            }
        }
        
        if !present{
            let fullView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            fullView.tag = 1000
            fullView.backgroundColor = UIColor(white: 0, alpha: 0.7)
            self.view.addSubview(fullView)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleViewDismiss(_:)))
            fullView.addGestureRecognizer(tap)
            fullView.isUserInteractionEnabled = true
            
            self.view.bringSubviewToFront(fullView)
        }
    }
    
    func sideMenuWillClose() {
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        return true
    }
    
    func sideMenuDidOpen() {
    }
    
    func sideMenuDidClose() {
        for views in self.view.subviews{
            if views.tag == 1000{
                views.removeFromSuperview()
            }
        }
    }
    
    @objc func handleViewDismiss(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
        toggleSideMenuView()
    }
}

//MARK:- Hit Api
extension ProfileVC{
    func GetProfileApiHit(){
        let paarams : [String : String] = [
            :
        ]
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.getProfile, withParameters: paarams as [String: AnyObject], ofType: GetProfile.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.UserData = profileAPI
            if profileAPI.profileImage != nil && profileAPI.profileImage != ""{
                self.imgProfile.yy_setImage(with: URL(string: CommonFunctions.getImage(profileAPI.profileImage ?? "", quality: .large)), placeholder: UIImage(named: "image_large"))
            }else{
                CommonFunctions.getImageWithLbl(imageView: self.imgProfile)
            }
            self.tfName.text = profileAPI.name ?? ""
            self.tfEmail.text = profileAPI.email ?? ""
            self.tfPhoneNumber.text = "\(profileAPI.phone ?? 0)"
            self.countryCode = profileAPI.countryCode ?? ""
            
            if profileAPI.isPhoneVerifed == 1{
                self.btnVerifyPhone.isHidden = false
            }else{
                self.btnVerifyPhone.isHidden = true
            }
            
            if profileAPI.isEmailVerified == 1{
                self.btnVerifyEmail.isHidden = true
            }else{
                self.btnVerifyEmail.isHidden = false
            }
            
            self.tfPhoneNumber.setFlag(countryNumCode: profileAPI.countryCode ?? "")
            
            coredata.shared.phone_no = "\(profileAPI.phone ?? 0)"
            coredata.shared.email = profileAPI.email ?? ""
            coredata.shared.name =  profileAPI.name ?? ""
            coredata.shared.countryCode = profileAPI.countryCode ?? ""
            coredata.shared.isEmailVerified = profileAPI.isEmailVerified ?? 0
            coredata.shared.dataSave()
            if (profileAPI.unreadComplains ?? 0) > 0 || (profileAPI.unreadNotifications ?? 0) > 0{
                self.vwnotificationCount.isHidden = false
            }else{
                self.vwnotificationCount.isHidden = true
            }
            print(profileAPI)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.GET, img: nil, imageParamater: "", headerPresent: true)
    }
    
    func PictureUpload( _ params: UIImage){
        CommonFunctions.showLoader(imgProfile)
        ApiHandler.callApiWithParameters(url: APIConstants.uploadImage, withParameters: [:], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader(self.imgProfile)
            self.profileImage = profileAPI.filename ?? ""
//            CommonFunctions.toster(profileAPI.message ?? "")
            self.EditUserApiHit(true, updateType: 1)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader(self.imgProfile)
        }, method: ApiMethod.PostWithImage, img: params, imageParamater: "file", headerPresent: true)
    }
    
    func EditUserApiHit(_ isProfilePic : Bool, updateType : Int){ //UpdateType:- 1- Profile, 2-Name, 3-Email, 4-Phone
        var paarams : [String : AnyObject] = [
            :
        ]
        
        if self.profileImage != nil{
            paarams["profile_image"] = profileImage as AnyObject
        }
        if updateType == 2{
            paarams["name"] = tfName.text?.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject
        }
        
        if updateType == 3{
            paarams["email"] = tfEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject
        }
        
        if updateType == 4{
            self.btnSavePhone.isUserInteractionEnabled = false
            paarams["phone"] = tfPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject
            paarams["country_code"] = countryCode as AnyObject
        }
        
//        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.editUser, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.hideLoader()
            self.btnSavePhone.isUserInteractionEnabled = true
            if isProfilePic{
                coredata.shared.profileimage = profileAPI.filename ?? ""
            }
            if updateType == 2{
                self.vwName.isHidden = true
                self.saveViewName.isHidden = true
                self.tfName.isUserInteractionEnabled = false
                self.NameConstant.constant = 68
                self.btnEditName.isHidden = false
            }
            if updateType == 3{
                isEmailPopShowOnce = true
                self.vwEmail.isHidden = true
                self.saveViewEmail.isHidden = true
                self.tfEmail.isUserInteractionEnabled = false
                self.emailConstant.constant = 68
                self.btnVerifyEmail.isHidden = false
                self.btnEditEmail.isHidden = false
            }
            if updateType == 4{
                self.vwPhone.isHidden = true
                self.saveViewPhone.isHidden = true
                self.tfPhoneNumber.isUserInteractionEnabled = false
                self.phoneConstant.constant = 68
                self.btnVerifyPhone.isHidden = false
                self.btnEditPhone.isHidden = false
                self.tfPhoneNumber.flagButton.isUserInteractionEnabled = false
                let vc = PhoneVerificationVC.instantiateFromAppStoryboard(appStoryboard: .Main)
                vc.phoneNumber = self.tfPhoneNumber.text ?? ""
                vc.fromEditProfile = true
                vc.ContryCode = self.countryCode
                self.navigationController?.pushViewController(vc, animated: true)
            }
            coredata.shared.dataSave()
            print(profileAPI)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadTableView"), object: nil)
            if updateType != 4{
                self.GetProfileApiHit()
                CommonFunctions.toster(profileAPI.message ?? "")
            }
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
            self.btnSavePhone.isUserInteractionEnabled = true
        }, method: ApiMethod.PUT, img: nil, imageParamater: "", headerPresent: true)
    }
    
    //MARK:- Hit Api
    func ResentEmailApiHit(){
        let paarams : [String : String] = [:]
        CommonFunctions.showLoader()
        ApiHandler.callApiWithParameters(url: APIConstants.VerificationEmailSendLink, withParameters: paarams as [String: AnyObject], ofType: ResentOtpAPI.self, success2: { (profileAPI) in
            CommonFunctions.toster(profileAPI.message ?? "")
            CommonFunctions.hideLoader()
            let vc = VerifyEmailVC.instantiateFromAppStoryboard(appStoryboard: .Main)
            self.navigationController?.pushViewController(vc, animated: true)
        }, failure: { (false, string) in
            CommonFunctions.hideLoader()
            print(string)
        }, method: ApiMethod.POST, img: nil, imageParamater: "", headerPresent: true)
    }
}
