//
//  SocketManager.swift
//  Traxi
//
//  Created by Think Recycling Pvt Ltd on 15/01/21.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    
    static let sharedInstance = SocketIOManager()
    var socket: SocketIOClient!
    
    var manager = SocketManager(socketURL: URL(string: BASE_URL.replacingOccurrences(of: "/api/", with: ""))!, config: [.log(true), .forceWebsockets(true)])
    
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func establishConnection() {
        socket.connect()
        print("socket connected")
    }
    
    func closeConnection() {
        socket.disconnect()
        print("socket disconnected")
    }
    
    func closeSocket(){
        socket.off("broadcast")
        print("socket off")
    }

    
    func getListenLatLng(completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        socket.on("broadcast") { (dataArray, socketAck) -> Void in
            print(dataArray)

            completionHandler((dataArray[0] as! Dictionary<String,AnyObject>))
        }
    }
}


