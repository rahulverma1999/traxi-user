
import UIKit
import CoreData
import Foundation

class coredata : NSObject{

    var accesstoken: String =  ""
    var name: String = ""
    var id : String = ""
    var isEmailVerified : Int = 0 // 0-notverified, 1-verified
//    var refreshToken: String = ""
    var email: String = ""
    var phone_no : String = ""
    var profileimage: String = ""
//    var google_id : String = ""
    var countryCode : String = "+61"
    var walletBalance : Double = 0.0
//    var stripeConnected : Bool = false
    var cardNumber : [String] = []
    var cardIds : [String] = []
    
    var newlyAddedCardData : [SavedCardCoreData] = []

    class var shared: coredata{
        struct singleTon {
            static let instance = coredata()
        }
        return singleTon.instance
    }

//    func addData(_ data: LoginApi){
//        accesstoken = data.accessToken ?? ""
//        dataSave()
////        addData()
//    }

    func addData(){
        dataSave()
    }

    func dataSave(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Login"))

        do {
            try context.execute(DelAllReqVar)
        } catch {
            print(error)
        }
        let newData = NSEntityDescription.insertNewObject(forEntityName: "Login", into: context)
        newData.setValue(accesstoken, forKey: "accesstoken")
        newData.setValue(name, forKey: "name")
        newData.setValue(isEmailVerified, forKey: "isEmailVerified")
        newData.setValue(id, forKey: "id")
        newData.setValue(profileimage, forKey: "profileimage")
        newData.setValue(email, forKey: "email")
//        newData.setValue(stripeConnected, forKey: "stripeConnected")
        newData.setValue(phone_no, forKey: "phone_no")
        newData.setValue(walletBalance, forKey: "walletBalance")
//        newData.setValue(password, forKey: "password")
//        newData.setValue(wma_id, forKey: "wma_id")
//        newData.setValue(wmaCode, forKey: "wmaCode")
//        newData.setValue(premise, forKey: "premise")
        newData.setValue(countryCode, forKey: "countryCode")
        do {
            try context.save()
            print(newData)
            print("new data saved")
        }catch{
            print("new data save error")
        }
    }
    
    func addDataCards(data : GetSavedCardAPI){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "SavedCardCoreData"))

        do {
            try context.execute(DelAllReqVar)
        } catch {
            print(error)
        }
        for dt in data.data ?? []{
            let newData = NSEntityDescription.insertNewObject(forEntityName: "SavedCardCoreData", into: context)
            newData.setValue(dt.last4, forKey: "cardNumber")
            newData.setValue(dt.id, forKey: "cardIds")
            do {
                try context.save()
                print(newData)
                print("new data saved")
            }catch{
                print("new data save error")
            }
        }
        self.getNewlyAddedCards()
    }
    
    func getNewlyAddedCards(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedCardCoreData")
        request.returnsObjectsAsFaults = false
        var ktempDrugMedName : [String] = []
        var ktempDrugMedId : [String] = []
        do {
            let result = try context.fetch(request)
            //print(result as! [NSManagedObject])
            newlyAddedCardData = result as! [SavedCardCoreData]
            var count = 0
            for data in newlyAddedCardData{
                count+=1
                ktempDrugMedName.append(data.value(forKey: "cardNumber") as? String ?? "")
                ktempDrugMedId.append(data.value(forKey: "cardIds") as? String ?? "")
                if count == newlyAddedCardData.count{
                }
            }
            self.cardNumber = ktempDrugMedName
            self.cardIds = ktempDrugMedId
            
            self.hitApi()

        }catch let error {
        }
    }

    func hitApi(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SavedCard"), object: nil)
        })
    }
    
    func deleteSavedCardData() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedCardCoreData")

        // Configure Fetch Request
        fetchRequest.includesPropertyValues = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        do {
            let items = try managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]

            for item in items {
                managedObjectContext.delete(item)
            }

            // Save Changes
            try managedObjectContext.save()

        } catch {
            // Error Handling
            // ...
        }
    }

    func getdata() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Login")
        request.returnsObjectsAsFaults = true
        do{
            let results = try context.fetch(request)
            if(results.count > 0){
                for result in results as![NSManagedObject]{
                    if let name = result.value(forKey: "name") as? String{
                        self.name = name
                        print("data get gender \(name)")
                    }
                    if let isEmailVerified = result.value(forKey: "isEmailVerified") as? Int{
                        self.isEmailVerified = isEmailVerified
                        print("data get gender \(isEmailVerified)")
                    }
                    if let walletBalance = result.value(forKey: "walletBalance") as? Double{
                        self.walletBalance = walletBalance
                        print("data get gender \(walletBalance)")
                    }
                    if let profileimage = result.value(forKey: "profileimage") as? String{
                        self.profileimage = profileimage
                        print("data get gender \(profileimage)")
                    }
                    if let email = result.value(forKey: "email") as? String{
                        self.email = email
                        print("data get gender \(email)")
                    }
                    if let id = result.value(forKey: "id") as? String{
                        self.id = id
                        print("data get gender \(id)")
                    }
//                    if let google_id = result.value(forKey: "google_id") as? String{
//                        self.google_id = google_id
//                        print("data get gender \(google_id)")
//                    }
                    if let phone_no = result.value(forKey: "phone_no") as? String{
                        self.phone_no = phone_no
                        print("data get gender \(phone_no)")
                    }
//                    if let address = result.value(forKey: "address") as? String{
//                        self.address = address
//                        print("data get gender \(address)")
//                    }
                    if let accesstoken = result.value(forKey: "accesstoken") as? String{
                        self.accesstoken = accesstoken
                        print("data get gender \(accesstoken)")
                    }
//                    if let is_phone_verified = result.value(forKey: "is_phone_verified") as? Int{
//                        self.is_phone_verified = is_phone_verified
//                        print("data get gender \(is_phone_verified)")
//                    }
//                    if let wma_id = result.value(forKey: "wma_id") as? Int{
//                        self.wma_id = wma_id
//                        print("data get gender \(wma_id)")
//                    }
//                    if let password = result.value(forKey: "password") as? String{
//                        self.password = password
//                        print("data get gender \(password)")
//                    }
                    if let countryCode = result.value(forKey: "countryCode") as? String{
                        self.countryCode = countryCode
                        print("data get gender \(countryCode)")
                    }
                }
            }
        }
        catch
        {
            print("something error during getting data")
        }
    }
    func deleteProfile() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Login"))
        accesstoken =  ""
        name = "User"
        isEmailVerified = 0
//        refreshToken = ""
        id = ""
        email = ""
//        stripeConnected = false
        profileimage = ""
        phone_no = ""
        walletBalance = 0.0
//        is_phone_verified = 0
//        wma_id = -1
//        wmaCode = ""
//        premise = ""
        countryCode = "+1"
        do {
            try context.execute(DelAllReqVar)
        } catch {
            print(error)
        }
    }
}

