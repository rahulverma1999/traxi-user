//
//  ProductsCoreData.swift
//  Traxi
//
//  Created by Bhanu Dhiman on 07/01/21.
//

import Foundation
import UIKit
import CoreData


class ProductsCoreData: NSObject{
    
    var itemCount : Int!
    var price : Double!
    var timeToMake : Double!
    var desciption : String!
    var image : String!
    var name : String!
    
    
    class var shared: ProductsCoreData{
        struct singleTon {
            static let instance = ProductsCoreData()
        }
        return singleTon.instance
    }
    
    func setData(_ dataArray : [StructProductsAddedData]){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsData"))
        
        do {
            try context.execute(DelAllReqVar)
        } catch {
            print(error)
        }
        
        for data in dataArray{
            let newData = NSEntityDescription.insertNewObject(forEntityName: "ProductsData", into: context)
            newData.setValue(data.itemCount, forKey: "itemCount")
            newData.setValue(data.price, forKey: "price")
            newData.setValue(data.timeToMake, forKey: "timeToMake")
            newData.setValue(data.description, forKey: "desciption")
            newData.setValue(data.imageOfProduct, forKey: "image")
            newData.setValue(data.name, forKey: "name")
            newData.setValue(data.productId, forKey: "productId")
            newData.setValue(data.catProductId, forKey: "catProductId")
            do {
                try context.save()
                print(newData)
                print("new data saved")
            }catch{
                print("new data save error")
            }
        }
    }
    
    
    func deleteAllData(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductsData")

        // Configure Fetch Request
        fetchRequest.includesPropertyValues = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        do {
            let items = try managedObjectContext.fetch(fetchRequest) as! [NSManagedObject]

            for item in items {
                managedObjectContext.delete(item)
            }

            // Save Changes
            try managedObjectContext.save()

        } catch {
            // Error Handling
            // ...
        }
    }
}
